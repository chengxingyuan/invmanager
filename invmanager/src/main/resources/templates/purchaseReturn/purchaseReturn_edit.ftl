<!-- purchase_return.ftl 退货单表 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">退货单号</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="code" style="width: 100%;"
                       data-options="prompt:'退货单号'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">供应商id</label>
            <div class="col-sm-8">
                <select id="relativeId" class="easyui-combobox" name="relativeId" style="width:100%;"
                        data-options="prompt:'供应商id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">退货商品数量</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="count" style="width:100%;"
                        data-options="prompt:'退货商品数量',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">退货单价</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="unitPrice" style="width: 100%;"
                       data-options="prompt:'退货单价'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">所退商品</label>
            <div class="col-sm-8">
                <select id="skuId" class="easyui-combobox" name="skuId" style="width:100%;"
                        data-options="prompt:'所退商品',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">退货日期</label>
            <div class="col-sm-8">
                <input class="easyui-datebox" name="orderTime" style="width: 100%;"
                       data-options="prompt:'退货日期',editable:false">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">实退金额</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="realPay" style="width: 100%;"
                       data-options="prompt:'实退金额'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">结清状态：0.未结清，1.已结清</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="payStatus" style="width:100%;"
                        data-options="prompt:'结清状态：0.未结清，1.已结清',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">出库状态：0，未出库，1.已出库</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="repositoryStatus" style="width:100%;"
                        data-options="prompt:'出库状态：0，未出库，1.已出库',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">单据状态：-1.删除，0，有效，</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">开单人</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="username" style="width: 100%;"
                       data-options="prompt:'开单人'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">备注</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="remark" style="width: 100%;"
                       data-options="prompt:'备注'">
            </div>
        </div>
    </form>
</div>
