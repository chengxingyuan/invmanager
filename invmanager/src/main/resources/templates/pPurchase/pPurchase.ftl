<!-- p_purchase.ftl 采购单 -->
<#include "./../common/lib/csslib.ftl">
<input type="hidden" id="hidSearchText" value="${searchText}"/>
<input type="hidden" id="hidRelativeId" value="${relativeId}"/>
<input type="hidden" id="hidRelativeName" value="${relativeName}"/>
<input type="hidden" id="hidStartTime" value="${startTime}" />
<input type="hidden" id="hidEndTime" value="${endTime}" />

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'100px'">
        <div id="pPurchase_btn" class="datagrid-toolbar"> </div>
        <form id="pPurchase_form" class="form-inline search-form" style="float: left">
            <div class="form-group">
                <label>查询内容</label>
                <input id="searchText" name="searchText" class="easyui-textbox" data-options="prompt:'请输入单号、商品名、供应商名'">
            </div>
            <#--<div class="form-group" style="line-height: 20px">-->
                <#--<label>供应商</label>-->
                <#--<select id="relativeId" name="relativeId" class="easyui-combobox" data-options="editable:false">-->
                <#--</select>-->
            <#--</div>-->
            <div class="form-group">
                <label>订单日期</label>
                <input id="startTime" name="startTime" style="width: 100px;" class="easyui-datebox">-<input id="endTime" style="width: 100px;" name="endTime" class="easyui-datebox">
            </div>
            <div class="form-group" style="line-height: 20px">
                <label>结算状态</label>
                <input name="payStatus" />
                </select>
            </div>
            <div class="form-group" style="line-height: 20px">
                <label>是否入库</label>
                <input name="orderStatus" />
                </select>
            </div>
        </form>
        <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询</button>
    </div>
    <div data-options="region:'center'">
        <table id="pPurchase_table"></table>
    </div>

    <div data-options="region:'south',height:'25%', title:'采购单商品信息'">
        <div id="printBtn" class="datagrid-toolbar"> </div>
        <table id="purchasePrint_table"></table>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/pPurchase.js?t=1"></script>