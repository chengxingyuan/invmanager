
<#include "./../../common/lib/csslib.ftl">
<input type="hidden" id="orderStatus" value="${purchase.orderStatus}"/>
<input type="hidden" id="payStatus" value="${purchase.payStatus}"/>
<input type="hidden" id="otherMoney" value="${otherMoney}"/>
<input type="hidden" id="settle" value="${purchase.settlementStatus}"/>

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <br>
        <#if purchase.orderStatus = 0>

        <button type="button" id="inRepositoryButton" style="border-radius:3px;color:white;border: 0px;background-color:#33CCDA;height: 20px;width: 60px;float: left;margin-left: 20px;font-size: 12px">入库</button>&nbsp;

        </#if>
        <#if purchase.orderStatus != 0>

            <button type="button" id="cancelInButton" style="border-radius:3px;color:white;border: 0px;background-color:#0FA2FF;height: 20px;width: 60px;float: left;margin-left: 20px;font-size: 12px">撤销入库</button>&nbsp;

        </#if>
        <#if endCount = 1>

        <button type="button" id="endCountButton" style="border-radius:3px;color:white;border: 0px;background-color:#FD9900;height: 20px;width: 60px;float: left;margin-left: 20px;font-size: 12px">结算</button>&nbsp;

    </#if>
    <#if purchase.payStatus = 1>

        <#--<button type="button" id="cancelPurchaseEnd" style="border-radius:3px;color:white;border: 0px;background-color:black;height: 20px;width: 60px;float: left;margin-left: 20px;font-size: 12px">撤销结算</button>&nbsp;-->

    </#if>
        <br>
        <div class="easyui-layout" data-options="fit:true">

            <div data-options="region:'north',height:'auto'" >
                <br>
                <!--startprint-->
            <#--<form method="post" class="form-inline search-form" style="float: left;">-->

                <div class="form-group" style="width: 100%">
                <div style="width: 25%;float: left">
                <label style="margin-left: 20px">采购日期:</label>
                <input type="hidden" id="purchaseId" value="${purchaseId}"/>
                ${ptime}</div>

                <div style="width: 25%;float: left">
                <label>采购单号:</label>
                ${purchase.code}</div>

                <div style="width: 25%;float: left">
                <label >采购类型:</label>
                ${ptype}
                </div>

                <div style="width: 25%;float: left">
                <label>采购员:</label>
                ${username}
                </div>
                <br>

                    <div style="width: 25%;float: left">
                <label style="margin-left: 20px">供应商:</label>${relative}</div>
                    <div style="width: 25%;float: left">
                <label>入库状态:</label>${orderStatus}</div>
                    <div style="width: 25%;float: left">
                <label>创建时间:</label>${ctime}</div>
                    <div style="width: 25%;float: left">
                <label>结清状态:</label>${payStatus}</div>
                    <br>

            </div>

        </div>

        <div data-options="region:'center'">

            <table id="pPurchaseDetail_table"  style="height:'auto';width: 100%" ></table>
            <br>
            <form method="post" class="form-inline search-form" style="float: left;width: 96%">
                <div class="form-group" style="width: 100%">
                    <div style="width: 20%;float: left">
                    <label style="width: 50%;float: left">总计金额:￥</label>
                    <input id="totalMoney" style="border:none;height: 30px;width:50%" value="${totalMoney}" readonly />
                        </div>
                    <#if otherMoney =1>
                    <div style="width: 20%;float: left">
                        <label style="width: 50%;float: left">实付金额:￥</label>
                        <input id="realPay" onkeyup="clearNoNum(this)" value="${purchase.amountIng}" style="border:none;height: 30px;width:50%;border-bottom:1px solid grey;"/>
                    </div>
                    <div style="width: 20%;float: left">
                        <label style="width: 50%;float: left">结算情景:</label>
                        <input  id="payStatusCommit" name="orderStatus"  style="width: 50%" />
                    </div>
                    <div style="width: 40%;float: left">
                        <label style="width: 25%;float: left">备注:</label>
                        <input id="remark"  value="${purchase.remark}" style="border:none;height: 30px;width: 75%;border-bottom:1px solid grey;"/>
                    </div>
                    </#if>
                <#if otherMoney !=1 && endCount =1>
                <div style="width: 20%;float: left">
                    <label style="float: left;width: 50%">实付金额:￥</label>
                    <input id="realPay" style="border:none;height: 30px;width: 50%" value="${hasPay}" readonly />
                </div>
                <div style="width: 20%;float: left">
                    <label style="float: left;width: 50%">待付金额:￥</label>
                    <input id="realPay" style="border:none;height: 30px;width: 50%" value="${amountIng}" readonly />
                </div>
                <div style="width: 40%;float: left">
                    <label style="float: left;width: 25%">备注:</label>
                    <input id="remark"  value="${purchase.remark}" style="border:none;height: 30px;width: 75%;" readonly />
                </div>
                </#if>

                    <#if endCount =2>
                    <div style="width: 20%;float: left">
                        <label style="float: left;width: 50%">实付金额:￥</label>
                        <input id="realPay" style="border:none;height: 30px;width: 50%" value="${hasPay}" readonly />
                        </div>
                        <#--<label style="margin-left: 30px">待付金额:</label>-->
                        <#--￥<input id="money" style="border:none;height: 30px;width: 60px" value="${purchase.amountIng}" />-->
                    <div style="width:60%;float: left">
                        <label style="float: left;width: 16.67%">备注:</label>
                        <input id="remark" style="border:none;height: 30px;width: 83.33%" value="${purchase.remark}"  readonly/>
                        </div>
                     </#if>

                </div>

            </form>
            <br>

        </div>



    </div>

</div>
</div>


<#include "./../../common/lib/jslib.ftl">
<script type="text/javascript">
    function clearNoNum(obj){
        obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
        obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的
        obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
        obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');//只能输入两个小数
        if(obj.value.indexOf(".")< 0 && obj.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
            obj.value= parseFloat(obj.value);
        }
    }
</script>
<script src="/bizjs/purchaseDetailWindow.js?t=2"></script>
