<!-- p_purchase.ftl 采购单 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">单号</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="code" style="width: 100%;"
                       data-options="prompt:'单号'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">供应商id</label>
            <div class="col-sm-8">
                <select id="relativeId" class="easyui-combobox" name="relativeId" style="width:100%;"
                        data-options="prompt:'供应商id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">预购商品金额</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="amount" style="width:100%;"
                        data-options="prompt:'预购商品金额',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">实际商品金额</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="amountActual" style="width:100%;"
                        data-options="prompt:'实际商品金额',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">已付金额</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="amountAlready" style="width:100%;"
                        data-options="prompt:'已付金额',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">供应商联系电话</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="contactTel" style="width: 100%;"
                       data-options="prompt:'供应商联系电话'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">供应商联系人</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="contactName" style="width: 100%;"
                       data-options="prompt:'供应商联系人'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">采购类型</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="ptype" style="width:100%;"
                        data-options="prompt:'采购类型',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">操作人姓名</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="username" style="width: 100%;"
                       data-options="prompt:'操作人姓名'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">备注</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="remark" style="width: 100%;"
                       data-options="prompt:'备注'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">状态（0 待入库，1 部分入库，2 全部入库， 3 作废）</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">支付状态（0 未结清，1已结清）</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="payStatus" style="width:100%;"
                        data-options="prompt:'支付状态（0 未结清，1已结清）',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">采购时间</label>
            <div class="col-sm-8">
                <input class="easyui-datebox" name="ptime" style="width: 100%;"
                       data-options="prompt:'采购时间',editable:false">
            </div>
        </div>
    </form>
</div>
