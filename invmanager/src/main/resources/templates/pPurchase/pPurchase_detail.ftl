<!-- 销售单详情 -->
<#include "./../common/lib/csslib.ftl">
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <div class="easyui-layout" data-options="fit:true">
            <input type="hidden" id="salesId" name="salesId" value="${id}"/>
            <input type="hidden" id="goods" name="goods"/>
            <input type="hidden" id="orderStatus" name="orderStatus" value="${purchase.orderStatus}"/>

            <div data-options="region:'north',height:445" title="采购详情">
                <div id="pPurchaseDetail_btn" class="datagrid-toolbar">

                </div>
                <form method="post" class="form-inline search-form" style="float: left;">
                    <div class="form-group">
                        <label>采购日期</label>
                        <input name="ptime" class="easyui-datebox" readonly value="${ptime}"/>
                        <label>单据号</label>
                        <input name="code" class="easyui-textbox" readonly value="${purchase.code}"/>
                        <label>创建时间</label>
                        <input name="ctime" class="easyui-textbox" readonly value="${ctime}" readonly/>
                    </div>
                    <br>
                    <div class="form-group">
                        <label>供应商</label>
                        <input name="relativeId" class="easyui-combobox" value="${purchase.relativeId}"
                               data-options="url: '/wRelativeUnit/supplier/list',panelHeight:'auto',valueField:'id',textField:'name',editable:false"
                               readonly>
                        <label>采购员</label>
                        <input id="cusTel" name="cusTel" class="easyui-textbox" value="${purchase.username}" readonly/>
                        <label>采购类型</label>
                        <input name="ptype" class="easyui-combobox" value="${purchase.ptype}"
                               data-options="url: '/sysDict/purchase_type',panelHeight:'auto',valueField:'id',textField:'text',editable:false"
                               readonly>
                        <label>订单状态</label>
                        <input name="orderStatus" class="easyui-combobox" value="${purchase.orderStatus}"
                               data-options="url: '/sysDict/purchase_order_status',panelHeight:'auto',valueField:'id',textField:'text',editable:false"
                               readonly>
                    </div>
                </form>
                <div class="easyui-layout" data-options="fit:true">
                    <div data-options="region:'center'">
                        <table id="pPurchaseDetail_table" style="height: 300px;"></table>
                    </div>
                </div>
            </div>
            <div data-options="region:'center'" title="新增付款">
                <div class="easyui-layout" data-options="fit:true">
                    <div data-options="region:'north',height:100">
                        <form id="addPayInfoForm" method="post" class="form-inline search-form" style="float: left;">

                            <input type="hidden" id="purchaseId" name="businessId" value="${purchaseId}"/>
                            <div class="form-group">
                                <label>总额</label>
                                <input id="amountActual" class="easyui-numberbox" readonly data-options="precision:2"
                                       value="${purchase.amountActual}"/>
                                <label>已付</label>
                                <input id="amountAlready" class="easyui-numberbox" readonly value="${purchase.amountAlready}" data-options="precision:2"/>
                                <label>待付</label>
                                <input id="amountIng" class="easyui-numberbox" readonly value="${purchase.amountIng}" data-options="precision:2"/>
                            </div>
                            <br>
                            <div class="form-group">
                                <label>本次付款：</label>
                                <input id="payFee" name="payFee" class="easyui-numberbox" value="0"  data-options="min:0,precision:2,onChange:PPurchaseDetail.changePayFee" >
                                <label>结算方式：</label>
                                <input id="payType" name="payType" style="width: 177px;">
                                <label>备注：</label>
                                <input id="payRemark" name="payRemark" class="easyui-textbox" style="width: 443px;"/>
                                <#if purchase.payStatus == 0>
                                <a id="btnPay" href="javascript:void(0)" onclick="PPurchaseDetail.pay()"  class="easyui-linkbutton" data-options="iconCls:'icon-save'">支付</a>
                                </#if>
                            </div>
                        </form>
                    </div>
                    <div data-options="region:'center'" title="结算记录">
                        <table id="pay_table"></table>
                    </div>
                </div>

            </div>
        </div>
    </div>

<#include "./../common/lib/jslib.ftl">
    <script src="/bizjs/pPurchase_detail.js?t=2"></script>