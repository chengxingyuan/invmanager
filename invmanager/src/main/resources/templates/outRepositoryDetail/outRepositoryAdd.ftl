<!-- 新建出库单 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <div class="easyui-layout" data-options="fit:true">
            <form id="myForm" method="post">
                <input type="hidden" id="goods" name="goods"/>
                <input type="hidden" id="type" name="type"/>

                <div data-options="region:'north',height:93">
                    <div id="outRepository_btn" class="datagrid-toolbar">

                    </div>
                    <div  class="form-inline search-form">
                        <div class="form-group">
                            <label>出库类型</label>
                            <input name="outRepositoryType"/>
                        </div>
                        <div class="form-group">
                            <label>出库时间</label>
                            <input name="orderTime"/>
                        </div>
                    </div>
                </div>
                <div data-options="region:'center'">
                    <table id="outRepository_table"></table>
                </div>
            </form>
        </div>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">
<script src="/bizjs/outRepositoryAdd.js"></script>