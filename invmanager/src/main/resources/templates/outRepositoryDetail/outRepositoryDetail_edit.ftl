<!-- out_repository_detail.ftl 出库单详情 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">所取商品</label>
            <div class="col-sm-8">
                <select id="skuId" class="easyui-combobox" name="skuId" style="width:100%;"
                        data-options="prompt:'所取商品'">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">出库单号</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="code" id="code" style="width: 100%;"
                       data-options="prompt:'出库单号'">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">供应商</label>
            <div class="col-sm-8">
                <select id="providerId" class="easyui-combobox" name="providerId" style="width:100%;"
                        data-options="prompt:'供应商'">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">仓库</label>
            <div class="col-sm-8">
                <select id="wearhouseId" class="easyui-combobox" name="wearhouseId" style="width:100%;"
                        data-options="prompt:'仓库'">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">库位</label>
            <div class="col-sm-8">
                <select id="positionId" class="easyui-combobox" name="positionId" style="width:100%;"
                        data-options="prompt:'库位'">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">单据时间</label>
            <div class="col-sm-8">
                <input class="easyui-datebox" name="orderTime" style="width: 100%;"
                       data-options="prompt:'单据时间',editable:false">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">出库类型</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="type" style="width:100%;"
                        data-options="prompt:'出库类型',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">数量</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="count" style="width:100%;"
                        data-options="prompt:'数量',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">操作人</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="username" style="width: 100%;"
                       data-options="prompt:'操作人'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">备注</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="remark" style="width: 100%;"
                       data-options="prompt:'备注'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">是否有效</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
    </form>
</div>
<script>
    $('#code').attr('readonly',true);
    $('#skuId').attr('readonly',true);
</script>
