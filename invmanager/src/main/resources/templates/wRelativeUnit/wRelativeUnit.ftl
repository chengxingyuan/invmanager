<!-- w_relative_unit.ftl 往来客户 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-panel" data-options="fit:true">
    <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'west',split:true,width:'15%'" title="合作性质">
            <ul id="rpropertyTree"></ul>
        </div>
        <div data-options="region:'center'">
            <div class="easyui-layout" data-options="fit:true">
                <div data-options="region:'north',height:86">
                    <div id="wRelativeUnit_btn" class="datagrid-toolbar">
                        <a href="javascript:void(0)" id="mb" class="easyui-menubutton"
                           data-options="menu:'#mm',iconCls:'icon-search'">联查</a>
                        <div id="mm" style="width:150px;">
                            <div id="lcxxdd">线下订单</div>
                            <div id="lcptdd">平台订单</div>
                            <div id="lcyskd">应收款单</div>
                        </div>

                        <a href="javascript:void(0)" id="mb2" class="easyui-menubutton"
                           data-options="menu:'#mm2',iconCls:'icon-search'" style="display: none">联查</a>
                        <div id="mm2" style="width:150px;">
                            <div id="lccgd">采购单</div>
                            <div id="lcyfkd">应付款单</div>
                        </div>
                    </div>
                    <form id="wRelativeUnit_form" class="form-inline search-form" style="float: left">
                        <div class="form-group">
                            <label>查询内容</label>
                            <input name="name" class="easyui-textbox" data-options="prompt:'输入名称/助记码/联系人/联系电话'" style="width:200px;">
                            <input name="rproperty" id="rproperty" class="easyui-textbox" type="hidden">
                        </div>
                    </form>
                    <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询(Enter)</button>
                </div>
                <div data-options="region:'center',border:false">
                    <table id="wRelativeUnit_table"></table>
                </div>
            </div>
        </div>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/wRelativeUnit.js?t=1"></script>