<!-- w_relative_unit.ftl 往来客户 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-1 control-label">名称</label>
            <div class="col-sm-3">
                <input id="unitName" class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'名称',required:true,validType:'length[1,128]'">
            </div>
            <#--<label class="col-sm-1 control-label">编码</label>-->
            <#--<div class="col-sm-3">-->
                <#--<input class="easyui-textbox" name="code" style="width: 100%;"-->
                       <#--data-options="prompt:'编码'">-->
            <#--</div>-->
            <#--<label class="col-sm-1 control-label">简称</label>-->
            <#--<div class="col-sm-3">-->
                <#--<input id="unitSimpleName" class="easyui-textbox" name="simpleName" style="width: 100%;"-->
                       <#--data-options="prompt:'简称'">-->
            <#--</div>-->
            <label class="col-sm-1 control-label">助记码</label>
            <div class="col-sm-3">
                    <span class="textbox easyui-fluid" style="width: 145px;">
                        <input id="unitHelpCode" name="helpCode" type="text"
                               class="textbox-text" placeholder="助记码" maxlength="128"
                               style="text-transform:uppercase;margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 143px;" />
                    </span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-1 control-label">性质</label>
            <div class="col-sm-3">
                <select id="unitRproperty" class="easyui-combobox" name="rproperty" style="width:100%;"
                        data-options="prompt:'性质',readonly:true,panelHeight:'auto',required:true,url:'/sysDict/relativeNuit_PR',editable:false">
                </select>
            </div>
            <label class="col-sm-1 control-label">会员类型</label>
            <div class="col-sm-3">
                <select id="unitMemberType" class="easyui-combobox" name="memberType" style="width:100%;"
                        data-options="prompt:'会员类型',panelHeight:'auto',url:'/sysDict/relativeNuit_MT',editable:false">
                </select>
            </div>
            <label class="col-sm-1 control-label">状态</label>
            <div class="col-sm-3">
                <select id="unitStatus" class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',panelHeight:'auto',editable:false">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>

        <div class="easyui-tabs" data-options="plain:true,pill:true">
            <div title="客户信息" style="padding:10px;overflow: hidden;">
                <div class="form-group">
                    <label class="col-sm-2 control-label">联系人</label>
                    <div class="col-sm-8">
                        <input id="unitContactor" class="easyui-textbox" name="contactor" style="width: 100%;"
                               data-options="prompt:'联系人',validType:'length[0,50]'">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">联系电话</label>
                    <div class="col-sm-8">
                        <input id="unitTel" class="easyui-textbox" name="tel" style="width: 100%;"
                               data-options="prompt:'联系电话',validType:'length[0,11]'">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">地址</label>
                    <div class="col-sm-8">
                        <input id="unitAddr" class="easyui-textbox" name="addr" style="width: 100%;"
                               data-options="prompt:'地址',validType:'length[0,128]'">
                    </div>
                </div>
            </div>
        </div>

    </form>
</div>
<script>
    var WRelativeUnitEdit = {};
    WRelativeUnitEdit.reset = function () {
        $("#unitName").textbox("setValue", "");
        $("#unitHelpCode").val("");
        $("#unitRproperty").combobox("setValue", "");
        $("#unitMemberType").combobox("setValue", "");
        $("#unitStatus").combobox("setValue", 0);
        $("#unitContactor").textbox("setValue", "");
        $("#unitTel").textbox("setValue", "");
        $("#unitAddr").textbox("setValue", "");
        setTimeout("$(\"#unitName\").next().find(\".textbox-text\").focus()", 500);
    };
    $(function () {
        setTimeout("$(\"#unitName\").next().find(\".textbox-text\").focus()", 500);

        $("#unitHelpCode").keydown(function(){
        });
    });
</script>