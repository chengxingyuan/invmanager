<!-- m_materiel.ftl 存货表 -->
<#include "./../common/lib/csslib.ftl">

<script>
    var Customer = {
        uList: '/wRelativeUnit/list'
    };

    Customer.dg = function () {
        var grid = new $yd.DataGrid(this.dtable);
        grid.url = Customer.uList;
        grid.queryParams = {
            rproperty: 1,
            status: 0
        };
        grid.singleSelect = true;
        grid.rownumbers = true;
        grid.columns = [[
            {field: 'id', hidden: true}
            , {field: 'name', title: '名称', width: 50}
            , {field: 'code', title: '编码', width: 50}
            , {field: 'simpleName', title: '简称', width: 50}
            , {field: 'contactor', title: '联系人', width: 50}
            , {field: 'tel', title: '联系电话', width: 50}
            , {field: 'addr', title: '地址', width: 50}
        ]];
        grid.loadGrid();
    };

    Customer.dg.getSels = function () {
        return Customer.dtable.datagrid('getSelections');
    };

    function doSearch() {
        $("#cusTb").datagrid({
            queryParams: {
                name: $("input[name='name']").val(),
                rproperty: 1
            }
        });
        $("#cusTb").datagrid("reload");
    }

    $(function () {
        Customer.dtable = $('#cusTb');
        Customer.dg(); // 数据表格
    });

</script>

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north'">
        <input name="name" class="easyui-searchbox" data-options="prompt:'请输入客户名称',searcher:doSearch" style="width:100%" />
    </div>
    <div data-options="region:'center'">
        <table id="cusTb"></table>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">