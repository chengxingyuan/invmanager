<#--导入-->
<#include "./../common/lib/csslib.ftl">

<div class="container-fluid">
    <form class="form-horizontal mform" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label class="col-sm-2 control-label">往来客户Excel</label>
            <div class="col-sm-8">
                <input class="easyui-filebox" name="excelFile" data-options="prompt:'请选择excel'" style="width:100%">
            </div>
        </div>
    </form>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/wRelativeUnit.js"></script>