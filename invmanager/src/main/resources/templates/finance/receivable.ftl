<!-- sale_return.ftl 销售退货表 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'100px'">
        <div id="saleReturn_btn" class="datagrid-toolbar">

        </div>
        <form id="saleReturn_form" class="form-inline search-form" style="float: left">
            <div class="form-group">
                <label>搜索</label>
                <input name="searchText" class="easyui-textbox" data-options="prompt:'单号、收款单位名'">
            </div>
            <div class="form-group">
                <label>订单日期</label>
                <input id="startTime" name="startTime" style="width: 100px;" class="easyui-datebox">-<input id="endTime" style="width: 100px;" name="endTime" class="easyui-datebox">
            </div>
        </form>
        <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询</button>
    </div>
    <div data-options="region:'center'">
        <table id="saleReturn_table"></table>
    </div>
    <div data-options="region:'south',height:'25%', title:'商品'">
    <#--<div id="printBtn" class="datagrid-toolbar"> </div>-->
        <table id="saleReturnGoods_table"></table>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/receivable.js"></script>