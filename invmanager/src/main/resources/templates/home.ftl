<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>首页</title>
    <link rel="stylesheet" href="/home/css/main.css">
</head>

<body>
<div class="wrapper">
    <div class="container index">
        <div class="tab_box">
            <a href="#" onclick="createTablePage('/repositoryCount','库存量查询');"><img src="/home/images/ic_kccx.png" alt=""> 库存量查询</a>
            <a href="#" onclick="createTablePage('/mSales/addSales','新建销售单');"><img src="/home/images/ic_xjxsd.png" alt="">新建销售单</a>
            <a href="#" onclick="createTablePage('/pPurchase/addPurchase','新建采购单');"><img src="/home/images/ic_xjcgd.png" alt="">新建采购单</a>
        </div>
        <div class="block_box">
            <div class="block b_1">
                <div class="top">
                    <p>库存总数量</p>
                    <span>${repositoryCount['0']}</span>
                </div>
                <div class="bottom">
                    <p>今日出库量</p>
                    <span>${repositoryCount['1']}</span>
                </div>
            </div>
            <div class="block b_2">
                <div class="top">
                    <p>今日线下销售总金额（元）</p>
                    <span>￥${todaySellCount['0']}</span>
                </div>
                <div class="bottom">
                    <p>今日线下销售总单量</p>
                    <span>${todaySellCount['1']}</span>
                </div>
            </div>
            <div class="block b_3">
                <div class="top">
                    <p>今日平台销售总金额（元）</p>
                    <span>￥${todayTticarSellCount['0']}</span>
                </div>
                <div class="bottom">
                    <p>今日平台销售总单量</p>
                    <span>${todayTticarSellCount['1']}</span>
                </div>
            </div>
            <div class="block b_4">
                <div class="top">
                    <p>待付款总金额（元）</p>
                    <span>￥${amountCount['0']}</span>
                </div>
                <div class="bottom">
                    <p>待收款总金额（元）</p>
                    <span>￥${amountCount['1']}</span>
                </div>
            </div>
            <div class="block b_5">
                <div class="top">
                    <p>本月线下销售总金额（元）</p>
                    <span>￥${monthSellCount['0']}</span>
                </div>
                <div class="bottom">
                    <p>本月线下销售总单量</p>
                    <span>${monthSellCount['1']}</span>
                </div>
            </div>
            <div class="block b_6">
                <div class="top">
                    <p>本月平台销售总金额（元）</p>
                    <span>￥${monthTticarSellCount['0']}</span>
                </div>
                <div class="bottom">
                    <p>本月平台销售总单量</p>
                    <span>${monthTticarSellCount['1']}</span>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    fontSize();
    window.onresize = function () {
        fontSize();
    }

    function fontSize() {
        var deviceWidth = document.documentElement.clientWidth || document.body.clientWidth;
        if (deviceWidth >= 1920) deviceWidth = 1920;
        if (deviceWidth <= 1000) deviceWidth = 1000;
        document.documentElement.style.fontSize = deviceWidth / 19.2 + 'px';
    }
    function createTablePage(url, name) {
        parent.inv.OpenPage(url,
                "<i class=\"menu-icon fa fa-flag\"></i>\n" +
                "<span class=\"menu-text\"> " + name +" </span>\n" +
                "<b class=\"arrow \"></b>", true);
    };

    // 刷新数据
    function refreshData(url,th) {
        $.get("/home/" + url,{},function (data) {
//            console.log(url+"获取的统计数据：" + JSON.stringify(data));
            $(th).parent().find("span:eq(0)").text(data['0']);
            $(th).parent().find("span:eq(1)").text(data['1']);
        });

    }
</script>
</body>

</html>