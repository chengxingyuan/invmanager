
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>表单打印</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="/public/assets/css/p_form2.css"/>
</head>
<body>

<#--<div class="p_form">-->
<div class="table-box">
<#-- 从前的开始！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！-->
    <table cellspacing="0" cellpadding="0" id="printDetail_table">
        <tr class="hidden">
            <th width="7.6923%"></th>
            <th width="7.6923%"></th>
            <th width="7.6923%"></th>
            <th width="7.6923%"></th>
            <th width="7.6923%"></th>
            <th width="7.6923%"></th>
            <th width="7.6923%"></th>
            <th width="7.6923%"></th>
            <th width="7.6923%"></th>
            <th width="7.6923%"></th>
            <th width="7.6923%"></th>
            <th width="7.6923%"></th>
            <th width="7.6923%"></th>
        </tr>
        <tr>
            <th colspan="13">入库单</th>
        </tr>
        <tr>
            <th colspan="1">入库单号</th>
            <td colspan="7">${data.code}</td>
            <th colspan="1">流水号</th>
            <td colspan="4">${data.rcode}</td>
        </tr>
        <tr>
            <th colspan="1">往来单位</th>
            <td colspan="7">${data.providerName}</td>
            <th colspan="1">入库日期</th>
            <td colspan="4">${data.orderTime}</td>
        </tr>
        <tr>
            <th colspan="1">入库仓库</th>
            <td colspan="7">${data.repository}</td>
            <th colspan="1">入库类型</th>
            <td colspan="4">${data.typeName}</td>
        </tr>
        <tr id="tr_header">
            <th colspan="1">序号</th>
            <th colspan="4">商品名称</th>
            <th colspan="2">规格</th>
            <th colspan="1">单位</th>
            <th colspan="1">单价（元）</th>
            <th colspan="1">数量</th>
            <th colspan="1">金额（元）</th>
            <th colspan="2">备注</th>
        </tr>
        <tr id="temp">
            <td class="center" colspan="1" id="num"></td>
            <td colspan="4" id="goodsName"></td>
            <td colspan="2" id="skus"></td>
            <td class="center" colspan="1" id="unitName"></td>
            <td class="center" colspan="1" id="amountActual"></td>
            <td class="center" colspan="1" id="count"></td>
            <td class="center" colspan="1" id="shouldPay"></td>
            <td colspan="2"></td>
        </tr>
<#--        <tr>
            <th class="center" colspan="1"></td>
            <th colspan="4">合计</th>
            <td colspan="2"></td>
            <td class="center" colspan="1"></td>
            <td class="center" colspan="1">${totalCount}</td>
            <td class="center" colspan="1"></td>
            <td class="center" colspan="1">${totalMoney}</td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <th class="center" colspan="1"></td>
            <th class="center" colspan="4">金额大写</th>
            <td class="center" colspan="8">RMB: ${capitalMoney}</td>
        </tr>
        <tr>
            <th class="center" colspan="1">记账</th>
            <td colspan="4"></td>
            <th class="center" colspan="2">审核人</th>
            <td colspan="1"></td>
            <th class="center" colspan="1">入库人</th>
            <td colspan="1">${nickname}</td>
            <th class="center" colspan="1">制单人</th>
            <td colspan="2">${Session.current_user.nickname}</td>
        </tr>
        <tr>
            <th class="center" colspan="1">备注</th>
            <td colspan="12"></td>
        </tr>-->
        <input type="hidden" id="codeData"  value="${data.code}"/>
    </table>
<#-- 从前的结束！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！-->

<#--    <div class="form_table-context">
     <table class="form_table form_table_account">
         <tr style="display: none">
             <th width="5%">序号</th>
             <th>商品名称</th>
             <th>规格</th>
         &lt;#&ndash;                <th>编号</th>&ndash;&gt;
             <th>单位</th>
             <th>单价(元)</th>
             <th>数量</th>
             <th>金额(元)</th>
             <th>备注</th>
         </tr>
         <tr></tr>
         <tr><td></td><td>合计</td><td></td><td></td><td></td><td>200</td><td>20008</td></tr>
         <tr class="total_money"> <td>金额大写</td><td colspan="7">RMB:jinnjscnjs</td></tr>
         <tr><td>记账</td><td style="width: 100px;"></td><td>审核人</td><td>闫豆</td><td>入库人</td><td>闫豆</td><td>制单人</td><td>闫豆</td></tr>
         <tr> <td>备注</td><td colspan="7"><input style="width: 100%; border: none; outline: none" type="text"></td></tr>
     </table>
 </div>-->

    <#--<div class="total_price">-->
        <#--<div class="total_title">合计</div>-->
        <#--<div class="total_money">-->
            <#--<span>数量：${totalCount}</span>-->
            <#--<span>金额：￥${totalMoney}</span>-->
        <#--</div>-->
    <#--</div>-->
<#--    <ul class="form_footer">
        <li>记账人签字（盖章）：</li>
        <li>审核人签字（盖章）：</li>
        <li>入库人签字（盖章）：</li>
        <li>制单人签字（盖章）：${Session.current_user.nickname}</li>
&lt;#&ndash;        <li>日期 ：</li>&ndash;&gt;
        <li>备注 ：</li>
    </ul>-->

    <div>

        <button type="button" id="printButton"
                style="float:right;margin-right:100px;border-radius:2px;color:white;border: 0px;background-color:#00a2d4;height: 30px;width: 86px" >打&nbsp;印</button>
    </div>
</div>
<#include "./../common/lib/jslib.ftl">
<script>
    window.onload = function() {
        $.ajax({
            url: "/inRepositoryDetail/getOne",
            type: "get",
            dataType: 'json',
//            jsonpCallback:"callback",
            data: {
                code: $("#codeData").val()
            },
            success: function (data) {
                $.each(data, function (i, n) {
                    var row = $("#temp").clone();
                    row.find("#num").text(i+1);
                    row.find("#goodsName").text(n.goodsName);
                    row.find("#skus").text(n.skus);
                    row.find("#code").text(n.code);
                    // row.find("#unitName").text(n.unitName);
                    if(n.unitName == null || n.unitName == ''){
                        row.find("#unitName").text(n.unit);
                    }else{
                        row.find("#unitName").text(n.unitName);
                    }
                    row.find("#amountActual").text(n.price);
                    row.find("#count").text(n.count);
                    if (n.price !=null && n.price != undefined) {
                        var totalSum = n.count*n.price;
                        row.find("#shouldPay").text(totalSum.toFixed(2));
                    };
                    console.log("row:"+row);
                    row.appendTo("#printDetail_table");//添加到模板的容器中
                    // row.after("#tr_header");
                });
                $("#temp").hide();
/*                var string = '<tr><td>合计</td><td></td><td></td><td></td><td></td><td>${totalCount}</td><td>${totalMoney}</td><td></td></tr>'+
                        '<tr class="total_money"> <td>金额大写</td><td colspan="7">RMB:${capitalMoney}</td></tr>'+
                        ' <tr><td>记账</td><td></td><td>审核人</td><td></td><td>入库人</td><td>${nickname}</td><td>制单人</td><td>${Session.current_user.nickname}</td></tr>'+
                        '<tr> <td>备注</td><td colspan="7"><input style="width: 100%; border: none; outline: none" type="text"></td></tr>';
                $('#printDetail_table').append(string);*/
                var string = '   <tr>\n' +
                        '            <th class="center" colspan="1"></td>\n' +
                        '            <th colspan="4">合计</th>\n' +
                        '            <td colspan="2"></td>\n' +
                        '            <td class="center" colspan="1"></td>\n' +
                        '            <td class="center" colspan="1"></td>\n' +
                        '            <td class="center" colspan="1">${totalCount}</td>\n' +
                        '            <td class="center" colspan="1">${totalMoney}</td>\n' +
                        '            <td colspan="2"></td>\n' +
                        '        </tr>\n' +
                        '        <tr>\n' +
                        '            <th class="center" colspan="1"></td>\n' +
                        '            <th class="center" colspan="4">金额大写</th>\n' +
                        '            <td class="center" colspan="8">RMB: ${capitalMoney}</td>\n' +
                        '        </tr>\n' +
                        '        <tr>\n' +
                        '            <th class="center" colspan="1">记账</th>\n' +
                        '            <td colspan="4"></td>\n' +
                        '            <th class="center" colspan="2">审核人</th>\n' +
                        '            <td colspan="1"></td>\n' +
                        '            <th class="center" colspan="1">入库人</th>\n' +
                        '            <td colspan="1">${nickname}</td>\n' +
                        '            <th class="center" colspan="1">制单人</th>\n' +
                        '            <td colspan="2">${Session.current_user.nickname}</td>\n' +
                        '        </tr>\n' +
                        '        <tr>\n' +
                        '            <th class="center" colspan="1">备注</th>\n' +
                        '            <td colspan="12"></td>\n' +
                        '        </tr>';
                $('#printDetail_table').append(string);
            },
            error: function (request) {
                console.log(JSON.stringify(request))
            }
        });
    }




    var wait = 1;
    document.getElementById("printButton").onclick = function() {
        time(this);
        window.print();
    }
    function time(o) {
        if (wait == 0) {
            $("#printButton").attr("style","display:block;float:right;margin-right:100px;border-radius:2px;color:white;border: 0px;background-color:#00a2d4;height: 30px;width: 86px");
            wait = 1;
        } else {
            $("#printButton").attr("style","display:none;");
            wait--;
            setTimeout(function() {
                time(o)
            }, 200)
        }
    }


    function doPrint() {
        window.print();
    }
</script>
<#--<script src="/bizjs/pPurchase_print.js?t=2"></script>-->
</body>

</html>
