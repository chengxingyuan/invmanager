
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>表单打印</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="/public/assets/css/p_form.css"/>
</head>
<body>
<div class="p_form">
    <#--<h3 class="form-title">仓库采购单</h3>-->
        <br>
        <div>
            <ul class="form-header">
                <li class="form_data">
                    <span>打印日期：</span>
                    <span> ${thisTime}</span>
                </li>
                <li class="form_data">
                    <span>打印人：</span>
                    <span> ${Session.current_user.nickname}</span>
                </li>
            </ul>
        </div>
    <#--<div class="form_content">-->
        <#--<ul class="form-header">-->
            <#--<li class="form_list">-->
                <#--<span>采购日期</span>-->
                <#--<span>${ptime}</span>-->
            <#--</li>-->
            <#--<li class="form_list">-->
                <#--<span>单据号</span>-->
                <#--<span>${purchase.code}</span>-->
            <#--</li>-->
            <#--<li class="form_list">-->
                <#--<span>采购员</span>-->
                <#--<span>${buyMan}</span>-->
            <#--</li>-->
            <#--<li class="form_list">-->
                <#--<span>采购类型</span>-->
                <#--<span>${typeName}</span>-->
            <#--</li>-->
            <#--<li class="form_list supplier">-->
                <#--<span>供应商</span>-->
                <#--<span>${relativeName}</span>-->
            <#--</li>-->
        <#--</ul>-->
    <#--</div>-->
    <div class="form_table-context">
    <table class="form_table" id="inRepository_table" style="margin-top: 10px">
        <tr><th>序号</th><th>单号</th><th>单据日期</th><th>商品</th><th>入库类型</th><th>供应商</th><th>仓库</th><th >操作人</th><th >备注</th></tr>
        <tr id="temp"><td id="num"></td><td id="code"><td id="orderTime"></td><td id="goodsName"></td><td id="typeName">
        </td><td id="providerName"></td><td  id="repository"></td><td id="username"></td><td id="remark"></td></tr>
    </table>
    <input type="hidden" id="rowData" value="${ids}" />
    </div>
    <div>
        <br>
        <button type="button" id="printButton"
                style="float:right;margin-right:100px;border-radius:2px;color:white;border: 0px;background-color:#00a2d4;height: 30px;width: 86px" >打&nbsp;印</button>
    </div>
</div>
<#include "./../common/lib/jslib.ftl">
<script>
    window.onload = function() {
        var id = $('#rowData').val();
        $.ajax({
            url: "/inRepositoryDetail/printData?ids=" + id,
            type: "get",
            dataType: 'json',
            success: function (data) {
                $.each(data, function (i, n) {
                    var row = $("#temp").clone();
                    row.find("#num").text(i + 1);
                    row.find("#code").text(n.code);
                    row.find("#orderTime").text(n.orderTime);
                    row.find("#goodsName").text(n.goodsName);
                    row.find("#typeName").text(n.typeName);
                    row.find("#providerName").text(n.providerName);
                    row.find("#repository").text(n.repository);
                    row.find("#username").text(n.username);
                    row.find("#remark").text(n.remark);
                    row.appendTo("#inRepository_table");//添加到模板的容器中
                });


            },
            error: function (request) {
                alert(JSON.stringify(request))
            }
        });
    }




    var wait = 1;
    document.getElementById("printButton").onclick = function() {
    time(this);
    window.print();
    }
    function time(o) {
    if (wait == 0) {
    $("#printButton").attr("style","display:block;float:right;margin-right:100px;border-radius:2px;color:white;border: 0px;background-color:#00a2d4;height: 30px;width: 86px");
    wait = 1;
    } else {
    $("#printButton").attr("style","display:none;");
    wait--;
    setTimeout(function() {
    time(o)
    }, 200)
    }
    }


    function doPrint() {
    window.print();
    }
</script>
<#--<script src="/bizjs/pPurchase_print.js?t=2"></script>-->
</body>

</html>
