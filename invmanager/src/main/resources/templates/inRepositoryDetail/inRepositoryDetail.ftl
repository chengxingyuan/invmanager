<!-- in_repository_detail.ftl 入库单详情 -->
<#include "./../common/lib/csslib.ftl">
<input type="hidden" id="hidHouseId" value="${houseId}" />
<input type="hidden" id="hidHouseName" value="${houseName}" />
<input type="hidden" id="hidStartTime" value="${startTime}" />
<input type="hidden" id="hidEndTime" value="${endTime}" />
<div class="easyui-layout" data-options="fit:true">

    <div data-options="region:'north',height:'100px'">
        <div id="inRepositoryDetail_btn" class="datagrid-toolbar">

        </div>
        <form id="inRepositoryDetail_form" class="form-inline search-form" style="float: left;">
            <div class="form-group">
                <label>查询内容</label>
                <input name="searchText" class="easyui-textbox" data-options="prompt:'请输入单号、关联单号、商品名称'" style="width: 200px;">
            </div>
            <div class="form-group" style="line-height: 20px">
                <label>仓库</label>
                <div style="width: 200px; display: inline-block; vertical-align: middle">
                    <input name="wearhouseId"  id="wearhouseId"/>
                </div>
            </div>
            <div class="form-group">
                <label>入库类型</label>
                <input name="inRepositoryType" />
            </div>
            <div class="form-group">
                <label>日期</label>
                <input class="easyui-datebox" id="startTime" style="width: 100px;" name="startTime" data-options="sharedCalendar:'#cc'">-
                <input class="easyui-datebox" id="endTime"  style="width: 100px;" name="endTime" data-options="sharedCalendar:'#cc'">
                <div id="cc" class="easyui-calendar"></div>
            </div>
            <#--<button  id="reset" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width:80px">重置</button>-->
        </form>
        <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询</button>
    </div>
    <div data-options="region:'center'" style="margin-top: 0px">
        <table id="inRepositoryDetail_table"></table>
    </div>
    <div data-options="region:'south',height:'30%', title:'入库商品'">
        <#--<div id="printBtn" class="datagrid-toolbar"> </div>-->
        <table id="inRepositoryGoods_table"></table>
        <input id="codeRK" type="hidden" />
    </div>
</div>



<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/inRepositoryDetail.js?t=1"></script>