<!-- in_repository_detail.ftl 入库单详情 -->
<#include "./../common/lib/csslib.ftl">


<div class="easyui-layout" data-options="fit:true">

    <div data-options="region:'north',height:'150px'">
        <div id="inRepositoryDetail_btn" class="datagrid-toolbar">

        </div>
        <form id="inRepositoryDetail_form" class="form-inline search-form" style="float: left;">
            <div class="form-group">
                <label>单据日期</label>
                <input class="easyui-datebox" id="orderTime" name="orderTime" value="${orderTime}" readonly="readonly">
            </div>
            <div class="form-group">
                <label>单据号</label>
                <input name="code" id="code" class="easyui-textbox" data-options="prompt:'请输入单据号'" value="${code}" readonly="readonly">
            </div>
            <br>
            <div class="form-group">
                <label>入库类别</label>
                <input id="type" value="${type}" class="easyui-combobox" name="type" data-options="url:'/sysDict/repository'" style="width:177px;" readonly="readonly">
                </input>
            </div>
            <div class="form-group">
                <label>操作人</label>
                <input name="username" id="username" class="easyui-textbox" value="${username}" readonly="readonly">
            </div>
            <div class="form-group">
                <label>创建时间</label>
                <input name="ctime" id="ctime" class="easyui-textbox" data-options="prompt:'请输入单据号'" value="${ctime}" readonly="readonly">
            </div>
        </form>
    </div>
    <div data-options="region:'center'" style="margin-top: 0px">
        <table id="inRepositoryDetail_table"></table>
    </div>
</div>



<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/inRepositoryDetailNew.js"></script>