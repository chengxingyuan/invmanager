<!-- m_materiel.ftl 存货表 -->
<#--<#include "./../common/lib/csslib.ftl">-->
<input type="hidden" id="skuId" value="${skuId}" />
<input type="hidden" id="batchCode" value="${batchCode}" />
<script>
    var KW = {editIndex: "-1", result: ""};

    //获取选中的结点
    KW.getResult = function () {
        KW.result = "";
        if(KW.editIndex != "-1") {
            $("#kwTb").datagrid('endEdit', KW.editIndex);
            KW.editIndex = "-1";
        }
        var data = $("#kwTb").datagrid("getRows");
        for(var i=0; i<data.length; i++) {
            if(!KW.isEmpty(data[i].outNum)) {
                KW.result += "," + data[i].batchCode + "_" + data[i].outNum;
            }
        }
        if(KW.result != "") {
            KW.result = KW.result.substr(1);
        }
        return KW.result;
    };

    KW.isEmpty = function(value) {
        if (value == undefined || value == null || value == "" || value == "0") {
            return true;
        }
        return false;
    };

    $(function () {
        $("#kwTb").datagrid({
            url: '/inRepositoryDetail/batchCodeGrid?skuId=' + $("#skuId").val() ,
            columns:[[
                {title:'批次',field:'batchCode',width:250},
                {title:'库存',field:'num',width:100},
                {title:'出库数目',field:'outNum',width:100,editor:{
                        type:'numberbox',
                        options:{
                            min: 1, precision: 0
                        }
                    }}
            ]],
            onClickRow: function (index,row) {
                $("#kwTb").datagrid("unselectAll");
                if (KW.editIndex != "-1") {
                    $("#kwTb").datagrid('endEdit', KW.editIndex);
                }
                $("#kwTb").datagrid("beginEdit", index);
                KW.editIndex = index;
            },
            onLoadSuccess: function () {
                var rows = $("#kwTb").datagrid("getRows");
                var batchCode = $("#batchCode").val();
                if(batchCode != "" && batchCode != undefined) {
                    var arr = batchCode.split(",");
                    for(var i=0; i<arr.length; i++) {
                        var arr2 = arr[i].split("_");

                        for(var j=0; j<rows.length; j++) {
                            if(rows[j].batchCode == arr2[0]) {
                                $('#kwTb').datagrid('updateRow',{
                                    index: j,
                                    row: {
                                        outNum: arr2[1],
                                    }
                                });
                                break;
                            }
                        }
                    }
                }
            }
        });
    });

</script>

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <ul id="kwTb"></ul>
    </div>
</div>
