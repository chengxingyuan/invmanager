<!-- in_repository_detail.ftl 入库单详情 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">入库单号</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="code" style="width: 100%;"
                       data-options="prompt:'入库单号'">
            </div>
        </div>
        <#--<div class="form-group">-->
            <#--<label class="col-sm-2 control-label">对应的采购单号</label>-->
            <#--<div class="col-sm-8">-->
                <#--<select id="buszId" class="easyui-combobox" name="buszId" style="width:100%;"-->
                        <#--data-options="prompt:'采购单详情id'">-->
                <#--</select>-->
            <#--</div>-->
        <#--</div>-->
        <div class="form-group">
            <label class="col-sm-2 control-label">供应商</label>
            <div class="col-sm-8">
                <select id="providerId" class="easyui-combobox" name="providerId" style="width:100%;"
                        data-options="prompt:'供应商'">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">批号</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="batchNumber" style="width: 100%;"
                       data-options="prompt:'批号'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">仓库</label>
            <div class="col-sm-8">
                <select id="wearhouseId" class="easyui-combobox" name="wearhouseId" style="width:100%;"
                        data-options="prompt:'所在仓库'">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">库位</label>
            <div class="col-sm-8">
                <select id="positionId" class="easyui-combobox" name="positionId" style="width:100%;"
                        data-options="prompt:'库位'">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">入库类型</label>
            <div class="col-sm-8">
                <input class="easyui-combobox" name="type" style="width:100%;"
                        <#--data-options="prompt:'',min:0"-->
                       data-options="prompt:'入库类型',panelHeight:'auto',required:true,url:'/sysDict/repository'">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">数量</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="count" style="width:100%;"
                        data-options="prompt:'数量',min:0">
                </input>
            </div>
        </div>
        <#--<div class="form-group">-->
            <#--<label class="col-sm-2 control-label">操作人</label>-->
            <#--<div class="col-sm-8">-->
                <#--<input class="easyui-textbox" name="username" style="width: 100%;"-->
                       <#--data-options="prompt:'操作人'">-->
            <#--</div>-->
        <#--</div>-->
        <div class="form-group">
            <label class="col-sm-2 control-label">备注</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="remark" style="width: 100%;"
                       data-options="prompt:'备注'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">是否有效</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
    </form>
</div>
