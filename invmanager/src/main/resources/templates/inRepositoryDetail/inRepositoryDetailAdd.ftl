<!-- 新建入库单 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <div class="easyui-layout" data-options="fit:true">
            <form id="myForm" method="post">
                <input type="hidden" id="goods" name="goods"/>
                <input type="hidden" id="type" name="type"/>

                <div data-options="region:'north',height:93">
                    <div id="inRepository_btn" class="datagrid-toolbar">

                    </div>
                    <div  class="form-inline search-form">
                        <div class="form-group">
                            <label>入库类型</label>
                            <input name="inRepositoryType"/>
                        </div>
                        <div class="form-group">
                            <label>入库时间</label>
                            <input name="orderTime"/>
                        </div>
                    </div>
                </div>
                <div data-options="region:'center'">
                    <table id="InRepository_table"></table>
                </div>
            </form>
        </div>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">
<script src="/bizjs/inRepositoryAdd.js"></script>