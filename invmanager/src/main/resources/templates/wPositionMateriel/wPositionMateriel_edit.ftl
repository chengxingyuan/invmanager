<!-- w_position_materiel.ftl 库位对应的商品 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">仓库id</label>
            <div class="col-sm-8">
                <select id="wearhouseId" class="easyui-combobox" name="wearhouseId" style="width:100%;"
                        data-options="prompt:'仓库id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">库位id</label>
            <div class="col-sm-8">
                <select id="positionId" class="easyui-combobox" name="positionId" style="width:100%;"
                        data-options="prompt:'库位id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">商品id</label>
            <div class="col-sm-8">
                <select id="materielId" class="easyui-combobox" name="materielId" style="width:100%;"
                        data-options="prompt:'商品id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">是否是默认（0不是默认，1 是默认）</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="isDefault" style="width:100%;"
                        data-options="prompt:'是否是默认（0不是默认，1 是默认）',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">status</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
    </form>
</div>
