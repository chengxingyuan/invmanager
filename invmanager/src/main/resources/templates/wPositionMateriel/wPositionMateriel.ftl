<!-- w_position_materiel.ftl 库位对应的商品 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'auto'">
        <div id="wPositionMateriel_btn" class="datagrid-toolbar">

        </div>
        <form id="wPositionMateriel_form" class="form-inline search-form">
            <div class="form-group">
                <label>名称</label>
                <input name="name" class="easyui-textbox" data-options="prompt:'名称'">
            </div>
        </form>
    </div>
    <div data-options="region:'center'">
        <table id="wPositionMateriel_table"></table>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/wPositionMateriel.js"></script>