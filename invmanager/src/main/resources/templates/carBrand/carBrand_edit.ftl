<!-- car_brand.ftl 车品牌 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">车品牌名中文名</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'车品牌名中文名'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">图片地址</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="logo" style="width: 100%;"
                       data-options="prompt:'图片地址'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">首字母</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="initial" style="width: 100%;"
                       data-options="prompt:'首字母'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">等级</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="level" style="width:100%;"
                        data-options="prompt:'等级',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">显示状态</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
    </form>
</div>
