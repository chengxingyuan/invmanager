<!-- car_brand.ftl 车品牌 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'auto'">
        <div id="carBrand_btn" class="datagrid-toolbar">

        </div>
        <form id="carBrand_form" class="form-inline search-form">
            <div class="form-group">
                <label>名称</label>
                <input name="name" class="easyui-textbox" data-options="prompt:'名称'">
            </div>
        </form>
    </div>
    <div data-options="region:'center'">
        <table id="carBrand_table"></table>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/carBrand.js"></script>