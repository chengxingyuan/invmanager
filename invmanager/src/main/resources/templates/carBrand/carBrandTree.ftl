<!-- m_materiel.ftl 存货表 -->
<input type="hidden" id="isCheck" value="${isCheck}" />
<script>
    function doSearch() {
        CarTree.tb.tree({
            queryParams: {
                searchText: $("#searchText3").val()
            }
        });
        CarTree.tb.tree("reload");
    }

    var CarTree = {
        searchText: "",
        isFocus: 0,
        tb: undefined,
        isCheck: $("#isCheck").val()
    };

    CarTree.init = function () {
        $("#searchText3").keyup(function () {
            var content = $("#searchText3").val();
            if (content == CarTree.searchText) {
                return;
            }
            CarTree.searchText = content;
            doSearch();
        });

        $("#searchText3").focusin(function () {
            CarTree.isFocus = 1;
        });
        $("#searchText3").focusout(function () {
            CarTree.isFocus = 0;
        });

        //快捷键
        // document.onkeydown = CarTree.keydown;

        setTimeout("$('#searchText3').focus()", 500);
        $("a").attr("tabindex", "-1");
    };

    CarTree.keydown = function (event) {
        var e = event || window.event || arguments.callee.caller.arguments[0];
        if (!e) {
            return;
        }
//        console.log("dg")
        switch (e.keyCode) {
            case 9: //tab
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else {
                    e.returnValue = false;
                }
            case 32:
                CarTree.space();
                break;
            case 37:
                CarTree.left();
                break;
            case 38:
                CarTree.up();
                break;
            case 39:
                CarTree.right();
                break;
            case 40:
                CarTree.down();
                break;
        }
    };

    CarTree.tree = function () {
        CarTree.tb = $("#carTb");
        CarTree.tb.tree({
            url: '/carBrand/tree',
            cascadeCheck: false,
            checkbox: CarTree.isCheck == 1
        });
    };

    //获取选中的规格集合
    CarTree.getChecked = function () {
        if(CarTree.isCheck == 1) {
            var result = [];
            var nodes = CarTree.tb.tree("getChecked");
            for(var i=0; i<nodes.length; i++) {
                var node = nodes[i];
                var arr = [];
                while(node) {
                    arr.push({
                        id: node.id,
                        name: node.text,
                        level: node.attributes.level
                    });
                    node = CarTree.tb.tree("getParent", node.target);
                }
                result.push(arr.reverse());
            }

//            console.log(result)
            return result;
        } else {
            var result = [];
            var node = CarTree.tb.tree("getSelected");
            while(node) {
                result.push({
                    id: node.id,
                    name: node.text,
                    level: node.attributes.level
                });
                node = CarTree.tb.tree("getParent", node.target);
            }

//            console.log(result)
            return result;
        }
    };

    CarTree.up = function () {
        if (CarTree.isFocus == 1) {//光标在搜索框，跳转到最下面
            var roots = CarTree.tb.tree("getRoots");
            var node = CarTree.tb.tree("find", roots[roots.length - 1].id);
            $("#carTb").focus();
            if (node.state == "closed") {
                CarTree.tb.tree("select", node.target);
            } else {
                CarTree.tb.tree("select", CarTree.tb.tree("find", node.children[node.children.length - 1].id).target);
            }
            return;
        }

        var node = CarTree.tb.tree("getSelected");
        if (!node) {
            return;
        }
        if (node.pid == 0) {
            var roots = CarTree.tb.tree("getRoots");
            var tmp = 0;
            for (var i = 0; i < roots.length; i++) {
                if (roots[i].id == node.id) {
                    tmp = i;
                    break;
                }
            }
            if (tmp == 0) {
                $("#carTb").find('.tree-node-selected').removeClass('tree-node-selected');
                $("#searchText3").focus();
                CarTree.set_text_value_position("searchText3", -1);
                return;
            }
            var prev = CarTree.tb.tree("find", roots[tmp - 1].id);
            if (prev.state == "closed") {
                CarTree.tb.tree("select", prev.target);
            } else {//前一个节点的最后一个叶子
                CarTree.tb.tree("select", CarTree.tb.tree("find", prev.children[prev.children.length - 1].id).target);
            }
        } else {
            var roots = CarTree.tb.tree("getChildren", CarTree.tb.tree("find", node.pid).target);
            var tmp = 0;
            for (var i = 0; i < roots.length; i++) {
                if (roots[i].id == node.id) {
                    tmp = i;
                    break;
                }
            }
            if (tmp == 0) {
                CarTree.tb.tree("select", CarTree.tb.tree("find", node.pid).target);
            } else {
                CarTree.tb.tree("select", CarTree.tb.tree("find", roots[tmp - 1].id).target);
            }
        }
    };

    CarTree.down = function () {
        if (CarTree.isFocus == 1) {//光标在搜索框
            $("#carTb").focus();
            var data = CarTree.tb.tree("getRoots");
            if (data.length > 0) {
                CarTree.tb.tree("select", data[0].target);
            }
            return;
        }
        var node = CarTree.tb.tree("getSelected");
        if (!node) {
            return;
        }
        if (node.pid == 0) {
            if (node.state == "open") {
                CarTree.tb.tree("select", CarTree.tb.tree("find", node.children[0].id).target);
            } else {
                var roots = CarTree.tb.tree("getRoots");
                var tmp = 0;
                for (var i = 0; i < roots.length; i++) {
                    if (roots[i].id == node.id) {
                        tmp = i;
                        break;
                    }
                }
                if (tmp != roots.length - 1) {//非最后一个
                    CarTree.tb.tree("select", CarTree.tb.tree("find", roots[tmp + 1].id).target);
                } else {
                    $("#carTb").find('.tree-node-selected').removeClass('tree-node-selected');
                    $("#searchText3").focus();
                    CarTree.set_text_value_position("searchText3", -1);
                }
            }
        } else {//当前为叶子节点
            var roots = CarTree.tb.tree("getChildren", CarTree.tb.tree("find", node.pid).target);
            var tmp = 0;
            for (var i = 0; i < roots.length; i++) {
                if (roots[i].id == node.id) {
                    tmp = i;
                    break;
                }
            }
            if (tmp != roots.length - 1) {
                CarTree.tb.tree("select", CarTree.tb.tree("find", roots[tmp + 1].id).target);
            } else {//找父节点的下一个
                roots = CarTree.tb.tree("getRoots");
                tmp = 0;
                for (var i = 0; i < roots.length; i++) {
                    if (roots[i].id == node.pid) {
                        tmp = i;
                        break;
                    }
                }
                if (tmp != roots.length - 1) {
                    CarTree.tb.tree("select", CarTree.tb.tree("find", roots[tmp + 1].id).target);
                } else {
                    $("#carTb").find('.tree-node-selected').removeClass('tree-node-selected');
                    $("#searchText3").focus();
                    CarTree.set_text_value_position("searchText3", -1);
                }
            }
        }
    };

    CarTree.left = function () {
        var node = CarTree.tb.tree("getSelected");
        if (!node) {
            return;
        }
        if (node.pid != 0) {
            var p = CarTree.tb.tree("find", node.pid);
            CarTree.tb.tree("collapseAll", p.target);
            CarTree.tb.tree("select", p.target);
        } else {
            CarTree.tb.tree("collapseAll", node.target);
            $("#carTb").find('.tree-node-selected').removeClass('tree-node-selected');
            $("#searchText3").focus();
            CarTree.set_text_value_position("searchText3", -1);
        }
    };

    CarTree.right = function () {
        var node = CarTree.tb.tree("getSelected");
        if (!node) {
            return;
        }
        if (node.pid == 0) {
            CarTree.tb.tree("expandAll", node.target);
        }
    };

    CarTree.space = function () {
        var node = CarTree.tb.tree("getSelected");
        if (!node) {
            return;
        }
        if (node.checked == true) {//全选
            CarTree.tb.tree("uncheck", node.target);
        } else if (node.checkState == "indeterminate") {//部分选中
            CarTree.tb.tree("uncheck", node.target);
        } else {//未选中
            CarTree.tb.tree("check", node.target);
        }
    };

    CarTree.set_text_value_position = function(obj, spos){
        var tobj = document.getElementById(obj);
        if(spos<0)
            spos = tobj.value.length;
        if(tobj.setSelectionRange){ //兼容火狐,谷歌
            setTimeout(function(){
                        tobj.setSelectionRange(spos, spos);
                        tobj.focus();}
                    ,0);
        }else if(tobj.createTextRange){ //兼容IE
            var rng = tobj.createTextRange();
            rng.move('character', spos);
            rng.select();
        }
    };

    $(function () {
        CarTree.init();
        CarTree.tree();
    });
</script>

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north'" style="margin: 2px 5px;">
        <span class="textbox easyui-fluid searchbox" style="width: 441px;">
            <span
                    class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;">
                <a href="javascript:;"
                   class="textbox-icon searchbox-button"
                   icon-index="0" tabindex="-1"
                   style="width: 26px; height: 28px;" onclick="doSearch()"></a>
            </span>
            <input tabindex="1"
                   id="searchText3" type="text" class="textbox-text validatebox-text" autocomplete="off"
                   autofocus="autofocus"
                   tabindex="" placeholder="请输入车型名称"
                   style="width: 413px; margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px;">
        </span>
    </div>
    <div data-options="region:'center'">
        <ul id="carTb" tabindex="2"></ul>
    </div>
</div>