<!-- 门店编辑 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post" enctype="multipart/form-data">
        <input id="id" type="hidden" name="id" value="${id}">
        <input id="storeOwnerId" type="hidden" name="storeOwnerId" value="">
        <input id="pictureUrlList" type="hidden" name="pictureUrlList" value="${pictureUrlList}">
        <div id="m_tabs" class="easyui-tabs" data-options="plain:true,pill:true">
            <#--注册信息不可编辑-->
        <#if id ??>
        <#else>
            <div title="注册信息" id="registerInfo" style="padding:10px;overflow: hidden;">
                <div class="form-group">
                    <label class="col-sm-2 control-label">注册电话</label>
                    <div class="col-sm-6">
                        <input class="easyui-textbox" id="registerTel" name="tel" style="width: 100%;"
                               data-options="prompt:'手机号'" required="required" validType="telValidate">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">验证码</label>
                    <div class="col-sm-6">
                        <input class="easyui-textbox" id="smsCode" name="captcha" style="width: 100%;"
                               data-options="buttonText:'获取验证码',prompt:'验证码'" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">登录名称</label>
                    <div class="col-sm-6">
                        <input class="easyui-textbox" name="uname" style="width: 100%;"
                               data-options="prompt:'登录名称',max:50" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">密码
                    </label>
                    <div class="col-sm-6">
                        <input type="password" class="easyui-textbox" id="password" name="password" style="width: 100%;"
                               data-options="prompt:'密码：6-12位数字加字母'" required="required" validType="pwdValidate['^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$']">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">确认密码</label>
                    <div class="col-sm-6">
                        <input type="password" class="easyui-textbox" id="passwordSecond" style="width: 100%;"
                               data-options="prompt:'再次输入密码'" required="required" validType="equals('#password')">
                    </div>
                </div>
            </div>
        </#if>
            <div title="店铺信息" id="storeInfo" style="padding:10px;overflow: hidden;">
                <div class="form-group">
                    <label class="col-sm-2 control-label">店铺名称</label>
                    <div class="col-sm-6">
                        <input class="easyui-textbox" name="name" style="width: 100%;"
                               data-options="prompt:'店铺名称',max:50" required="required">
                    </div>
                </div>
                <#if id ??>
                <#else>
                <div class="form-group">
                    <label class="col-sm-2 control-label">店主姓名</label>
                    <div class="col-sm-6">
                        <input class="easyui-textbox" name="username" style="width: 100%;"
                               data-options="prompt:'店主姓名'" required="required">
                    </div>
                </div>
                </#if>
                <div class="form-group">
                    <label class="col-sm-2 control-label">企业名称</label>
                    <div class="col-sm-6">
                        <input class="easyui-textbox" name="companyName" style="width: 100%;"
                               data-options="prompt:'公司名称'" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">办公电话</label>
                    <div class="col-sm-6">
                        <input class="easyui-textbox" name="workTel" style="width: 100%;"
                               data-options="prompt:'办公电话'" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">客户经理</label>
                    <div class="col-sm-6">
                        <input class="easyui-textbox" name="customMgr" style="width: 100%;"
                               data-options="prompt:'客户经理身份ID'">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">店铺地址</label>
                    <div class="col-sm-6">
                        <input class="easyui-textbox" name="addr" style="width: 100%;"
                               data-options="prompt:'店铺具体地址'">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">经营范围</label>
                    <div class="col-sm-6">
                        <input class="easyui-tree" id="mgrScope" name="mgrScope" url="" checkbox="true" style="width: 100%;"
                               data-options="prompt:'经营范围',editable:false">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">经营品牌</label>
                    <div class="col-sm-6">
                        <input class="easyui-combogrid" id="mgrBrands" name="mgrBrands" style="width: 100%;"
                               data-options="prompt:'经营品牌',editable:false,multiple: true">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">店铺说明</label>
                    <div class="col-sm-6">
                        <input class="easyui-textbox" id="comment" name="comment" style="width: 100%;"
                               data-options="prompt:'店铺说明',editable:false">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">优惠政策</label>
                    <div class="col-sm-6">
                        <input class="easyui-textbox" id="preferential" name="preferential" style="width: 100%;"
                               data-options="prompt:'优惠政策',editable:false">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">店铺人数</label>
                    <div class="col-sm-6">
                        <select class="easyui-combobox" id="personNum" name="personNum" style="width: 100%;"
                               data-options="prompt:'店铺人数',editable:false">
                            <option value="0-10人">0-10人</option>
                            <option value="10-20人">10-20人</option>
                            <option value="20-30人">20-30人</option>
                            <option value="30-40人">30-40人</option>
                            <option value="40-50人">40-50人</option>
                            <option value="50-100人">50-100人</option>
                            </select>
                    </div>
                </div>
            </div>
        <#--<#if id ??>-->
        <#--<#else>-->
            <div title="企业照片" id="companyPicture" style="padding:10px;overflow: hidden;">
                <div class="form-group">
                    <label class="col-sm-2 control-label">上传图片</label>
                    <div class="col-sm-6">
                        <input class="easyui-filebox" id="files" name="files" style="width: 100%;"
                               data-options="prompt:'最多5张，支持格式（JPG, JPEG, PNG, GIF）',multiple: true">
                    </div>
                </div>
                <div class="form-group">
                    <img src="" alt="">
                </div>
            </div>
        <#--</#if>-->
        </div>

    </form>
</div>
