<!-- w_store.ftl 门店 -->
<#include "../../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'100px'">
        <div id="wStore_btn" class="datagrid-toolbar">

        </div>
        <form id="wStore_form" class="form-inline search-form" style="float: left" >
            <div class="form-group">
                <label>店铺拥有者</label>
                <input class="easyui-textbox" data-options="" disabled="disabled" value="${storeOwner.username}">
                <label>拥有者电话</label>
                <input class="easyui-textbox" data-options="" disabled="disabled" value="${storeOwner.mobile}">
                <label>账号状态</label>
                <input class="easyui-textbox" data-options="" disabled="disabled" value="${storeOwner.statusName}">
                <label>搜索内容</label>
                <input name="storeOwnerId" type="hidden" class="easyui-textbox" data-options="" value="${storeOwner.id}">
                <input name="name" class="easyui-textbox" data-options="prompt:'店铺名称/手机号'">
            </div>
        </form>
        <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-top:10px;margin-left:10px;" >查询</button><br>
    </div>
    <div data-options="region:'center'">
        <table id="wStore_table"></table>
    </div>
</div>

<#include "../../common/lib/jslib.ftl">

<script src="/bizjs/tticarStore.js"></script>