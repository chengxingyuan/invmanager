<!-- goods_order.ftl 产品订单表 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <div class="form-group">
            <label class="col-sm-2 control-label">手机号</label>
            <div class="col-sm-6">
                <input class="easyui-textbox" id="registerTel" name="phoneNum" style="width: 100%;"
                       data-options="prompt:'天天爱车登录的手机号'" autofocus="autofocus" required="required" validType="telValidate">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">密码</label>
            <div class="col-sm-6">
                <input type="password" class="easyui-textbox" id="password" name="password" style="width: 100%;"
                       required="required" data-options="validType:'length[4,20]'" >
            </div>
        </div>

    </form>
</div>
