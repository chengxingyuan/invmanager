<!-- goods.ftl 产品表 -->
<#include "../../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div id="goods_btn" class="datagrid-toolbar">

    </div>
    <div data-options="region:'center'">
        <table id="goods_table"></table>
    </div>
</div>

<#include "../../common/lib/jslib.ftl">

<script src="/bizjs/goods.js"></script>