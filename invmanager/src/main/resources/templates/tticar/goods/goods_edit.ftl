<!-- goods.ftl 产品表 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">父节点</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="pid" style="width: 100%;"
                        data-options="panelHeight:'auto',readonly:true,editable:false">
                    <option value="${parent.pid!}" selected>${parent.name!}</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">商铺id</label>
            <div class="col-sm-8">
                <select id="storeId" class="easyui-combobox" name="storeId" style="width:100%;"
                        data-options="prompt:'商铺id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">品牌id</label>
            <div class="col-sm-8">
                <select id="brandId" class="easyui-combobox" name="brandId" style="width:100%;"
                        data-options="prompt:'品牌id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">分类id</label>
            <div class="col-sm-8">
                <select id="categoryId" class="easyui-combobox" name="categoryId" style="width:100%;"
                        data-options="prompt:'分类id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">店铺分类id</label>
            <div class="col-sm-8">
                <select id="storeCategoryId" class="easyui-combobox" name="storeCategoryId" style="width:100%;"
                        data-options="prompt:'店铺分类id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">产品名称</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'产品名称'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">产品名称拼音简写</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="nameSpell" style="width: 100%;"
                       data-options="prompt:'产品名称拼音简写'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">游客价格，未登录
80-100</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="price" style="width: 100%;"
                       data-options="prompt:'游客价格，未登录
80-100'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">游客价格，未登录
80-100</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="maxPrice" style="width: 100%;"
                       data-options="prompt:'游客价格，未登录
80-100'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">店铺已登录价格
100-220</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="pricemember" style="width: 100%;"
                       data-options="prompt:'店铺已登录价格
100-220'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">店铺已登录价格
100-220</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="maxPricemember" style="width: 100%;"
                       data-options="prompt:'店铺已登录价格
100-220'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">门店申请供应商会员价格
200-20</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="pricevip" style="width: 100%;"
                       data-options="prompt:'门店申请供应商会员价格
200-20'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">门店申请供应商会员价格
200-20</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="maxPricevip" style="width: 100%;"
                       data-options="prompt:'门店申请供应商会员价格
200-20'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">零售价低</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="pricesell" style="width: 100%;"
                       data-options="prompt:'零售价低'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">零售价高</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="maxPricesell" style="width: 100%;"
                       data-options="prompt:'零售价高'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">库存
货源充足／货源紧张／2000件</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="inventory" style="width: 100%;"
                       data-options="prompt:'库存
货源充足／货源紧张／2000件'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">产地</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="placeOrigin" style="width: 100%;"
                       data-options="prompt:'产地'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">最低购买数量</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="minNum" style="width:100%;"
                        data-options="prompt:'最低购买数量',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">默认是0，表示不限购</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="maxNum" style="width:100%;"
                        data-options="prompt:'默认是0，表示不限购',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">产品备注简单描述</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="memo" style="width: 100%;"
                       data-options="prompt:'产品备注简单描述'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">0 上架 1 下架  2待上架 3 删除</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">上架时间</label>
            <div class="col-sm-8">
                <input class="easyui-datebox" name="uptime" style="width: 100%;"
                       data-options="prompt:'上架时间',editable:false">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">下架时间</label>
            <div class="col-sm-8">
                <input class="easyui-datebox" name="downtime" style="width: 100%;"
                       data-options="prompt:'下架时间',editable:false">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">规格配置表
颜色 ：</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="standardCfg" style="width: 100%;"
                       data-options="prompt:'规格配置表
颜色 ：'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">图文详情</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="description" style="width: 100%;"
                       data-options="prompt:'图文详情'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">代运营产品id</label>
            <div class="col-sm-8">
                <select id="refGoodsId" class="easyui-combobox" name="refGoodsId" style="width:100%;"
                        data-options="prompt:'代运营产品id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">运费</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="fee" style="width: 100%;"
                       data-options="prompt:'运费'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">置顶排序</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="sort" style="width:100%;"
                        data-options="prompt:'置顶排序',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">创建时间</label>
            <div class="col-sm-8">
                <input class="easyui-datebox" name="createtime" style="width: 100%;"
                       data-options="prompt:'创建时间',editable:false">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">修改时间</label>
            <div class="col-sm-8">
                <input class="easyui-datebox" name="modifytime" style="width: 100%;"
                       data-options="prompt:'修改时间',editable:false">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">搜索关键字</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="keyWords" style="width: 100%;"
                       data-options="prompt:'搜索关键字'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">是否启用老客户价（0是1否）</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="isOlder" style="width:100%;"
                        data-options="prompt:'是否启用老客户价（0是1否）',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">视频地址</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="video" style="width: 100%;"
                       data-options="prompt:'视频地址'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">时长</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="videoDuration" style="width: 100%;"
                       data-options="prompt:'时长'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">商品编号</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="goodNo" style="width: 100%;"
                       data-options="prompt:'商品编号'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">是否自动接单 0是 1否（手动接单）</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="isAutoReceipt" style="width:100%;"
                        data-options="prompt:'是否自动接单 0是 1否（手动接单）',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">商品来源 默认0app供应商 1总后台 2供应商后台</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="upSource" style="width:100%;"
                        data-options="prompt:'商品来源 默认0app供应商 1总后台 2供应商后台',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">橱窗（0 上 1 下）</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="showcase" style="width:100%;"
                        data-options="prompt:'橱窗（0 上 1 下）',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">上下橱窗操作时间</label>
            <div class="col-sm-8">
                <input class="easyui-datebox" name="showcaseTime" style="width: 100%;"
                       data-options="prompt:'上下橱窗操作时间',editable:false">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">推荐状态，0：不推荐，1：优先推荐，NULL和其他为正常推荐</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="recStatus" style="width:100%;"
                        data-options="prompt:'推荐状态，0：不推荐，1：优先推荐，NULL和其他为正常推荐',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">v2价格</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="pricevip2" style="width: 100%;"
                       data-options="prompt:'v2价格'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">v3价格</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="pricevip3" style="width: 100%;"
                       data-options="prompt:'v3价格'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">0 免运费 1有运费  2运费到付</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="feeType" style="width:100%;"
                        data-options="prompt:'0 免运费 1有运费  2运费到付',min:0">
                </input>
            </div>
        </div>
    </form>
</div>
