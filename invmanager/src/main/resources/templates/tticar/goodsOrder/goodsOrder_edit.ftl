<!-- goods_order.ftl 产品订单表 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">订单编号</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="orderCode" style="width: 100%;"
                       data-options="prompt:'订单编号'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">门店id</label>
            <div class="col-sm-8">
                <select id="storeId" class="easyui-combobox" name="storeId" style="width:100%;"
                        data-options="prompt:'门店id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">供应商id</label>
            <div class="col-sm-8">
                <select id="sellStoreId" class="easyui-combobox" name="sellStoreId" style="width:100%;"
                        data-options="prompt:'供应商id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">收货人</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="recName" style="width: 100%;"
                       data-options="prompt:'收货人'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">收货人电话</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="recTel" style="width: 100%;"
                       data-options="prompt:'收货人电话'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">recProvinceName</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="recProvinceName" style="width: 100%;"
                       data-options="prompt:'recProvinceName'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">recCityName</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="recCityName" style="width: 100%;"
                       data-options="prompt:'recCityName'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">recAreaName</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="recAreaName" style="width: 100%;"
                       data-options="prompt:'recAreaName'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">详细地址</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="recAddr" style="width: 100%;"
                       data-options="prompt:'详细地址'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">邮编</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="recPostCode" style="width: 100%;"
                       data-options="prompt:'邮编'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">总金额</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="totalMoney" style="width: 100%;"
                       data-options="prompt:'总金额'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">总运费</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="totalFee" style="width: 100%;"
                       data-options="prompt:'总运费'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">总数量</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="totalCount" style="width:100%;"
                        data-options="prompt:'总数量',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">配送方式 字典表id</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="sendType" style="width:100%;"
                        data-options="prompt:'配送方式 字典表id',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">买家留言</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="buyMsg" style="width: 100%;"
                       data-options="prompt:'买家留言'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">卖家留言</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="sellMsg" style="width: 100%;"
                       data-options="prompt:'卖家留言'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">平台留言</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="platformMsg" style="width: 100%;"
                       data-options="prompt:'平台留言'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">支付方式 字典表id</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="payType" style="width:100%;"
                        data-options="prompt:'支付方式 字典表id',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">订单状态</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">是否删除：0 未删除 1 已删除</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="isDel" style="width:100%;"
                        data-options="prompt:'是否删除：0 未删除 1 已删除',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">订单生成时间</label>
            <div class="col-sm-8">
                <input class="easyui-datebox" name="createtime" style="width: 100%;"
                       data-options="prompt:'订单生成时间',editable:false">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">updatetime</label>
            <div class="col-sm-8">
                <input class="easyui-datebox" name="updatetime" style="width: 100%;"
                       data-options="prompt:'updatetime',editable:false">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">支付来源：0：支付宝 1：微信  10账户余额 11账户余额+支付宝 12账户余额+微信</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="payOrigin" style="width:100%;"
                        data-options="prompt:'支付来源：0：支付宝 1：微信  10账户余额 11账户余额+支付宝 12账户余额+微信',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">物流处理状态,0:新增订单，1：供应商已处理，2：物流已处理</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="deliveryStatus" style="width:100%;"
                        data-options="prompt:'物流处理状态,0:新增订单，1：供应商已处理，2：物流已处理',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">是否结束，并把收益转到可提现余额
默认0 已转1</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="isFinish" style="width:100%;"
                        data-options="prompt:'是否结束，并把收益转到可提现余额
默认0 已转1',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">供应商优惠</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="discount" style="width: 100%;"
                       data-options="prompt:'供应商优惠'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">是否是加急订单  0 默认不是  1是</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="isUrgent" style="width:100%;"
                        data-options="prompt:'是否是加急订单  0 默认不是  1是',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">优惠券优惠</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="reduction" style="width: 100%;"
                       data-options="prompt:'优惠券优惠'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">支付方式名称</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="payTypeName" style="width: 100%;"
                       data-options="prompt:'支付方式名称'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">发货方式名称</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="sendTypeName" style="width: 100%;"
                       data-options="prompt:'发货方式名称'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">realRefundMoney</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="realRefundMoney" style="width: 100%;"
                       data-options="prompt:'realRefundMoney'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">实付金额</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="realPayMoney" style="width: 100%;"
                       data-options="prompt:'实付金额'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">积分优惠</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="intergalDiscount" style="width: 100%;"
                       data-options="prompt:'积分优惠'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">平台金额</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="platformMoney" style="width: 100%;"
                       data-options="prompt:'平台金额'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">加急产生费用</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="urgentMoney" style="width: 100%;"
                       data-options="prompt:'加急产生费用'">
            </div>
        </div>
    </form>
</div>
