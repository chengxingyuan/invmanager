<!-- goods_order.ftl 产品订单表 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">拒绝理由</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="reason" style="width: 100%;"
                       data-options="prompt:'拒绝理由'">
                <input type="hidden" name="orderId"  value="${goodsOrder.orderCode}"/>
                <input type="hidden" name="status"  value="12"/>
            </div>
        </div>

    </form>
</div>
