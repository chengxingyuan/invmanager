
<#include "./../../common/lib/csslib.ftl">
<div class="easyui-layout" data-options="fit:true">

    <form  id="placeOrderForm" class="form-horizontal mform" method="post" >
        <input type="hidden" id="placeOrderMessage" name="placeOrderMessage"/>
    <div data-options="region:'center'">
        <br>
        <button type="button" id="saveButton" style="border-radius:3px;color:white;border: 0px;background-color:#0FA2FF;height: 20px;width: 60px;float: left;margin-left: 20px;font-size: 12px">保存</button>&nbsp;
        <button type="button" id="resetButton" style="border-radius:3px;color:white;border: 0px;background-color:#FF0000;height: 20px;width: 60px;float: left;margin-left: 10px;font-size: 12px">重置</button>
        <br>
        <br>
        <label style="font-size: 10px;color: #e4e4e4;margin-left: 20px" > ————  代下单店铺  ————————————————————————————————————————————————————————————————————————————————————————</label>
        <br>
        <label style="margin-left: 20px;color: #999999 ">选择店铺</label>
        <select id="ttstoreId" name="ttstoreId" style="width:30%;height: 25px"
                        data-options="prompt:'请选择下单店铺',editable:false">
        </select>
        <br>

        <br>
        <label style="font-size: 10px;color: #e4e4e4;margin-left: 20px" > ————  客户信息  —————————————————————————————————————————————————————————————————————————————————————————</label>
        <br>
        <label style="margin-left: 20px;color: #999999">选择客户</label>
        <select id="custom" class="easyui-combobox"  name="custom" style="width:60%;height: 25px"
                <#--data-options="prompt:'请选择客户信息',editable:true"-->>
        </select>
        <br>
        <br>
        <label style="margin-left: 20px;color: #999999">收货地址</label>
        <select id="customAddr" class="easyui-combobox" name="customAddr" style="width:60%;height: 25px"
                data-options="prompt:'请选择收货信息',editable:false">
        </select>

        <br>
        <br>
        <label style="font-size: 10px;color: #e4e4e4;margin-left: 20px" > ————  商品信息  —————————————————————————————————————————————————————————————————————————————————————————</label>
        <table id="placeOrderGoods_table" style="height:220px;width: 100%"></table>
        <br>
        <br>

        <label style="margin-left: 20px;color: #666">应付总额：￥</label>
        <input id="totalFee" name="totalFee" class="easyui-numberbox"
               data-options="precision:2,prompt:'商品总额'" value="0" readonly style="height: 25px">
        <#--<label style="margin-left: 10px;color: #666">应付总额：￥</label>-->
        <#--<input id="needPay" name="needPay" class="easyui-numberbox"-->
               <#--data-options="precision:2,prompt:'应付总额'" value="0" readonly style="height: 25px">-->
        <label style="margin-left: 10px;color: #666">商品总数：</label>
        <input id="totalNum" name="totalNum" class="easyui-numberbox" value="0" readonly  style="height: 25px"/>

        <#--<label style="margin-left: 20px;color: #666">优惠总额：￥</label>-->
        <#--<input id="totalDiscount" name="totalDiscount" class="easyui-numberbox"-->
               <#--data-options="precision:2,prompt:'0.00'" value="0" readonly style="height: 25px">-->
        <#--<label style="margin-left: 10px;color: #666">总计运费：￥</label>-->

        <#--<input id="totalTransportFee" name="totalTransportFee" class="easyui-numberbox"-->
               <#--data-options="precision:2,prompt:'总运费'" value="0" readonly style="height: 25px">-->
        <#--<br>-->

        <label style="margin-left: 20px;color: #666">留言信息：</label>
        <input id="message" name="message" class="easyui-textbox" style="height: 25px;width: 300px">
        <input type="hidden" name="rows" id="rows"/>
        <input type="hidden"  id="goodsAndSkusId"/>
    </div>


        <#--<div data-options="region:'center'">-->
            <#--<label style="font-size: 10px;color: #999999;margin-left: 20px" > ————  商品信息  —————————————</label>-->
            <#--<table id="placeOrderGoods_table"></table>-->
        <#--</div>-->



    </form>
    </div>


    <#include "./../../common/lib/jslib.ftl">
    <script src="/bizjs/goodsOrderPlaceOrder.js?t=2"></script>
