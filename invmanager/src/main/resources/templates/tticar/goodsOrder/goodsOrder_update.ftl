<!-- 销售单详情 -->
<#include "./../../common/lib/csslib.ftl">
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <div class="easyui-layout" data-options="fit:true">
            <input type="hidden" id="orderStatus" name="orderStatus" value="${purchase.orderStatus}"/>

            <div data-options="region:'north',height:'auto'" >
                <br>
                <!--startprint-->
            <#--<form method="post" class="form-inline search-form" style="float: left;">-->

                    <div class="form-group">
                        &nbsp;&nbsp;<label>订单号:</label>

                        ${goodsOrder.orderCode}
                        <label>门店/客户:</label>
                        ${goodsOrder.storeId}
                        <label>下单时间:</label>
                        ${createTime}
                        <br>
                        <br>
                        &nbsp;&nbsp;<label>收货人:</label>
                        ${goodsOrder.storeId}
                        <label>收货人电话:</label>
                        ${goodsOrder.storeId}

                    </div>
                    <br>

                <#--</form>-->


            <br>


            </div> <!--打印区域结束-->

                <div data-options="region:'center'">
                    <table id="pPurchaseDetail_table"  style="height:'auto';width: 100%" ></table>
                    <br>
                    <form method="post" class="form-inline search-form" style="float: left;">
                    <div class="form-group">
                        <input type="hidden" name="orderCode" id="orderCode" value="${goodsOrder.orderCode}"/>
                        <input type="hidden" name="sellStoreId" id="sellStoreId" value="${goodsOrder.sellStoreId}"/>
                        &nbsp;&nbsp;<label>商品总数:</label>
                        ${goodsOrder.totalCount}件 &nbsp;&nbsp;&nbsp;&nbsp;
                        <label>商品总价:</label>
                        ${goodsOrder.totalMoney}元 &nbsp;&nbsp;&nbsp;&nbsp;
                        <label>优惠:</label>
                        <input id="changeMoney" name="changeMoney" class="easyui-numberbox"  data-options="precision:2"/>元
                        <br>

                        &nbsp;&nbsp;<input type="radio" id="free" name="transportFee" checked="checked" value="0">免邮 &nbsp;&nbsp;
                        <input type="radio" id="notFree" name="transportFee" value="1">运费
                        <input id="fee" name="totalFee" type="text" onKeyUp="this.value=this.value.replace(/[^\d\.]/g,'')" style="width: 60px;height: 30px"/>

                    </div>

                    </form>
                    <br>

                </div>
          </div>

        </div>
    </div>


<#include "./../../common/lib/jslib.ftl">
    <script src="/bizjs/goodsOrderUpdate.js?t=2"></script>
<script type="text/javascript">
    $(function(){
        showCont();
        $("input[name=transportFee]").click(function(){

            showCont();
        });
    });
    function showCont(){
        switch($("input[name=transportFee]:checked").attr("id")){
            case "free":

                $("#fee").attr("readOnly","true");
//                document.getElementById("fee").readOnly=readOnly;
                $("#fee").val(0)
                break;
            case "notFree":
                $("#fee").attr("readOnly",false);
                $("#fee").val('');
                $("#fee").focus();
                break;
            default:
                break;
        }
    }
</script>