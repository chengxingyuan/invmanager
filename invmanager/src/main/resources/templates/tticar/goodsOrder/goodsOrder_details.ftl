
<#include "./../../common/lib/csslib.ftl">
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <div class="easyui-layout" data-options="fit:true">

            <div data-options="region:'north',height:'auto'" >
                <br>
                <!--startprint-->
            <#--<form method="post" class="form-inline search-form" style="float: left;">-->

                <div class="form-group" ">
                    &nbsp;&nbsp;<label>订单号:</label>
                    <input type="hidden" id="orderCode" value="${goodsOrder.orderCode}"/>
                ${goodsOrder.orderCode}
                    <label>门店/客户:</label>
                ${storeName}
                    <label>下单时间:</label>
                ${createTime}
                    <br>
                    <br>
                    &nbsp;&nbsp;<label>收货人:</label>
                ${goodsOrder.recName}
                    <label>收货人电话:</label>
                ${goodsOrder.recTel}

                </div>

            </div> <!--打印区域结束-->

            <div data-options="region:'center'">
                <table id="pPurchaseDetail_table"  style="height:'auto';width: 100%" ></table>
                <br>
                <form method="post" class="form-inline search-form" style="float: left;">
                    <div class="form-group">
                        &nbsp;&nbsp;<label>商品总数:</label>${goodsOrder.totalCount}件 &nbsp;&nbsp;&nbsp;&nbsp;
                        <label>商品总价:</label>${totalMoney}元 &nbsp;&nbsp;&nbsp;&nbsp;
                        <label>实付金额:</label>${realPayMoney}元&nbsp;&nbsp;&nbsp;&nbsp;
                        <br>

                        &nbsp;&nbsp;<label>积分优惠:</label>${goodsOrder.intergalDiscount}元&nbsp;&nbsp;&nbsp;&nbsp;

                        <label>商家优惠:</label>${goodsOrder.discount}元&nbsp;&nbsp;&nbsp;&nbsp;

                        <label>优惠券优惠:</label>${goodsOrder.reduction}元&nbsp;&nbsp;&nbsp;&nbsp;

                        <br>

                        &nbsp;&nbsp;<label>配送方式:</label>${goodsOrder.sendTypeName}&nbsp;&nbsp;&nbsp;&nbsp;

                        <label>指定物流:</label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>邮费:</label>${goodsOrder.totalFee}元&nbsp;&nbsp;&nbsp;&nbsp;

                        <br>
                        &nbsp;&nbsp;<label>收&nbsp;货&nbsp;人:</label>${goodsOrder.recName}&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>收货人电话:</label>${goodsOrder.recTel}&nbsp;&nbsp;&nbsp;&nbsp;
                        <br>

                        &nbsp;&nbsp;<label>收货地址:</label>${goodsOrder.recProvinceName}${goodsOrder.recCityName}${goodsOrder.recAreaName}${goodsOrder.recAddr}&nbsp;&nbsp;&nbsp;&nbsp; <br>
                        &nbsp;&nbsp;<label>买家留言:${goodsOrder.buyMsg}</label>&nbsp;&nbsp;&nbsp;&nbsp; <br>
                        &nbsp;&nbsp;<label>卖家备注:${goodsOrder.sellMsg}</label>&nbsp;&nbsp;&nbsp;&nbsp;   <label style="font-size: 13px;color: red">商家实收:</label>${goodsOrder.realPayMoney}元



                    </div>

                </form>
                <br>

            </div>



        </div>

    </div>
</div>


<#include "./../../common/lib/jslib.ftl">
<script src="/bizjs/goodsOrderDetails.js?t=2"></script>
<script type="text/javascript">
    $(function(){
        showCont();
        $("input[name=transportFee]").click(function(){

            showCont();
        });
    });
    function showCont(){
        switch($("input[name=transportFee]:checked").attr("id")){
            case "free":

                $("#fee").attr("readOnly","true");
//                document.getElementById("fee").readOnly=readOnly;
                $("#fee").val(0)
                break;
            case "notFree":
                $("#fee").attr("readOnly",false);
                break;
            default:
                break;
        }
    }
</script>