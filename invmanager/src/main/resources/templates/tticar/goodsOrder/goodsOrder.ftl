<!-- goods_order.ftl 产品订单表 -->
<#include "../../common/lib/csslib.ftl">
<input type="hidden" id="hidSearchText" value="${searchText}" />
<input type="hidden" id="hidStartTime" value="${startTime}" />
<input type="hidden" id="hidEndTime" value="${endTime}" />

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'west',width:'12%',split:true" title="店铺列表">
        <ul id="wstore_list" class="easyui-datalist"></ul>
    </div>
    <div data-options="region:'center'">
        <div class="easyui-layout" data-options="fit:true">
            <div data-options="region:'north',height:'100px'">
                <div id="goodsOrder_btn" class="datagrid-toolbar">

                </div>
                <form id="goodsOrder_form" class="form-inline search-form" style="float: left">
                    <div class="form-group">
                        <label>订单号/门店</label>
                        <input id="searchText" name="name" class="easyui-textbox" data-options="prompt:'订单号/门店/客户名称'">
                    </div>
                    <div class="form-group">
                        <label>订单状态</label>
                        <input name="orderStatus" class="easyui-combobox"
                               data-options="url: '/sysDict/tticar_order_status',panelHeight:'auto',valueField:'id',textField:'text',editable:false">
                    </div>
                    <div class="form-group">
                        <label>日期</label>
                        <input class="easyui-datebox" id="startTime" style="width: 100px;" name="startTime" data-options="sharedCalendar:'#cc'">-
                        <input class="easyui-datebox" id="endTime"  style="width: 100px;" name="endTime" data-options="sharedCalendar:'#cc'">
                        <div id="cc" class="easyui-calendar"></div>
                    </div>
                    <input type="hidden" id="sellStoreId" name="sellStoreId">
                </form>
                <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询</button>
            </div>
            <div data-options="region:'center'">
                <table id="goodsOrder_table"></table>
            </div>
        </div>
    </div>
</div>

<#include "../../common/lib/jslib.ftl">
<script src="/public/easyui/ex/datagrid-detailview.js"></script>
<script src="/bizjs/goodsOrder.js"></script>