<!-- goods_production.ftl 商品-产品表 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">父节点</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="pid" style="width: 100%;"
                        data-options="panelHeight:'auto',readonly:true,editable:false">
                    <option value="${parent.pid!}" selected>${parent.name!}</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">goodId</label>
            <div class="col-sm-8">
                <select id="goodId" class="easyui-combobox" name="goodId" style="width:100%;"
                        data-options="prompt:'goodId',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">skuId</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="skuId" style="width: 100%;"
                       data-options="prompt:'skuId'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">name</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'name'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">price</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="price" style="width: 100%;"
                       data-options="prompt:'price'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">pricemember</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="pricemember" style="width: 100%;"
                       data-options="prompt:'pricemember'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">pricevip</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="pricevip" style="width: 100%;"
                       data-options="prompt:'pricevip'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">零售价</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="pricesell" style="width: 100%;"
                       data-options="prompt:'零售价'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">inventory</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="inventory" style="width: 100%;"
                       data-options="prompt:'inventory'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">创建时间</label>
            <div class="col-sm-8">
                <input class="easyui-datebox" name="createtime" style="width: 100%;"
                       data-options="prompt:'创建时间',editable:false">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">修改时间</label>
            <div class="col-sm-8">
                <input class="easyui-datebox" name="modifytime" style="width: 100%;"
                       data-options="prompt:'修改时间',editable:false">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">规格图片，图片地址</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="skuPic" style="width: 100%;"
                       data-options="prompt:'规格图片，图片地址'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">v2价格</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="pricevip2" style="width: 100%;"
                       data-options="prompt:'v2价格'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">v3价格</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="pricevip3" style="width: 100%;"
                       data-options="prompt:'v3价格'">
            </div>
        </div>
    </form>
</div>
