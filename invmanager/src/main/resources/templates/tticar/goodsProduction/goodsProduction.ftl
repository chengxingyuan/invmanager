<!-- goods_production.ftl 商品-产品表 -->
<#include "../../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div id="goodsProduction_btn" class="datagrid-toolbar">

    </div>
    <div data-options="region:'center'">
        <table id="goodsProduction_table"></table>
    </div>
</div>

<#include "../../common/lib/jslib.ftl">

<script src="/bizjs/goodsProduction.js"></script>