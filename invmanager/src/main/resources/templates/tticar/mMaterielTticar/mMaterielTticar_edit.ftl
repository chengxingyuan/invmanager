<!-- m_materiel_tticar.ftl 天天爱车产品 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id">
        <input type="hidden" name="ttgoodsId">

        <div class="form-group">
            <label class="col-sm-2 control-label" id="selectInv">选择存货</label>
            <div class="col-sm-8">
                <select id="materielId" class="easyui-combogrid" name="materielId" style="width:100%;"
                        data-options="prompt:'请选择存货',editable:false,required:true">
                </select>
            </div>
        </div>
        <input type="hidden" id="materielIdForSubmit" name="materielIdForSubmit">
        <div class="form-group">
            <label class="col-sm-2 control-label">选择上传店铺</label>
            <div class="col-sm-8">
                <select id="ttstoreId" class="easyui-combobox" name="ttstoreId" style="width:100%;"
                        data-options="prompt:'请选择上传店铺',editable:false,required:true">
                </select>
            </div>
        </div>

        <div class="easyui-tabs" data-options="plain:true,pill:true">
            <div title="基本信息" style="padding:10px;overflow: hidden;">
                <div class="form-group">
                    <label class="col-sm-1 control-label">产品名称</label>
                    <div class="col-sm-7">
                        <input id="name" class="easyui-textbox" name="name" style="width:100%;"
                               data-options="prompt:'产品名称',reuqired:true"/>
                    </div>
                    <#--<label class="col-sm-1 control-label">搜索关键字</label>-->
                    <#--<div class="col-sm-2">-->
                        <#--<input class="easyui-textbox" name="keywords" style="width:100%;"-->
                               <#--data-options="prompt:'搜索关键字'"/>-->
                    <#--</div>-->
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">分类</label>
                    <div class="col-sm-3">
                        <select id="categoryId" class="easyui-combotreegrid" name="categoryId" style="width: 100%;"
                                data-options="prompt:'分类',panelMinWidth:350">
                        </select>
                    </div>
                    <label class="col-sm-1 control-label">品牌</label>
                    <div class="col-sm-3">
                        <select id="brandId" class="easyui-combogrid" name="brandId" style="width: 100%;"
                                data-options="prompt:'品牌',editable:false,panelMinWidth:350"></select>
                    </div>
                    <label class="col-sm-1 control-label">产地</label>
                    <div class="col-sm-2">
                        <input class="easyui-textbox" name="placeOrigin" style="width: 100%;"
                               data-options="prompt:'产地'">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">最低购买数量</label>
                    <div class="col-sm-3">
                        <input class="easyui-numberbox" name="minNum" style="width: 100%;"
                               data-options="prompt:'最低购买数量'">
                    </div>
                    <label class="col-sm-1 control-label">最高限购量</label>
                    <div class="col-sm-3">
                        <input class="easyui-numberbox" name="maxNum" style="width: 100%;"
                               data-options="prompt:'最高限购量'">
                    </div>
                    <label class="col-sm-1 control-label">状态</label>
                    <div class="col-sm-2">
                        <select class="easyui-combobox" name="status" style="width: 100%;"
                                data-options="prompt:'最高限购量',editable:false,panelHeight:'auto'">
                            <option value="0" selected>上架</option>
                            <option value="1">下架</option>
                            <option value="2">待下架</option>
                            <option value="3">删除</option>
                        </select>
                    </div>
                </div>
            </div>
            <div title="SKU信息" style="padding:10px;overflow: hidden;">
                <table id="sukTable"></table>
            </div>
            <div title="图文详情" style="padding:10px;overflow: hidden;">
                <div id="pic_desc_container" class="form-group">
                    <div class="col-sm-2 col-sm-offset-2 list-group">
                        <a href="javascript:;" class="list-group-item text">+文本</a>
                        <a href="javascript:;" class="list-group-item image">+图片</a>
                    <#--<a href="javascript:;" class="list-group-item video">+视频</a>-->
                    </div>
                    <ul class="col-sm-6 list-group pic-desc">
                        <li class="list-group-item">
                            <div class="panel-tool">
                                <a href="javascript:;" class="icon-edit panel-tool-a"></a>
                                <a href="javascript:;" class="panel-tool-close"></a>
                            </div>
                            <div class="content">请在次编辑内容...</div>
                        </li>
                    </ul>
                </div>
            </div>
            <div title="轮播图" style="padding:10px;overflow: hidden;">
                <div class="file-loading">
                    <input id="pictrues" type="file" multiple>
                </div>
            </div>
        </div>
    </form>
</div>
