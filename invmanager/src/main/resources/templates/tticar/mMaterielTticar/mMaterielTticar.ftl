<!-- m_materiel_tticar.ftl 天天爱车产品 -->
<#include "../../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'west',width:'12%',split:true" title="店铺列表">
        <ul id="wstore_list" class="easyui-datalist"></ul>
    </div>
    <div data-options="region:'center'">
        <div class="easyui-layout" data-options="fit:true">
            <div data-options="region:'north',height:'100px'">
                <div id="mMaterielTticar_btn" class="datagrid-toolbar">

                </div>
                <form id="mMaterielTticar_form" class="form-inline search-form" style="float: left">
                    <input type="hidden" id="queryStoreId" name="queryStoreId">
                    <div class="form-group">
                        <label>商品名称</label>
                        <input name="name" class="easyui-textbox" data-options="prompt:'商品名称'">
                    </div>
                    <#--<div class="form-group">-->
                        <#--<label>商品编码</label>-->
                        <#--<input name="code" class="easyui-textbox" data-options="prompt:'天天爱车平台商品编码'">-->
                    <#--</div>-->
                    <div class="form-group">
                        <label>关联状态</label>
                        <input name="relativeStatus" class="easyui-combobox"
                               data-options="url: '/sysDict/is_relative',panelHeight:'auto',valueField:'id',textField:'text',editable:false">
                    </div>
                    <div class="form-group">
                        <label>上架状态</label>
                        <input name="onlineStatus" class="easyui-combobox"
                               data-options="url: '/sysDict/online',panelHeight:'auto',valueField:'id',textField:'text',editable:false">
                    </div>
                </form>
                <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询</button>
            </div>

            <div data-options="region:'center'">
                <table id="mMaterielTticar_table"></table>
            </div>
            <div data-options="region:'south',height:200, title:'关联的仓库存货'">
                <table id="relativeGoods_table"></table>
            </div>

        </div>
    </div>
</div>

<#include "../../common/lib/jslib.ftl">
<script src="/public/easyui/ex/datagrid-detailview.js"></script>
<script src="/bizjs/mMaterielTticar.js"></script>