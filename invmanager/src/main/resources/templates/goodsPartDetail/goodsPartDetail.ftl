<!-- goods_part_detail.ftl 盘点单详情 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'auto'">
        <div id="goodsPartDetail_btn" class="datagrid-toolbar">

        </div>
        <form id="goodsPartDetail_form" class="form-inline search-form">
            <div class="form-group">
                <label>名称</label>
                <input name="name" class="easyui-textbox" data-options="prompt:'名称'">
            </div>
        </form>
    </div>
    <div data-options="region:'center'">
        <table id="goodsPartDetail_table"></table>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/goodsPartDetail.js"></script>