<!-- goods_part_detail.ftl 盘点单详情 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">拆装总单id</label>
            <div class="col-sm-8">
                <select id="partId" class="easyui-combobox" name="partId" style="width:100%;"
                        data-options="prompt:'拆装总单id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">仓库</label>
            <div class="col-sm-8">
                <select id="wearhouseId" class="easyui-combobox" name="wearhouseId" style="width:100%;"
                        data-options="prompt:'仓库',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">库位id</label>
            <div class="col-sm-8">
                <select id="positionId" class="easyui-combobox" name="positionId" style="width:100%;"
                        data-options="prompt:'库位id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">商品skuid</label>
            <div class="col-sm-8">
                <select id="skuId" class="easyui-combobox" name="skuId" style="width:100%;"
                        data-options="prompt:'商品skuid',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">批号</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="batchNumber" style="width: 100%;"
                       data-options="prompt:'批号'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">拆装数量</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="count" style="width:100%;"
                        data-options="prompt:'拆装数量',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">拆装单价</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="amount" style="width: 100%;"
                       data-options="prompt:'拆装单价'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">拆装总额</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="totalAmount" style="width: 100%;"
                       data-options="prompt:'拆装总额'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">操作人姓名</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="username" style="width: 100%;"
                       data-options="prompt:'操作人姓名'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">备注</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="remark" style="width: 100%;"
                       data-options="prompt:'备注'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">是否删除</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
    </form>
</div>
