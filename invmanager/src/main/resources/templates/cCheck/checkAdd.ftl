<!-- 新建入库单 -->
<#include "./../common/lib/csslib.ftl">
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'west', width:'400px'">
        <div class="easyui-layout" data-options="fit:true">
            <div data-options="region:'north',height:'133px', title:'商品选择', collapsible:false">
                <form id="checkAdd_form" class="form-inline search-form">
                    <div class="form-group" style="line-height: 20px;margin-left: 20px;">
                        <input id="houseId" name="houseId" class="easyui-textbox" style="width: 177px">
                    </div>
                    <div class="form-group" style="line-height: 20px;margin-left: 20px;">
                        <input id="cateogryId" name="cateogryId" class="easyui-textbox" style="width: 177px" data-options="prompt:'分类',editable:false">
                    </div>
                    <div class="form-group" style="line-height: 20px;margin-left: 20px;">
                        <input id="searchText" name="searchText" class="easyui-searchbox" data-options="prompt:'名称',searcher:CheckAdd.doSearch" style="width:370px;" />
                    </div>
                </form>
            </div>
            <div data-options="region:'center'">
                <table id="goodsTb" style="width: 100%"></table>
            </div>
        </div>
    </div>
    <div data-options="region:'center'">
        <div id="checkAdd_btn" class="datagrid-toolbar">

        </div>
        <table id="checkAdd_table"></table>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">
<script src="/bizjs/checkAdd.js"></script>