<!-- c_check.ftl 盘点单 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'100px'">
        <div id="cCheck_btn" class="datagrid-toolbar">

        </div>
        <form id="cCheck_form" class="form-inline search-form" style="float: left">
            <input type="hidden" id="checkStatus">
            <div class="form-group">
                <label>单号</label>
                <input name="code" class="easyui-textbox" data-options="">
            </div>
            <div class="form-group" style="line-height: 20px">
                <label>仓库</label>
                <input id="houseId" name="houseId" class="easyui-textbox" style="width: 70px">
            </div>
            <div class="form-group">
                <label>日期</label>
                <input class="easyui-datebox" id="startTime" style="width: 100px;" name="startTime" data-options="sharedCalendar:'#cc'">-
                <input class="easyui-datebox" id="endTime" style="width: 100px;" name="endTime" data-options="sharedCalendar:'#cc'">
                <div id="cc" class="easyui-calendar"></div>
            </div>
        </form>
        <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询</button>
    </div>
    <div data-options="region:'center'">
        <table id="cCheck_table"></table>
    </div>
    <div data-options="region:'south',height:320, title:'盘点商品信息'">
        <div id="checkGoodsBtn" class="datagrid-toolbar" style="display: none"> </div>
        <table id="checkGoods_table"></table>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/cCheck.js"></script>