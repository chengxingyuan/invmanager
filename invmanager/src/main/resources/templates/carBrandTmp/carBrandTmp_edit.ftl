<!-- car_brand_tmp.ftl 车品牌 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">品牌名称</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="brandName" style="width: 100%;"
                       data-options="prompt:'品牌名称'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">子品牌名称</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="subName" style="width: 100%;"
                       data-options="prompt:'子品牌名称'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">系列名称</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="seriesName" style="width: 100%;"
                       data-options="prompt:'系列名称'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">车型名称</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="modelName" style="width: 100%;"
                       data-options="prompt:'车型名称'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">车款名称</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="skuName" style="width: 100%;"
                       data-options="prompt:'车款名称'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">图片地址</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="src" style="width: 100%;"
                       data-options="prompt:'图片地址'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">显示状态</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
    </form>
</div>
