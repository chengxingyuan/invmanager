<!-- 新建销售单 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <form id="myForm" method="post">
        <input type="hidden" id="goodsFrom" name="goodsFrom"/>
        <input type="hidden" id="goodsTo" name="goodsTo"/>

        <div data-options="region:'north',height:95">
            <div id="part_btn" class="datagrid-toolbar">

            </div>
            <div class="form-inline search-form">
                <div class="form-group">
                    <label>日期</label>
                    <input id="orderTime" name="orderTime" tabindex="-1"/>
                </div>
                <div class="form-group" style="line-height: 20px;margin-left: 20px;">
                    <label>仓库</label>
                    <input id="houseId" name="houseId" class="easyui-textbox" style="width: 177px">
                </div>
                <div class="form-group">
                    <label>备注</label>
                    <input id="remark" name="remark" class="easyui-textbox" style="width: 400px">
                </div>
            </div>
        </div>

        <div data-options="region:'center',title:'拆装货品信息'">
            <div class="easyui-layout" data-options="fit:true">
                <div data-options="region:'north',height:'100px'">
                    <table id="part_table" tabindex="-1"></table>
                </div>
                <div data-options="region:'center', title:'拆分成货品'">
                    <table id="part_table2" tabindex="-1"></table>
                </div>
            </div>
        </div>
    </form>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/goodsPartAdd.js?t=4"></script>