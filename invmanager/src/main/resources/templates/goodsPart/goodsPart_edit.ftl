<!-- 新建销售单 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <form id="myForm" method="post">
        <input type="hidden" id="id" name="id" value="${part.id}"/>
        <input type="hidden" id="status" value="${part.status}"/>
        <input type="hidden" id="goodsFrom" name="goodsFrom"/>
        <input type="hidden" id="goodsTo" name="goodsTo"/>

        <div data-options="region:'north',height:135">
            <div id="part_btn" class="datagrid-toolbar">

            </div>
            <div class="form-inline search-form">
                <div class="form-group">
                    <label>日期</label>
                    <input id="orderTime" name="orderTime" class="easyui-datebox" value="${orderTime}" readonly/>
                </div>
                <div class="form-group" style="line-height: 20px;margin-left: 20px;">
                    <label>仓库</label>
                    <input id="houseId" name="houseId" class="easyui-textbox" style="width: 177px" value="${part.wearhouseId}" readonly>
                </div>
                <div class="form-group">
                    <label>状态</label>
                    <select class="easyui-combobox" style="width:177px;" readonly>
                        <option value="0" <#if part.status == 0>selected</#if> >待审核</option>
                        <option value="1" <#if part.status == 1>selected</#if> >已审核</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>备注</label>
                    <input id="remark" name="remark" class="easyui-textbox" style="width: 400px" value="${part.remark}">
                </div><br>
                <div class="form-group">
                    <label>创建人</label>
                    <input id="cname" name="cname" class="easyui-textbox" value="${part.cname}" readonly/>
                </div>
                <div class="form-group" style="margin-left: 20px;">
                    <label>创建日期</label>
                    <input id="ctime" name="ctime" class="easyui-datebox" value="${ctime}" readonly/>
                </div>
                <div class="form-group">
                    <label>审核人</label>
                    <input id="oname" name="oname" class="easyui-textbox" value="${part.oname}" readonly/>
                </div>
                <div class="form-group">
                    <label>审核日期</label>
                    <input id="otime" name="otime" class="easyui-datebox" value="${otime}" readonly/>
                </div>
            </div>
        </div>

        <div data-options="region:'center',title:'拆装货品信息'">
            <div class="easyui-layout" data-options="fit:true">
                <div data-options="region:'north',height:'100px'">
                    <table id="part_table" tabindex="-1"></table>
                </div>
                <div data-options="region:'center', title:'拆分成货品'">
                    <table id="part_table2" tabindex="-1"></table>
                </div>
            </div>
        </div>
    </form>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/goodsPartEdit.js?t=6"></script>