<#include "./../common/lib/csslib.ftl">
<input type="hidden" id="hidSearchText" value="${searchText}" />
<input type="hidden" id="hidStartTime" value="${startTime}" />
<input type="hidden" id="hidEndTime" value="${endTime}" />
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'100px'">
        <div id="goodsPart_btn" class="datagrid-toolbar">

        </div>
        <form id="goodsPart_form" class="form-inline search-form" style="float: left">
            <div class="form-group">
                <label>日期</label>
                <input id="startTime" name="startTime2" style="width: 100px;" class="easyui-datebox">-<input id="endTime" name="endTime2" style="width: 100px;" class="easyui-datebox">
            </div>
            <div class="form-group">
                <label>仓库</label>
                <input id="houseId" name="wearhouseId" class="easyui-textbox" style="width: 177px">
            </div>
            <div class="form-group">
                <label>创建人</label>
                <input id="userName" class="easyui-combobox" name="username" data-options="prompt:'',editable:false,url:'/sysUser/userList',valueField:'username',textField:'username'">
            </div>
        </form>
        <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询</button>
    </div>
    <div data-options="region:'center'">
        <div class="easyui-layout" data-options="fit:true">
            <div data-options="region:'north',height:'60%'">
                <table id="goodsPart_table"></table>
            </div>
            <div data-options="region:'center', title:'商品明细'">
                <table id="goodsPart_table2"></table>
            </div>
        </div>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/goodsPart.js"></script>