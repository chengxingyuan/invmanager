package ${package.ServiceImpl};

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import ${package.Entity}.${entity};
import com.tticar.invmanager.entity.vo.MyPage;
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;
<#if cfg.isTree>
import com.baomidou.mybatisplus.entity.Columns;
</#if>

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * ${table.comment} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}> implements ${table.serviceName} {

    <#if !cfg.isService>
    @Override
    public Page selectPage(${entity} ${table.entityPath}, MyPage<${entity}> page) {
        Wrapper wrapper = new EntityWrapper();
        <#if cfg.isTree>
        Columns columns = Columns.create().column("*")
        .column("(select case count(1) when 0 then 'open' else 'closed' end from ${table.name} t where t.pid=${table.name}.id)", "state");
        wrapper.setSqlSelect(columns);
        wrapper.eq(${entity}.PID, ${table.entityPath}.getId());
        </#if>
        <#--wrapper.orderBy(${entity}.SORT, false);-->
        //wrapper.like(Strings.isNotEmpty(${table.entityPath}.getName()), ${entity}.NAME, ${table.entityPath}.getName());
        return this.selectMapsPage(page, wrapper);
    }
    <#if cfg.isOpt>
    @Override
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            ${entity} ${table.entityPath} = new ${entity}();
            ${table.entityPath}.setId(id);
            ${table.entityPath}.setStatus(1);
            list.add(${table.entityPath});
        }
        this.updateBatchById(list);
    }
    </#if>
    </#if>

}
</#if>
