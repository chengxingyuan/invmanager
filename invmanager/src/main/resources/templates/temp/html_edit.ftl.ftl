<!-- ${table.name}.ftl ${table.comment} -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="${id!}">
    <#if cfg.isTree>
        <div class="form-group">
            <label class="col-sm-2 control-label">父节点</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="pid" style="width: 100%;"
                        data-options="panelHeight:'auto',readonly:true,editable:false">
                    <option value="${r'${parent.pid!}'}" selected>${r'${parent.name!}'}</option>
                </select>
            </div>
        </div>
    </#if>
    <#list table.fields as field>
        <#if field.propertyName!='pid' && field.propertyName!='id' && field.propertyName!='tenantId' && field.propertyName!='ctime' && field.propertyName!='utime'>
        <#if field.comment!=''>
            <#assign _title = field.comment/>
        <#else>
            <#assign _title = field.propertyName/>
        </#if>
        <div class="form-group">
            <label class="col-sm-2 control-label">${_title}</label>
            <div class="col-sm-8">
            <#if field.propertyType=='Double'>
                <input class="easyui-numberbox" name="${field.propertyName!}" style="width: 100%;"
                       data-options="prompt:'${_title}'}'">
            <#elseif field.propertyType=='Long' || field.propertyType=='Integer'>
                <#if field.propertyName=='status'>
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
                <#elseif field.propertyName?ends_with("Id")>
                <select id="${field.propertyName!}" class="easyui-combobox" name="${field.propertyName!}" style="width:100%;"
                        data-options="prompt:'${_title}',editable:false">
                </select>
                <#else>
                <input class="easyui-numberbox" name="${field.propertyName!}" style="width:100%;"
                        data-options="prompt:'${_title}',min:0">
                </input>
                </#if>
            <#elseif field.propertyType=='Date' && field.propertyName!='ctime' && field.propertyName!='utime' >
                <input class="easyui-datebox" name="${field.propertyName!}" style="width: 100%;"
                       data-options="prompt:'${_title}',editable:false">
            <#else>
                <input class="easyui-textbox" name="${field.propertyName!}" style="width: 100%;"
                       data-options="prompt:'${_title}'">
            </#if>
            </div>
        </div>
        </#if>
    </#list>
    </form>
</div>
