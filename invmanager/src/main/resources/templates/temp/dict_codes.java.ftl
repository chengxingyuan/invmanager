package com.tticar.invmanager.entity.vo;

import java.util.Map;

/**
* Created by yandou on 2018/7/1.
*/
public enum DictCodes {

<#list sysDicts as dict>
    ${dict.code}, // ${dict.name!}
</#list>
    ;

    private static Map<String, Integer> map = null;
    public boolean getEditable() {return map.get(this.toString()) == 1;}
    public static Map<String, Integer> getMap() {return map;}
    public static void setMap(Map<String, Integer> map) {DictCodes.map = map;}

    public static DictCodes get(String code) {
        return DictCodes.valueOf(code);
    }
}
