<!-- ${table.name}.ftl ${table.comment} -->
<#assign p>${entity}</#assign>
var ${p} = {
    u: '/${table.entityPath}'
    , uList: '/${table.entityPath}/list'
    , uEdit: '/${table.entityPath}/edit'
    , uDel: '/${table.entityPath}/delete'
};

${p}.dg = function () {
    <#if cfg.isTree>
    var grid = new $yd.TreeGrid(this.dtable);
    <#if cfg.isOpt>
    grid.onDblClickRow = function (row) {
        ${p}._edit(row['id']);
    };
    </#if>
    <#else>
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        ${p}._edit(row['id']);
    };
    </#if>
    <#if !cfg.isSearch>
    grid.toolbar = ${p}.obtn;
    </#if>
    grid.url = ${p}.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        <#list table.fields as field>
        <#if field.propertyName!='pid' && field.propertyName!='id' && field.propertyName!='tenantId' && field.propertyName!='ctime' && field.propertyName!='utime'>
        <#if field.comment!=''>
            <#assign _title = field.comment/>
        <#else>
            <#assign _title = field.propertyName/>
        </#if>
        <#if field.propertyName=='status'>
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        <#else>
        , {field: '${field.propertyName!}', title: '${_title}', width: 80}
        </#if>
        </#if>
        </#list>
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
${p}.dg.getOne = function () {
    var sels = ${p}.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
${p}.dg.getSels = function () {
<#if cfg.isTree>
    return ${p}.dtable.treegrid('getSelections');
<#else>
    return ${p}.dtable.datagrid('getSelections');
</#if>
};
// 处理按钮
${p}.btn = function () {
    var sb = new $yd.SearchBtn($(${p}.obtn));
    <#if cfg.isOpt>
    sb.create('remove', '删除', ${p}.remove);
    sb.create('refresh', '刷新', ${p}.refresh);
    sb.create('edit', '编辑', ${p}.edit);
    sb.create('plus', '新增', ${p}.plus);
    </#if>
    <#if cfg.isSearch>
    sb.create('search', '查询', ${p}.search);
    </#if>
};
<#if cfg.isOpt>
//删除
${p}.remove = function () {
    var sels = ${p}.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(${p}.uDel, {ids: ids}, function () {
            ${p}.refresh();
        });
    })
};
//
${p}.edit = function () {
    var sel = ${p}.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    ${p}._edit(sel['id'])
};
<#if (comboList?size>0) >
// 表单预处理
${p}.preForm = function () {
    <#list comboList as combo>
    new $yd.combobox("${combo.name}").load("/${combo.pre!}/select");
    </#list>
};
</#if>
${p}._edit = function (id) {
    $yd.win.create('编辑${table.comment!}', ${p}.uEdit + '?id=' + id).open(function (that, $win) {
            <#if (comboList?size>0) >
            ${p}.preForm();
            </#if>
            $yd.get(${p}.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(${p}.u).submit(function () {
                that.close();
                ${p}.refresh();
            });
        }
    );
};
//新增
${p}.plus = function () {
    var sel = ${p}.dg.getOne();
    var href = ${p}.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增${table.comment!}', href).open(function (that, $win){
            <#if (comboList?size>0) >
            ${p}.preForm();
            </#if>
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(${p}.u).submit(function () {
                that.close();
                ${p}.refresh();
            });
        }
    );
};
</#if>
//
${p}.refresh = function () {
<#if cfg.isTree>
    ${p}.dtable.treegrid('reload');
<#else>
    ${p}.dtable.datagrid('reload');
</#if>
};
<#if cfg.isSearch>
//搜索
${p}.search = function () {
<#if cfg.isTree>
    ${p}.dtable.treegrid('load', ${p}.sform.serializeObject());
<#else>
    ${p}.dtable.datagrid('load', ${p}.sform.serializeObject());
</#if>
};
</#if>

$(function () {
    ${p}.sform = $('#${table.entityPath}_form');
    ${p}.dtable = $('#${table.entityPath}_table');
    ${p}.obtn = '#${table.entityPath}_btn';

    ${p}.dg(); // 数据表格
    ${p}.btn(); // 初始化按钮
    <#if cfg.isSearch>
    $yd.bindEnter(${p}.search);
    </#if>
});