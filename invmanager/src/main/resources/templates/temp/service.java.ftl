package ${package.Service};

import ${package.Entity}.${entity};
import ${superServiceClassPackage};
<#if !cfg.isService>
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
</#if>

/**
 * <p>
 * ${table.comment} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {

    <#if !cfg.isService>
    Page selectPage(${entity} ${table.entityPath}, MyPage<${entity}> page);

    <#if cfg.isOpt>
    void delete(List<Long> ids);
    </#if>
    </#if>

}
</#if>
