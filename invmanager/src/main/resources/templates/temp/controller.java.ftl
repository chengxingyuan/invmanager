package ${package.Controller};

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.${table.serviceName};
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ${package.Entity}.${entity};
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
<#--import ${superControllerClassPackage};-->
</#if>
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * ${table.comment} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {

    @Autowired
    private ${table.serviceName} ${table.entityPath}Service;

<#if cfg.isOpt>
    @PostMapping
    @ResponseBody
    public MyResponseBody post(${entity} ${table.entityPath}) {
        ${table.entityPath}Service.insertOrUpdate(${table.entityPath});
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        ${table.entityPath}Service.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(${table.entityPath}Service.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(${entity} ${table.entityPath}, ModelMap modelMap) {
<#if cfg.isTree>
        if (java.util.Objects.isNull(${table.entityPath}.getPid())) {
            ${table.entityPath}.setPid(0l);
            ${table.entityPath}.setName("顶级目录");
        }
        modelMap.put("parent", ${table.entityPath});
</#if>
        modelMap.put("id", ${table.entityPath}.getId());
        return "/${table.entityPath}/${table.entityPath}_edit";
    }
</#if>

    @GetMapping("list")
    @ResponseBody
    public Page list(${entity} ${table.entityPath}, MyPage<${entity}> page) {
        return ${table.entityPath}Service.selectPage(${table.entityPath}, page);
    }

    @GetMapping
    public String index() {
        return "/${table.entityPath}/${table.entityPath}";
    }

}
<#else>
public class ${table.controllerName} {

}
</#if>
</#if>
