<!-- ${table.name}.ftl ${table.comment} -->
${csslib!}

<div class="easyui-layout" data-options="fit:true">
<#if cfg.isSearch>
    <div data-options="region:'north',height:'auto'">
        <div id="${table.entityPath}_btn" class="datagrid-toolbar">

        </div>
        <form id="${table.entityPath}_form" class="form-inline search-form">
            <div class="form-group">
                <label>名称</label>
                <input name="name" class="easyui-textbox" data-options="prompt:'名称'">
            </div>
        </form>
    </div>
<#else>
    <div id="${table.entityPath}_btn" class="datagrid-toolbar">

    </div>
</#if>
    <div data-options="region:'center'">
        <table id="${table.entityPath}_table"></table>
    </div>
</div>

${jslib!}

<script src="/bizjs/${table.entityPath}.js"></script>