<!-- sys_logs.ftl 请求日志 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">用户名</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'用户名'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">请求地址</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="ruri" style="width: 100%;"
                       data-options="prompt:'请求地址'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">请求方法</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="rmethod" style="width: 100%;"
                       data-options="prompt:'请求方法'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">请求参数</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="rargs" style="width: 100%;"
                       data-options="prompt:'请求参数'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">请求ip</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="ipAddr" style="width: 100%;"
                       data-options="prompt:'请求ip'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">请求码</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="statuCode" style="width: 100%;"
                       data-options="prompt:'请求码'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">请求标示</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="sessionId" style="width: 100%;"
                       data-options="prompt:'请求标示'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">请求耗时</label>
            <div class="col-sm-8">
                <select class="easyui-numberbox" name="rtime" style="width:100%;"
                        data-options="prompt:'请求耗时',min:0">
                </select>
            </div>
        </div>
    </form>
</div>
