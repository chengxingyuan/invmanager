<!-- sys_pictrue.ftl 图片资源 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">父节点</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="pid" style="width: 100%;"
                        data-options="panelHeight:'auto',readonly:true,editable:false">
                    <option value="${parent.pid!}" selected>${parent.name!}</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">文件夹名称</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'图片名称'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">描述</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="memo" style="width: 100%;"
                       data-options="prompt:'描述'">
            </div>
        </div>
    </form>
</div>
