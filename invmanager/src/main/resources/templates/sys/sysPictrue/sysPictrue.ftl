<!-- sys_pictrue.ftl 图片资源 -->
<#include "../../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div id="sysPictrue_btn" class="datagrid-toolbar">

    </div>
    <div id="picture_table_tb">
        <input id="picture_input" type="file" multiple>
    </div>

    <div data-options="region:'west',width:'18%',border:false,split:true">
        <table id="sysPictrue_table"></table>
    </div>
    <div data-options="region:'center'">
        <table id="picture_table"></table>
    </div>
</div>

<#include "../../common/lib/jslib.ftl">
<script src="/bizjs/sysPictrue.js"></script>

