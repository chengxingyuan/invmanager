<!-- sys_user.ftl 管理员账号 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">角色名称</label>
            <div class="col-sm-8">
                <select id="roleId" class="easyui-combobox" name="roleId" style="width:100%;"
                        data-options="prompt:'角色名称',editable:false">
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">部门</label>
            <div class="col-sm-8">
                <select id="deptId" class="easyui-combobox" name="deptId" style="width:100%;"
                        data-options="prompt:'部门',editable:false" >
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">用户名</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="username" style="width: 100%;"
                       data-options="prompt:'用户名'" required="required">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">姓名</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="nickname" style="width: 100%;"
                       data-options="prompt:'姓名'">
            </div>
        </div>
        <div class="form-group" id="pwd_div">
            <label class="col-sm-2 control-label">密码</label>
            <div class="col-sm-8">
                <input id="password" class="easyui-textbox" name="password" style="width: 100%;"
                       data-options="prompt:'密码'" required="required">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">手机号</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="mobile" style="width: 100%;"
                       data-options="prompt:'手机号'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">状态</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',panelHeight:'auto',editable:false">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
    </form>
</div>
