<!-- sys_user.ftl 管理员账号 -->
<div class="easyui-panel" data-options="fit:true,border:false">
    <div class="easyui-layout" data-options="fit:true,border:false">
        <div data-options="region:'north',height:'100px',border:false">
            <div id="sysUser_btn" class="datagrid-toolbar">

            </div>
            <form id="sysUser_form" class="form-inline search-form" style="float: left" >
                <div class="form-group">
                    <label>用户名</label>
                    <input name="username" class="easyui-textbox" data-options="prompt:'用户名'">
                </div>
            </form>
            <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询</button>
        </div>
        <div data-options="region:'center',border:false">
            <table id="sysUser_table"></table>
        </div>
    </div>
</div>


