<!-- sys_user.ftl 管理员账号 -->
<#include "../../common/lib/csslib.ftl">

<div class="easyui-panel" title="" data-options="iconCls:'icon-save',fit:true" style="overflow: hidden;">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="${id!}">
        <div class="form-group">
            <label class="col-sm-2 control-label">角色名称</label>
            <div class="col-sm-4">
                <select id="roleId" class="easyui-combobox" name="roleId" style="width:100%;"
                        data-options="prompt:'角色id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">部门</label>
            <div class="col-sm-4">
                <select id="deptId" class="easyui-combobox" name="deptId" style="width:100%;"
                        data-options="prompt:'部门id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">用户名</label>
            <div class="col-sm-4">
                <input id="username" class="easyui-textbox" name="username" style="width: 100%;"
                       data-options="prompt:'用户名'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">别名</label>
            <div class="col-sm-4">
                <input class="easyui-textbox" name="nickname" style="width: 100%;"
                       data-options="prompt:'别名'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">手机号</label>
            <div class="col-sm-4">
                <input class="easyui-textbox" name="mobile" style="width: 100%;"
                       data-options="prompt:'手机号'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">状态</label>
            <div class="col-sm-4">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',panelHeight:'auto',editable:false">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
        <hr/>
        <div class="form-group">
            <label class="col-sm-2 control-label">重置密码</label>
            <div class="col-sm-4">
                <input id="newPassword" class="easyui-passwordbox" name="newPassword" style="width: 100%;"
                       data-options="prompt:'新密码',showEye:true">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
                <a id="save" href="javascript:;" class="btn btn-sm btn-primary">保存</a>
            </div>
        </div>
    </form>
</div>

<#include "../../common/lib/jslib.ftl">
<script src="/bizjs/sys/sysUser.js"></script>

<script>
    $(function () {
        var id = $('#id').val();
        SysUser.preForm();
        $yd.get(SysUser.u + "/" + id, function (data) {
            if (data.roleId == 2) {
                $('#username,#roleId').textbox('readonly');
                $('#roleId').combobox("loadData", [{id: 2, name: '系统管理员'}])
            }
            $('form').form('load', data);
        });

        $('#save').click(function () {
            var newPassword = $('#newPassword').val(), confirmMsg = "确定修改信息?";
            if (newPassword) {
                confirmMsg += "修改密码后，需重新登录!";
            }
            $yd.confirm(confirmMsg, function () {
                $yd.form.create($('form')).url(SysUser.u).submit(function () {
                    $yd.alert("保存成功");
                    if (newPassword) {
                        parent.$('#logout').click();
                    }
                });
            })
        })
    });
</script>