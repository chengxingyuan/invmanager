<!-- sys_department.ftl 部门表 -->
<#include "../../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true,border:false">
    <div id="sysDepartment_btn" class="datagrid-toolbar">

    </div>
    <div data-options="region:'west',width:'20%',border:false,split:true" title="部门">
        <table id="sysDepartment_table"></table>
    </div>
    <div data-options="region:'center',border:false">
    <#include "../../sys/sysUser/sysUser.ftl">
    </div>
</div>

<#include "../../common/lib/jslib.ftl">

<script src="/bizjs/sys/sysUser.js"></script>
<script src="/bizjs/sys/sysDepartment.js"></script>
