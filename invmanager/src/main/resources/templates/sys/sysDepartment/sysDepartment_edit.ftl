<!-- sys_department.ftl 部门表 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <#--<div class="form-group">-->
            <#--<label class="col-sm-2 control-label">父节点</label>-->
            <#--<div class="col-sm-8">-->
                <#--<select class="easyui-combobox" name="pid" style="width: 100%;"-->
                        <#--data-options="panelHeight:'auto',readonly:true">-->
                    <#--<option value="${parent.pid!}" selected>${parent.name!}</option>-->
                <#--</select>-->
            <#--</div>-->
        <#--</div>-->
        <div class="form-group">
            <label class="col-sm-2 control-label">部门名称</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'部门名称'">
            </div>
        </div>
        <#--<div class="form-group">-->
            <#--<label class="col-sm-2 control-label">部门编码</label>-->
            <#--<div class="col-sm-8">-->
                <#--<input class="easyui-textbox" name="code" style="width: 100%;"-->
                       <#--data-options="prompt:'部门编码'">-->
            <#--</div>-->
        <#--</div>-->
        <div class="form-group">
            <label class="col-sm-2 control-label">状态</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',panelHeight:'auto',editable:false">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>

    </form>
</div>
