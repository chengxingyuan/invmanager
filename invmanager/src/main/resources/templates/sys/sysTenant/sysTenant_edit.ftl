<!-- sys_tenant.ftl 租户 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input type="hidden" name="id">
        <input type="hidden" name="sysUserId">
        <div class="form-group">
            <label class="col-sm-2 control-label">租户名称</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'租户名称',required:true">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">详细地址</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="addr" style="width: 100%;"
                       data-options="prompt:'详细地址'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">描述</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="memo" style="width: 100%;"
                       data-options="prompt:'描述'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">状态</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',panelHeight:'auto',editable:false">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>

        <h5 class="title">生成管理员</h5>

        <div class="form-group">
            <label class="col-sm-2 control-label">用户名</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="username" style="width: 100%;"
                       data-options="prompt:'用户名',required:true">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">别名</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="nickname" style="width: 100%;"
                       data-options="prompt:'别名',required:true">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">手机号</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="mobile" validType="telValidate" style="width: 100%;"
                       data-options="prompt:'此手机号应该与天天爱车供应商店铺注册手机号一致',required:true">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">密码</label>
            <div class="col-sm-8">
                <input class="easyui-passwordbox" name="newpassword" style="width: 100%;"
                       data-options="prompt:'密码'">
            </div>
        </div>
    </form>
</div>
