<!-- sys_role.ftl 角色表 -->
<#include "../../common/lib/csslib.ftl">

<div class="easyui-panel" data-options="fit:true,border:false">
    <div class="easyui-layout" data-options="fit:true,border:false">
        <div data-options="region:'north',height:'100px'">
            <div id="sysRole_btn" class="datagrid-toolbar">

            </div>
            <form id="sysRole_form" class="form-inline search-form" style="float: left">
                <div class="form-group">
                    <label>角色名称</label>
                    <input name="name" class="easyui-textbox" data-options="prompt:'角色名称'">
                </div>
            </form>
            <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询</button>
        </div>
        <div data-options="region:'center',border:false">
            <table id="sysRole_table"></table>
        </div>
        <div data-options="region:'east',split:true, width:'20%'" title="配置权限" style="padding: 6px;">
            <ul id="role_tree" class="hide">

            </ul>
        </div>
    </div>
</div>

<#include "../../common/lib/jslib.ftl">

<script src="/bizjs/sys/sysRole.js"></script>