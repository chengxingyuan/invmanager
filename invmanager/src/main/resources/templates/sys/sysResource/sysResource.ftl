<!-- sys_resource.ftl 资源管理 -->
<#include "../../common/lib/csslib.ftl">

<div class="easyui-panel" data-options="fit:true,border:false">
    <div class="easyui-layout" data-options="fit:true,border:false">
        <div data-options="region:'north',height:'auto'">
            <div id="sysResource_btn" class="datagrid-toolbar">

            </div>
            <form id="sysResource_form" class="form-inline search-form">
                <div class="form-group">
                    <label>名称</label>
                    <input name="name" class="easyui-textbox" data-options="prompt:'名称'">
                </div>
            </form>
        </div>
        <div data-options="region:'center',border:false">
            <table id="sysResource_table"></table>
        </div>
    </div>
</div>

<#include "../../common/lib/jslib.ftl">

<script src="/bizjs/sys/sysResource.js"></script>