<!-- sys_resource.ftl 资源管理 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">父节点</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="pid" style="width: 100%;"
                        data-options="panelHeight:'auto',readonly:true,editable:false">
                    <option value="${parent.pid!}" selected>${parent.name!}</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">资源名称</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'资源名称'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">路径</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="src" style="width: 100%;"
                       data-options="prompt:'路径'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">权限</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="permission" style="width: 100%;"
                       data-options="prompt:'权限'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">图标</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="icon" style="width: 100%;"
                       data-options="prompt:'图标'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">排序</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="sort" style="width:100%;"
                        data-options="prompt:'排序',panelHeight:'auto'">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">资源类型</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="type" style="width:100%;"
                        data-options="prompt:'资源类型',panelHeight:'auto',editable:false,url:'/sysDict/resourceType'">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">状态</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',panelHeight:'auto',editable:false">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>

    </form>
</div>
