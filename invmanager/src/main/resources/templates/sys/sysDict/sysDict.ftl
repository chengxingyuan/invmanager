<!-- sys_dict.ftl 字典表 -->
<#include "../../common/lib/csslib.ftl">

<div class="easyui-panel" data-options="fit:true">
    <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'north',height:'100px'">
            <div id="sysDict_btn" class="datagrid-toolbar">

            </div>
            <form id="sysDict_form" class="form-inline search-form">
                <div class="form-group">
                    <label>名称</label>
                    <input name="name" class="easyui-textbox" data-options="prompt:'名称'">
                </div>
            </form>
        </div>
        <div data-options="region:'center'">
            <table id="sysDict_table"></table>
        </div>
    </div>
</div>

<#include "../../common/lib/jslib.ftl">

<script src="/bizjs/sys/sysDict.js"></script>