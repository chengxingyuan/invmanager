<!-- sys_dict.ftl 字典表 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">父节点</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="pid" style="width: 100%;"
                        data-options="panelHeight:'auto',readonly:true,editable:false">
                    <option value="${parent.pid!}" selected>${parent.name!}</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">编码</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="code" style="width: 100%;"
                       data-options="prompt:'编码(可空)'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">显示值</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'显示值',requird:true">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">值</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="value" style="width: 100%;"
                       data-options="prompt:'值(默认0)',requird:true">
            </div>
        </div>
    <#--<div class="form-group">-->
    <#--<label class="col-sm-2 control-label">img</label>-->
    <#--<div class="col-sm-8">-->
    <#--<input class="easyui-textbox" name="img" style="width: 100%;"-->
    <#--data-options="prompt:'img'">-->
    <#--</div>-->
    <#--</div>-->
        <div class="form-group">
            <label class="col-sm-2 control-label">排序</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="sort" style="width:100%;"
                       data-options="prompt:'排序',min:0">
                </input>
            </div>
        </div>
    <#if Session.current_user.tenantId==1>
        <div class="form-group">
            <label class="col-sm-2 control-label">系统预置？</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="editable" style="width:100%;"
                        data-options="prompt:'能否修改',panelHeight:'auto',editable:false">
                    <option value="0">否</option>
                    <option value="1">是</option>
                </select>
            </div>
        </div>
    </#if>
        <div class="form-group">
            <label class="col-sm-2 control-label">状态</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">备注</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="memo" style="width: 100%;height: 60px;"
                       data-options="prompt:'备注',multiline:true">
            </div>
        </div>
    </form>
</div>
