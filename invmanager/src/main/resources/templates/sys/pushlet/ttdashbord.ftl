<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="easyui-panel" title="&#12288;当日订单" style="min-height: 260px;"
                 data-options="iconCls:'icon-large-chart'">
                <table id="t_order_visit_${storeId}"></table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="easyui-panel" title="&#12288;当日产品访客" style="min-height: 260px;"
                 data-options="iconCls:'icon-large-chart'">
                <table id="t_goods_visit_${storeId}"></table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="easyui-panel" title="&#12288;当日店铺访客" data-options="iconCls:'icon-large-chart',height:360">
                <table id="t_store_visit_${storeId}"></table>
            </div>
        </div>
        <div class="col-md-4">
            <div class="easyui-panel" title="&#12288;当日电话记录" style="min-height: 260px;"
                 data-options="iconCls:'icon-large-chart'">
                <table id="t_call_visit_${storeId}"></table>
            </div>
        </div>
        <div class="col-md-4">
            <div class="easyui-panel" title="&#12288;当日聊天记录" style="min-height: 260px;"
                 data-options="iconCls:'icon-large-chart'">
                <table id="t_chat_visit_${storeId}"></table>
            </div>
        </div>
    </div>
</div>

