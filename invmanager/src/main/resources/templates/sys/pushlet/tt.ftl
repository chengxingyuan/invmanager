<#include "../../common/lib/csslib.ftl">
<style>
    .col-md-4, .col-md-6, .col-md-12 {
        padding: 6px 6px 0 6px;
    }

    .panel-header {
        border-style: solid;
    }
</style>
<div id="__tabs" title="天天爱车统计面板" class="easyui-tabs" data-options="plain:true,pill:true,border:false">

</div>

<script src="/public/pushlet/ajax-pushlet-client.js"></script>
<#include "../../common/lib/jslib.ftl">

<script>

    $(function () {
        pushletInit();
    });

    // 是否开通了天天爱车并拉取数据
    function pushletInit() {
        p_join_listen();
//        p_subscribe("pushlet", "pushlet");

        var $tabs = $('#__tabs');
        $tabs.tabs({
            onSelect: function (title, index) {
                var tab = $(this).tabs('getSelected'), id = tab.attr('id');
                setTimeout(function () {
                    loadAllTabs(id);
                }, 100);
            }
        });
        $.each(parent.window.isOpenTTicar, function (i, n) {
            var options = {
                id: n['ttstore_id'],
                title: n['name'],
                closable: false,
                selected: false,
                tools: [{
                    iconCls: 'icon-mini-refresh',
                    handler: function () {
                        loadAllTabs(n['ttstore_id']);
                    }
                }]
            };
            options.href = '/pushlet/tt/' + options.id;
            $tabs.tabs('add', options);
        });
    }

    function loadAllTabs(id) {
        t_goods_visit_(id);
        t_store_visit_(id);
        t_order_visit_(id);
        t_call_visit_('t_call_visit_', id, 'call_visit');
        t_call_visit_('t_chat_visit_', id, 'chat_visit');
    }

    // 订单
    function t_order_visit_(id) {
        var $t = $('#t_order_visit_' + id);
        if ($t.data('loaded')) {
            $t.datagrid('reload');
            return;
        }
        var dgrid = new $yd.DataGrid($t);
        dgrid.url = '/tticar/pushlet?ptype=order_visit&storeId=' + id;
        dgrid.columns = [[
            {field: 'vname', title: '门店', width: 100},
            {field: 'gname', title: '产品', width: 100},
            {field: 'status', title: '状态', width: 100},
            {field: 'createtime', title: '下单时间', width: 100}
        ]];
        dgrid.loadGrid();
        $t.data('loaded', true);
    }
    // 产品统计
    function t_goods_visit_(id) {
        var $t = $('#t_goods_visit_' + id);
        if ($t.data('loaded')) {
            $t.datagrid('reload');
            return;
        }
        var dgrid = new $yd.DataGrid($t);
        dgrid.url = '/tticar/pushlet?ptype=goods_visit&storeId=' + id;
        dgrid.columns = [[
            {field: 'vname', title: '门店', width: 100},
            {field: 'gname', title: '产品', width: 100},
            {field: 'checktime', title: '时间', width: 100}
        ]];
        dgrid.loadGrid();
        $t.data('loaded', true);
    }
    // 店铺统计
    function t_store_visit_(id) {
        var $t = $('#t_store_visit_' + id);
        if ($t.data('loaded')) {
            $t.datagrid('reload');
            return;
        }
        var dgrid = new $yd.DataGrid($t);
        dgrid.url = '/tticar/pushlet?ptype=store_visit&storeId=' + id;
        dgrid.columns = [[
            {field: 'vname', title: '门店', width: 100},
            {field: 'checktime', title: '时间', width: 100}
        ]];
        dgrid.loadGrid();
        $t.data('loaded', true);
    }
    // 电话/聊天统计
    function t_call_visit_(tab, id, type) {
        var $t = $('#' + tab + id);
        if ($t.data('loaded')) {
            $t.datagrid('reload');
            return;
        }
        var dgrid = new $yd.DataGrid($t);
        dgrid.url = '/tticar/pushlet?ptype=' + type + '&storeId=' + id;
        dgrid.columns = [[
            {field: 'vname', title: '门店', width: 100},
            {field: 'ctime', title: '时间', width: 100}
        ]];
        dgrid.loadGrid();
        $t.data('loaded', true);
    }

    function onData(event) {
        var sub = event.get("p_subject");

        alert(sub);

    }

</script>



