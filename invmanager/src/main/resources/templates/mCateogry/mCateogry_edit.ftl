<!-- m_cateogry.ftl 存货分类表 -->
<div class="container-fluid">
    <form  class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">

        <#if id ??>
            <#else>
                <div class="form-group">
                <label class="col-sm-2 control-label">父节点</label>
                <div class="col-sm-8">
                    <select class="easyui-combobox" name="pid" style="width: 100%;"
                            data-options="panelHeight:'auto',readonly:true,editable:false">
                        <option value="${parent.id!}" selected>${parent.name!}</option>
                    </select>
                </div>
                </div>
        </#if>
        <input type="hidden" name="token" value="${tokenOnly}" >
        <div class="form-group">
            <label class="col-sm-2 control-label">分类名称</label>
            <div class="col-sm-8">
                <input id="focusInput" class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'分类名称',required:true,validType:'length[1,64]'" >
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">排序值</label>
            <div class="col-sm-8">
                <input  id="sort" class="easyui-numberbox" name="sort" style="width: 100%;"
                       data-options="prompt:'排序值',precision:0">
            </div>
        </div>
        <#--<div class="form-group">-->
            <#--<label class="col-sm-2 control-label">分类编码</label>-->
            <#--<div class="col-sm-8">-->
                <#--<input class="easyui-maskedbox" mask="${parent.code!}**" id="code" name="code" style="width: 100%;"-->
                       <#--data-options="prompt:'分类编码',required:true">-->
            <#--</div>-->
        <#--</div>-->

        <div class="form-group">
            <label class="col-sm-2 control-label">状态</label>
            <div class="col-sm-8">
                <select id="status" class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',panelHeight:'auto',editable:false">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
    </form>
</div>

<script>
    var MCateogryEdit = {};
    MCateogryEdit.reset = function () {
        $("#focusInput").textbox("setValue", "");
        $("#sort").numberbox("setValue", "");
        $("#status").combobox("setValue", 0);

        setTimeout("$(\"#focusInput\").next().find(\".textbox-text\").focus()", 500);
    };
    $(function () {
        setTimeout("$(\"#focusInput\").next().find(\".textbox-text\").focus()", 500);
    });
</script>
