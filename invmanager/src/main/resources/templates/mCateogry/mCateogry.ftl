<!-- m_cateogry.ftl 存货分类表 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-panel" data-options="fit:true">
    <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'center'">
            <div class="easyui-layout" data-options="fit:true">
                <div data-options="region:'north',height:100">
                    <div id="mCateogry_btn" class="datagrid-toolbar">

                    </div>
                    <form id="mCateogry_form" class="form-inline search-form" style="float: left">
                        <div class="form-group">
                            <label>名称</label>
                            <input id="name" name="name" class="easyui-textbox" data-options="prompt:'名称'">
                        </div>
                    </form>
                    <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询</button>
                </div>
                <div data-options="region:'center'">
                    <table id="mCateogry_table"></table>
                </div>
            </div>
        </div>
        <div title="规格" data-options="region:'east',width:'30%',split:true">
            <#--<div id="mSku"></div>-->
                <table id="mSku"></table>
        </div>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">
<script>
    window.onload =function () {
        $('#name').textbox().next('span').find('input').focus();
    }

</script>
<script src="/bizjs/mCateogry.js"></script>