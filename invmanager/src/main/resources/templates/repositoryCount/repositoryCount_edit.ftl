<!-- repository_count.ftl 商品库存数量表 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">商品skuId</label>
            <div class="col-sm-8">
                <select id="skuId" class="easyui-combobox" readonly="true" name="skuId" style="width:100%;"
                        data-options="prompt:'商品skuId',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">商品数量</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="count" style="width:100%;"
                        data-options="prompt:'商品数量',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">最小库存数量</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="minCount" style="width:100%;"
                       data-options="prompt:'商品数量',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">最大库存数量</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="maxCount" style="width:100%;"
                       data-options="prompt:'商品数量',min:0">
                </input>
            </div>
        </div>
    </form>
</div>
