<!-- repository_count.ftl 商品库存数量表 -->
<#include "./../common/lib/csslib.ftl">
<input type="hidden" id="hidHouseId" value="${houseId}" />
<input type="hidden" id="hidHouseName" value="${houseName}" />
<input type="hidden" id="hidSearchText" value="${searchText}"/>
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'100px'">
        <div id="repositoryCount_btn" class="datagrid-toolbar">

        </div>
        <form id="repositoryCount_form" class="form-inline search-form" style="float: left">
            <div class="form-group" style="line-height: 20px">
                <label>仓库</label>
                <select name="wearhouseId" id="wearhouseIdForSearch" style="width: 70px" class="easyui-combobox" data-options="prompt:'所在仓库',editable:false">
                </select>
            </div>
            <div class="form-group" style="line-height: 20px">
                <label>商品分类</label>
                <select name="categoryId"  id="categoryId" style="width: 100px" class="easyui-combobox" data-options="prompt:'商品分类',editable:false">
                </select>
            </div>
            <div class="form-group">
                <label>搜索</label>
                <input name="searchText" id="searchText" class="easyui-textbox" data-options="prompt:'商品名称，规格名，商品编号'">
            </div>
            <input type="hidden" id="shieldZero" name="shieldZero">
        </form>
        <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询</button>
        <button id="isZero" onmouseover="this.style.backgroundColor='grey'" onmouseout="this.style.backgroundColor='#FF3300'" style="float:right;width:82px;margin-top:10px;margin-right:418px;background-color: #FF3300;border-radius:2px;color:white;border: 0px;" href="#" >屏蔽0库存</button>
        <button id="notZero" onmouseover="this.style.backgroundColor='#FF3300'" onmouseout="this.style.backgroundColor='grey'" style="display:none;float:right;width:82px;margin-top:10px;margin-right:418px;background-color: grey;border-radius:2px;color:white;border: 0px;" href="#" >屏蔽0库存</button>
    </div>
    <div data-options="region:'center'">
        <table id="repositoryCount_table"></table>
    </div>
    <div data-options="region:'east',split:true,collapsible:false" style="width:28%;">
        <table id="kwTb"></table>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">
n
<script src="/bizjs/repositoryCount.js"></script>