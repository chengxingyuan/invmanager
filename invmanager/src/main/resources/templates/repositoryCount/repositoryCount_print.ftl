
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>表单打印</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="/public/assets/css/p_form.css"/>
</head>
<body>
<div class="p_form">
    <#--<h3 class="form-title">仓库采购单</h3>-->
        <br>
        <div>
            <ul class="form-header">
                <li class="form_data">
                    <span>打印日期：</span>
                    <span> ${thisTime}</span>
                </li>
                <li class="form_data">
                    <span>打印人：</span>
                    <span> ${Session.current_user.nickname}</span>
                </li>
            </ul>
        </div>
    <#--<div class="form_content">-->
        <#--<ul class="form-header">-->
            <#--<li class="form_list">-->
                <#--<span>采购日期</span>-->
                <#--<span>${ptime}</span>-->
            <#--</li>-->
            <#--<li class="form_list">-->
                <#--<span>单据号</span>-->
                <#--<span>${purchase.code}</span>-->
            <#--</li>-->
            <#--<li class="form_list">-->
                <#--<span>采购员</span>-->
                <#--<span>${buyMan}</span>-->
            <#--</li>-->
            <#--<li class="form_list">-->
                <#--<span>采购类型</span>-->
                <#--<span>${typeName}</span>-->
            <#--</li>-->
            <#--<li class="form_list supplier">-->
                <#--<span>供应商</span>-->
                <#--<span>${relativeName}</span>-->
            <#--</li>-->
        <#--</ul>-->
    <#--</div>-->
    <div class="form_table-context">
    <table class="form_table" id="repository_table" style="margin-top: 10px">
        <tr><th>序号</th><th>存货名称</th><th>规格</th><th>分类</th><th>商品编号</th><th>现存量</th><th>仓库</th><th >默认供应商</th></tr>
        <tr id="temp"><td id="num"></td><td id="goodsName"><td id="skus"></td><td id="category"></td><td id="code"></td><td id="count"></td><td  id="repository"></td><td id="supplier"></td></tr>
        <#--<tr><td>1</td><td>自行车行车行车自行车自行车</td><td>19939932</td><td>100</td><td>2</td><td>200</td></tr>-->
        <#--<tr><td>1</td><td>自行车行车行车自行车自行车</td><td>19939932</td><td>100</td><td>2</td><td>200</td></tr>-->
        <#--<tr><td>1</td><td>自行车行车行车自行车自行车</td><td>19939932</td><td>100</td><td>2</td><td>200</td></tr>-->
    </table>

    <input type="hidden" id="rowData" value="${ids}" />
    </div>
    <#--<div class="total_price">-->
        <#--<div class="total_title">合计</div>-->
        <#--<div class="total_money">-->
            <#--<span>金额：￥${purchase.amountActual}</span>-->
            <#--<span>数量：${purchase.totalNum}</span>-->
        <#--</div>-->
    <#--</div>-->
    <#--<ul class="form_footer">-->
        <#--<li>入库签字（盖章）：</li>-->
        <#--<li>核单签字（盖章）：</li>-->
        <#--<li>日期 ：</li>-->
        <#--<li>备注 ：</li>-->
    <#--</ul>-->

    <div>
        <br>
        <button type="button" id="printButton"
                style="float:right;margin-right:100px;border-radius:2px;color:white;border: 0px;background-color:#00a2d4;height: 30px;width: 86px" >打&nbsp;印</button>
    </div>
</div>
<#include "./../common/lib/jslib.ftl">
<script>
    window.onload = function() {
//        var data = $('#rowData').val();
//        $.each(data, function (i, n) {
//            var row = $("#temp").clone();
//            row.find("#num").text(i + 1);
//            row.find("#goodsName").text(n.goodsName);
//            row.find("#skus").text(n.skus);
//            row.find("#category").text(n.code);
//            row.find("#code").text(n.unitName);
//            row.find("#count").text(n.amountActual);
//            row.find("#repository").text(n.count);
//            row.find("#supplier").text(n.count * n.amountActual);
//            row.appendTo("#repository_table");//添加到模板的容器中
//        });
//
//    }
        var id = $('#rowData').val();
        $.ajax({
            url: "/repositoryCount/printData?ids=" + id,
            type: "get",
            dataType: 'json',
            //jsonpCallback:"callback",
//            data: {
//                ids: id
//            },
            success: function (data) {
                $.each(data, function (i, n) {
                    var row = $("#temp").clone();
                    row.find("#num").text(i + 1);
                    row.find("#goodsName").text(n.goodsName);
                    row.find("#skus").text(n.skus);
                    row.find("#category").text(n.categoryName);
                    row.find("#code").text(n.skuCode);
                    row.find("#count").text(n.totalNum);
                    row.find("#repository").text(n.wearhouseName);
                    row.find("#supplier").text(n.supplierName);
                    row.appendTo("#repository_table");//添加到模板的容器中
                });


            },
            error: function (request) {
                alert(JSON.stringify(request))
            }
        });
    }




    var wait = 1;
    document.getElementById("printButton").onclick = function() {
    time(this);
    window.print();
    }
    function time(o) {
    if (wait == 0) {
    $("#printButton").attr("style","display:block;float:right;margin-right:100px;border-radius:2px;color:white;border: 0px;background-color:#00a2d4;height: 30px;width: 86px");
    wait = 1;
    } else {
    $("#printButton").attr("style","display:none;");
    wait--;
    setTimeout(function() {
    time(o)
    }, 200)
    }
    }


    function doPrint() {
    window.print();
    }
</script>
<#--<script src="/bizjs/pPurchase_print.js?t=2"></script>-->
</body>

</html>
