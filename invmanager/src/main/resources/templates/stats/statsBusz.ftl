<!-- m_sales.ftl  -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'145'">
        <div id="mSales_btn" class="datagrid-toolbar">

        </div>
        <form id="mSales_form" class="form-inline search-form" style="float: left">
            <div class="form-group">
                <label>订单日期</label>
                <input name="startTime" style="width: 100px;" class="easyui-datebox">-
                <input name="endTime" style="width: 100px;" class="easyui-datebox">
                <label>操作人</label>
                <input id="userName" class="easyui-combobox" name="username" data-options="prompt:'',editable:false,url:'/sysUser/userList',valueField:'username',textField:'username'">
                <br/>
                <label>总订单数</label>
                <input id="totalNum" class="easyui-textbox" readonly>
                <label>总销售额</label>
                <input id="totalFee" class="easyui-textbox" readonly>
            </div>
        </form>
        <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-top:10px;margin-left:10px;" >查询</button><br>
    </div>
    <div data-options="region:'center'">
        <table id="mSales_table"></table>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/statsBusz.js"></script>