<!-- m_sales.ftl  -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'auto',height:145">
        <div id="mSales_btn" class="datagrid-toolbar">

        </div>
        <table>
            <tr>
                <td>
                    <form id="mSales_form" class="form-inline search-form">
                        <div class="form-group">
                            <label>订单日期</label>
                            <input name="startTime" class="easyui-datebox" style="width: 100px;">-
                            <input name="endTime" class="easyui-datebox" style="width: 100px;">
                            <label>商品名称</label>
                            <input id="searchText" class="easyui-textbox" name="searchText">
                        </div>
                        <br>
                        <div class="form-group">
                            <label>总销售量</label>
                            <input id="totalNum" class="easyui-textbox" readonly style="width: 208px;">
                            <label>总销售额</label>
                            <input id="totalFee" class="easyui-textbox" readonly>
                        </div>
                    </form>
                </td>
                <td style="vertical-align: top; padding-top: 10px;">
                    <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px" >查询</button>
                </td>
            </tr>
        </table>

    </div>
    <div data-options="region:'center'">
        <table id="mSales_table"></table>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/statsIncome.js"></script>