<!-- m_brand.ftl 存货品牌 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">品牌名称</label>
            <div class="col-sm-8">
                <input id="brandName" class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'品牌名称',required: true,validType:'length[1,64]'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">状态</label>
            <div class="col-sm-8">
                <select id="brandStatus" class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',panelHeight:'auto',editable:false">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>

    </form>
</div>
<script>
    var MBrandEdit = {};
    MBrandEdit.reset = function () {
        $("#brandName").textbox("setValue", "");
        $("#brandStatus").combobox("setValue", 0);
        setTimeout("$(\"#brandName\").next().find(\".textbox-text\").focus()", 500);
    };
    $(function () {
        setTimeout("$(\"#brandName\").next().find(\".textbox-text\").focus()", 500);
    });
</script>