<!-- m_brand.ftl 存货品牌 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-panel" data-options="fit:true,border:false">
    <div class="easyui-layout" data-options="fit:true,border:false">
        <div data-options="region:'north',height:86">
            <div id="mBrand_btn" class="datagrid-toolbar">

            </div>
            <form id="mBrand_form" class="form-inline search-form" style="float: left">
                <div class="form-group">
                    <label>名称</label>
                    <input id="searchName" name="name" class="easyui-textbox" data-options="prompt:'名称'">
                </div>
            </form>
            <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询(Enter)</button>
        </div>
        <div data-options="region:'center',border:false">
            <table id="mBrand_table"></table>
        </div>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/mBrand.js"></script>