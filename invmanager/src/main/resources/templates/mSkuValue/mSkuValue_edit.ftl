<!-- m_sku_value.ftl 物料规格值 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">规格</label>
            <div class="col-sm-8">
                <#--<select id="skuId" class="easyui-combobox" name="skuId" style="width:100%;"-->
                        <#--data-options="prompt:'请选择规格'">-->
                <#--</select>-->
                <input id="skuId" class="easyui-textbox" name="skuId" style="width: 100%;"
                       data-options="prompt:'请选择规格'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">规格值名称</label>
            <div class="col-sm-8">
                <input id="skuValueName" class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'规格值名称',required:true,validType:'length[1,64]'">
            </div>
        </div>
        <#--<div class="form-group">-->
            <#--<label class="col-sm-2 control-label">规格值编号</label>-->
            <#--<div class="col-sm-8">-->
                <#--<input class="easyui-textbox" name="code" style="width: 100%;"-->
                       <#--data-options="prompt:'规格值编号'">-->
            <#--</div>-->
        <#--</div>-->
        <div class="form-group">
            <label class="col-sm-2 control-label">排序</label>
            <div class="col-sm-8">
                <input id="skuValueSort" class="easyui-numberbox" name="sort" style="width:100%;"
                        data-options="prompt:'数字排序',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">是否默认</label>
            <div class="col-sm-8">
                <select id="skuValueIsDefault" class="easyui-combobox" name="isDefault" style="width:100%;"
                        data-options="prompt:'是否默认',panelHeight:'auto',editable:false">
                    <option value="0">否</option>
                    <option value="1">是</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">状态</label>
            <div class="col-sm-8">
                <select id="skuValueStatus" class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',panelHeight:'auto',editable:false">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
    </form>
</div>
<script>
    MSkuValueEdit = {};
    MSkuValueEdit.reset = function () {
        $("#skuValueName").textbox("setValue", "");
        $("#skuValueSort").textbox("setValue", "");
        $("#skuValueIsDefault").combobox("setValue", 0);
        $("#skuValueStatus").combobox("setValue", 0);
        setTimeout("$(\"#skuValueName\").next().find(\".textbox-text\").focus()", 500);
    };
    $(function () {
        setTimeout("$(\"#skuValueName\").next().find(\".textbox-text\").focus()", 500);
    });
</script>