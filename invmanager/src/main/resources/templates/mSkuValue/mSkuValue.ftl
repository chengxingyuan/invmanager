<!-- m_sku_value.ftl 物料规格值 -->
<div class="easyui-panel" data-options="fit:true">
    <div class="easyui-layout" data-options="fit:true">
        <div id="mSkuValue_btn" class="datagrid-toolbar">

        </div>
        <div data-options="region:'center'">
            <table id="mSkuValue_table"></table>
        </div>
    </div>
</div>
