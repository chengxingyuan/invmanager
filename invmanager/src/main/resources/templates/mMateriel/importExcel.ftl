<#--导入-->
<#include "./../common/lib/csslib.ftl">

<div class="container-fluid">
    <form class="form-horizontal mform" method="post" enctype="multipart/form-data">
        <#--<input id="skuIds" type="hidden" name="skuIds" value="${skuIds}">-->
        <div class="form-group">
            <label class="col-sm-2 control-label">存货商品Excel</label>
            <div class="col-sm-8">
                <input class="easyui-filebox" name="excelFile" data-options="prompt:'请选择excel'" style="width:100%">
            </div>
        </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-8">
                    注意：
                    <br>
                    1、必须使用指定模板，不可改变模板表头格式<br>
                    2、必填的不能为空，要求数字的必须是数字<br>
                    3、存货商品名称一样的将会被合并为同一条数据（规格不合并）<br>
                    4、导入的数据内容必须符合格式要求<br>
                </div>
            </div>
    </form>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/mMateriel.js"></script>