<!-- m_materiel.ftl 存货表 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <input id="hiddenCategoryId" type="hidden" value="${categoryId}">
        <div class="form-group">
            <label class="col-sm-1 control-label">存货名称</label>
            <div class="col-sm-3">
                <input class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'名称',required:true,validType:'length[1,200]'">
            </div>
            <label class="col-sm-1 control-label">存货分类</label>
            <div class="col-sm-3">
                <#--<#if id ?? && categoryId != 0>-->
                    <#--<input class="easyui-textbox" name="categoryName" value="${cateogryName}" style="width: 100%;"-->
                           <#--data-options="prompt:'名称',readonly:true">-->
                    <#--<select id="categoryId" name="categoryId" class="easyui-combobox" style="width: 100%;"-->
                            <#--data-options="panelHeight:'auto',editable:false">-->
                        <#--<option value="${categoryId}" selected>${cateogryName}</option>-->
                    <#--</select>-->
                <#--<#else>-->
                    <input id="categoryId" name="categoryId"  style="width:100%;"
                              data-options="prompt:'请选择三级分类.',required:true,editable:false"/>
                <#--</#if>-->
            </div>
            <label class="col-sm-1 control-label">品牌</label>
            <div class="col-sm-3">
                <input id="brandId" name="brandId"
                       data-options="prompt:'品牌',editable:false"/>
            </div>
        </div>
        <div id="m_tabs" class="easyui-tabs" data-options="plain:true,pill:true">

            <div title="基本信息" style="padding:10px;overflow: hidden;">
                <div class="form-group">
                    <label class="col-sm-2 control-label">默认供应商</label>
                    <div class="col-sm-4">
                        <input id="defaultUintId" name="defaultUintId"
                               data-options="prompt:'默认供应商',editable:false"/>
                    </div>
                    <label class="col-sm-1 control-label">仓库/库位<span style="color:red">*</span></label>
                    <div class="col-sm-3">
                        <input id="house" name="house" type="hidden"/>
                        <input id="defaultWhouseId" class="easyui-textbox" style="width: 300px;" readonly/>
                        <a href="javascript:void(0)" id="houseSet">设置</a>
                    </div>
                </div>
                <#--<div class="form-group">-->
                    <#--<label class="col-sm-2 control-label">存货属性</label>-->
                    <#--<div class="col-sm-8">-->
                        <#--<select class="easyui-combobox" name="attribute" style="width:100%;"-->
                                <#--data-options="prompt:'存货属性',panelHeight:'auto',multiple:true,editable:false,url:'/sysDict/materiel_ATTR'">-->
                        <#--</select>-->
                    <#--</div>-->
                <#--</div>-->
                <div class="form-group">
                    <label class="col-sm-2 control-label">最低库存</label>
                    <div class="col-sm-4">
                        <input class="easyui-numberbox" name="minInventory" style="width:100%;"
                               data-options="prompt:'最低库存',min:0"/>
                    </div>
                    <label class="col-sm-1 control-label">最高库存</label>
                    <div class="col-sm-3">
                        <input class="easyui-numberbox" name="maxInventory" style="width:100%;"
                               data-options="prompt:'最高库存',min:0"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">计量单位</label>
                    <div class="col-sm-4">
                        <select class="easyui-combobox" name="mUnit" style="width:100%;"
                                data-options="prompt:'计量单位',panelHeight:'auto',editable:false,url:'/sysDict/materiel_MU'">
                        </select>
                    </div>
                    <label class="col-sm-1 control-label">状态</label>
                    <div class="col-sm-3">
                        <select class="easyui-combobox" name="status" style="width:100%;"
                                data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                            <option value="0">启用</option>
                            <option value="1">禁用</option>
                        </select>
                    </div>
                </div>
            <#--<div class="form-group">-->
            <#--<label class="col-sm-2 control-label">描述</label>-->
            <#--<div class="col-sm-8">-->
            <#--<div id="description" class="easyui-texteditor" name="description"-->
            <#--style="width:100%;min-height:300px;height:auto;padding:10px">-->
            <#--</div>-->
            <#--</div>-->
            <#--</div>-->
            </div>
            <div id="skuContainer" title="规格型号" style="padding:10px;overflow: hidden;;min-height: 100px;">

            </div>
            <div title="库存价格" style="overflow: hidden;">
                <table id="priceTab"></table>
            </div>
            <div title="图文详情" style="padding:10px;overflow: hidden;;min-height: 100px;">
                <div id="pic_desc_container" class="form-group">
                    <div class="col-sm-2 col-sm-offset-2 list-group">
                        <a href="javascript:;" class="list-group-item text">+文本</a>
                        <a href="javascript:;" class="list-group-item image">+图片</a>
                    <#--<a href="javascript:;" class="list-group-item video">+视频</a>-->
                    </div>
                    <ul class="col-sm-6 list-group pic-desc">
                        <li class="list-group-item">
                            <div class="panel-tool">
                                <a href="javascript:;" class="icon-edit panel-tool-a"></a>
                                <a href="javascript:;" class="panel-tool-close"></a>
                            </div>
                            <div class="content">请在次编辑内容...</div>
                        </li>
                    </ul>
                </div>
            </div>
            <div title="轮播图" style="padding:10px;overflow: hidden">
                <div class="file-loading">
                    <input id="pictrues" type="file" multiple>
                </div>
            </div>
            <div title="适用车型" style="padding:10px;overflow: hidden">
            <#--<div class="easyui-layout" data-options="fit:true" style="width:100%;height:600px;">-->
            <#--<div data-options="region:'west', width:'400px'">-->
            <#--<span class="textbox searchbox" style="width: 380px;">-->
            <#--<span class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;">-->
            <#--<a href="javascript:;" class="textbox-icon searchbox-button" icon-index="0" tabindex="-1" style="width: 26px; height: 28px;"></a>-->
            <#--</span>-->
            <#--<input id="searchText" type="text" class="textbox-text validatebox-text textbox-prompt" autocomplete="off" tabindex="" placeholder="请输入车型名称" style="margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 352px;">-->
            <#--</span>-->
            <#--<ul id="goodsTb"></ul>-->
            <#--</div>-->
            <#--<div data-options="region:'center'">-->
            <#--<table id="carTab"></table>-->
            <#--</div>-->
            <#--</div>-->
                <table id="carTab"></table>
            </div>
        </div>
    </form>
</div>
