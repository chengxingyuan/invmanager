<!-- m_materiel.ftl 存货表 -->
<#include "./../common/lib/csslib.ftl">
<input type="hidden" id="hidSearchText" value="${searchText}"/>
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'west',width:'14%',split:true" title="存货分类">
        <ul id="mCateogry_tree"></ul>
    </div>
    <div data-options="region:'center'">
        <div class="easyui-layout" data-options="fit:true">
            <div data-options="region:'north',height:100">
                <div id="mMateriel_btn" class="datagrid-toolbar">
                    <a href="javascript:void(0)" id="mb" class="easyui-menubutton"
                       data-options="menu:'#mm',iconCls:'icon-search'">联查</a>
                    <div id="mm" style="width:150px;">
                        <div id="lcxxdd">线下订单</div>
                        <#--<div id="lcptdd">平台订单</div>-->
                        <div id="lccgd">采购单</div>
                        <div id="lckcl">库存量</div>
                    </div>
                </div>
                <form id="mMateriel_form" class="form-inline search-form" style="float: left">
                    <div class="form-group">
                        <label>查询内容</label>
                        <input id="searchText" name="name" class="easyui-textbox" data-options="prompt:'商品名称'">
                    </div>
                    <div class="form-group">
                        <label>适用车型</label>
                        <input name="car" class="easyui-textbox" data-options="prompt:'适用车型'">
                    </div>
                </form>
                <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询(Enter)</button>
            </div>
            <div data-options="region:'center'" style="OVERFLOW:auto;">
                <table id="mMateriel_table"></table>
            </div>
        </div>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">
<script src="/public/easyui/ex/datagrid-detailview.js"></script>
<script src="/bizjs/mMateriel.js"></script>