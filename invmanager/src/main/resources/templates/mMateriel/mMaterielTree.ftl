<!-- m_materiel.ftl 存货树 -->
<#include "./../common/lib/csslib.ftl">
<input type="hidden" id="hidType" value="${type}"/>

<script>
    function doSearch() {
        GoodsTree.tb.tree({
            queryParams: {
                type: $("#hidType").val(),
                searchText: $("#searchText").val()
            }
        });
        GoodsTree.tb.tree("reload");
    }

    var GoodsTree = {
        searchText: "",
        isFocus: 0,
        tb: undefined
    };

    GoodsTree.init = function () {
        $("#searchText").keyup(function () {
            var content = $("#searchText").val();
            if (content == GoodsTree.searchText) {
                return;
            }
            GoodsTree.searchText = content;
            doSearch();
        });

        $("#searchText").focusin(function () {
            GoodsTree.isFocus = 1;
        });
        $("#searchText").focusout(function () {
            GoodsTree.isFocus = 0;
        });

        //快捷键
        document.onkeydown = GoodsTree.keydown;

        setTimeout("$('#searchText').focus()", 500);
        $("a").attr("tabindex", "-1");
    };

    GoodsTree.keydown = function (event) {
        var e = event || window.event || arguments.callee.caller.arguments[0];
        if (!e) {
            return;
        }
//        console.log("dg")
        switch (e.keyCode) {
            case 9: //tab
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else {
                    e.returnValue = false;
                }
            case 32:
                GoodsTree.space();
                break;
            case 37:
                GoodsTree.left();
                break;
            case 38:
                GoodsTree.up();
                break;
            case 39:
                GoodsTree.right();
                break;
            case 40:
                GoodsTree.down();
                break;
        }
    };

    GoodsTree.tree = function () {
        GoodsTree.tb = $("#goodsTb");
        GoodsTree.tb.tree({
            url: '/mMateriel/tree',
            queryParams: {
                type: $("#hidType").val()
            },
            singleSelect: false,
            checkbox: true
        });
    };

    //获取选中的规格集合
    GoodsTree.getChecked = function () {
        var result = [];
        var nodes = GoodsTree.tb.tree("getChecked");
        for (var i = 0; i < nodes.length; i++) {
            var node = nodes[i];
            if (node.pid == 0) {
                var child = GoodsTree.tb.tree("getChildren", node.target);
                if (child.length != 0) {//子节点已被加载
                    continue;
                } else {
                    $.ajax({
                        type: "post",
                        url: "/mMateriel/tree",
                        data: {type: 0, id: node.id},
                        async: false,
                        success: function (data) {
                            for (var j = 0; j < data.length; j++) {
                                var obj = {
                                    skuId: data[j].id,
                                    skusId: data[j].id,
                                    skusName: data[j].text,
                                    goodsId: data[j].pid,
                                    goodsName: node.text,
                                    code: data[j].attributes.code,
                                    unit: node.attributes.unit,
                                    ralativeId: node.attributes.ralativeId, //供应商
                                    ralativeName: node.attributes.ralativeName,
                                    wearhouseId: node.attributes.wearhouseId, //仓位
                                    wearhouseName: node.attributes.wearhouseName,
                                    positionId: node.attributes.positionId, //仓位
                                    positionName: node.attributes.positionName,
                                    amount: data[j].attributes.amount, //参考进价
                                    salesAmout: data[j].attributes.salesAmout, //参考售价
                                    skusNum: data[j].attributes.skusNum,
                                    house: data[j].attributes.house,
                                    car: node.attributes.car, //试用车型
                                };
                                result.push(obj);
                            }
                        }
                    });
                }
            } else { //子节点
                var p = GoodsTree.tb.tree("getParent", node.target);
                var obj = {
                    skuId: node.id,
                    skusId: node.id,
                    skusName: node.text,
                    goodsId: node.pid,
                    goodsName: p.text,
                    code: node.attributes.code,
                    unit: p.attributes.unit,
                    ralativeId: p.attributes.ralativeId, //供应商
                    ralativeName: p.attributes.ralativeName,
                    wearhouseId: p.attributes.wearhouseId, //仓位
                    wearhouseName: p.attributes.wearhouseName,
                    positionId: p.attributes.positionId, //仓位
                    positionName: p.attributes.positionName,
                    amount: node.attributes.amount, //参考进价
                    salesAmout: node.attributes.salesAmout, //参考售价
                    skusNum: node.attributes.skusNum,
                    house: node.attributes.house,
                    car: p.attributes.car, //试用车型
                };
                result.push(obj);
            }
        }
        return result;
    };

    GoodsTree.up = function () {
        if (GoodsTree.isFocus == 1) {//光标在搜索框，跳转到最下面
            var roots = GoodsTree.tb.tree("getRoots");
            var node = GoodsTree.tb.tree("find", roots[roots.length - 1].id);
            $("#goodsTb").focus();
            if (node.state == "closed") {
                GoodsTree.tb.tree("select", node.target);
            } else {
                GoodsTree.tb.tree("select", GoodsTree.tb.tree("find", node.children[node.children.length - 1].id).target);
            }
            return;
        }

        var node = GoodsTree.tb.tree("getSelected");
        if (!node) {
            return;
        }
        if (node.pid == 0) {
            var roots = GoodsTree.tb.tree("getRoots");
            var tmp = 0;
            for (var i = 0; i < roots.length; i++) {
                if (roots[i].id == node.id) {
                    tmp = i;
                    break;
                }
            }
            if (tmp == 0) {
                $("#goodsTb").find('.tree-node-selected').removeClass('tree-node-selected');
                $("#searchText").focus();
                GoodsTree.set_text_value_position("searchText", -1);
                return;
            }
            var prev = GoodsTree.tb.tree("find", roots[tmp - 1].id);
            if (prev.state == "closed") {
                GoodsTree.tb.tree("select", prev.target);
            } else {//前一个节点的最后一个叶子
                GoodsTree.tb.tree("select", GoodsTree.tb.tree("find", prev.children[prev.children.length - 1].id).target);
            }
        } else {
            var roots = GoodsTree.tb.tree("getChildren", GoodsTree.tb.tree("find", node.pid).target);
            var tmp = 0;
            for (var i = 0; i < roots.length; i++) {
                if (roots[i].id == node.id) {
                    tmp = i;
                    break;
                }
            }
            if (tmp == 0) {
                GoodsTree.tb.tree("select", GoodsTree.tb.tree("find", node.pid).target);
            } else {
                GoodsTree.tb.tree("select", GoodsTree.tb.tree("find", roots[tmp - 1].id).target);
            }
        }
    };

    GoodsTree.down = function () {
        if (GoodsTree.isFocus == 1) {//光标在搜索框
            $("#goodsTb").focus();
            var data = GoodsTree.tb.tree("getRoots");
            if (data.length > 0) {
                GoodsTree.tb.tree("select", data[0].target);
            }
            return;
        }
        var node = GoodsTree.tb.tree("getSelected");
        if (!node) {
            return;
        }
        if (node.pid == 0) {
            if (node.state == "open") {
                GoodsTree.tb.tree("select", GoodsTree.tb.tree("find", node.children[0].id).target);
            } else {
                var roots = GoodsTree.tb.tree("getRoots");
                var tmp = 0;
                for (var i = 0; i < roots.length; i++) {
                    if (roots[i].id == node.id) {
                        tmp = i;
                        break;
                    }
                }
                if (tmp != roots.length - 1) {//非最后一个
                    GoodsTree.tb.tree("select", GoodsTree.tb.tree("find", roots[tmp + 1].id).target);
                } else {
                    $("#goodsTb").find('.tree-node-selected').removeClass('tree-node-selected');
                    $("#searchText").focus();
                    GoodsTree.set_text_value_position("searchText", -1);
                }
            }
        } else {//当前为叶子节点
            var roots = GoodsTree.tb.tree("getChildren", GoodsTree.tb.tree("find", node.pid).target);
            var tmp = 0;
            for (var i = 0; i < roots.length; i++) {
                if (roots[i].id == node.id) {
                    tmp = i;
                    break;
                }
            }
            if (tmp != roots.length - 1) {
                GoodsTree.tb.tree("select", GoodsTree.tb.tree("find", roots[tmp + 1].id).target);
            } else {//找父节点的下一个
                roots = GoodsTree.tb.tree("getRoots");
                tmp = 0;
                for (var i = 0; i < roots.length; i++) {
                    if (roots[i].id == node.pid) {
                        tmp = i;
                        break;
                    }
                }
                if (tmp != roots.length - 1) {
                    GoodsTree.tb.tree("select", GoodsTree.tb.tree("find", roots[tmp + 1].id).target);
                } else {
                    $("#goodsTb").find('.tree-node-selected').removeClass('tree-node-selected');
                    $("#searchText").focus();
                    GoodsTree.set_text_value_position("searchText", -1);
                }
            }
        }
    };

    GoodsTree.left = function () {
        var node = GoodsTree.tb.tree("getSelected");
        if (!node) {
            return;
        }
        if (node.pid != 0) {
            var p = GoodsTree.tb.tree("find", node.pid);
            GoodsTree.tb.tree("collapseAll", p.target);
            GoodsTree.tb.tree("select", p.target);
        } else {
            GoodsTree.tb.tree("collapseAll", node.target);
            $("#goodsTb").find('.tree-node-selected').removeClass('tree-node-selected');
            $("#searchText").focus();
            GoodsTree.set_text_value_position("searchText", -1);
        }
    };

    GoodsTree.right = function () {
        var node = GoodsTree.tb.tree("getSelected");
        if (!node) {
            return;
        }
        if (node.pid == 0) {
            GoodsTree.tb.tree("expandAll", node.target);
        }
    };

    GoodsTree.space = function () {
        var node = GoodsTree.tb.tree("getSelected");
        if (!node) {
            return;
        }
        if (node.checked == true) {//全选
            GoodsTree.tb.tree("uncheck", node.target);
        } else if (node.checkState == "indeterminate") {//部分选中
            GoodsTree.tb.tree("uncheck", node.target);
        } else {//未选中
            GoodsTree.tb.tree("check", node.target);
        }
    };

    GoodsTree.set_text_value_position = function(obj, spos){
        var tobj = document.getElementById(obj);
        if(spos<0)
            spos = tobj.value.length;
        if(tobj.setSelectionRange){ //兼容火狐,谷歌
            setTimeout(function(){
                        tobj.setSelectionRange(spos, spos);
                        tobj.focus();}
                    ,0);
        }else if(tobj.createTextRange){ //兼容IE
            var rng = tobj.createTextRange();
            rng.move('character', spos);
            rng.select();
        }
    };

    $(function () {
        GoodsTree.init();
        GoodsTree.tree();
    });
</script>

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north'" style="margin: 2px 5px;">
        <span class="textbox easyui-fluid searchbox" style="width: 441px;">
            <span
                    class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;">
                <a href="javascript:;"
                   class="textbox-icon searchbox-button"
                   icon-index="0" tabindex="-1"
                   style="width: 26px; height: 28px;" onclick="doSearch()"></a>
            </span>
            <input tabindex="1"
                   id="searchText" type="text" class="textbox-text validatebox-text" autocomplete="off"
                   autofocus="autofocus"
                   tabindex="" placeholder="请输入商品名称"
                   style="width: 413px; margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px;">
    </div>
    <div data-options="region:'center'">
        <ul id="goodsTb" tabindex="2"></ul>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">