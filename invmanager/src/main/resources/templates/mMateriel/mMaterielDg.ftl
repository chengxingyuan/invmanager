<!-- m_materiel.ftl 存货表 -->
<input type="hidden" id="isSingle" value="${isSingle}"/>
<input type="hidden" id="hidHouseId" value="${houseId}"/>
<input type="hidden" id="hidBindHouseId" value="${bindHouseId}"/>

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'50px'">
        <div id="mSales_btn" class="datagrid-toolbar"> </div>
        <form id="mSales_form" class="form-inline search-form" style="float: left">
            <#if houseId != 0>
                <div class="form-group">
                    <label>仓库</label>
                    <input id="houseId" name="houseId" class="easyui-combobox" value="${houseId}" readonly
                           data-options="url: '/wWearhouse/list', panelHeight:'auto', editable:false, valueField:'id',textField:'name'" style="width: 177px">
                </div>
            </#if>
            <#if bindHouseId != 0>
                <div class="form-group">
                    <label>仓库</label>
                    <input id="bindHouseId" name="bindHouseId" class="easyui-combobox" value="${bindHouseId}" readonly
                           data-options="url: '/wWearhouse/list', panelHeight:'auto', editable:false, valueField:'id',textField:'name'" style="width: 177px">
                </div>
            </#if>
            <div class="form-group">
                <label>查询内容 </label>&nbsp;
                <#--<input id="searchText" name="searchText" class="easyui-textbox" data-options="prompt:'商品名称/配件编码' ">-->
                <input id="searchText" name="searchText" placeholder="商品名/规格/编码/OE编码" oninput="checkInput()" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 176px;"/>
            </div>
            <div class="form-group">
                <label>适用车型 </label>&nbsp;
<#--                <input id="searchCar" name="searchCar" class="easyui-textbox" data-options="prompt:'适用车型'">-->
                <input id="searchCar" name="searchCar" placeholder="适用车型" oninput="checkInput()" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 176px;"/>
            </div>
            <#--<div class="form-group">-->
                <#--<label>查询内容</label>-->
                <#--<span class="textbox" style="width: 200px;">-->
                    <#--<input id="searchText" type="text" class="textbox-text validatebox-text" autocomplete="off" tabindex="" placeholder="商品名称\配件编码" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 200px;">-->
                <#--</span>-->
            <#--</div>-->
            <#--<div class="form-group">-->
                <#--<label>适用车型</label>-->
                 <#--<span class="textbox" style="width: 200px;">-->
                    <#--<input id="searchCar" type="text" class="textbox-text validatebox-text" autocomplete="off" tabindex="" placeholder="适用车型" style="margin: 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px; width: 200px;">-->
                <#--</span>-->
                <#--&lt;#&ndash;<span class="textbox easyui-fluid searchbox" style="width: 600px;">&ndash;&gt;-->
                    <#--&lt;#&ndash;<span&ndash;&gt;-->
                            <#--&lt;#&ndash;class="textbox-addon textbox-addon-right" style="right: 0px; top: 0px;">&ndash;&gt;-->
                        <#--&lt;#&ndash;<a id="aCar" href="javascript:;"&ndash;&gt;-->
                           <#--&lt;#&ndash;class="textbox-icon searchbox-button"&ndash;&gt;-->
                           <#--&lt;#&ndash;icon-index="0" tabindex="-1"&ndash;&gt;-->
                           <#--&lt;#&ndash;style="width: 26px; height: 28px;"></a>&ndash;&gt;-->
                    <#--&lt;#&ndash;</span>&ndash;&gt;-->
                    <#--&lt;#&ndash;<input type="hidden" id="carBrandId" />&ndash;&gt;-->
                    <#--&lt;#&ndash;<input tabindex="1"&ndash;&gt;-->
                           <#--&lt;#&ndash;id="car" type="text" class="textbox-text validatebox-text" autocomplete="off"&ndash;&gt;-->
                           <#--&lt;#&ndash;autofocus="autofocus"&ndash;&gt;-->
                           <#--&lt;#&ndash;tabindex="" readonly&ndash;&gt;-->
                           <#--&lt;#&ndash;style="width: 613px; margin: 0px 26px 0px 0px; padding-top: 0px; padding-bottom: 0px; height: 28px; line-height: 28px;">&ndash;&gt;-->
                <#--&lt;#&ndash;</span>&ndash;&gt;-->
            <#--</div>-->
        </form>
<#--        <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询</button>-->
    </div>
    <div data-options="region:'center'">
        <ul id="goodsTb" tabindex="2"></ul>
    </div>
</div>

<script src="/bizjs/mMaterielDg.js?t=3"></script>