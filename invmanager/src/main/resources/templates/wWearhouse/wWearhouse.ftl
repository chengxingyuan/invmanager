<!-- w_wearhouse.ftl  -->
<#include "./../common/lib/csslib.ftl">
<div class="easyui-panel" data-options="fit:true,border:false">
    <div class="easyui-layout" data-options="fit:true,border:false">
        <div data-options="region:'north',height:'100px'">
            <div id="wWearhouse_btn" class="datagrid-toolbar">
                <a href="javascript:void(0)" id="mb" class="easyui-menubutton"
                   data-options="menu:'#mm',iconCls:'icon-search'">联查</a>
                <div id="mm" style="width:150px;">
                    <div id="lckcl">库存量</div>
                    <div id="lcrkd">入库单</div>
                    <div id="lcckd">出库单</div>
                </div>
            </div>
            <form id="wWearhouse_form" class="form-inline search-form" style="float: left">

                <div class="form-group">
                    <label>仓库名</label>
                    <input id="searchName" name="name" class="easyui-textbox" data-options="prompt:'仓库名称'">
                </div>
            </form>
            <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询(Enter)</button>
        </div>
        <div data-options="region:'center',border:false">
            <table id="wWearhouse_table"></table>
        </div>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/wWearhouse.js"></script>