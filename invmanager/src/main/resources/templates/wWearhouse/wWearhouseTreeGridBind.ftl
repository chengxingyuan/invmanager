<!-- m_materiel.ftl 存货表 -->
<#--<#include "./../common/lib/csslib.ftl">-->
<input type="hidden" id="materielId" value="${materielId}"/>
<input type="hidden" id="house" value="${house}"/>
<script>

    $.extend($.fn.treegrid.methods, {
        //iscontains是否包含父节点（即子节点被选中时是否也取父节点）
        getAllChecked: function (jq, iscontains) {
            var keyValues = new Array();
            /*
              tree-checkbox2 有子节点被选中的css
              tree-checkbox1 节点被选中的css
              tree-checkbox0 节点未选中的css
            */
            var checkNodes = jq.treegrid("getPanel").find(".tree-checkbox1");
            for (var i = 0; i < checkNodes.length; i++) {
                var keyValue1 = $($(checkNodes[i]).closest('tr')[0]).attr("node-id");
                if (keyValue1.substr(0, 1) == "h") {
                    keyValues.push(keyValue1);
                }
            }

            if (iscontains) {
                var childCheckNodes = jq.treegrid("getPanel").find(".tree-checkbox2");
                for (var i = 0; i < childCheckNodes.length; i++) {
                    var keyValue2 = $($(childCheckNodes[i]).closest('tr')[0]).attr("node-id");
                    if (keyValue2.substr(0, 1) == "h") {
                        keyValues.push(keyValue2);
                    }
                }
            }

            return keyValues;
        }
    });

    var KW = {initOk: 0};

    //获取选中的结点
    KW.getResult = function () {
        var result = "";
        var kv = $("#kwTb").treegrid("getAllChecked", true);
        $.each(kv, function (i, n) {
            var node = $("#kwTb").treegrid("find", n);
            var p = $("#kwTb").treegrid("getParent", node.id);
            result += "," + node.id.substr(1) + "_" + node.text + "_" + node.pid + "_" + p.text + "_" +
                    ($("#isDefault_" + node.id).is(':checked') ? 1 : 0);
        });

        if (result != "") {
            result = result.substr(1);
        }
        return result;
    };

    $(function () {
        $("#kwTb").treegrid({
            url: '/wWearhouse/treeGridBind?materielId=' + $("#materielId").val(),
            singleSelect: false,
            idField: 'id',
            treeField: 'text',
            checkbox: true,
            onlyLeafCheck: true,
            columns: [[
                {title: '仓库/库位', field: 'text', width: 250},
                {title: 'pid', field: 'pid', hidden: true},
                {
                    title: '是否默认', field: 'isDefault', width: 200, formatter: function (value, row, index) {
                        if (row.id.substr(0, 1) != "h") {
                            return "";
                        }
                        return "<input name='isDefault' style='display:none' id='isDefault_" + row.id + "' type='radio'/>";
                    }
                    
                }
            ]],
            onClickRow: function (row) {
                $("#kwTb").treegrid("unselectAll");
            },
            onLoadSuccess: function () {
                var house = $("#house").val();
                if (house != "" && house != undefined) {
                    var arr = house.split(",");
                    for (var i = 0; i < arr.length; i++) {
                        var arr2 = arr[i].split("_");
                        var id = "h" + arr2[0];
                        var isDefault = arr2[4];
                        $('#kwTb').treegrid('checkNode', id);
                        $("#isDefault_" + id).show();
                        if (isDefault == 1) {
                            $("#isDefault_" + id).prop("checked", true);
                        }
                    }
                }
                KW.initOk = 1;
            },
            onCheckNode: function (row, checked) {
                if (KW.initOk == 0) {
                    return;
                }

                if (checked == true) {
                    $("#isDefault_" + row.id).show();
                } else {
                    $("#isDefault_" + row.id).hide();
                }

                if (checked == false && $("#isDefault_" + row.id).is(':checked')) {
                    $("#isDefault_" + row.id).prop("checked", false);
                    //其他选中状态的，取第一个为默认勾选
                    var kv = $("#kwTb").treegrid("getAllChecked", true);
                    if (kv && kv.length > 0) {
                        $("#isDefault_" + kv[0]).prop("checked", true);
                    }
                }
                if (checked == true && $("input[name='isDefault']:checked").length == 0) {
                    //选中时，若没有勾选状态的，将其至于勾选状态
                    $("#isDefault_" + row.id).prop("checked", true);
                }
            }
        });
    });

</script>

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <ul id="kwTb"></ul>
    </div>
</div>
