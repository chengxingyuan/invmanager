<!-- w_wearhouse.ftl  -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">仓库名称</label>
            <div class="col-sm-8">
                <input id="houseName" class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'仓库名称',required:true,validType:'length[1,64]'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">负责人</label>
            <div class="col-sm-8">
                <input id="housePrincipal" class="easyui-textbox" name="principal" style="width: 100%;"
                       data-options="prompt:'负责人',validType:'length[0,255]'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">负责人电话</label>
            <div class="col-sm-8">
                <input id="housePrincipalTel" class="easyui-textbox" name="principalTel" style="width: 100%;"
                       data-options="prompt:'负责人电话',validType:'length[0,11]'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">地址</label>
            <div class="col-sm-8">
                <input id="houseLocation" class="easyui-textbox" name="location" style="width: 100%;"
                       data-options="prompt:'地址',validType:'length[0,128]'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">状态</label>
            <div class="col-sm-8">
                <select id="houseStatus" class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',panelHeight:'auto',editable:false">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
    </form>
</div>
<script>
    var WWearhouseEdit = {};
    WWearhouseEdit.reset = function () {
        $("#houseName").textbox("setValue", "");
        $("#housePrincipal").textbox("setValue", "");
        $("#housePrincipalTel").textbox("setValue", "");
        $("#houseLocation").textbox("setValue", "");
        $("#houseStatus").combobox("setValue", 0);

        setTimeout("$(\"#houseName\").next().find(\".textbox-text\").focus()", 500);
    };
    $(function () {
        setTimeout("$(\"#houseName\").next().find(\".textbox-text\").focus()", 500);
    });
</script>