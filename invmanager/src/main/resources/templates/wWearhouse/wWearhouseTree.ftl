<!-- m_materiel.ftl 存货表 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <ul id="kwTb"></ul>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script>
    $(function () {
        $("#kwTb").tree({
            url: '/wWearhouse/tree',
            singleSelect: true
        })
    });

</script>