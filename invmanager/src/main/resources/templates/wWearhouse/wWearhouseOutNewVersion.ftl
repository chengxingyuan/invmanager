<!-- m_materiel.ftl 存货表 -->
<#--<#include "./../common/lib/csslib.ftl">-->
<input type="hidden" id="skuId" value="${skuId}" />

<script>
    var KW = {editId: "-1", result: ""};
    KW.getResult = function () {
        KW.result = "";
        if(KW.editId != "-1") {
            $("#kwTb").datagrid('endEdit', KW.editId);
            KW.editId = "-1";
        }
        var data = $("#kwTb").datagrid("getRows");
        for(var i=0; i<data.length; i++) {//data[i] 仓库




                    if(!KW.isEmpty(data[i].outNum)) {
                        if(Number(data[i].num) < Number(data[i].outNum)) {
                            $yd.alert("出库数目不能大于当前库存");
                            return "error";
                        }
                        KW.result += "," + data[i].text + "_" + data[i].num + "_" + data[i].outNum;
                    }


        }

        if(KW.result != "") {
            KW.result = KW.result.substr(1);
        }
        return KW.result;
    };

    KW.isEmpty = function(value) {
        if (value == undefined || value == null || value == "" || value == "0") {
            return true;
        }
        return false;
    }

    $(function () {
        $("#kwTb").datagrid({
            url: '/repositoryCount/getSpecific?skuId=' + $("#skuId").val(), //type 0出库 1入库
            singleSelect: true,
            columns:[[
                {title:'仓库',field:'repositoryName',width:200},
                {title:'库存数量',field:'count',width:100}
                {title:'出库数目',field:'outNum',width:100,editor:{
                        type:'numberbox',
                        options:{
                            min: 1, precision: 0
                        }
                    },formatter:function (value,row) {

                }},
                {title:'pid', field:'pid', hidden:true}
            ]],
            onClickCell: function (field,row) {
                if (KW.editId != row.id) {
                    if (KW.editId != "-1") {
                        $("#kwTb").datagrid('endEdit', KW.editId);
                    }
                    KW.editId = row.id;
                }
                if(row.id.substr(0,1) == "b" && field == "outNum") {
                    $("#kwTb").datagrid('beginEdit', row.id);
                }
            },
            onClickRow:function () {
                $("#kwTb").treegrid("unselectAll");
            },

        });
    });

</script>

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <ul id="kwTb"></ul>
    </div>
</div>
