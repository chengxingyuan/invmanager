<!-- m_materiel.ftl 存货表 -->
<#--<#include "./../common/lib/csslib.ftl">-->
<input type="hidden" id="skuId" value="${skuId}" />
<input type="hidden" id="house" value="${house}" />
<input type="hidden" id="houseId" value="${houseId}" />

<script>
    var KW = {editId: "-1", result: ""};

    //获取选中的结点
    KW.getResult = function () {
        KW.result = "";
        if(KW.editId != "-1") {
            $("#kwTb").treegrid('endEdit', KW.editId);
            KW.editId = "-1";
        }
        var data = $("#kwTb").treegrid("getData");
        for(var i=0; i<data.length; i++) {//data[i] 仓库
            var child = data[i].children;
            for(var j=0; j<child.length; j++) {//child[j] 库位
                var child2 = child[j].children;
                for(var k=0; k<child2.length; k++) {//child2[k] 批次
                    if(!KW.isEmpty(child2[k].outNum)) {
                        if(Number(child2[k].num) < Number(child2[k].outNum)) {
                            $yd.alert("出库数目不能大于当前库存");
                            return "error";
                        }
                        KW.result += "," + data[i].id + "_" + data[i].text + "_" + child[j].id.substr(1) + "_" + child[j].text + "_" + child2[k].text + "_" + child2[k].num + "_" + child2[k].outNum;
                    }
                }
            }
        }

        if(KW.result != "") {
            KW.result = KW.result.substr(1);
        }
        return KW.result;
    };

    KW.isEmpty = function(value) {
        if (value == undefined || value == null || value == "" || value == "0") {
            return true;
        }
        return false;
    }

    $(function () {
        $("#kwTb").treegrid({
            url: '/wWearhouse/treeGrid?skuId=' + $("#skuId").val() + "&type=0&houseId=" + $("#houseId").val(), //type 0出库 1入库
            singleSelect: true,
            idField:'id',
            treeField:'text',
            columns:[[
                {title:'仓库/库位/批次',field:'text',width:250},
                {title:'库存',field:'num',width:100},
                {title:'出库数目',field:'outNum',width:100,editor:{
                        type:'numberbox',
                        options:{
                            min: 1, precision: 0
                        }
                    }},
                {title:'pid', field:'pid', hidden:true}
            ]],
            onClickCell: function (field,row) {
                if (KW.editId != row.id) {
                    if (KW.editId != "-1") {
                        $("#kwTb").treegrid('endEdit', KW.editId);
                    }
                    KW.editId = row.id;
                }
                if(row.id.substr(0,1) == "b" && field == "outNum") {
                    $("#kwTb").treegrid('beginEdit', row.id);
                }
            },
            onClickRow:function () {
                $("#kwTb").treegrid("unselectAll");
            },
            onLoadSuccess: function () {
                var house = $("#house").val();
                if(!KW.isEmpty(house)) {
                    var arr = house.split(",");
                    for(var i=0; i<arr.length; i++) {
                        var arr2 = arr[i].split("_");
                        var id = "b" + arr2[4];
                        var num = arr2[6];

                        $('#kwTb').treegrid('update',{
                            id: id,
                            row: {
                                outNum: num
                            }
                        });
                    }
                }
            }
        });
    });

</script>

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <ul id="kwTb"></ul>
    </div>
</div>
