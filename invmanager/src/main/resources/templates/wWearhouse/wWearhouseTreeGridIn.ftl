<!-- m_materiel.ftl 存货表 -->
<#--<#include "./../common/lib/csslib.ftl">-->
<input type="hidden" id="skuId" value="${skuId}" />
<input type="hidden" id="house" value="${house}" />
<input type="hidden" id="hidHouseId" value="${houseId}" />
<script>
    var KW = {lastId: -1};

    //获取选中的结点
    KW.getResult = function () {
        var node = $("#kwTb").treegrid("getSelected");
        var p = $("#kwTb").treegrid("getParent", node.id);
        return node.pid + "_" + p.text + "_" + node.id.substr(1) + "_" + node.text;
    };

    $(function () {
        $("#kwTb").treegrid({
            url: '/wWearhouse/treeGrid?skuId=' + $("#skuId").val() + "&type=1&houseId=" + $("#hidHouseId").val(),
            singleSelect: true,
            idField:'id',
            treeField:'text',
            columns:[[
                {title:'仓库/库位',field:'text',width:250},
                {title:'库存',field:'num',width:200},
                {title:'pid', field:'pid', hidden:true}
            ]],
            onClickRow:function (row) {
                if(row.id.substr(0,1) != "h") {
                    $("#kwTb").treegrid("unselectAll");
                    if(KW.lastId != -1) {
                        $("#kwTb").treegrid("select", KW.lastId);
                    }
                } else {
                    KW.lastId = row.id;
                }
            },
            onLoadSuccess: function () {
                var house = $("#house").val();
                if(house != "" && house != undefined) {
                    var arr = house.split("_");
                    $("#kwTb").treegrid("select", "h" + arr[2]);
                    KW.lastId = "h" + arr[2];
                }
            }
        });
    });

</script>

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <ul id="kwTb"></ul>
    </div>
</div>
