<!-- 新建采购单 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <div class="easyui-layout" data-options="fit:true">
            <form id="myForm" method="post">
                <input type="hidden" id="goods" name="goods"/>
                <input type="hidden" id="type" name="type"/>
                <input type="hidden" name="token" value="${addSaleReturnToken}">

                <div data-options="region:'north',height:93">
                    <div id="pPurchase_btn" class="datagrid-toolbar">

                    </div>
                    <div  class="form-inline search-form">
                        <div class="form-group">
                            <label>退货时间</label>
                            <input name="ptime" tabindex="-1"/>
                        </div>
                        <div class="form-group" style="line-height: 20px">
                            <label>客户</label>
                            <input id="relativeId" name="relativeId" />
                            </select>
                        </div>
                    </div>
                </div>
                <div data-options="region:'center'">
                    <table id="pPurchase_table" tabindex="2"></table>
                </div>
                <div data-options="region:'south',height:220, title:'结算信息'">
                    <br>
                    <label style="margin-left: 20px">总计金额:</label>
                    ￥<input id="totalMoney" name="totalMoney" value="0" style="border:none;height: 30px;width: 60px" readonly />
                    <label style="margin-left: 30px">实退金额:</label>
                    ￥<input id="realPay" name="realPay" style="border:none;height: 30px;width: 90px;border-bottom:1px solid grey;" onkeyup="clearNoNum(this)"/>
                    <label style="margin-left: 30px">结算情景:</label>
                    <input id="payStatus" name="payStatus"  />
                    <label style="margin-left: 10px">备注:</label>
                    <input id="remark" name="remark" style="border:none;height: 30px;width: 350px;border-bottom:1px solid grey;"/>

                </div>
            </form>
        </div>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/saleReturnAdd.js?t=4"></script>