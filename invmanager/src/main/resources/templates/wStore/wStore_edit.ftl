<!-- w_store.ftl 门店 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">名称</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'名称'" required="required">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">店铺管理人</label>
            <div class="col-sm-8">
                <select id="userId" class="easyui-combobox" name="userId" style="width:100%;"
                        data-options="prompt:'店铺管理人',editable:false" required="required">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">店铺联系电话</label>
            <div class="col-sm-8">
                <input id="mobile" class="easyui-textbox" name="mobile" style="width: 100%;"
                       data-options="prompt:'店铺联系电话'" required="required">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">详细地址</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="addr" style="width: 100%;"
                       data-options="prompt:'详细地址'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">企业名称</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="companyName" style="width: 100%;"
                       data-options="prompt:'企业名称'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">企业税号</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="companyNum" style="width: 100%;"
                       data-options="prompt:'企业税号'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">状态</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">简单描述</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="memo" style="width: 100%;height: 60px;"
                       data-options="prompt:'简单描述',multiline:true">
            </div>
        </div>
    </form>
</div>
