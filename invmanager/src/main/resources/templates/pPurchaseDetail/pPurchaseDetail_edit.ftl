<!-- p_purchase_detail.ftl 采购单详情 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">采购总单id</label>
            <div class="col-sm-8">
                <select id="purchaseId" class="easyui-combobox" name="purchaseId" style="width:100%;"
                        data-options="prompt:'采购总单id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">商品id</label>
            <div class="col-sm-8">
                <select id="skuId" class="easyui-combobox" name="skuId" style="width:100%;"
                        data-options="prompt:'商品id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">预购数量</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="count" style="width:100%;"
                        data-options="prompt:'预购数量',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">实际采购数量</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="countActual" style="width:100%;"
                        data-options="prompt:'实际采购数量',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">预计采购金额</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="amount" style="width:100%;"
                        data-options="prompt:'预计采购金额',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">实际采购金额</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="amountActual" style="width:100%;"
                        data-options="prompt:'实际采购金额',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">折扣金额</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="amountDiscount" style="width:100%;"
                        data-options="prompt:'折扣金额',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">备注</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="remark" style="width: 100%;"
                       data-options="prompt:'备注'">
            </div>
        </div>
    </form>
</div>
