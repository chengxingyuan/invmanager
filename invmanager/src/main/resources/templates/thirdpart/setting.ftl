<!-- sys_user.ftl 管理员账号 -->
<#include "./../common/lib/csslib.ftl">
<input id="cTenantId" type="hidden" value="${Session.current_user.tenantId}">
<div class="easyui-panel" title="" data-options="iconCls:'icon-save',fit:true" style="overflow: hidden;">
    <form class="form-horizontal mform" method="post">
        <div class="form-group">
            <label class="col-sm-2 control-label">选择账号</label>
            <div class="col-sm-4">
                <select id="tenant" name="tenantId" class="easyui-combobox" style="width: 100%;"
                        data-options="panelHeight:'auto',prompt:'选择账号',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
                <a onclick="submit()" href="javascript:;" class="btn btn-sm btn-primary">提交</a>
            </div>
        </div>
    </form>
</div>

<#include "./../common/lib/jslib.ftl">
<script>

    $(function () {
        var cb = new $yd.combobox($('#tenant'));
        cb.load("/sysTenant/select");
        cb.setValue($('#cTenantId').val());
    });

    function submit() {
        $yd.confirm("确认要切换账号吗？", function () {
            $yd.form.create(".mform").url("/setting/save").submit(function () {
                top.location.href = "/";
//            $yd.alert("操作成功", function () {
//            });
            })
        });
    }

</script>