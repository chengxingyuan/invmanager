<!-- sys_user.ftl 管理员账号 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-panel" title="天天爱车配置" data-options="iconCls:'icon-save',fit:true" style="overflow: hidden;">
    <form class="form-horizontal mform" method="post">
    <#list settings as setting>
        <#if setting.children??>
            <h5 class="title">价格配置</h5>
            <#list setting.children as  sc >
                <div class="form-group">
                    <label class="col-sm-2 control-label">${sc.text!}</label>
                    <div class="col-sm-8">
                        <select class="easyui-combobox" name="${sc.code!}" style="width:100%;"
                                data-options="prompt:'${sc.text!}',panelHeight:'auto',editable:false,url:'/sysDict/materiel_PL'">
                        </select>
                    </div>
                </div>
            </#list>
        <#else>
            <div class="form-group">
                <label class="col-sm-2 control-label">${setting.text!}</label>
                <div class="col-sm-8">
                    <#if setting.code?contains('_is_')>
                    <#--<input class="easyui-switchbutton" name="${setting.code!}">-->
                        <input class="switchbutton" name="${setting.code!}" value="0">
                    <#else>
                        <input class="easyui-textbox" name="${setting.code!}" style="width:100%;"
                               data-options="prompt:'${setting.text!}'"/>
                    </#if>
                </div>
            </div>
        </#if>
    </#list>

        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
                <a onclick="submit()" href="javascript:;" class="btn btn-sm btn-primary">保存</a>
            </div>
        </div>
    </form>
</div>

<#include "./../common/lib/jslib.ftl">
<script>

    $(function () {
        var form = $yd.form.create(".mform");
        $yd.get("/sysDict/form/tticar", function (data) {
            form.load(data);
        });

        $(".switchbutton").switchbutton({
            checked: true,
            onChange: function (checked) {
                this.value = 2;
            }
        })

    });

    function submit() {
        $yd.form.create(".mform").url("/sysDict/form").submit(function () {
            $yd.alert("操作成功");
        })
    }

</script>