<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>库存管理软件登录</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/public/login/login.css" media="all"/>
</head>
<body>
<#--<p class="projectTitle">库存管理软件登录</p>-->
<div class="login-left"></div>
<div class="login">
    <div style="position:relative;">
        <div class="login-right">
            <h1>库存管理系统登录</h1>
            <div class="form">
                <p class="message"></p>
                <div class="layui-form-item userName">
                    <i></i>
                    <input id="username" placeholder="用户名" type="text" value=""
                           autocomplete="off">
                </div>
                <div class="layui-form-item passWord">
                    <i></i>
                    <input id="password" placeholder="密码" type="password" value=""
                           autocomplete="off">
                </div>
                <button onclick="login(this)">登录</button>
            </div>
        </div>
        <div class="bgImg"></div>
    </div>
</div>
<script src="/public/assets/js/jquery-2.1.4.min.js"></script>
<script>
    function login(obj) {
        $('#username,#password').removeClass('danger');
        var username = $('#username').val();
        var password = $('#password').val();
        if (!username) {
            $('#username').focus().addClass('danger');
            return;
        }
        if (!password) {
            $('#password').focus().addClass('danger');
            return;
        }
//        $(obj).html("登录中...");
        $.post("login", {username: username, password: password}, function (data) {
            console.info(data);
            if (data.code == 0) {
                location.href = "/";
            } else {
                $('p.message').html(data.message).show();
            }
        })
    }

    $(function () {
        if (window.top != null && window.top.document.URL != document.URL) {
            window.top.location = document.URL;
        }

        $(document).ready(function(e) {
            $(this).keydown(function (e){
                if(e.which == "13"){
                    login();//触发该事件
                }
            })
        });
    })
</script>
</body>
</html>