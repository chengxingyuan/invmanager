<!-- change_repository.ftl 调拨单 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'100px'">
        <div id="changeRepository_btn" class="datagrid-toolbar">

        </div>
        <form id="changeRepository_form" class="form-inline search-form" style="float: left">
            <div class="form-group">
                <label>调拨单号</label>
                <input name="code" class="easyui-textbox" data-options="prompt:'调拨单号'">
            </div>
        </form>
        <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询</button>
    </div>
    <div data-options="region:'center'">
        <table id="changeRepository_table"></table>
    </div>
    <div data-options="region:'south',height:'30%', title:'调拨明细'">
        <div id="checkGoodsBtn" class="datagrid-toolbar" style="display: none"> </div>

        <table id="changeGoods_table" ></table>

        <input type="hidden" id="changeCode">
    </div>
</div>

<#include "./../common/lib/jslib.ftl">
<script>

//    function getDate(){
//        //获取当前时间
//        var date = new Date();
//        //格式化为本地时间格式
//        var date1 = date.toLocaleString();
//        //获取div
//        var div1 = document.getElementById("auditTime");
//        //将时间写入div
//        div1.innerHTML = date1;
//    }
//    setInterval("getDate()",1000);
</script>
<script src="/bizjs/changeRepository.js"></script>