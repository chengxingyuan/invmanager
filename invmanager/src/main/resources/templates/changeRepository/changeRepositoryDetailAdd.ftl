<!-- 新建调拨单 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <div class="easyui-layout" data-options="fit:true">
            <form id="myForm" method="post">
                <input type="hidden" id="goods" name="goods"/>
                <input type="hidden" id="type" name="type"/>

                <div data-options="region:'north',height:93">
                    <div id="changeRepository_btn" class="datagrid-toolbar">

                    </div>
                    <div  class="form-inline search-form">
                        <div class="form-group" style="line-height: 20px">
                            <label>源仓库</label>
                            <select name="fromWearhouseId"  id="fromWearhouseId" style="width: 70px" class="easyui-combobox" data-options="prompt:'所在仓库',editable:false">
                            </select>
                        </div>
                        <div class="form-group" style="line-height: 20px">
                            <label>目的仓库</label>
                            <select name="toWearhouseId"  id="toWearhouseId" style="width: 70px" class="easyui-combobox" data-options="prompt:'所在仓库',editable:false">
                            </select>
                        </div>
                        <div class="form-group">
                                <label>调拨成本</label>
                                <input name="money" id="money" class="easyui-numberbox" data-options="prompt:'调拨成本',min:0,precision:2" required="required">元
                        </div>
                    </div>
                </div>
                <div data-options="region:'center'">
                    <table id="changeRepository_table"></table>
                </div>
            </form>
        </div>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">
<script src="/bizjs/changeRepositoryAdd.js"></script>