<!-- in_repository_detail.ftl 入库单详情 -->
<#include "./../common/lib/csslib.ftl">
<#setting number_format="0.#">

<div class="easyui-layout" data-options="fit:true">

    <div data-options="region:'north',height:'auto'">
        <div id="changeRepositoryDetail_btn" class="datagrid-toolbar">

        </div>
        <form id="changeRepositoryDetail_form" class="form-inline search-form" style="float: left;">
            <div class="form-group">
                <label>单据号</label>
                <input name="code" id="code" class="easyui-textbox" data-options="prompt:'请输入单据号'" value="${code}" readonly="readonly">
            </div>
            <div class="form-group">
                <label>调拨成本</label>
                <input name="money" id="money" class="easyui-textbox" value="${money}"   readonly="readonly">
            </div>
        </form>

    </div>
    <div data-options="region:'center'" style="margin-top: 0px">
        <table id="changeRepositoryDetail_table"></table>
    </div>
</div>



<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/changeRepositoryDetailNew.js"></script>
<#--<script>-->
    <#--function addTab(title, url){-->
        <#--if ($('#mtab-main').tabs('exists', title)){-->
            <#--$('#mtab-main').tabs('select', title);-->
        <#--} else {-->
            <#--var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';-->
            <#--$('#mtab-main').tabs('add',{-->
                <#--title:title,-->
                <#--content:content,-->
                <#--closable:true-->
            <#--});-->
        <#--}-->
    <#--}-->
<#--</script>-->