<!-- m_sales_detail.ftl  -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">销售订单</label>
            <div class="col-sm-8">
                <select id="salesId" class="easyui-combobox" name="salesId" style="width:100%;"
                        data-options="prompt:'销售订单',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">商品规格id</label>
            <div class="col-sm-8">
                <select id="skuId" class="easyui-combobox" name="skuId" style="width:100%;"
                        data-options="prompt:'商品规格id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">批次</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="batchCode" style="width: 100%;"
                       data-options="prompt:'批次'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">售价</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="price" style="width: 100%;"
                       data-options="prompt:'售价'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">销售数量</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="num" style="width:100%;"
                        data-options="prompt:'销售数量',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">总金额</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="fee" style="width: 100%;"
                       data-options="prompt:'总金额'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">备注</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="remark" style="width: 100%;"
                       data-options="prompt:'备注'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">是否删除</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
    </form>
</div>
