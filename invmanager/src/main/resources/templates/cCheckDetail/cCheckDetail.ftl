<!-- c_check_detail.ftl 盘点单详情列表 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'113px'">
        <div id="cCheckDetail_btn" class="datagrid-toolbar"></div>
        <form class="form-inline search-form">
            <input type="hidden" id="checkId" name="checkId" value="${checkId}">
            <input type="hidden" id="checkStatus" value="${check.status}">
            <div class="form-group">
                <label style="color:red">盘点状态</label>
                <input name="status" class="easyui-textbox" value="<#if check.status== 0>未校正<#elseif check.status==1>已校正<#else >已作废</#if>" readonly>
            </div>
            <div class="form-group">
                <label>盘点日期</label>
                <input name="ctime" class="easyui-datebox" value="${ctime}" readonly>
            </div>
            <div class="form-group">
                <label>盘点单号</label>
                <input name="code" class="easyui-textbox" value="${check.code}" readonly>
            </div>
            <div class="form-group">
                <label>盘点仓库</label>
                <input name="houseName" class="easyui-textbox" value="${houseName}" readonly>
            </div>
        </form>
        <div style="color:red; margin-left: 30px;display: none" id="tips">盘点过程中库存出现变化，库存数量中（）为实时库存量</div>
    </div>
    <div data-options="region:'center'">
        <table id="cCheckDetail_table"></table>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/cCheckDetail.js?t=1"></script>