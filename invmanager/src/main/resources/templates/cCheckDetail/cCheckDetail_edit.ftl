<!-- c_check_detail.ftl 盘点单详情 -->
<!--
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">盘点总单id</label>
            <div class="col-sm-8">
                <select id="checkId" class="easyui-combobox" name="checkId" style="width:100%;"
                        data-options="prompt:'盘点总单id'">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">商品skuid</label>
            <div class="col-sm-8">
                <select id="skuId" class="easyui-combobox" name="skuId" style="width:100%;"
                        data-options="prompt:'商品skuid'">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">批次号</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="batchCode" style="width: 100%;"
                       data-options="prompt:'批次号'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">库存数量</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="repCount" style="width:100%;"
                        data-options="prompt:'库存数量',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">盘点数量</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="checkCount" style="width:100%;"
                        data-options="prompt:'盘点数量',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">成本单价</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="amount" style="width: 100%;"
                       data-options="prompt:'成本单价'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">状态（0 有效，1无效）</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">操作人姓名</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="username" style="width: 100%;"
                       data-options="prompt:'操作人姓名'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">备注</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="remark" style="width: 100%;"
                       data-options="prompt:'备注'">
            </div>
        </div>
    </form>
</div>
-->