<!-- w_position.ftl 库位表 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'100px'">
        <div id="wPosition_btn" class="datagrid-toolbar">

        </div>
        <form id="wPosition_form" class="form-inline search-form" style="float: left;">
            <div class="form-group" style="line-height: 20px">
                <label>仓库</label>
                <select name="wearhouseIdForSearch"  id="wearhouseIdForSearch" style="width: 170px" class="easyui-combobox" data-options="prompt:'所在仓库',editable:false">
                </select>
            </div>
            <div class="form-group">
                <label>库位名</label>
                <input id="searchName" name="name" class="easyui-textbox" data-options="prompt:'库位名称'">
            </div>
        </form>
        <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询(Enter)</button>
    </div>
    <div data-options="region:'center'">
        <table id="wPosition_table"></table>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/wPosition.js"></script>