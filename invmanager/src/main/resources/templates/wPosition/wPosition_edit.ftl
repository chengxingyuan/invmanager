<!-- w_position.ftl 库位表 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">所属仓库</label>
            <div class="col-sm-8">
                <input id="wearhouseId" name="wearhouseId" class="easyui-textbox" style="width: 100%;">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">库位名称</label>
            <div class="col-sm-8">
                <input id="positionName" class="easyui-textbox" name="name" style="width: 100%;"
                       data-options="prompt:'库位名称',required:true,validType:'length[1,64]'">
            </div>
        </div>
        <#--<div class="form-group">-->
            <#--<label class="col-sm-2 control-label">库位编码</label>-->
            <#--<div class="col-sm-8">-->
                <#--<input class="easyui-textbox" name="code" style="width: 100%;"-->
                       <#--data-options="prompt:'编码'">-->
            <#--</div>-->
        <#--</div>-->
        <div class="form-group">
            <label class="col-sm-2 control-label">状态</label>
            <div class="col-sm-8">
                <select id="positionStatus" class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
    </form>
</div>
<script>
    var WPositionEdit = {};
    WPositionEdit.reset = function () {
        $("#positionName").textbox("setValue", "");
        $("#positionStatus").combobox("setValue", 0);

        setTimeout("$(\"#positionName\").next().find(\".textbox-text\").focus()", 500);
    };
    $(function () {
        setTimeout("$(\"#positionName\").next().find(\".textbox-text\").focus()", 500);
        $("#wearhouseId").combobox({
            prompt: '仓库',
            url: "/wWearhouse/list",
            panelHeight: 'auto',
            editable: false,
            required: true,
            valueField: 'id',
            textField: 'name'
        });
    });
</script>