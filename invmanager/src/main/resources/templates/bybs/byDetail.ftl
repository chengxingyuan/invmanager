<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <form id="myForm" method="post">
        <input type="hidden" id="id" name="id" value="${bybs.id}"/>
        <input type="hidden" id="status" value="${bybs.status}"/>
        <input type="hidden" id="goods" name="goods"/>

        <div data-options="region:'north',height:185">
            <div id="part_btn" class="datagrid-toolbar">

            </div>
            <div class="form-inline search-form">
                <div class="form-group">
                    <label>单据类型</label>
                    <input id="orderType" readonly value="${bybs.orderType}" class="easyui-combobox" name="orderType" data-options="prompt:'单据类型',editable:false,url:'/sysDict/bybs_type'">
                </div>
                <div class="form-group">
                    <label>日期</label>
                    <input id="orderTime" name="orderTime" class="easyui-datebox" value="${orderTime}" readonly/>
                </div>
                <div class="form-group" style="line-height: 20px;">
                    <label>仓库</label>
                    <input id="houseId" name="houseId" class="easyui-textbox" style="width: 177px" value="${bybs.wearhouseId}" readonly>
                </div>
                <div class="form-group">
                    <label>状态</label>
                    <select class="easyui-combobox" style="width:177px;" readonly>
                        <option value="0" <#if bybs.status == 0>selected</#if> >待审核</option>
                        <option value="1" <#if bybs.status == 1>selected</#if> >已审核</option>
                    </select>
                </div><br>
                <div class="form-group">
                    <label>创建人</label>
                    <input id="cname" name="cname" class="easyui-textbox" value="${bybs.cname}" readonly/>
                </div>
                <div class="form-group">
                    <label>创建日期</label>
                    <input id="ctime" name="ctime" class="easyui-datebox" value="${ctime}" readonly/>
                </div>
                <div class="form-group">
                    <label>审核人</label>
                    <input id="oname" name="oname" class="easyui-textbox" value="${bybs.oname}" readonly/>
                </div>
                <div class="form-group">
                    <label>审核日期</label>
                    <input id="otime" name="otime" class="easyui-datebox" value="${otime}" readonly/>
                </div><br>
                <div class="form-group">
                    <label>备注</label>
                    <input id="remark" name="remark" class="easyui-textbox" style="width: 440px" value="${bybs.remark}">
                </div><br>
            </div>
        </div>

        <div data-options="region:'center'">
            <table id="part_table" tabindex="-1"></table>
        </div>
    </form>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/byDetail.js?t=4"></script>