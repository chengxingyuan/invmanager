<#include "./../common/lib/csslib.ftl">
<input type="hidden" id="hidSearchText" value="${searchText}" />
<input type="hidden" id="hidStartTime" value="${startTime}" />
<input type="hidden" id="hidEndTime" value="${endTime}" />
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'100px'">
        <div id="bybs_btn" class="datagrid-toolbar">

        </div>
        <form id="bybs_form" class="form-inline search-form" style="float: left">

            <div class="form-group">
                <label>日期</label>
                <input id="startTime" name="startTime" style="width: 100px;" class="easyui-datebox">-<input id="endTime" name="endTime" style="width: 100px;" class="easyui-datebox">
            </div>
            <div class="form-group">
                <label>仓库</label>
                <input id="houseId" name="wearhouseId" class="easyui-textbox" style="width: 177px">
            </div>
            <div class="form-group">
                <label>单据类型</label>
                <input id="orderType" class="easyui-combobox" name="orderType" data-options="prompt:'单据类型',editable:false,url:'/sysDict/bybs_type'">
            </div>
            <div class="form-group">
                <label>审核状态</label>
                <input id="status" class="easyui-combobox" name="status" data-options="prompt:'审核状态',editable:false,url:'/sysDict/audit_status'">
            </div>
        </form>
        <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询</button>
    </div>
    <div data-options="region:'center'">
        <div class="easyui-layout" data-options="fit:true">
            <div data-options="region:'north',height:'60%'">
                <table id="bybs_table"></table>
            </div>
            <div data-options="region:'center', title:'商品明细'">
                <table id="bybs_table2"></table>
            </div>
        </div>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/bybs.js?t=1"></script>