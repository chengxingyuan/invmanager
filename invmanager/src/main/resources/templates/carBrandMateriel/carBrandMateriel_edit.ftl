<!-- car_brand_materiel.ftl  -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">车型id</label>
            <div class="col-sm-8">
                <select id="carBrandId" class="easyui-combobox" name="carBrandId" style="width:100%;"
                        data-options="prompt:'车型id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">商品id</label>
            <div class="col-sm-8">
                <select id="materielId" class="easyui-combobox" name="materielId" style="width:100%;"
                        data-options="prompt:'商品id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">year</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="year" style="width: 100%;"
                       data-options="prompt:'year'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">车型信息</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="car" style="width: 100%;"
                       data-options="prompt:'车型信息'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">status</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
    </form>
</div>
