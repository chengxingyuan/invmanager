<!-- m_sku.ftl 规格配置 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-panel" data-options="fit:true">
    <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'center'">
            <div class="easyui-layout" data-options="fit:true">
                <div data-options="region:'north',height:86">
                    <div id="mSku_btn" class="datagrid-toolbar">

                    </div>
                    <form id="mSku_form" class="form-inline search-form" style="float: left">
                        <div class="form-group">
                            <label>名称</label>
                            <input id="searchName" name="name" class="easyui-textbox" data-options="prompt:'名称'">
                        </div>
                    </form>
                    <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询(Enter)</button>
                </div>
                <div data-options="region:'center'">
                    <table id="mSku_table"></table>
                </div>
            </div>

        </div>
        <div data-options="region:'east',split:true,width:'36%'" title="规格值">
        <#include "./../mSkuValue/mSkuValue.ftl">
        </div>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/mSkuValue.js?t=1"></script>
<script src="/bizjs/mSku.js?t=1"></script>
