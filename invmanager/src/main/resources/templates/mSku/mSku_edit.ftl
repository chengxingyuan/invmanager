<!-- m_sku.ftl 物料规格 -->
<div class="container-fluid">
    <form id="skuEditFm" class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">规格名称</label>
            <div class="col-sm-8">
                <input id="name" name="name" style="width: 100%;"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">排序</label>
            <div class="col-sm-8">
                <input id="sort" name="sort" style="width:100%;"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">状态</label>
            <div class="col-sm-8">
                <input id="status" name="status" style="width:100%;" value="0"/>
            </div>
        </div>
    </form>
</div>
<script>
    var MSkuEdit = {};
    MSkuEdit.addValid = function () {
        $("#name").textbox({
            prompt: '规格名称',
            required: true,
            validType:{
                length:[1,20],
                remote:['/mSku/valid?id=' + $("#id").val(),'name'],
            },
            invalidMessage:'名称已存在'
        });
    };
    MSkuEdit.init = function () {
        $("#name").textbox({
            prompt: '规格名称',
            required: true,
            validType:'length[1,64]'
        });
        $("#sort").textbox({
            prompt: '排序',
            min: 0
        });
        $("#status").combobox({
            prompt: '状态',
            panelHeight: 'auto',
            valueField: 'id',
            textField: 'text',
            editable:false,
            data:[
                {
                    id:0,
                    text:'启用'
                },
                {
                    id:1,
                    text:'禁用'
                }
            ],
            onLoadSuccess: function () {
                $("#status").combobox("setValue", 0);
            }
        });
        setTimeout("$(\"#name\").next().find(\".textbox-text\").focus()", 500);
    };
    MSkuEdit.reset = function() {
        $("#name").textbox("setValue", "");
        $("#sort").textbox("setValue", "");
        $("#status").combobox("setValue", 0);
        setTimeout("$(\"#name\").next().find(\".textbox-text\").focus()", 500);
    };

    $(function () {
        MSkuEdit.init();
    });
</script>