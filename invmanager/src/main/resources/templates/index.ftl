<#assign index=true>
<#include "common/lib/csslib.ftl">

<#include "common/header.ftl">

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

<#include "common/left-menu.ftl">

    <div class="main-content">
    <#--<#include "common/top-menu.ftl">-->

        <div class="main-content-inner">

            <div class="hidden-xs mtab" id="mtab">
                <a href="javascript:void(0);" class="mtab-btn"><i class="fa fa-backward"></i></a>
                <div class="mtab-body">
                    <div class="mtab-main" id="mtab-main"></div>
                </div>
                <div class="dropdown pull-right">
                    <a href="#" class="mtab-btn" data-toggle="dropdown"><span class="caret caret-lg"></span></a>
                    <ul class="dropdown-menu dropdown-yellow" id="mtab-menu">
                        <li><a><i class="fa fa-fw fa-refresh blue"></i> 刷新当前</a></li>
                        <li><a><i class="fa fa-fw fa-close orange"></i> 关闭其他</a></li>
                        <li><a><i class="fa fa-fw fa-power-off red"></i> 关闭全部</a></li>
                    </ul>
                </div>
                <a href="javascript:void(0);" class="pull-right mtab-btn"><i class="fa fa-forward"></i></a>
            </div>

            <div class="tab-content" id="mtabox">
            <#--<#include "common/setting.ftl">-->

            </div>

        </div>
    </div>
<#--<#include "common/footer.ftl">-->

</div>
<input id="_urlPre" type="hidden" value="${_urlPre!}">
<#include "common/lib/jslib.ftl">
<script src="/bizjs/my/init.js"></script>