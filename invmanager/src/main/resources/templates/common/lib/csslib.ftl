<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>天天爱车库存管理系统</title>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" type="text/css" href="/public/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/public/assets/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/public/assets/fileinput/css/fileinput.min.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="/public/assets/fileinput/themes/explorer/theme.min.css" media="all"/>

<#if index??>
    <!-- ace styles -->
    <link rel="stylesheet" href="/public/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style"/>
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="/public/assets/css/ace-part2.min.css" class="ace-main-stylesheet"/>
    <link rel="stylesheet" href="/public/assets/css/ace-ie.min.css"/>
    <![endif]-->
</#if>

    <link rel="stylesheet" href="/public/easyui/themes/bootstrap/easyui.css"></link>
    <link rel="stylesheet" href="/public/easyui/themes/icon.css"></link>
    <link rel="stylesheet" href="/public/easyui/ex/texteditor.css">
    <link rel="stylesheet" href="/bizjs/my/init.css"/>
    <link rel="stylesheet" href="/bizjs/my/admin.css"></link>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
    <!--[if lte IE 8]>
    <script src="/public/assets/js/html5shiv.min.js"></script>
    <script src="/public/assets/js/respond.min.js"></script>
    <![endif]-->

</head>

<body class="no-skin">
