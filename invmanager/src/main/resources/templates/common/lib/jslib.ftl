</body>
<!--[if !IE]> -->
<script src="/public/assets/js/jquery-2.1.4.min.js"></script>
<!-- <![endif]-->

<!--[if IE]>
<script src="/public/assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='/public/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="/public/assets/js/bootstrap.min.js"></script>

<script src="/bizjs/my/permission.js"></script>
<script>
    <#if index??>
    //全局图片前缀
    $.urlPre = $('#_urlPre').val();
    //按钮js权限
        <#list Session.current_user.pers as per>
        $.addPer('${per}');
        </#list>
    <#else>
    $.hasPer = parent.$.hasPer;
    $.urlPre = parent.$.urlPre;
    </#if>
</script>

<script src="/public/assets/js/jquery-ui.min.js"></script>

<#if index??>
<!-- ace scripts -->
<script src="/public/assets/js/ace-elements.min.js"></script>
<script src="/public/assets/js/ace.min.js"></script>
<script src="/public/assets/js/ace-extra.min.js"></script>
<#else>

<#--fileinput-->
<script src="/public/assets/fileinput/js/plugins/piexif.min.js" type="text/javascript"></script>
<script src="/public/assets/fileinput/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="/public/assets/fileinput/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="/public/assets/fileinput/js/fileinput.js"></script>
<script src="/public/assets/fileinput/themes/explorer/theme.js"></script>
<script src="/public/assets/fileinput/js/locales/zh.js"></script>

<script src="/public/easyui/jquery.easyui.min.js"></script>
<#--<script src="/public/easyui/ex/datagrid-dnd.js"></script>-->
<#--<script src="/public/easyui/ex/datagrid-filter.js"></script>-->
<script src="/public/easyui/ex/datagrid-cellediting.js"></script>
<script src="/public/easyui/ex/datagrid-export.js"></script>

<script src="/public/easyui/ex/jquery.texteditor.js"></script>
<script src="/public/easyui/locale/easyui-lang-zh_CN.js"></script>

<script src="/bizjs/my/admin.js"></script>
<script type="text/javascript">
    function clearNoNum(obj){
        obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
        obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的
        obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
        obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');//只能输入两个小数
        if(obj.value.indexOf(".")< 0 && obj.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
            obj.value= parseFloat(obj.value);
        }
    }
</script>
</#if>

</html>