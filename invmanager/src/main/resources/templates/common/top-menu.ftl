<div class="sidebar h-sidebar navbar-collapse collapse ace-save-state">
    <ul class="nav nav-list">
        <li class="hover">
            <a href="#">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
        </li>

        <li class="hover">
            <a href="#">
                <i class="menu-icon fa fa-list-alt"></i>
                <span class="menu-text">Widgets</span>
            </a>

            <b class="arrow"></b>
        </li>

        <li class="hover sidebar-collapse" data-target="#sidebar2">
            <a href="#">
                <i class="menu-icon fa fa-caret-right"></i>
            </a>
        </li>
    </ul><!-- /.nav-list -->
</div><!-- .sidebar -->
