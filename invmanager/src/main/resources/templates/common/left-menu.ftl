<div id="sidebar" class="sidebar responsive  ace-save-state" data-sidebar="true" data-sidebar-scroll="true"
     data-sidebar-hover="true">
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="ace-icon fa fa-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="ace-icon fa fa-pencil"></i>
            </button>

            <button class="btn btn-warning">
                <i class="ace-icon fa fa-users"></i>
            </button>

            <button class="btn btn-danger">
                <i class="ace-icon fa fa-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- /.sidebar-shortcuts -->

    <ul class="nav nav-list" style="top: 0px;">

        <li class="">
            <a href="#/home">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> 首页 </span>
            </a>

            <b class="arrow"></b>
        </li>

    <#list resources as resource >
        <li>
            <a href="#${resource.src}" <#if resource.children??>class="dropdown-toggle"</#if>>
                <i class="menu-icon ${resource.icon!'fa fa-flag'}"></i>
                <span class="menu-text"> ${resource.name!} </span>
                <b class="arrow <#if resource.children??>fa fa-angle-down</#if>"></b>
            </a>
            <b class="arrow"></b>
            <#if resource.children??>
                <ul class="submenu nav-hide" style="display: none;">
                    <#list resource.children as child >
                        <li>
                            <a href="#${child.src}" <#if child.children??>class="dropdown-toggle"</#if>>
                                <i class="menu-icon ${child.icon!'fa fa-flag'}"></i>
                                <span class="menu-text"> ${child.name!} </span>
                                <b class="arrow <#if child.children??>fa fa-angle-down</#if>"></b>
                            </a>
                            <b class="arrow"></b>
                            <#if child.children??>
                                <ul class="submenu nav-hide" style="display: none;">
                                    <#list child.children as c >
                                        <li>
                                            <a href="#${c.src}">
                                                <i class="menu-icon ${c.icon!'fa fa-flag'}"></i>
                                                <span class="menu-text"> ${c.name!} </span>
                                            </a>
                                            <b class="arrow"></b>
                                        </li>
                                    </#list>
                                </ul>
                            </#if>
                        </li>
                    </#list>
                </ul>
            </#if>
        </li>
    </#list>

    </ul><!-- /.nav-list -->

    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state"
           data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
</div>