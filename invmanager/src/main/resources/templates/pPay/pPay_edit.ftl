<!-- p_pay.ftl 采购单支付流水 -->
<div class="container-fluid">
    <form class="form-horizontal mform" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div class="form-group">
            <label class="col-sm-2 control-label">采购总单id</label>
            <div class="col-sm-8">
                <select id="purchaseId" class="easyui-combobox" name="purchaseId" style="width:100%;"
                        data-options="prompt:'采购总单id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">往来商户id</label>
            <div class="col-sm-8">
                <select id="relativeId" class="easyui-combobox" name="relativeId" style="width:100%;"
                        data-options="prompt:'往来商户id',editable:false">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">支付类型（0 现金，1 转账，2 支付宝，3 微信）</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="type" style="width:100%;"
                        data-options="prompt:'支付类型（0 现金，1 转账，2 支付宝，3 微信）',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">金额</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="amount" style="width:100%;"
                        data-options="prompt:'金额',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">备注</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="remark" style="width: 100%;"
                       data-options="prompt:'备注'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">操作人姓名</label>
            <div class="col-sm-8">
                <input class="easyui-textbox" name="username" style="width: 100%;"
                       data-options="prompt:'操作人姓名'">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">支付用途（0 支付，1 收款）</label>
            <div class="col-sm-8">
                <input class="easyui-numberbox" name="useType" style="width:100%;"
                        data-options="prompt:'支付用途（0 支付，1 收款）',min:0">
                </input>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">状态（0 有效，1 无效）</label>
            <div class="col-sm-8">
                <select class="easyui-combobox" name="status" style="width:100%;"
                        data-options="prompt:'状态',editable:false,panelHeight:'auto'">
                    <option value="0">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>
        </div>
    </form>
</div>
