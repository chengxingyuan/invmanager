<!-- 销售单详情 -->
<#include "./../common/lib/csslib.ftl">
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <div class="easyui-layout" data-options="fit:true">
            <form id="myForm" method="post" class="form-inline search-form" style="float: left;">
                <input type="hidden" id="salesId" name="salesId" value="${id}"/>
                <input  type="hidden" id="goods" name="goods"/>

                <div data-options="region:'north',height:120">
                    <div class="form-group">
                        <label>单据号</label>
                        <input name="code" class="easyui-textbox" readonly value="${mSales.code}"/>
                        <label>销售日期</label>
                        <input name="orderTime" class="easyui-datebox" readonly value="${orderTimeCn}"/>
                        <label>订单状态</label>
                        <input id="orderStatus" name="orderStatus" value="${mSales.orderStatus}" readonly/>
                    </div>
                    <div class="form-group" type="客户信息" >
                        <#--<label>客户</label>-->
                        <#--<div style="display: inline-block; width: 177px;padding-top: 10px;">-->
                            <#--<input id="cusId" name="cusId">-->
                        <#--</div>-->
                        <label>联系人</label>
                        <input id="cusName" name="cusName" class="easyui-textbox" value="${mSales.cusName}" readonly/>
                        <label>联系电话</label>
                        <input id="cusTel" name="cusTel" class="easyui-textbox" value="${mSales.cusTel}" readonly/>
                        <label>客户地址</label>
                        <input id="cusAddr" name="cusAddr" class="easyui-textbox" style="width: 443px;" value="${mSales.cusAddr}" readonly/>
                    </div>
                </div>
                <div data-options="region:'center'">
                    <div id="mSales_btn" class="datagrid-toolbar">

                    </div>
                    <table id="mSales_table" style="height: 200px;"></table>

                    <div class="form-group">
                        <label>销售数量：</label>
                        <input id="totalNum" name="totalNum" class="easyui-numberbox"  readonly value="${mSales.totalNum}"/>
                        <label>商品总额：</label>
                        <input id="goodsFee" name="goodsFee" class="easyui-numberbox" data-options="precision:2"  readonly value="${mSales.goodsFee}"/>
                        <label>应付总额：</label>
                        <input id="totalFee" name="totalFee" class="easyui-numberbox" data-options="precision:2" readonly value="${mSales.totalFee}">
                    </div>
                    <div class="form-group">
                        <label>优惠：</label>
                        <input id="discountFee" name="discountFee" class="easyui-numberbox" data-options="precision:2"  readonly value="${mSales.discountFee}"/>
                        <label>已付金额：</label>
                        <input id="payedFee" name="payedFee" class="easyui-numberbox" data-options="precision:2"  readonly value="${mSales.payedFee}"/>
                        <label>剩余待付：</label>
                        <input id="payingFee" name="payingFee" class="easyui-numberbox" data-options="precision:2"  readonly value="${mSales.payingFee}"/>
                    </div>
                    <div class="form-group">
                        <label>物流公司：</label>
                        <input name="logisticsName" class="easyui-textbox" readonly value="${mSales.logisticsName}"/>
                        <label>物流单号：</label>
                        <input name="logisticsNo" class="easyui-textbox" readonly value="${mSales.logisticsNo}"/>
                        <label>配送员：</label>
                        <input name="distributeName" class="easyui-textbox" readonly value="${mSales.distributeName}"/>
                        <label>配送员电话：</label>
                        <input name="distributeTel" class="easyui-textbox" readonly value="${mSales.distributeTel}"/>
                    </div>
                    <#if mSales.payStatus == 0>
                    <div class="form-group">
                        <label>本次付款：</label>
                        <input id="payFee" name="payFee" class="easyui-numberbox" value="0"  data-options="min:0,precision:2,onChange:Msales.changePayFee" >
                        <label>结算方式：</label>
                        <input id="payType" name="payType" style="width: 177px;">
                        <label>备注：</label>
                        <input id="payRemark" name="payRemark" class="easyui-textbox" style="width: 443px;"/>
                        <a id="btnPay" href="javascript:void(0)" onclick="Msales.pay()"  class="easyui-linkbutton" data-options="iconCls:'icon-save'">支付</a>
                    </div>
                    </#if>

                    <div class="form-group" style="height: 10px;">
                    </div>


                </div>
                <div data-options="region:'south', title:'支付记录', height: '200px;', collapsible:false">
                    <table id="pay_table"></table>
                </div>
            </form>
        </div>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">
<script src="/bizjs/mSalesEdit.js?t=2"></script>