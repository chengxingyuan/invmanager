<!-- 新建销售单 -->
<#include "./../common/lib/csslib.ftl">

<div class="easyui-layout" data-options="fit:true">
    <form id="myForm" method="post">
        <input type="hidden" id="goods" name="goods"/>
        <input type="hidden" id="type" name="type"/>

        <div data-options="region:'north',height:95">
            <div id="mSales_btn" class="datagrid-toolbar">

            </div>
            <div class="form-inline search-form">
                <input type="hidden" name="token" value="${addSaleToken}">
                <div class="form-group">
                    <label>销售日期</label>
                    <input id="orderTime" name="orderTime" tabindex="-1"/>
                </div>
                <div class="form-group">
                    <label>销售类型</label>
                    <input name="orderType"/>
                </div>
                <div class="form-group" style="line-height: 20px;">
                    <label>客户</label>
                    <select style="width: 200px" id="cusId" name="cusId" class="easyui-combogrid"
                            data-options="prompt:'选择客户',editable:false" tabindex="-1"></select>
                </div>
                <#--<div class="form-group">-->
                    <#--<label>联系人</label>-->
                    <#--<input id="cusName" name="cusName" class="easyui-textbox" data-options="prompt:'联系人',validType:'length[0,128]'" tabindex="3"/>-->
                <#--</div>-->
                <#--<div class="form-group">-->
                    <#--<label>联系电话</label>-->
                    <#--<input id="cusTel" name="cusTel" class="easyui-textbox" data-options="prompt:'联系电话',validType:'length[0,11]'" tabindex="4"/>-->
                <#--</div>-->
                <#--<div class="form-group">-->
                    <#--<label>客户地址</label>-->
                    <#--<input id="cusAddr" name="cusAddr" class="easyui-textbox" style="width: 443px;"-->
                           <#--data-options="prompt:'客户地址',validType:'length[0,128]'" tabindex="4"/>-->
                <#--</div>-->
            </div>
        </div>

        <div data-options="region:'center'">
            <table id="mSales_table" tabindex="-1"></table>
        </div>

        <#--<div data-options="region:'south', height:260, title:'订单信息'">-->
            <#--<div class="form-inline search-form">-->
                <#--<div class="form-group">-->
                    <#--<label>销售数量：</label>-->
                    <#--<input id="totalNum" name="totalNum" class="easyui-numberbox" value="0" readonly tabindex="-1"/>-->
                <#--</div>-->
                <#--<div class="form-group">-->
                    <#--<label>商品总额：</label>-->
                    <#--<input id="goodsFee" name="goodsFee" class="easyui-numberbox" data-options="precision:2"-->
                           <#--value="0"-->
                           <#--readonly tabindex="-1"/>-->
                <#--</div>-->
                <#--<div class="form-group">-->
                    <#--<label>应付总额：</label>-->
                    <#--<input id="totalFee" name="totalFee" class="easyui-numberbox"-->
                           <#--data-options="precision:2,prompt:'应付总额',onChange:Msales.changeTotalFee" value="0" tabindex="5">-->
                <#--</div>-->
                <#--<div class="form-group">-->
                    <#--<label>优惠：</label>-->
                    <#--<input id="discountFee" name="discountFee" class="easyui-numberbox"-->
                           <#--data-options="precision:2"-->
                           <#--value="0" readonly tabindex="-1"/>-->
                <#--</div>-->
                <#--<div class="form-group">-->
                    <#--<label>已付金额：</label>-->
                    <#--<input id="payedFee" name="payedFee" class="easyui-numberbox" data-options="precision:2"-->
                           <#--value="0"-->
                           <#--readonly tabindex="-1"/>-->
                <#--</div>-->
                <#--<div class="form-group">-->
                    <#--<label>剩余待付：</label>-->
                    <#--<input id="payingFee" name="payingFee" class="easyui-numberbox" data-options="precision:2"-->
                           <#--value="0"-->
                           <#--readonly tabindex="-1"/>-->
                <#--</div>-->
            <#--</div>-->

            <#--<div class="form-inline search-form">-->
                <#--<div class="form-group">-->
                    <#--<label>本次付款：</label>-->
                    <#--<input id="payFee" name="payFee" class="easyui-numberbox" value="0"-->
                           <#--data-options="min:0,precision:2,prompt:'本次付款',onChange:Msales.changePayFee" tabindex="6">-->
                <#--</div>-->
                <#--<div class="form-group">-->
                    <#--<label>结算方式：</label>-->
                    <#--<input id="payType" name="payType" style="width: 174px;" tabindex="7">-->
                <#--</div>-->
                <#--<div class="form-group">-->
                    <#--<label>备注：</label>-->
                    <#--<input name="payRemark" class="easyui-textbox" style="width: 435px;"-->
                           <#--data-options="prompt:'备注'" tabindex="8"/>-->
                <#--</div>-->
            <#--</div>-->

            <#--<div class="form-inline search-form">-->
                <#--<div class="form-group">-->
                    <#--<label>物流公司：</label>-->
                    <#--<input name="logisticsName" class="easyui-textbox" data-options="prompt:'物流公司'" tabindex="9"/>-->
                <#--</div>-->
                <#--<div class="form-group">-->
                    <#--<label>物流单号：</label>-->
                    <#--<input name="logisticsNo" class="easyui-textbox" data-options="prompt:'物流单号'" tabindex="10"/>-->
                <#--</div>-->
                <#--<div class="form-group">-->
                    <#--<label>配送员：</label>-->
                    <#--<input name="distributeName" class="easyui-textbox" data-options="prompt:'配送员'" tabindex="11"/>-->
                <#--</div>-->
                <#--<div class="form-group">-->
                    <#--<label>配送员电话：</label>-->
                    <#--<input name="distributeTel" class="easyui-textbox" data-options="prompt:'配送员电话',validType:'length[0,11]'" tabindex="12"/>-->
                <#--</div>-->
            <#--</div>-->
        <#--</div>-->
        <div data-options="region:'south',height:220, title:'备注信息'">
        <br>
        <label style="margin-left: 20px">总计金额:</label>
        ￥<input id="totalFee" name="totalFee" value="0" style="border:none;height: 30px;width: 60px" />
            <label style="margin-left: 10px">总计数量:</label>
            <input id="totalNum" name="totalNum" value="0" style="border:none;height: 30px;width: 60px" />
            <label style="margin-left: 30px">实付金额:</label>
        <input  id="realPay" name="realPay"  style="border:none;height: 30px;width: 90px;border-bottom:1px solid grey;" value="0" onkeyup="clearNoNum(this)"/>
        <label style="margin-left: 30px">结算情景:</label>
        <input  id="payStatus" name="payStatus" value="0"  />
        <label style="margin-left: 10px">备注:</label>
        <input id="remark" name="remark" style="border:none;height: 30px;width: 350px;border-bottom:1px solid grey;"/>

    </div>
    </form>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/mSalesAdd.js?t=4"></script>