<!-- m_sales.ftl  -->
<#include "./../common/lib/csslib.ftl">
<input type="hidden" id="hidSearchText" value="${searchText}" />
<input type="hidden" id="hidStartTime" value="${startTime}" />
<input type="hidden" id="hidEndTime" value="${endTime}" />
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',height:'100px'">
        <div id="mSales_btn" class="datagrid-toolbar">

        </div>
        <form id="mSales_form" class="form-inline search-form" style="float: left">
            <div class="form-group">
                <label>查询内容</label>
                <input id="searchText" name="searchText" class="easyui-textbox" data-options="prompt:'请输入单号、商品名称、客户'">
            </div>
            <div class="form-group">
                <label>订单日期</label>
                <input id="startTime" name="startTime" style="width: 100px;" class="easyui-datebox">-<input id="endTime" style="width: 100px;" name="endTime" class="easyui-datebox">
            </div>
            <div class="form-group" style="line-height: 20px">
                <label>结算状态</label>
                <input name="payStatus" />
                </select>
            </div>
            <div class="form-group" style="line-height: 20px">
                <label>是否出库</label>
                <input name="orderStatus" />
                </select>
            </div>
        </form>
        <button  id="searchButton" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="margin-left:10px;margin-top:10px" >查询</button>
    </div>
    <div data-options="region:'center'">
        <table id="mSales_table"></table>
    </div>
    <div data-options="region:'south',height:'25%', title:'销售商品'">
    <#--<div id="printBtn" class="datagrid-toolbar"> </div>-->
        <table id="saleGoods_table"></table>
    </div>
</div>

<#include "./../common/lib/jslib.ftl">

<script src="/bizjs/mSales.js"></script>