﻿var inv = {
    //Mobile
    isPC: function () {
        return !navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)
    },
    //DOM集合某一项是否包含节点
    contains: function (arr, target) {
        var item = null;
        $(arr).each(function () {
            if (this.contains(target)) {
                item = this;
                return false;
            }
        });
        return item;
    },
    //选项卡定位
    PositionTab: function () {
        var mm = $('#mtab-main'), vw = mm.parent().width(), sw = mm[0].scrollWidth;
        if (sw > vw) {
            var max = 0, min = -1 * (sw - vw), ml = 3, left = parseFloat(mm.css('left')), ca;
            mm.children().each(function () {
                ml += $(this).outerWidth() + 3;
                if (this.className.indexOf("active") >= 0) {
                    ca = this;
                    return false;
                }
            });
            if (ml + left > vw) {
                ml = -1 * ml + (vw / 2);
                ml = Math.max(min, ml);
                mm.css('left', ml);
            } else if (ml - $(ca).outerWidth() + left < 0) {
                ml = -1 * ml + (vw / 2);
                ml = Math.min(0, ml);
                mm.css('left', ml);
            }
        }
    },
    //打开选项卡 链接、含图标的标题，false不显示关闭按钮(可选)
    OpenPage: function (url, title, close) {
        var isopen = false, mmc = $('#mtab-main').children(), mb = $('#mtabox');
        mmc.removeClass('active');
        mb.children().removeClass('active');
        mb.find('iframe').each(function () {
            var shorturl = (this.src.split(location.host)[1] || "").toLowerCase();
            shorturl = shorturl.split('?')[0];
            // if (shorturl != "" && url.toLowerCase().indexOf(shorturl) > -1) {
            if (shorturl != "" && url.toLowerCase() == shorturl) {
                //取消注释，点击导航会刷新页面
                //this.src = url;
                var pageid = $(this).parent().addClass('active')[0].id;
                mmc.each(function () {
                    if (this.hash == "#" + pageid) {
                        $(this).addClass('active');
                        return false;
                    }
                });
                isopen = true;
                return false;
            }
        });
        if (!isopen) {
            var pageid = "page_" + new Date().valueOf(), close = close == false ? '' : '<em class="fa fa-close"></em>';
            mmc.end().append('<a href="#' + pageid + '" class="active">' + title + close + '</a></li>');
            mb.append('<div class="tab-pane active" id="' + pageid + '"><iframe frameborder="0" src="about:blank"></iframe></div>');
            $('#' + pageid).find('iframe')[0].src = url;
        }

        //定位、调整
        inv.PositionTab();
        inv.IframeAuto();

        //Mobile 打开时隐藏菜单导航
        if (!inv.isPC()) {
            var mt = $('#menu-toggler');
            mt.hasClass('display') && $('#menu-toggler')[0].click();
        }
    },
    //Iframe自适应、延迟响应
    IframeAuto: function () {
        clearTimeout(window.deferIA);
        window.deferIA = setTimeout(function () {
            var box = $('#mtabox'), top = 0;
            if (box.offset()) top = box.offset().top;
            var sh = $(window).height() - top;
            box.children('div').css("height", sh);
        }, 100);
    }
};

//点击选项卡
$('#mtab').click(function (e) {
    $('#sidebar').find('li').removeClass("active");
    e = e || window.event;
    var target = e.target || e.srcElement;
    if (target.nodeName == "EM" && target.className.indexOf('fa-close') >= 0) {
        //关闭
        var ta = $(target).parent();
        if (ta.hasClass('active')) {
            ta.prev().addClass('active');
            $(ta[0].hash).prev().addClass('active');
        }
        $(ta[0].hash).remove();
        ta.remove();
        inv.PositionTab();
        return false;
    } else if ($(this).children('a')[0].contains(target)) {
        //左滑
        var mm = $('#mtab-main'), vw = mm.parent().width(),
            left = parseFloat(mm.css('left')) + (vw / 2);
        mm.css('left', Math.min(0, left));
    } else if ($(this).children('a').last()[0].contains(target)) {
        //右滑
        var mm = $('#mtab-main'), vw = mm.parent().width(),
            min = -1 * (mm[0].scrollWidth - mm.parent().width()),
            left = parseFloat(mm.css('left')) - (vw / 2);
        mm.css('left', Math.max(min, left));
    } else {
        var item = inv.contains($('#mtab-menu').children(), target);
        if (item) {
            //菜单项
            var cn = $(item).find('i')[0].className;
            if (cn.indexOf("fa-refresh") >= 0) {
                //刷新
                $('#mtabox').children('div.active').children()[0].contentWindow.location.reload(false);
            } else if (cn.indexOf("fa-close") >= 0) {
                //关闭其他
                var mmc = $('#mtab-main').children();
                mmc.each(function (i) {
                    if (i && this.className.indexOf("active") == -1) {
                        $(this.hash).remove();
                        $(this).remove();
                    }
                }).end().css('left', 0);
            } else if (cn.indexOf("fa-power-off") >= 0) {
                //关闭全部
                var mmc = $('#mtab-main').children();
                mmc.each(function (i) {
                    if (i) {
                        $(this.hash).remove();
                        $(this).remove();
                    }
                }).first().addClass('active');
                $(mmc.first()[0].hash).addClass('active');
                mmc.end().css('left', 0);
            }
        } else {
            var mmc = $('#mtab-main').children();
            item = inv.contains(mmc, target);
            if (item) {
                if (item.className.indexOf('active') == -1) {
                    //选项卡标签
                    mmc.removeClass('active');
                    $(item).addClass("active");
                    $('#mtabox').children().removeClass('active');
                    $(item.hash).addClass('active');
                    inv.PositionTab();
                }
                return false;
            }
        }
    }
});
//菜单点击
$('#sidebar').click(function (e) {
    var that = this;
    if (e && e.preventDefault) {
        e.preventDefault()
    } else {
        window.event.returnValue = false
    }
    e = e || window.event;
    var target = e.target || e.srcElement;
    $(this).find('a').each(function (idx) {
        if (this.contains(target)) {
            if (this.href.split('#')[1]) {
                $(that).find('li').removeClass('active');
                $(this).parent().addClass("active");
                inv.OpenPage(this.href.split('#')[1], this.innerHTML, idx != 0);
            }
            return false;
        }
    });
}).find('a:first').click();

// 全局ajax配置
$.ajaxSetup({
    dataType: "JSON",
    contentType: "application/json",
    // contentType: "application/x-www-form-urlencoded;charset=utf-8",
    cache: false,
    complete: function (xhr) {
        //token过期，则跳转到登录页面

        // console.info(xhr);
    }
});

$(window).ready(function () {
    $('#logout').click(function () {
        $.get("logout", {}, function () {
            location.href = "/login";
        });
    });

    // setTimeout(function () {
    //     $.get("/pushlet/info", {}, function (data) {
    //         if (data.data) {
    //             inv.OpenPage("/pushlet/tt", "天天爱车面板", true);
    //             window.isOpenTTicar = data.data;
    //         }
    //     })
    // }, 500);

}).resize(inv.IframeAuto);



