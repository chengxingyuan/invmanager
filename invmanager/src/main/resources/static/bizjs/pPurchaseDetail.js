<!-- p_purchase_detail.ftl 采购单详情 -->
var PPurchaseDetail = {
    u: '/pPurchaseDetail'
    , uList: '/pPurchaseDetail/list'
    , uEdit: '/pPurchaseDetail/edit'
    , uDel: '/pPurchaseDetail/delete'
};

PPurchaseDetail.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        PPurchaseDetail._edit(row['id']);
    };
    grid.url = PPurchaseDetail.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
       // , {field: 'purchaseId', title: '采购总单id', width: 80}
        , {field: 'sku_id', title: '商品id', width: 80}
        , {field: 'materiel_name', title: '商品名称', width: 80}
        , {field: 'count', title: '预购数量', width: 80}
        , {field: 'count_actual', title: '实际采购数量', width: 80}
        , {field: 'amount', title: '预计采购金额', width: 80}
        , {field: 'amount_actual', title: '实际采购金额', width: 80}
        , {field: 'amount_discount', title: '折扣金额', width: 80}
        , {field: 'remark', title: '备注', width: 80}
        , {field: 'ctime', title: '创建时间', width: 80}
    ]];
    grid.loadGrid();
};
PPurchaseDetail.dg.getOne = function () {
    var sels = PPurchaseDetail.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
PPurchaseDetail.dg.getSels = function () {
    return PPurchaseDetail.dtable.datagrid('getSelections');
};
// 处理按钮
PPurchaseDetail.btn = function () {
    var sb = new $yd.SearchBtn($(PPurchaseDetail.obtn));
    sb.create('remove', '删除', PPurchaseDetail.remove);
    sb.create('refresh', '刷新', PPurchaseDetail.refresh);
    sb.create('edit', '编辑', PPurchaseDetail.edit);
    sb.create('plus', '新增', PPurchaseDetail.plus);
    sb.create('search', '查询', PPurchaseDetail.search);
};
//删除
PPurchaseDetail.remove = function () {
    var sels = PPurchaseDetail.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(PPurchaseDetail.uDel, {ids: ids}, function () {
            PPurchaseDetail.refresh();
        });
    })
};
//
PPurchaseDetail.edit = function () {
    var sel = PPurchaseDetail.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    PPurchaseDetail._edit(sel['id'])
};
// 表单预处理
PPurchaseDetail.preForm = function () {
    new $yd.combobox("purchaseId").load("/purchase/select");
    new $yd.combobox("skuId").load("/sku/select");
};
PPurchaseDetail._edit = function (id) {
    $yd.win.create('编辑采购单详情', PPurchaseDetail.uEdit + '?id=' + id).open(function (that, $win) {
            PPurchaseDetail.preForm();
            $yd.get(PPurchaseDetail.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(PPurchaseDetail.u).submit(function () {
                that.close();
                PPurchaseDetail.refresh();
            });
        }
    );
};
//新增
PPurchaseDetail.plus = function () {
    var sel = PPurchaseDetail.dg.getOne();
    var href = PPurchaseDetail.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增采购单详情', href).open(function (that, $win){
            PPurchaseDetail.preForm();
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(PPurchaseDetail.u).submit(function () {
                that.close();
                PPurchaseDetail.refresh();
            });
        }
    );
};
//
PPurchaseDetail.refresh = function () {
    PPurchaseDetail.dtable.datagrid('reload');
};
//搜索
PPurchaseDetail.search = function () {
    PPurchaseDetail.dtable.datagrid('load', PPurchaseDetail.sform.serializeObject());
};

$(function () {
    PPurchaseDetail.sform = $('#pPurchaseDetail_form');
    PPurchaseDetail.dtable = $('#pPurchaseDetail_table');
    PPurchaseDetail.obtn = '#pPurchaseDetail_btn';

    PPurchaseDetail.dg(); // 数据表格
    PPurchaseDetail.btn(); // 初始化按钮
    $yd.bindEnter(PPurchaseDetail.search);
});