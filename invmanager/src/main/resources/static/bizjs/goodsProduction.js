<!-- goods_production.ftl 商品-产品表 -->
var GoodsProduction = {
    u: '/goodsProduction'
    , uList: '/goodsProduction/list'
    , uEdit: '/goodsProduction/edit'
    , uDel: '/goodsProduction/delete'
};

GoodsProduction.dg = function () {
    var grid = new $yd.TreeGrid(this.dtable);
    grid.onDblClickRow = function (row) {
        GoodsProduction._edit(row['id']);
    };
    grid.toolbar = GoodsProduction.obtn;
    grid.url = GoodsProduction.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'goodId', title: 'goodId', width: 80}
        , {field: 'skuId', title: 'skuId', width: 80}
        , {field: 'name', title: 'name', width: 80}
        , {field: 'price', title: 'price', width: 80}
        , {field: 'pricemember', title: 'pricemember', width: 80}
        , {field: 'pricevip', title: 'pricevip', width: 80}
        , {field: 'pricesell', title: '零售价', width: 80}
        , {field: 'inventory', title: 'inventory', width: 80}
        , {field: 'createtime', title: '创建时间', width: 80}
        , {field: 'modifytime', title: '修改时间', width: 80}
        , {field: 'skuPic', title: '规格图片，图片地址', width: 80}
        , {field: 'pricevip2', title: 'v2价格', width: 80}
        , {field: 'pricevip3', title: 'v3价格', width: 80}
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
GoodsProduction.dg.getOne = function () {
    var sels = GoodsProduction.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
GoodsProduction.dg.getSels = function () {
    return GoodsProduction.dtable.treegrid('getSelections');
};
// 处理按钮
GoodsProduction.btn = function () {
    var sb = new $yd.SearchBtn($(GoodsProduction.obtn));
    sb.create('remove', '删除', GoodsProduction.remove);
    sb.create('refresh', '刷新', GoodsProduction.refresh);
    sb.create('edit', '编辑', GoodsProduction.edit);
    sb.create('plus', '新增', GoodsProduction.plus);
};
//删除
GoodsProduction.remove = function () {
    var sels = GoodsProduction.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(GoodsProduction.uDel, {ids: ids}, function () {
            GoodsProduction.refresh();
        });
    })
};
//
GoodsProduction.edit = function () {
    var sel = GoodsProduction.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    GoodsProduction._edit(sel['id'])
};
// 表单预处理
GoodsProduction.preForm = function () {
    new $yd.combobox("goodId").load("/good/select");
};
GoodsProduction._edit = function (id) {
    $yd.win.create('编辑商品-产品表', GoodsProduction.uEdit + '?id=' + id).open(function (that, $win) {
            GoodsProduction.preForm();
            $yd.get(GoodsProduction.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(GoodsProduction.u).submit(function () {
                that.close();
                GoodsProduction.refresh();
            });
        }
    );
};
//新增
GoodsProduction.plus = function () {
    var sel = GoodsProduction.dg.getOne();
    var href = GoodsProduction.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增商品-产品表', href).open(function (that, $win){
            GoodsProduction.preForm();
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(GoodsProduction.u).submit(function () {
                that.close();
                GoodsProduction.refresh();
            });
        }
    );
};
//
GoodsProduction.refresh = function () {
    GoodsProduction.dtable.treegrid('reload');
};

$(function () {
    GoodsProduction.sform = $('#goodsProduction_form');
    GoodsProduction.dtable = $('#goodsProduction_table');
    GoodsProduction.obtn = '#goodsProduction_btn';

    GoodsProduction.dg(); // 数据表格
    GoodsProduction.btn(); // 初始化按钮
});