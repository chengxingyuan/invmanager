<!-- m_brand.ftl 存货品牌 -->
var MBrand = {
    u: '/mBrand'
    , uList: '/mBrand/list'
    , uEdit: '/mBrand/edit'
    , uDel: '/mBrand/delete'
    , dgType: undefined
    , winThat: undefined
    , win: undefined
};

MBrand.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        MBrand._edit(row['id']);
    };
    grid.url = MBrand.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'name', title: '品牌名称', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else if (value == 1) {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: '创建时间', width: 80}
    ]];
    grid.loadGrid();
};
MBrand.dg.getOne = function () {
    var sels = MBrand.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
MBrand.dg.getSels = function () {
    return MBrand.dtable.datagrid('getSelections');
};
// 处理按钮
MBrand.btn = function () {
    var sb = new $yd.SearchBtn($(MBrand.obtn));
    sb.create('remove', '删除(DEL)', MBrand.remove);
    sb.create('refresh', '刷新(F2)', MBrand.refresh);
    sb.create('edit', '编辑(Shift+E)', MBrand.edit);
    sb.create('plus', '新增(Shift+N)', MBrand.plus);
    // sb.create('search', '查询', MBrand.search);
};
//删除
MBrand.remove = function () {
    var sels = MBrand.dg.getSels();
    if (sels.length == 0) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(MBrand.uDel, {ids: ids}, function () {
            MBrand.refresh();
        });
    })
};
//
MBrand.edit = function () {
    var sel = MBrand.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    MBrand._edit(sel['id'])
};
MBrand._edit = function (id) {
    MBrand.dgType = "brand";
    var dg = $yd.win2.create('编辑品牌', MBrand.uEdit + '?id=' + id);
    dg.width = "500px";
    dg.height = "400px";
    dg.open(function (that, $win) {
            MBrand.win = $win;
            MBrand.winThat = that;
            $yd.get(MBrand.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function () {
            MBrand.save();
        }, function () {
            MBrand.closeDg(1);
        }
    );
};
//新增
MBrand.plus = function () {
    var sel = MBrand.dg.getOne();
    var href = MBrand.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];

    MBrand.dgType = "brand";
    var dg = $yd.win2.create('新增品牌', href);
    dg.width = "500px";
    dg.height = "400px";
    dg.open(function (that, $win) {
            MBrand.win = $win;
            MBrand.winThat = that;
        }, function () {
            MBrand.save();
        }, function () {
            MBrand.closeDg(1);
        } , function () {
            MBrand.saveNext();
        }
    );
};
//
MBrand.refresh = function () {
    $('#mBrand_table').datagrid('unselectAll');
    MBrand.dtable.datagrid('reload');
};
//搜索
MBrand.search = function () {
    MBrand.dtable.datagrid('load', MBrand.sform.serializeObject());
};
MBrand.closeDg = function (closeEd) {
    if(closeEd == 0) {
        MBrand.winThat.close();
    }

    MBrand.dgType = undefined;
    MBrand.winThat = undefined;
    MBrand.win = undefined;
};
MBrand.save = function () {
    $yd.form.create(MBrand.win.find('form')).url(MBrand.u).submit(function () {
        MBrand.refresh();
        MBrand.closeDg(0);
    });
};
MBrand.saveNext = function () {
    $yd.form.create(MBrand.win.find('form')).url(MBrand.u).submit(function () {
        MBrandEdit.reset();
        MBrand.refresh();
    });
};
MBrand.keyUp = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (!e) {
        return;
    }
    // console.log(e.keyCode);
    if($yd.isForbidCutKey == 1) {
        return;
    }
    if($yd.isEmpty(MBrand.dgType)) { //未弹框
        if (e.shiftKey && e.keyCode == 78) { //Shift+N
            MBrand.plus();
        }
        if (e.shiftKey && e.keyCode == 69) { //Shift+E
            MBrand.edit();
        }
        if(e.keyCode == 113) {//F2
            MBrand.refresh();
        }
        if(e.keyCode == 46) {//DEL
            MBrand.remove();
        }
        if(e.keyCode == 13) {
            MBrand.search();
        }
    } else{ //弹框
        if(e.keyCode == 27) {//ESC
            MBrand.closeDg(0);
        }
        if(e.keyCode == 13) {//Enter
            if(e.ctrlKey) {//Ctrl+Enter
                MBrand.saveNext();
            } else {
                MBrand.save();
            }
        }
    }
};
MBrand.init = function () {
    //快捷键
    document.onkeyup = MBrand.keyUp;

    //先获取焦点再失去焦点，否则打开页面后若不点击页面，快捷键无效
    $("#searchName").next().find(".textbox-text").focus()
    $("#searchName").next().find(".textbox-text").blur()
};
$(function () {
    MBrand.sform = $('#mBrand_form');
    MBrand.dtable = $('#mBrand_table');
    MBrand.obtn = '#mBrand_btn';

    MBrand.dg(); // 数据表格
    MBrand.btn(); // 初始化按钮
    $yd.bindEnter(MBrand.search);
    MBrand.init();
});
var btn1 = document.getElementById("searchButton");
btn1.addEventListener("click", function() {
    $('#mBrand_table').datagrid('unselectAll');
    MBrand.dtable.datagrid('load', MBrand.sform.serializeObject());
});