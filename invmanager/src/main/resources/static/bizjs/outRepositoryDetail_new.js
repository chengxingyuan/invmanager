<!-- in_repository_detail.ftl 出库单详情 -->
var OutRepositoryDetail = {
    u: '/outRepositoryDetail'
    , uList: '/outRepositoryDetail/getOne',
    unitArr: []
};
OutRepositoryDetail.code =0;
OutRepositoryDetail.dg = function () {
    $.ajax({
        type: "POST",
        url: "/sysDict/materiel_MU",
        dataType: "json",
        async:true,
        success: function (data) {
            OutRepositoryDetail.unitArr = data;
        }
    });

    OutRepositoryDetail.dtable.datagrid({
        url: OutRepositoryDetail.uList + "?code="+ OutRepositoryDetail.code,
        fitColumns: true,
        autoRowHeight: true,
        rownumbers: true,
        singleSelect: false,
        pagination: false,
        columns: [[
            {field: 'id', checkbox: false, hidden:true},
            {field: 'goodsName', title: '商品名称', width: 150},
            {field: 'skus', title: '规格', width: 200},
            {
                field: 'unit', title: '单位', width: 80, formatter: function (value, row, index) {
                    for (var i = 0; i < OutRepositoryDetail.unitArr.length; i++) {
                        if (value == OutRepositoryDetail.unitArr[i].id) {
                            return OutRepositoryDetail.unitArr[i].text;
                        }
                    }
                    return value;
                }
            }
            , {field: 'count', title: '出库数量', width: 50}
            , {field: 'houseName', title: '仓库', width: 80}
            , {field: 'positionName', title: '库位', width: 80}
            , {field: 'batchCode', title: '批号', width: 80}
            , {field: 'remark', title: '备注', width: 120}
        ]]
    });
};
// 处理按钮
OutRepositoryDetail.btn = function () {
    var sb = new $yd.SearchBtn($(OutRepositoryDetail.obtn));
    sb.create('print green', '打印', function () {
        OutRepositoryDetail.dtable.datagrid('print', '入库表');
    });
};

$(function () {
    OutRepositoryDetail.sform = $('#outRepositoryDetail_form');
    OutRepositoryDetail.dtable = $('#outRepositoryDetail_table');
    OutRepositoryDetail.obtn = '#outRepositoryDetail_btn';
    OutRepositoryDetail.code = $("#code").val();
    OutRepositoryDetail.dg(); // 数据表格
    OutRepositoryDetail.btn(); // 初始化按钮
    $yd.bindEnter(OutRepositoryDetail.search);
});


