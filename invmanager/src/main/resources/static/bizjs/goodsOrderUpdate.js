// 详情
var PPurchaseDetail = {
    uList: '/pPurchaseDetail/list'
    , orderStatus: 0
    , unitArr: []
    , purchaseId: 0
};

// 详情按钮
PPurchaseDetail.btn = function () {
    if (PPurchaseDetail.orderStatus == 0) {
        var sb = new $yd.SearchBtn($(PPurchaseDetail.obtn));
        // sb.create('plus', '入库', PPurchaseDetail.inRepository);
    }
};

// 详情的表格
PPurchaseDetail.dg = function () {


    PPurchaseDetail.dtable.datagrid({
        url: "/tticar/goodsOrder/list/goodsTmp?orderId=" + PPurchaseDetail.orderCode,
        fitColumns: true,
        autoRowHeight: true,
        rownumbers: true,
        singleSelect: false,
        columns: [[
            // {field: 'wearhouseId', hidden: true},//仓库

            // {field: 'skusId', checkbox: false, hidden: true},//规格id
            {field: 'picUrl', title: '商品图片', width: 150, formatter: function (value, row, index) {
                if (value != null && value != '') {
                    var imageSrc = 'http://tticar-test.oss-cn-shanghai.aliyuncs.com/' + value;
                    return '<div><img  style="height: 50px;width: 140px;"  src="' + imageSrc + '"></div>'
                }
            }},
            {field: 'goodsName', title: '商品名称', width: 80},
            {field: 'standardName', title: '规格', width: 200},
            {field: 'price', title: '成交单价', width: 80},
           
            {field: 'count', title: '数量', width: 80},
            {field: 'fee', title: '运费', width: 80},

        ]]
    });
};

//入库
PPurchaseDetail.inRepository = function () {
    $yd.confirm(function () {
        //入库保存数据
        $yd.post("/pPurchase/inRepository", {purchaseId: PPurchaseDetail.purchaseId}, function (data) {
            $yd.alert("操作成功", function () {
                window.location.reload();
            });
        });
    });
};

// 结算记录
PPurchaseDetail.dg2 = function () {
    PPurchaseDetail.ptable = $('#pay_table');
    PPurchaseDetail.ptable.datagrid({
        url: "/pPurchaseDetail/payList?purchaseId=" + PPurchaseDetail.purchaseId,
        fitColumns: true,
        autoRowHeight: true,
        rownumbers: true,
        singleSelect: false,
        columns: [[
            {field: 'ctime', title: '日期', width: 80},
            {field: 'amount', title: '本次付款', width: 80},
            {field: 'remain_amount', title: '剩余未结算', width: 80},
            {field: 'type_name', title: '结算方式', width: 80},
            {field: 'username', title: '操作人', width: 80},
            {field: 'remark', title: '备注', width: 80}
        ]]
    });
};

PPurchaseDetail.changePayFee = function(newValue,oldValue) {
    if(newValue == "") {
        $("#payFee").numberbox("setValue", 0);
        return;
    }
    if(Number(newValue) > Number($("#amountIng").val())) {
        $yd.alert("本次付款金额不能大于剩余待付！", function () {
            $("#payFee").numberbox("setValue", $("#amountIng").val());
        });
    }
};

// 保存结算信息
PPurchaseDetail.pay = function () {
    var payFee = $("#payFee").val();
    var payType = $("#payType").val();
    var payRemark = $("#payRemark").val();
    if(Number(payFee) == 0) {
        $yd.alert("付款金额不能为0");
        return;
    }

    $.post("/pPurchase/pay",{
        purchaseId: PPurchaseDetail.purchaseId,
        payType: payType,
        payFee: payFee,
        payRemark: payRemark
    },function (data) {
        if(data.code == 0) {
            $yd.alert("支付成功", function () {
                window.location.reload();
            });
        }
    });
};

$("input[name='time']").datebox({
    required: true,
    editable: false,
    value: new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()
});

$(function () {

    PPurchaseDetail.orderCode = $("#orderCode").val();

    PPurchaseDetail.dtable = $('#pPurchaseDetail_table');
    PPurchaseDetail.obtn = '#pPurchaseDetail_btn';
    PPurchaseDetail.btn(); // 初始化按钮
    PPurchaseDetail.dg(); // 采购详情数据表格

    PPurchaseDetail.dg2(); // 结算记录数据表格

    $("input[name='payType']").combobox({
        url: "/sysDict/pay_type",
        panelHeight:'auto',
        editable:false,
        required:true,
        valueField:'id',
        textField:'text',
        onLoadSuccess: function () {
            var d = $(this).combobox("getData");
            $(this).combobox("setValue", d[0].value);
        }
    });
});