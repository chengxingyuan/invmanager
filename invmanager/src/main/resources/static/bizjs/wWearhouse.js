<!-- w_wearhouse.ftl  -->
var WWearhouse = {
    u: '/wWearhouse'
    , uList: '/wWearhouse/list'
    , uEdit: '/wWearhouse/edit'
    , uDel: '/wWearhouse/delete'
    , dgType: undefined
    , winThat: undefined
    , win: undefined
};

WWearhouse.dg = function () {
    var grid = new $yd.TreeGrid(this.dtable);
    grid.onDblClickRow = function (row) {
        WWearhouse._edit(row['id']);
    };
    grid.url = WWearhouse.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'name', title: '仓库名称', width: 80}
        , {field: 'code', title: '编码', width: 80}
        , {field: 'principal', title: '负责人', width: 80}
        , {field: 'principal_tel', title: '负责人电话', width: 80}
        , {field: 'location', title: '地址', width: 80}

        // , {field: 'namespell', title: '名字拼写', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: '创建时间', width: 80}
    ]];
    grid.loadGrid();
};
WWearhouse.dg.getOne = function () {
    var sels = WWearhouse.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
WWearhouse.dg.getSels = function () {
    return WWearhouse.dtable.treegrid('getSelections');
};
WWearhouse.lckcl = function() {
    var sel = WWearhouse.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    parent.inv.OpenPage("/repositoryCount?houseId=" + sel.id, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\">库存量查询</span>\n" +
        "<b class=\"arrow \"></b>", true)
};
WWearhouse.lcrkd = function() {
    var sel = WWearhouse.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    parent.inv.OpenPage("/inRepositoryDetail?houseId=" + sel.id, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\">入库单列表</span>\n" +
        "<b class=\"arrow \"></b>", true)
};
WWearhouse.lcckd = function() {
    var sel = WWearhouse.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    parent.inv.OpenPage("/outRepositoryDetail?houseId=" + sel.id, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\">出库单列表</span>\n" +
        "<b class=\"arrow \"></b>", true)
};
// 处理按钮
WWearhouse.btn = function () {
    var sb = new $yd.SearchBtn($(WWearhouse.obtn));
    sb.create('remove', '删除(DEL)', WWearhouse.remove);
    sb.create('refresh', '刷新(F2)', WWearhouse.refresh);
    sb.create('edit', '编辑(Shift+E)', WWearhouse.edit);
    sb.create('plus', '新增(Shift+N)', WWearhouse.plus);
    // sb.create('search', '查询', WWearhouse.search);
    //
    // $("#wWearhouse_btn").append('<a href="javascript:void(0)" id="mb" class="easyui-menubutton" data-options="menu:\'#mm\',iconCls:\'icon-edit\'">Edit</a>');

    $("#lckcl").click(WWearhouse.lckcl);
    $("#lcrkd").click(WWearhouse.lcrkd);
    $("#lcckd").click(WWearhouse.lcckd);
};
//删除
WWearhouse.remove = function () {
    var sels = WWearhouse.dg.getSels();
    if (!sels||sels.length <1) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(WWearhouse.uDel, {ids: ids}, function () {
            WWearhouse.refresh();
        });
    })
};
//
WWearhouse.edit = function () {
    var sel = WWearhouse.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    WWearhouse._edit(sel['id'])
};
WWearhouse._edit = function (id) {
    WWearhouse.dgType = "house";
    var dg = $yd.win2.create('编辑', WWearhouse.uEdit + '?id=' + id);
    dg.width = "500px";
    dg.height = "400px";
    dg.open(function (that, $win) {
            WWearhouse.win = $win;
            WWearhouse.winThat = that;
            $yd.get(WWearhouse.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function () {
            WWearhouse.save();
        }, function () {
            WWearhouse.closeDg(1);
        }
    );
};
//新增
WWearhouse.plus = function () {
    var href = WWearhouse.uEdit;

    WWearhouse.dgType = "house";
    var dg = $yd.win2.create('新增', href);
    dg.width = "500px";
    dg.height = "400px";
    dg.open(function (that, $win) {
            WWearhouse.win = $win;
            WWearhouse.winThat = that;
        }, function () {
            WWearhouse.save();
        }, function () {
            WWearhouse.closeDg(1);
        } , function () {
            WWearhouse.saveNext();
        }
    );
};
WWearhouse.closeDg = function (closeEd) {
    if(closeEd == 0) {
        WWearhouse.winThat.close();
    }

    WWearhouse.dgType = undefined;
    WWearhouse.winThat = undefined;
    WWearhouse.win = undefined;
};
WWearhouse.save = function () {
    $yd.form.create(WWearhouse.win.find('form')).url(WWearhouse.u).submit(function () {
        WWearhouse.refresh();
        WWearhouse.closeDg(0);
    });
};
WWearhouse.saveNext = function () {
    $yd.form.create(WWearhouse.win.find('form')).url(WWearhouse.u).submit(function () {
        WWearhouseEdit.reset();
        WWearhouse.refresh();
    });
};
WWearhouse.keyUp = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (!e) {
        return;
    }
    if($yd.isForbidCutKey == 1) {
        return;
    }
    if($yd.isEmpty(WWearhouse.dgType)) { //未弹框
        if (e.shiftKey && e.keyCode == 78) { //Shift+N
            WWearhouse.plus();
        }
        if (e.shiftKey && e.keyCode == 69) { //Shift+E
            WWearhouse.edit();
        }
        if(e.keyCode == 113) {//F2
            WWearhouse.refresh();
        }
        if(e.keyCode == 46) {//DEL
            WWearhouse.remove();
        }
        if(e.keyCode == 13) {
            WWearhouse.search();
        }
    } else{ //弹框
        if(e.keyCode == 27) {//ESC
            WWearhouse.closeDg(0);
        }
        if(e.keyCode == 13) {//Enter
            if(e.ctrlKey) {//Ctrl+Enter
                WWearhouse.saveNext();
            } else {
                WWearhouse.save();
            }
        }
    }
};
WWearhouse.init = function () {
    //快捷键
    document.onkeyup = WWearhouse.keyUp;

    //先获取焦点再失去焦点，否则打开页面后若不点击页面，快捷键无效
    $("#searchName").next().find(".textbox-text").focus()
    $("#searchName").next().find(".textbox-text").blur()
};
//
WWearhouse.refresh = function () {
    $('#wWearhouse_table').datagrid('unselectAll');
    WWearhouse.dtable.treegrid('reload');
};
//搜索
WWearhouse.search = function () {

    WWearhouse.dtable.treegrid('load', WWearhouse.sform.serializeObject());
};

$(function () {
    WWearhouse.sform = $('#wWearhouse_form');
    WWearhouse.dtable = $('#wWearhouse_table');
    WWearhouse.obtn = '#wWearhouse_btn';
  
    WWearhouse.dg(); // 数据表格
    WWearhouse.btn(); // 初始化按钮
    $yd.bindEnter(WWearhouse.search);
    WWearhouse.init();
});
var btn1 = document.getElementById("searchButton");
btn1.addEventListener("click", function() {
    $('#wWearhouse_table').datagrid('unselectAll');
    //WWearhouse.dtable.datagrid('load', WWearhouse.sform.serializeObject());
    WWearhouse.dtable.treegrid('load', WWearhouse.sform.serializeObject());
});