<!-- c_check_detail.ftl 盘点单详情 -->
var CheckAdd = {
    uList: '/cCheck/repositoryList',
    editIndex: -1,
    cateogryId: 0,
    isConfirm: 1
};

CheckAdd.initForm = function () {
    $("#houseId").combobox({
        prompt:'仓库',
        url: "/wWearhouse/list",
        panelHeight:'auto',
        editable:false,
        required:true,
        valueField:'id',
        textField:'name',
        onLoadSuccess: function () {
            var d = $(this).combobox("getData");
            $(this).combobox("setValue", d[0].id);
        },
        onChange: function (newValue, oldValue) {
            if(CheckAdd.isConfirm == 0) {
                return;
            }
            if(!$yd.isEmpty(oldValue)) {
                $.messager.confirm({
                    title: '确认框',
                    border: false,
                    top: 0,
                    msg: "切换仓库清除当前仓库已盘点的商品，确定切换？",
                    fn: function (r) {
                        if (r) {
                            CheckAdd.doSearch(1);
                        } else {
                            CheckAdd.isConfirm = 0;
                            $("#houseId").combobox("setValue", oldValue);
                            CheckAdd.isConfirm = 1;
                        }
                    }
                })
            } else {
                CheckAdd.doSearch();
            }
        }
    });

    $("#cateogryId").combotree({
        prompt:'分类',
        url: "/mCateogry/tree",
        panelHeight:'200px',
        editable:false,
        required:false,
        valueField:'id',
        textField:'text',
        onChange: function (node) {
            CheckAdd.doSearch();
        }
    });
};

CheckAdd.doSearch = function(isFreshDg) {
    CheckAdd.tree.treegrid({
        url: '/mMateriel/treegridForCheck',
        queryParams: {
            houseId: $("#houseId").val(),
            searchText: $("#searchText").val(),
            categoryId: $("#cateogryId").val()
        }
    });
    CheckAdd.tree.treegrid("reload");
    CheckAdd.uncheckAll(isFreshDg);
};
CheckAdd.uncheckAll = function (isFreshDg) {
    var data = CheckAdd.tree.treegrid("getData");
    for(var i=0; i<data.length; i++) {
        CheckAdd.tree.treegrid("uncheckNode", data[i].id);
        if(isFreshDg && isFreshDg == 1) {
            CheckAdd.checkOff(data[i]);
        }
    }
};
CheckAdd.tree = function () {
    CheckAdd.tree = $("#goodsTb");
    CheckAdd.tree.treegrid({
        singleSelect: false,
        checkbox: true,
        idField:'id',
        treeField:'text',
        columns:[[
            {title:'商品/规格',field:'text',width:250},
            {title:'分类',field:'categoryName',width:150},
            {title:'pid', field:'pid', hidden:true}
        ]],
        onClickRow: function (row) {
            CheckAdd.tree.treegrid("unselectAll");
        },
        onCheckNode: function (row,checked) {
            if(checked == true) {
                var data = row.id.substr(0,1) == "s" ? {skuId: row.id.substr(1)} : {goodsId: row.id};
                data.houseId = $("#houseId").val();
                $.post("/inRepositoryDetail/getDetailBySku", data, function (d) {
                    for(var i=0; i<d.length; i++) {
                        if(!CheckAdd.isExsit(d[i].skuId, d[i].positionId)) {
                            CheckAdd.dtable.datagrid("insertRow", {
                                index: 0,
                                row:{
                                    skuId:d[i].skuId,
                                    goodsId:d[i].goodsId,
                                    goodsName:d[i].goodsName,
                                    skus:d[i].skus,
                                    skuCode:d[i].skuCode,
                                    positionId:d[i].positionId,
                                    positionName:d[i].positionName,
                                    count:d[i].count,
                                    checkCount:d[i].count,
                                    nextPositionName:d[i].nextPositionName
                                }
                            });
                        }
                    }
                });
            }
        }
    });
};
CheckAdd.checkOff = function (row) {
    if(row.id.substr(0,1) == "s") {//规格取消
        CheckAdd.delBySku(row.id.substr(1));
    } else if(row.children.length != 0) {//已加载子节点
        for(var i=0; i<row.children.length; i++) {
            CheckAdd.delBySku(row.children[i].id.substr(1));
        }
    } else {
        CheckAdd.delByGoods(row.id);
    }
}
CheckAdd.isExsit = function (skuId, positionId) {
    var rows = CheckAdd.dtable.datagrid("getRows");
    for(var i=0; i<rows.length; i++) {
        if(rows[i].positionId == positionId && rows[i].skuId == skuId) {
            return true;
        }
    }
    return false;
};

CheckAdd.delBySku = function (skuId, last) {
    if($yd.isEmpty(last)) {
        last = 0;
    }
    var rows = CheckAdd.dtable.datagrid("getRows");
    if(last >= rows.length) {
        return;
    }
    for(var i=last; i<rows.length; i++) {
        if(rows[i].skuId == skuId) {
            CheckAdd.dtable.datagrid("deleteRow", i);
            CheckAdd.delBySku(skuId, i);
            return;
        }
    }
};

CheckAdd.delByGoods = function (goodsId, last) {
    if($yd.isEmpty(last)) {
        last = 0;
    }
    var rows = CheckAdd.dtable.datagrid("getRows");
    if(last >= rows.length) {
        return;
    }
    for(var i=last; i<rows.length; i++) {
        if(rows[i].goodsId == goodsId) {
            CheckAdd.dtable.datagrid("deleteRow", i);
            CheckAdd.delByGoods(goodsId, i);
            return;
        }
    }
};

CheckAdd.delByBatch = function (positionId, skuId) {
    var rows = CheckAdd.dtable.datagrid("getRows");
    for(var i=0; i<rows.length; i++) {
        if(rows[i].positionId == positionId && rows[i].skuId == skuId) {
            CheckAdd.dtable.datagrid("deleteRow", i);
            return;
        }
    }
};

CheckAdd.remove = function() {
    var rows = CheckAdd.getCheckeds();
    if(rows.length == 0) {
        $yd.alert("选中记录");
        return;
    }
    var arr = [], arr2 = [];
    for(var i=0; i<rows.length; i++) {
        arr[i] = rows[i].positionId;
        arr2[i] = rows[i].skuId;
    }
    for(var i=0; i<arr.length; i++) {
        CheckAdd.delByBatch(arr[i], arr2[i]);
    }
};

CheckAdd.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.pagination = false; // 不分页
    grid.checkOnSelect = false; // 选择行不选中复选框
    grid.columns = [[
        {field: 'skuId', checkbox: true}
        , {field: 'goodsId', title: '商品Id', hidden: true}
        , {field: 'goodsName', title: '商品名称', width: 80}
        , {field: 'skus', title: '规格', width: 150}
        , {field: 'skuCode', title: '编码', width: 80}
        // , {field: 'positionId', title: '库位id', width: 80, hidden: true}
        , {field: 'nextPositionName', title: '默认库位名', width: 80, hidden: true}
        // , {field: 'positionName', title: '库位', width: 80,formatter:function (value,row) {
        //     if (value == null || value == '' || value == undefined) {
        //         return row['nextPositionName']
        //     }else {
        //         return value;
        //     }
        // }}
        , {field: 'count', title: '当前库存', width: 80,formatter:function(value,row){
            if (value == null || value == '') {
                return 0
            }else {
                return value;
            }
        }}
        , {
            field: 'checkCount', title: '<span style="color:blue">盘点库存</span><span style="color:red">*</span>', width: 80,
            editor: {
                type: 'numberbox', options: {
                    required: true,
                    min :0,
                    precision: 0
                }
            }
        },
        {
            field: 'remark',
            title: '<span style="color:blue">备注</span>',
            width: 100,
            editor: {type: 'textbox'}
        }

    ]];
    grid.onClickCell = function (index, field) {
        if (CheckAdd.editIndex != index) {
            if (CheckAdd.editIndex > -1) {
                CheckAdd.dtable.datagrid('endEdit', CheckAdd.editIndex);
            }
            CheckAdd.editIndex = index;
        }
        if (field == "checkCount") {
            CheckAdd.dtable.datagrid('beginEdit', index);
        }
    };
    grid.loadGrid();
    CheckAdd.dtable.datagrid('enableCellEditing'); // 启用编辑
};
// 处理按钮
CheckAdd.btn = function () {
    var sb = new $yd.SearchBtn($(CheckAdd.obtn));
    sb.create('remove red', '删除', CheckAdd.remove);
    sb.create('save green', '保存', CheckAdd.save);
};

// 重置
CheckAdd.reset = function () {
    $("form.search-form").form('clear');
}
// 获得选中行
CheckAdd.getCheckeds = function () {
    return CheckAdd.dtable.datagrid('getChecked');
};
//保存
CheckAdd.save = function () {
    //结束编辑状态
    var eaRows = CheckAdd.dtable.datagrid('getRows');
    $(eaRows).each(function (index, item) {
        CheckAdd.dtable.datagrid('endEdit', index);
    });
    var sels = CheckAdd.dtable.datagrid('getRows');
    if (sels.length == 0) {
        $yd.alert('无记录');
        return;
    }
    // 校验必填项
    for (var i = 0; i < sels.length; i++) {
        var row = sels[i];
        if ($yd.isEmpty(row.checkCount)) {
            $yd.alert("盘点数量不能为空");
            return false;
        }
    }
    $yd.confirm(function () {
        var data = JSON.stringify(sels);
        $yd.post("/cCheck/save", {details: data, houseId: $("#houseId").val()}, function () {
            $yd.alert("操作成功", function () {
                window.location.reload();
            });
        });
    });
};

$(function () {
    CheckAdd.initForm();
    CheckAdd.tree();
    CheckAdd.obtn = '#checkAdd_btn';
    CheckAdd.sform = $('#checkAdd_form');
    CheckAdd.dtable = $('#checkAdd_table');
    CheckAdd.dg(); // 数据表格
    CheckAdd.btn(); // 初始化按钮
});