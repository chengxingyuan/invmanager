<!-- w_position_materiel.ftl 库位对应的商品 -->
var WPositionMateriel = {
    u: '/wPositionMateriel'
    , uList: '/wPositionMateriel/list'
    , uEdit: '/wPositionMateriel/edit'
    , uDel: '/wPositionMateriel/delete'
};

WPositionMateriel.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        WPositionMateriel._edit(row['id']);
    };
    grid.url = WPositionMateriel.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'wearhouseId', title: '仓库id', width: 80}
        , {field: 'positionId', title: '库位id', width: 80}
        , {field: 'materielId', title: '商品id', width: 80}
        , {field: 'isDefault', title: '是否是默认（0不是默认，1 是默认）', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
WPositionMateriel.dg.getOne = function () {
    var sels = WPositionMateriel.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
WPositionMateriel.dg.getSels = function () {
    return WPositionMateriel.dtable.datagrid('getSelections');
};
// 处理按钮
WPositionMateriel.btn = function () {
    var sb = new $yd.SearchBtn($(WPositionMateriel.obtn));
    sb.create('remove', '删除', WPositionMateriel.remove);
    sb.create('refresh', '刷新', WPositionMateriel.refresh);
    sb.create('edit', '编辑', WPositionMateriel.edit);
    sb.create('plus', '新增', WPositionMateriel.plus);
    sb.create('search', '查询', WPositionMateriel.search);
};
//删除
WPositionMateriel.remove = function () {
    var sels = WPositionMateriel.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(WPositionMateriel.uDel, {ids: ids}, function () {
            WPositionMateriel.refresh();
        });
    })
};
//
WPositionMateriel.edit = function () {
    var sel = WPositionMateriel.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    WPositionMateriel._edit(sel['id'])
};
// 表单预处理
WPositionMateriel.preForm = function () {
    new $yd.combobox("tenantId").load("/tenant/select");
    new $yd.combobox("wearhouseId").load("/wearhouse/select");
    new $yd.combobox("positionId").load("/position/select");
    new $yd.combobox("materielId").load("/materiel/select");
};
WPositionMateriel._edit = function (id) {
    $yd.win.create('编辑库位对应的商品', WPositionMateriel.uEdit + '?id=' + id).open(function (that, $win) {
            WPositionMateriel.preForm();
            $yd.get(WPositionMateriel.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(WPositionMateriel.u).submit(function () {
                that.close();
                WPositionMateriel.refresh();
            });
        }
    );
};
//新增
WPositionMateriel.plus = function () {
    var sel = WPositionMateriel.dg.getOne();
    var href = WPositionMateriel.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增库位对应的商品', href).open(function (that, $win){
            WPositionMateriel.preForm();
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(WPositionMateriel.u).submit(function () {
                that.close();
                WPositionMateriel.refresh();
            });
        }
    );
};
//
WPositionMateriel.refresh = function () {
    WPositionMateriel.dtable.datagrid('reload');
};
//搜索
WPositionMateriel.search = function () {
    WPositionMateriel.dtable.datagrid('load', WPositionMateriel.sform.serializeObject());
};

$(function () {
    WPositionMateriel.sform = $('#wPositionMateriel_form');
    WPositionMateriel.dtable = $('#wPositionMateriel_table');
    WPositionMateriel.obtn = '#wPositionMateriel_btn';

    WPositionMateriel.dg(); // 数据表格
    WPositionMateriel.btn(); // 初始化按钮
    $yd.bindEnter(WPositionMateriel.search);
});