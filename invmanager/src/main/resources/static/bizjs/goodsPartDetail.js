<!-- goods_part_detail.ftl 盘点单详情 -->
var GoodsPartDetail = {
    u: '/goodsPartDetail'
    , uList: '/goodsPartDetail/list'
    , uEdit: '/goodsPartDetail/edit'
    , uDel: '/goodsPartDetail/delete'
};

GoodsPartDetail.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        GoodsPartDetail._edit(row['id']);
    };
    grid.url = GoodsPartDetail.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'partId', title: '拆装总单id', width: 80}
        , {field: 'wearhouseId', title: '仓库', width: 80}
        , {field: 'positionId', title: '库位id', width: 80}
        , {field: 'skuId', title: '商品skuid', width: 80}
        , {field: 'batchNumber', title: '批号', width: 80}
        , {field: 'count', title: '拆装数量', width: 80}
        , {field: 'amount', title: '拆装单价', width: 80}
        , {field: 'totalAmount', title: '拆装总额', width: 80}
        , {field: 'username', title: '操作人姓名', width: 80}
        , {field: 'remark', title: '备注', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
GoodsPartDetail.dg.getOne = function () {
    var sels = GoodsPartDetail.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
GoodsPartDetail.dg.getSels = function () {
    return GoodsPartDetail.dtable.datagrid('getSelections');
};
// 处理按钮
GoodsPartDetail.btn = function () {
    var sb = new $yd.SearchBtn($(GoodsPartDetail.obtn));
    sb.create('remove', '删除', GoodsPartDetail.remove);
    sb.create('refresh', '刷新', GoodsPartDetail.refresh);
    sb.create('edit', '编辑', GoodsPartDetail.edit);
    sb.create('plus', '新增', GoodsPartDetail.plus);
    sb.create('search', '查询', GoodsPartDetail.search);
};
//删除
GoodsPartDetail.remove = function () {
    var sels = GoodsPartDetail.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(GoodsPartDetail.uDel, {ids: ids}, function () {
            GoodsPartDetail.refresh();
        });
    })
};
//
GoodsPartDetail.edit = function () {
    var sel = GoodsPartDetail.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    GoodsPartDetail._edit(sel['id'])
};
// 表单预处理
GoodsPartDetail.preForm = function () {
    new $yd.combobox("partId").load("/part/select");
    new $yd.combobox("wearhouseId").load("/wearhouse/select");
    new $yd.combobox("positionId").load("/position/select");
    new $yd.combobox("skuId").load("/sku/select");
};
GoodsPartDetail._edit = function (id) {
    $yd.win.create('编辑盘点单详情', GoodsPartDetail.uEdit + '?id=' + id).open(function (that, $win) {
            GoodsPartDetail.preForm();
            $yd.get(GoodsPartDetail.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(GoodsPartDetail.u).submit(function () {
                that.close();
                GoodsPartDetail.refresh();
            });
        }
    );
};
//新增
GoodsPartDetail.plus = function () {
    var sel = GoodsPartDetail.dg.getOne();
    var href = GoodsPartDetail.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增盘点单详情', href).open(function (that, $win){
            GoodsPartDetail.preForm();
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(GoodsPartDetail.u).submit(function () {
                that.close();
                GoodsPartDetail.refresh();
            });
        }
    );
};
//
GoodsPartDetail.refresh = function () {
    GoodsPartDetail.dtable.datagrid('reload');
};
//搜索
GoodsPartDetail.search = function () {
    GoodsPartDetail.dtable.datagrid('load', GoodsPartDetail.sform.serializeObject());
};

$(function () {
    GoodsPartDetail.sform = $('#goodsPartDetail_form');
    GoodsPartDetail.dtable = $('#goodsPartDetail_table');
    GoodsPartDetail.obtn = '#goodsPartDetail_btn';

    GoodsPartDetail.dg(); // 数据表格
    GoodsPartDetail.btn(); // 初始化按钮
    $yd.bindEnter(GoodsPartDetail.search);
});