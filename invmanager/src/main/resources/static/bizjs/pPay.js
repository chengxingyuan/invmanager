<!-- p_pay.ftl 采购单支付流水 -->
var PPay = {
    u: '/pPay'
    , uList: '/pPay/list'
    , uEdit: '/pPay/edit'
    , uDel: '/pPay/delete'
};

PPay.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        PPay._edit(row['id']);
    };
    grid.url = PPay.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'purchaseId', title: '采购总单id', width: 80}
        , {field: 'relativeId', title: '往来商户id', width: 80}
        , {field: 'type', title: '支付类型（0 现金，1 转账，2 支付宝，3 微信）', width: 80}
        , {field: 'amount', title: '金额', width: 80}
        , {field: 'remark', title: '备注', width: 80}
        , {field: 'username', title: '操作人姓名', width: 80}
        , {field: 'useType', title: '支付用途（0 支付，1 收款）', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
PPay.dg.getOne = function () {
    var sels = PPay.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
PPay.dg.getSels = function () {
    return PPay.dtable.datagrid('getSelections');
};
// 处理按钮
PPay.btn = function () {
    var sb = new $yd.SearchBtn($(PPay.obtn));
    sb.create('remove', '删除', PPay.remove);
    sb.create('refresh', '刷新', PPay.refresh);
    sb.create('edit', '编辑', PPay.edit);
    sb.create('plus', '新增', PPay.plus);
    sb.create('search', '查询', PPay.search);
};
//删除
PPay.remove = function () {
    var sels = PPay.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(PPay.uDel, {ids: ids}, function () {
            PPay.refresh();
        });
    })
};
//
PPay.edit = function () {
    var sel = PPay.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    PPay._edit(sel['id'])
};
// 表单预处理
PPay.preForm = function () {
    new $yd.combobox("purchaseId").load("/purchase/select");
    new $yd.combobox("relativeId").load("/relative/select");
};
PPay._edit = function (id) {
    $yd.win.create('编辑采购单支付流水', PPay.uEdit + '?id=' + id).open(function (that, $win) {
            PPay.preForm();
            $yd.get(PPay.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(PPay.u).submit(function () {
                that.close();
                PPay.refresh();
            });
        }
    );
};
//新增
PPay.plus = function () {
    var sel = PPay.dg.getOne();
    var href = PPay.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增采购单支付流水', href).open(function (that, $win){
            PPay.preForm();
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(PPay.u).submit(function () {
                that.close();
                PPay.refresh();
            });
        }
    );
};
//
PPay.refresh = function () {
    PPay.dtable.datagrid('reload');
};
//搜索
PPay.search = function () {
    PPay.dtable.datagrid('load', PPay.sform.serializeObject());
};

$(function () {
    PPay.sform = $('#pPay_form');
    PPay.dtable = $('#pPay_table');
    PPay.obtn = '#pPay_btn';

    PPay.dg(); // 数据表格
    PPay.btn(); // 初始化按钮
    $yd.bindEnter(PPay.search);
});