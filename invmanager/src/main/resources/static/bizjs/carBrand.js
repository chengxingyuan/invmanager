<!-- car_brand.ftl 车品牌 -->
var CarBrand = {
    u: '/carBrand'
    , uList: '/carBrand/list'
    , uEdit: '/carBrand/edit'
    , uDel: '/carBrand/delete'
};

CarBrand.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        CarBrand._edit(row['id']);
    };
    grid.url = CarBrand.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'name', title: '车品牌名中文名', width: 80}
        , {field: 'logo', title: '图片地址', width: 80}
        , {field: 'initial', title: '首字母', width: 80}
        , {field: 'level', title: '等级', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
CarBrand.dg.getOne = function () {
    var sels = CarBrand.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
CarBrand.dg.getSels = function () {
    return CarBrand.dtable.datagrid('getSelections');
};
// 处理按钮
CarBrand.btn = function () {
    var sb = new $yd.SearchBtn($(CarBrand.obtn));
    sb.create('remove', '删除', CarBrand.remove);
    sb.create('refresh', '刷新', CarBrand.refresh);
    sb.create('edit', '编辑', CarBrand.edit);
    sb.create('plus', '新增', CarBrand.plus);
    sb.create('search', '查询', CarBrand.search);
};
//删除
CarBrand.remove = function () {
    var sels = CarBrand.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(CarBrand.uDel, {ids: ids}, function () {
            CarBrand.refresh();
        });
    })
};
//
CarBrand.edit = function () {
    var sel = CarBrand.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    CarBrand._edit(sel['id'])
};
CarBrand._edit = function (id) {
    $yd.win.create('编辑车品牌', CarBrand.uEdit + '?id=' + id).open(function (that, $win) {
            $yd.get(CarBrand.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(CarBrand.u).submit(function () {
                that.close();
                CarBrand.refresh();
            });
        }
    );
};
//新增
CarBrand.plus = function () {
    var sel = CarBrand.dg.getOne();
    var href = CarBrand.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增车品牌', href).open(function (that, $win){
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(CarBrand.u).submit(function () {
                that.close();
                CarBrand.refresh();
            });
        }
    );
};
//
CarBrand.refresh = function () {
    CarBrand.dtable.datagrid('reload');
};
//搜索
CarBrand.search = function () {
    CarBrand.dtable.datagrid('load', CarBrand.sform.serializeObject());
};

$(function () {
    CarBrand.sform = $('#carBrand_form');
    CarBrand.dtable = $('#carBrand_table');
    CarBrand.obtn = '#carBrand_btn';

    CarBrand.dg(); // 数据表格
    CarBrand.btn(); // 初始化按钮
    $yd.bindEnter(CarBrand.search);
});