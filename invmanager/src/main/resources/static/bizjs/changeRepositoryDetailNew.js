<!-- in_repository_detail.ftl 入库单详情 -->
var ChangeRepositoryDetail = {
    u: '/changeRepository'
    , uList: '/changeRepository/getOne'
    , uEdit: '/changeRepository/edit'
    , uDel: '/changeRepository/delete'
};
ChangeRepositoryDetail.code =0;
ChangeRepositoryDetail.dg = function () {

    var grid = new $yd.DataGrid(this.dtable);
    // grid.onDblClickRow = function (index,row) {
    //     ChangeRepositoryDetail._edit(row['id']);
    // };
    grid.url = ChangeRepositoryDetail.uList + "?code="+ ChangeRepositoryDetail.code;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'code', title: '调拨单号', width: 80}
        // , {field: 'purchaseDetailId', title: '关联的采购单号', width: 80}
        , {field: 'ctime', title: '单据日期', width: 80,formatter:function(value,row,index){
            if (value == null){
                return '';
            }else {
                value = value.substr(0, value.length - 9);
                return value;
            }
        }}
        , {field: 'goodsName', title: '入库商品', width: 80}
        , {field: 'count', title: '数量', width: 50}
        , {field: 'unit', title: '单位', width: 50}
        // , {field: 'typeValue', title: '入库类型', width: 80}
        // , {field: 'providerName', title: '供应商', width: 80}
        , {field: 'batch_number', title: '批号', width: 80}
        , {field: 'fromWearhouseName', title: '源仓库', width: 80}
        , {field: 'fromPositionName', title: '源库位', width: 80}
        , {field: 'toWearhouseName', title: '目的仓库', width: 80}
        , {field: 'toPositionName', title: '目的库位', width: 80}
        , {field: 'username', title: '操作人', width: 50}
        , {field: 'remark', title: '备注', width: 120}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        ,
        {field: 'ctime', title: '创建时间', width: 80}
    ]];
    grid.loadGrid();
};
ChangeRepositoryDetail.dg.getOne = function () {
    var sels = ChangeRepositoryDetail.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
ChangeRepositoryDetail.dg.getSels = function () {
    return ChangeRepositoryDetail.dtable.datagrid('getSelections');
};
// 处理按钮
ChangeRepositoryDetail.btn = function () {
    var sb = new $yd.SearchBtn($(ChangeRepositoryDetail.obtn));
    sb.create('repeat', '重置', ChangeRepositoryDetail.reset);
    sb.create('print green', '打印', function () {
        ChangeRepositoryDetail.dtable.datagrid('print', '入库表');
    });
    sb.create('remove', '删除', ChangeRepositoryDetail.remove);
    sb.create('refresh', '刷新', ChangeRepositoryDetail.refresh);
    // sb.create('edit', '编辑', ChangeRepositoryDetail.edit);  //href="javascript:void(0)"  onclick="self.parent.addTab('新窗口标题','url')"
    sb.create('plus', '新增', ChangeRepositoryDetail.plus);
    sb.create('search', '查询', ChangeRepositoryDetail.search);
};
//删除
ChangeRepositoryDetail.remove = function () {
    var sels = ChangeRepositoryDetail.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(ChangeRepositoryDetail.uDel, {ids: ids}, function () {
            ChangeRepositoryDetail.refresh();
        });
    })
};
//
ChangeRepositoryDetail.edit = function () {
    var sel = ChangeRepositoryDetail.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    ChangeRepositoryDetail._edit(sel['id'])
};
// 表单预处理
ChangeRepositoryDetail.preForm = function () {
    new $yd.combotreegrid("#providerId").addCol("name", "名称").addCol("code", "编码").load("/wRelativeUnit/list");
    new $yd.combotreegrid("#wearhouseId").addCol("name", "名称").addCol("code", "编码").load("/wWearhouse/list");
    new $yd.combotreegrid("#positionId").addCol("name", "名称").addCol("code", "编码").load("/wPosition/list");

};
ChangeRepositoryDetail._edit = function (id) {
    $yd.win.create('编辑入库单详情', ChangeRepositoryDetail.uEdit + '?id=' + id).open(function (that, $win) {
            ChangeRepositoryDetail.preForm();
            $yd.get(ChangeRepositoryDetail.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(ChangeRepositoryDetail.u).submit(function () {
                that.close();
                ChangeRepositoryDetail.refresh();
            });
        }
    );
};
//新增
ChangeRepositoryDetail.plus = function () {
    parent.inv.OpenPage("/changeRepository/newAddChangeRepository", "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\"> 新增入库单 </span>\n" +
        "<b class=\"arrow \"></b>",true)


};
//
ChangeRepositoryDetail.refresh = function () {
    ChangeRepositoryDetail.dtable.datagrid('reload');
};
//搜索
ChangeRepositoryDetail.search = function () {
    ChangeRepositoryDetail.dtable.datagrid('load', ChangeRepositoryDetail.sform.serializeObject());
};
//重置
ChangeRepositoryDetail.reset = function () {
    $(".search-form").form('clear');
}

$(function () {
    ChangeRepositoryDetail.sform = $('#changeRepositoryDetail_form');
    ChangeRepositoryDetail.dtable = $('#changeRepositoryDetail_table');
    ChangeRepositoryDetail.obtn = '#changeRepositoryDetail_btn';
    ChangeRepositoryDetail.code = $("#code").val();
    ChangeRepositoryDetail.dg(); // 数据表格
    ChangeRepositoryDetail.btn(); // 初始化按钮
    new $yd.combotreegrid("#wearhouseIdForSearch").addCol("name", "名称").addCol("code", "编码").load("/wWearhouse/list");
    $yd.bindEnter(ChangeRepositoryDetail.search);
});


