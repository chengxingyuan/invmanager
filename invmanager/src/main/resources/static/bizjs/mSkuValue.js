<!-- m_sku_value.ftl 物料规格值 -->
var MSkuValue = {
    u: '/mSkuValue'
    , uList: '/mSkuValue/list'
    , uEdit: '/mSkuValue/edit'
    , uDel: '/mSkuValue/delete'
};

MSkuValue.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index, row) {
        MSkuValue._edit(row['id']);
    };
    grid.toolbar = MSkuValue.obtn;
    grid.url = MSkuValue.uList;
    grid.rownumbers = true;
    grid.columns = [[
        {field: 'id', checkbox: false, hidden: true}
        , {field: 'name', title: '规格值名称', width: 80}
        // , {field: 'code', title: '编码', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else if (value == 1){
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'isDefault', title: '是否默认', width: 80, formatter: function (value) {
            if (value == 0) {
                return '否';
            } else {
                return '是';
            }
            }
        }
    ]];
    grid.onLoadSuccess = function() {
        MSkuValue.dtable.datagrid("unselectAll");
        var rows = MSkuValue.dtable.datagrid("getRows");
        if(rows.length != 0) {
            MSkuValue.dtable.datagrid("selectRow", 0);
        }
    };
    grid.loadGrid();
};
MSkuValue.dg.getOne = function () {
    var sels = MSkuValue.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
MSkuValue.dg.getSels = function () {
    return MSkuValue.dtable.datagrid('getSelections');
};
// 处理按钮
MSkuValue.btn = function () {
    var sb = new $yd.SearchBtn($(MSkuValue.obtn));
    sb.create('remove', '删除', MSkuValue.remove);
    sb.create('refresh', '刷新', MSkuValue.refresh);
    sb.create('edit', '编辑', MSkuValue.edit);
    sb.create('plus', '新增', MSkuValue.plus);
};
//删除
MSkuValue.remove = function () {
    var sels = MSkuValue.dg.getSels();
    if (sels.length == 0) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(MSkuValue.uDel, {ids: ids}, function () {
            MSkuValue.refresh();
        });
    })
};
//
MSkuValue.edit = function () {
    var sel = MSkuValue.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    MSkuValue._edit(sel['id'])
};
// 表单预处理
MSkuValue.preForm = function () {
    var record = MSku.dtable.datagrid('getSelected');
    new $yd.combobox("skuId").load("/mSku/select").setValue(record);
};
MSkuValue._edit = function (id) {
    MSku.dgType = "skuValue";
    var dg = $yd.win2.create('编辑存货规格值', MSkuValue.uEdit + '?id=' + id);
    dg.width = "500px";
    dg.height = "400px";
    dg.open(function (that, $win) {
            MSku.win = $win;
            MSku.winThat = that;
            MSkuValue.preForm();
            $yd.get(MSkuValue.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            MSkuValue.saveSku();
        }, function (that, $win) {
            MSku.closeDg(1);
        }
    );
};
//新增
MSkuValue.plus = function () {
    var sel = MSku.dg.getOne();
    if(!sel) {
        $yd.alert("请先添加规格");
        return;
    }
    var href = MSkuValue.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];

    MSku.dgType = "skuValue";
    var dg = $yd.win2.create('新增存货规格值', href);
    dg.width = "500px";
    dg.height = "400px";
    dg.open(function (that, $win) {
            MSku.win = $win;
            MSku.winThat = that;
            MSkuValue.preForm();
        }, function (that, $win) {
            MSkuValue.saveSku();
        }, function (that, $win) {
            MSku.closeDg(1);
        } , function (that, $win) {
            MSkuValue.saveSkuNext();
        }
    );
};
MSkuValue.saveSku = function () {
    $yd.form.create(MSku.win.find('form')).url(MSkuValue.u).submit(function () {
        MSkuValue.refresh();
        MSku.closeDg(0);
    });
};
MSkuValue.saveSkuNext = function () {
    $yd.form.create(MSku.win.find('form')).url(MSkuValue.u).submit(function () {
        MSkuValueEdit.reset();
        MSkuValue.refresh();
    });
};
//
MSkuValue.refresh = function () {
    MSkuValue.dtable.datagrid('unselectAll');
    MSkuValue.dtable.datagrid("reload");
};

$(function () {
    MSkuValue.sform = $('#mSkuValue_form');
    MSkuValue.dtable = $('#mSkuValue_table');
    MSkuValue.obtn = '#mSkuValue_btn';

    MSkuValue.dg(); // 数据表格
    MSkuValue.btn(); // 初始化按钮
});