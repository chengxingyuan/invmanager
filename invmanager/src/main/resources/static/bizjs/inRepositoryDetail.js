<!-- in_repository_detail.ftl 入库单详情 -->
var InRepositoryDetail = {
    uList: '/inRepositoryDetail/list'
    , typeArr: []
    , unitArr: []
};

InRepositoryDetail.dg = function () {
    $.ajax({
        type: "POST",
        url: "/sysDict/repository",
        dataType: "json",
        success: function (data) {
            InRepositoryDetail.typeArr = data;
        }
    });

    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index, row) {
        // InRepositoryDetail.detail(row.code);
        InRepositoryDetail.printDetail(row.id);
    };
    grid.rownumbers = true;
    grid.onClickRow = function() {
        InRepositoryDetail.dtable.datagrid("unselectAll");
    };
    grid.url = InRepositoryDetail.uList;
    grid.singleSelect=true,
    grid.onClickRow = function (index,row) {
           
            InRepositoryDetail.goodsTbale.datagrid("reload", {
                code:row.code,
            });
    }
    grid.columns = [[
        {field: 'id', title: '_', checkbox: true,hidden:true,value:1}
        , {
            field: 'code', title: '入库单号', width: 80,halign: 'center',align:'center'
        }

        , {
            field: 'orderTime', title: '单据日期', width: 80,halign: 'center',align:'center',  formatter: function (value, row, index) {
                if (value == null) {
                    return '';
                } else {
                    value = value.substr(0, value.length - 9);
                    return value;
                }
            }
        }
        // , {
        //     field: 'goodsName', title: '商品摘要', width: 160, formatter: function (value) {
        //         return InRepositoryDetail.formatDetail(value);
        //     }
        // }
        , {field: 'count', title: '数量', width: 50,halign: 'center',align:'center', }
        , {field: 'typeName', title: '入库类型', width: 80,halign: 'center',align:'center'
            // , formatter: function (value, row, index) {
            //     for(var i=0; i<InRepositoryDetail.typeArr.length; i++) {
            //         if(InRepositoryDetail.typeArr[i].value == value) {
            //             return InRepositoryDetail.typeArr[i].text;
            //         }
            //     }
            //     return "";
            // }
        }
        , {
            field: 'providerName', title: '往来单位', width: 80,halign: 'center',align:'center',
            // , formatter: function (value) {
            //     return InRepositoryDetail.formatDetail(value);
            // }
        }
        // , {field: 'wearhouseName', title: '仓库', width: 80, formatter: function (value) {
        //         return InRepositoryDetail.formatDetail(value);
        //     }}
        , {field: 'username', title: '创建人', width: 50,halign: 'center',align:'center', }
        , {
            field: 'rcode', title: '关联单号', width: 100,halign: 'center',align:'center',
        }
        , {field: 'remark', title: '备注', width: 120,halign: 'center',align:'center', }
        , {field: 'ctime', title: '创建时间', width: 80,halign: 'center',align:'center', }
    ]];
    grid.loadGrid();
};

InRepositoryDetail.isEmpty = function (value) {
    if (value == undefined || value == null || value == "") {
        return true;
    }
    return false;
};
InRepositoryDetail.formatDetail = function (value) {
    if (InRepositoryDetail.isEmpty(value)) {
        return "";
    }
    var result = "";
    var first = "";
    var arr = value.split(",");
    if (arr.length == 1) {
        return "<a style='color:purple' title='" + value + "'>" + value + "</a>";
    }
    for (var i = 0; i < arr.length; i++) {
        result += arr[i] + "\n";
        if (i == 0) {
            first = result;
        }
    }
    var html = "<a style='color:purple' title='" + result + "'>" + first + "..." + "</a>";

    return html;
};
InRepositoryDetail.dg.getOne = function () {
    var sels = InRepositoryDetail.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
InRepositoryDetail.dg.getSels = function () {
    return InRepositoryDetail.dtable.datagrid('getSelections');
};
// 处理按钮
InRepositoryDetail.btn = function () {
    var sb = new $yd.SearchBtn($(InRepositoryDetail.obtn));
    sb.create('repeat red', '重置', InRepositoryDetail.reset);
    sb.create('print green', '打印',InRepositoryDetail.print);
    sb.create('refresh', '刷新', InRepositoryDetail.refresh);
    // sb.create('plus', '新增', InRepositoryDetail.plus);
};
InRepositoryDetail.print = function () {
    var rows = InRepositoryDetail.dtable.datagrid('getRows');
    var ids = [];
    for (var i=0;i<rows.length;i++) {
        ids.push(rows[i]['id'])
    }
    parent.inv.OpenPage("/inRepositoryDetail/printList?ids=" + ids, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\"> 打印入库列表</span>\n" +
        "<b class=\"arrow \"></b>", true)

};

InRepositoryDetail.detail = function (code) {
    parent.inv.OpenPage("/inRepositoryDetail/detail?code=" + code,"<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\"> 入库单详情</span>\n" +
        "<b class=\"arrow \"></b>", true);
};
InRepositoryDetail.printDetail = function (code) {
    parent.inv.OpenPage("/inRepositoryDetail/printDetail?code=" + code,"<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\"> 打印入库单</span>\n" +
        "<b class=\"arrow \"></b>", true);
};
InRepositoryDetail.refresh = function () {
    window.location.reload();
};
//搜索
InRepositoryDetail.search = function () {
    InRepositoryDetail.dtable.datagrid('load', InRepositoryDetail.sform.serializeObject());
};
//重置
InRepositoryDetail.reset = function () {
    $(".search-form").form('clear');
    InRepositoryDetail.search();
};
//新增
InRepositoryDetail.plus = function () {
    parent.inv.OpenPage("/inRepositoryDetail/newAddInRepository", "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\"> 新增入库单 </span>\n" +
        "<b class=\"arrow \"></b>", true)
}

InRepositoryDetail.loadGoodsTable = function () {
    $.ajax({
        type: "POST",
        url: "/sysDict/materiel_MU",
        dataType: "json",
        success: function (data) {
            InRepositoryDetail.unitArr = data;
        }
    });
    // MMaterielTticar.relativeTable = $("#relativeGoods_table");
    var grid = new $yd.DataGrid(InRepositoryDetail.goodsTbale);
    grid.url ='/inRepositoryDetail/getOne';
    grid.rownumbers = true;
    grid.singleSelect = true;
    grid.pagination=false,
    grid.columns= [[
        {field: 'id', checkbox: false, hidden:true},
        {field: 'goodsName', title: '商品名称', width: 150,halign:'center',align:'center'},
        {field: 'skus', title: '规格', width: 200,halign:'center',align:'center'},
        {field: 'code', title: '商品编号', width: 100,halign:'center',align:'center'},

        {
            field: 'unit', title: '单位', width: 80,halign:'center',align:'center', formatter: function (value, row, index) {
            for (var i = 0; i < InRepositoryDetail.unitArr.length; i++) {
                if (value == InRepositoryDetail.unitArr[i].id) {
                    return InRepositoryDetail.unitArr[i].text;
                }
            }
            return value;
        }
        }

        , {field: 'providerName', title: '供应商', width: 80,halign:'center',align:'center'}
        , {field: 'houseName', title: '仓库', width: 80,halign:'center',align:'center'}
        // , {field: 'houseName', title: '仓库/库位', width: 80,halign:'center',align:'center',formatter:function (value,row) {
        //     return value+ ':' + row['positionName'];
        // }}
        , {field: 'positionName', title: '库位', width: 80,hidden:true}
        , {field: 'price', title: '单价',halign:'center',align:'center', width: 80}
        , {field: 'count', title: '数量', width: 50,halign:'center',align:'center'}
        , {field: 'remark', title: '总额', width: 80,halign:'center',align:'center',formatter:function (value,row) {
            if(row['price'] == null || row['price'] == undefined || row['price']=='') {
                return 0;
            }
            return row['price'] * row['count'];
        }}
    ]]
    grid.loadGrid();
}

$(function () {
    InRepositoryDetail.sform = $('#inRepositoryDetail_form');
    InRepositoryDetail.dtable = $('#inRepositoryDetail_table');
    InRepositoryDetail.obtn = '#inRepositoryDetail_btn';

    InRepositoryDetail.goodsTbale = $('#inRepositoryGoods_table');
    InRepositoryDetail.dg(); // 数据表格
    InRepositoryDetail.btn(); // 初始化按钮
    InRepositoryDetail.loadGoodsTable();
    new $yd.combotreegrid("#wearhouseId").addCol("name", "名称").addCol("code", "编码").load("/wWearhouse/listForAvailable");

    $("input[name='inRepositoryType']").combobox({
        url: "/sysDict/repository",
        panelHeight: 'auto',
        valueField: 'id',
        textField: 'text',
        editable: false,
    });
    // $("#wearhouseId").combobox({
    //     editable: false,
    //     valueField: 'id',
    //     textField: 'name',
    //     url:"/wWearhouse/listForAvailable"
    // });

    $yd.bindEnter(InRepositoryDetail.search);

    if(!$yd.isEmpty($("#hidHouseId").val())) {
        $("#wearhouseId").combotreegrid("setValue", {id:$("#hidHouseId").val(),name:$("#hidHouseName").val()});
        $("#startTime").datebox("setValue", $("#hidStartTime").val());
        $("#endTime").datebox("setValue", $("#hidEndTime").val());
        InRepositoryDetail.search();
    }
});

var btn1 = document.getElementById("searchButton");
btn1.addEventListener("click", function () {
    InRepositoryDetail.dtable.datagrid('load', InRepositoryDetail.sform.serializeObject());
});
