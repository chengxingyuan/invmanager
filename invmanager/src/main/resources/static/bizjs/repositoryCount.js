<!-- repository_count.ftl 商品库存数量表 -->
var RepositoryCount = {
    uList: '/repositoryCount/list'
    , unitArr: []
};

RepositoryCount.dg = function () {
    // $.ajax({
    //     type: "POST",
    //     url: "/sysDict/materiel_MU",
    //     dataType: "json",
    //     async: true,
    //     success: function (data) {
    //         RepositoryCount.unitArr = data;
    //     }
    // });

    var grid = new $yd.DataGrid(this.dtable);
    grid.onClickRow = function(index,row) {
        // RepositoryCount.tree.treegrid("reload", {
        //     skuId: row.id,
        //     type: 0
        // });
        RepositoryCount.repositoryTable.datagrid("reload", {
            skuId: row.id,
        });
    };
    grid.onLoadSuccess = function() {
        var rows = RepositoryCount.dtable.datagrid("getRows");
        // if(rows.length > 0) {
        //     RepositoryCount.dtable.datagrid("selectRow",0);
        //     RepositoryCount.tree.treegrid("reload", {
        //         skuId: rows[0].id,
        //         type: 0
        //     });
        // } else {
        //     RepositoryCount.tree.treegrid("reload", {
        //         skuId: 0,
        //         type: 0
        //     });
        // }
        if(rows.length > 0) {
            RepositoryCount.dtable.datagrid("selectRow",0);
            RepositoryCount.repositoryTable.datagrid("reload", {
                skuId: rows[0].id,
            });
        } else {
            RepositoryCount.repositoryTable.datagrid("reload", {
                skuId: 0,
            });
        }
    };
    grid.singleSelect = true;
    grid.url = RepositoryCount.uList;
    grid.rownumbers = true;
    grid.fitColumns = true;
    grid.columns = [[
        {field: 'id', checkbox: false, hidden: true,halign:'center',align:"center"}
        , {field: 'goodsName', title: '存货名称', width: 80,halign:'center',align:"center"}

        , {field: 'skus', title: '规格', width: 100,halign:'center',align:"center",halign:'center',align:"center"}
        , {field: 'categoryName', title: '分类', width: 40,halign:'center',align:"center"}
        , {field: 'skuCode', title: '商品编号', width: 40,halign:'center',align:"center"}
        // , {
        //     field: 'unit', title: '单位', width: 20, formatter: function (value, row, index) {
        //         for (var i = 0; i < RepositoryCount.unitArr.length; i++) {
        //             if (value == RepositoryCount.unitArr[i].id) {
        //                 return RepositoryCount.unitArr[i].text;
        //             }
        //         }
        //         return value;
        //     }
        // }
        , {field: 'totalNum', title: '库存', width: 40,halign:'center',align:"center"}
        , {field: 'wearhouseName', title: '默认仓库', width: 80,halign:'center',align:"center"}
        , {field: 'supplierName', title: '默认供应商', width: 80,halign:'center',align:"center"}
    ]];
    grid.loadGrid();
};

// RepositoryCount.tree = function() {
//     RepositoryCount.tree = $("#kwTb");
//     RepositoryCount.tree.treegrid({
//         url: '/wWearhouse/treeGrid',
//         idField:'id',
//         treeField:'text',
//         columns:[[
//             {title:'仓库/库位/批次',field:'text',width:250},
//             {title:'库存',field:'num',width:200}
//         ]]
//     });
// };


RepositoryCount.repositoryTableInit = function() {
    RepositoryCount.repositoryTable = $("#kwTb");
    var grid = new $yd.DataGrid(RepositoryCount.repositoryTable);
    RepositoryCount.repositoryTable.datagrid({
        url: '/repositoryCount/getSpecific',
        fitColumns:true,
        columns:[[
            {title:'仓库',field:'repositoryName',width:250},
            {title:'库存数量',field:'count',width:200}
        ]]
    });
};

RepositoryCount.formatDetail = function (value) {
    if ($yd.isEmpty(value)) {
        return "";
    }
    var result = "";
    var first = "";
    var arr = value.split(",");
    if (arr.length == 1) {
        return "<a style='color:purple' title='" + value + "'>" + value + "</a>";
    }
    for (var i = 0; i < arr.length; i++) {
        result += arr[i] + "\n";
        if (i == 0) {
            first = result;
        }
    }
    var html = "<a style='color:purple' title='" + result + "'>" + first + "..." + "</a>";

    return html;
};
RepositoryCount.dg.getOne = function () {
    var sels = RepositoryCount.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
RepositoryCount.dg.getSels = function () {
    return RepositoryCount.dtable.datagrid('getSelections');
};
// 处理按钮
RepositoryCount.btn = function () {
    var sb = new $yd.SearchBtn($(RepositoryCount.obtn));
    sb.create('print yellow', '打印', RepositoryCount.print);
    sb.create('repeat red', '重置', RepositoryCount.reset);
    sb.create('refresh', '刷新', RepositoryCount.refresh);
};
RepositoryCount.print = function(){
    var rows = RepositoryCount.dtable.datagrid('getRows');
    var ids = [];
    for (var i=0;i<rows.length;i++) {
        ids.push(rows[i]['id'])
    }
    // var rowData = JSON.stringify(rows);
    parent.inv.OpenPage("/repositoryCount/print?ids=" + ids, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\"> 打印库存量</span>\n" +
        "<b class=\"arrow \"></b>", true)
};

RepositoryCount.edit = function () {
    var sel = RepositoryCount.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    RepositoryCount._edit(sel['id'])
};
RepositoryCount.refresh = function () {
    window.location.reload();
};
//搜索
RepositoryCount.search = function () {
    RepositoryCount.dtable.datagrid('load', RepositoryCount.sform.serializeObject());
};
//重置
RepositoryCount.reset = function () {
    $(".search-form").form('clear');
    $('#shieldZero').val(0);
    $("#notZero").attr("style","display:none;");
    $("#isZero").attr("style","display:block;float:right;width:82px;margin-top:10px;margin-right:418px;background-color: #FF3300;border-radius:2px;color:white;border: 0px;");
    RepositoryCount.search();
};
$(function () {
    RepositoryCount.sform = $('#repositoryCount_form');
    RepositoryCount.dtable = $('#repositoryCount_table');
    RepositoryCount.obtn = '#repositoryCount_btn';
    new $yd.combotreegrid("#wearhouseIdForSearch").addCol("name", "名称").addCol("code", "编码").load("/wWearhouse/listForAvailable");
    new $yd.combotreegrid("#categoryId").addCol("name", "名称").load("/mCateogry/list");
    RepositoryCount.dg(); // 数据表格
    RepositoryCount.btn(); // 初始化按钮
    // RepositoryCount.tree();
    RepositoryCount.repositoryTableInit();
    $yd.bindEnter(RepositoryCount.search);
    if(!$yd.isEmpty($("#hidHouseId").val())) {
        $("#wearhouseIdForSearch").combotreegrid("setValue", {id:$("#hidHouseId").val(),name:$("#hidHouseName").val()});
        RepositoryCount.search();
    }

    if(!$yd.isEmpty($("#hidSearchText").val())) {
        $("#searchText").textbox("setValue", $("#hidSearchText").val());
        setTimeout("RepositoryCount.search()", 100);
        $("#hidSearchText").val("");
    }
});

var btn1 = document.getElementById("searchButton");
btn1.addEventListener("click", function () {
    $('#repositoryCount_table').datagrid('unselectAll');
    RepositoryCount.dtable.datagrid('load', RepositoryCount.sform.serializeObject());
});

var btn2 = document.getElementById("isZero");
btn2.addEventListener("click", function () {
    $('#repositoryCount_table').datagrid('unselectAll');
    $('#shieldZero').val(1);
    $("#isZero").attr("style","display:none;");
    $("#notZero").attr("style","display:block;float:right;width:82px;margin-top:10px;margin-right:418px;background-color: grey;border-radius:2px;color:white;border: 0px;");
    RepositoryCount.dtable.datagrid('load', RepositoryCount.sform.serializeObject());
});
var btn3 = document.getElementById("notZero");
btn3.addEventListener("click", function () {
    $('#repositoryCount_table').datagrid('unselectAll');
    $('#shieldZero').val(0);
    $("#notZero").attr("style","display:none;");
    $("#isZero").attr("style","display:block;float:right;width:82px;margin-top:10px;margin-right:418px;background-color: #FF3300;border-radius:2px;color:white;border: 0px;");
    RepositoryCount.dtable.datagrid('load', RepositoryCount.sform.serializeObject());
});