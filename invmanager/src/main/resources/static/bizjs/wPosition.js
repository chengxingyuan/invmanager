<!-- w_position.ftl 库位表 -->
var WPosition = {
    u: '/wPosition'
    , uList: '/wPosition/list'
    , uEdit: '/wPosition/edit'
    , uDel: '/wPosition/delete'
    , dgType: undefined
    , winThat: undefined
    , win: undefined
};

WPosition.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index, row) {
        WPosition._edit(row['id']);
    };
    grid.url = WPosition.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'wname', title: '所属仓库', width: 80}

        , {field: 'name', title: '库位名称', width: 80}
        // , {field: 'bindGoods', title: '已绑定商品', width: 80}
        , {field: 'code', title: '编码', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: '创建时间', width: 80}
    ]];
    grid.loadGrid();
};
WPosition.dg.getOne = function () {
    var sels = WPosition.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
WPosition.dg.getSels = function () {
    return WPosition.dtable.datagrid('getSelections');
};
WPosition.lcch = function() {
    var sel = WPosition.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    if($yd.isEmpty(sel.bindGoods)) {
        $yd.alert('无绑定存货');
        return;
    }
    parent.inv.OpenPage("/mMateriel?searchText=" + sel.bindGoods, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\">存货列表</span>\n" +
        "<b class=\"arrow \"></b>", true)
};
// 处理按钮
WPosition.btn = function () {
    var sb = new $yd.SearchBtn($(WPosition.obtn));
    sb.create('search', '联查存货', WPosition.lcch);
    sb.create('repeat red', '重置', WPosition.reset);
    sb.create('remove', '删除(DEL)', WPosition.remove);
    sb.create('refresh', '刷新(F2)', WPosition.refresh);
    sb.create('edit', '编辑(Shift+E)', WPosition.edit);
    sb.create('plus', '新增(Shift+N)', WPosition.plus);
    // sb.create('search', '查询', WPosition.search);
};
//重置
WPosition.reset = function () {
    $(".search-form").form('clear');
}
//删除
WPosition.remove = function () {
    var sels = WPosition.dg.getSels();
    if (!sels || sels.length <1) {
        $yd.alert('请至少选中一条记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        var flag = 0;
        $.each(sels, function (i, n) {
            if (n['bindGoods'] !=null && n['bindGoods'] !=''){
                flag = 1;
            }
            ids.push(n['id']);
        });
        if (flag ==1){
            $yd.alert('已绑定商品的库位禁止删除');
            return false;
        }
        $yd.post(WPosition.uDel, {ids: ids}, function () {
            WPosition.refresh();
        });
    })
};
//
WPosition.edit = function () {
    var sel = WPosition.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    WPosition._edit(sel['id'])
};
// 表单预处理
WPosition.preForm = function () {
    // new $yd.combotreegrid("#wearhouseId").addCol("name", "名称").addCol("code", "编码").load("/wWearhouse/listForAvailable");
};
WPosition._edit = function (id) {
    WPosition.dgType = "position";
    var dg = $yd.win2.create('编辑', WPosition.uEdit + '?id=' + id);
    dg.width = "500px";
    dg.height = "400px";
    dg.open(function (that, $win) {
            WPosition.win = $win;
            WPosition.winThat = that;
            WPosition.preForm();
            $yd.get(WPosition.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function () {
            WPosition.save();
        }, function () {
            WPosition.closeDg(1);
        }
    );
};
//新增
WPosition.plus = function () {
    var sel = WPosition.dg.getOne();
    var href = WPosition.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];

    var href = WPosition.uEdit;

    WPosition.dgType = "position";
    var dg = $yd.win2.create('新增', href);
    dg.width = "500px";
    dg.height = "400px";
    dg.open(function (that, $win) {
            WPosition.win = $win;
            WPosition.winThat = that;
            WPosition.preForm();
        }, function () {
            WPosition.save();
        }, function () {
            WPosition.closeDg(1);
        } , function () {
            WPosition.saveNext();
        }
    );
};
WPosition.closeDg = function (closeEd) {
    if(closeEd == 0) {
        WPosition.winThat.close();
    }

    WPosition.dgType = undefined;
    WPosition.winThat = undefined;
    WPosition.win = undefined;
};
WPosition.save = function () {
    $yd.form.create(WPosition.win.find('form')).url(WPosition.u).submit(function () {
        WPosition.refresh();
        WPosition.closeDg(0);
    });
};
WPosition.saveNext = function () {
    $yd.form.create(WPosition.win.find('form')).url(WPosition.u).submit(function () {
        WPositionEdit.reset();
        WPosition.refresh();
    });
};
WPosition.keyUp = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (!e) {
        return;
    }
    if($yd.isForbidCutKey == 1) {
        return;
    }
    if($yd.isEmpty(WPosition.dgType)) { //未弹框
        if (e.shiftKey && e.keyCode == 78) { //Shift+N
            WPosition.plus();
        }
        if (e.shiftKey && e.keyCode == 69) { //Shift+E
            WPosition.edit();
        }
        if(e.keyCode == 113) {//F2
            WPosition.refresh();
        }
        if(e.keyCode == 46) {//DEL
            WPosition.remove();
        }
        if(e.keyCode == 13) {
            WPosition.search();
        }
    } else{ //弹框
        if(e.keyCode == 27) {//ESC
            WPosition.closeDg(0);
        }
        if(e.keyCode == 13) {//Enter
            if(e.ctrlKey) {//Ctrl+Enter
                WPosition.saveNext();
            } else {
                WPosition.save();
            }
        }
    }
};
WPosition.init = function () {
    //快捷键
    document.onkeyup = WPosition.keyUp;

    //先获取焦点再失去焦点，否则打开页面后若不点击页面，快捷键无效
    $("#searchName").next().find(".textbox-text").focus()
    $("#searchName").next().find(".textbox-text").blur()
};
//
WPosition.refresh = function () {
    $('#wPosition_table').datagrid('unselectAll');
    WPosition.dtable.datagrid('reload');
};
//搜索
WPosition.search = function () {
    WPosition.dtable.datagrid('load', WPosition.sform.serializeObject());
};

$(function () {
    WPosition.sform = $('#wPosition_form');
    WPosition.dtable = $('#wPosition_table');
    WPosition.obtn = '#wPosition_btn';
    new $yd.combotreegrid("#wearhouseIdForSearch").addCol("name", "名称").addCol("code", "编码").load("/wWearhouse/listForAvailable");
    WPosition.dg(); // 数据表格
    WPosition.btn(); // 初始化按钮
    $yd.bindEnter(WPosition.search);
    WPosition.init();
});
var btn1 = document.getElementById("searchButton");
btn1.addEventListener("click", function() {
    $('#wPosition_table').datagrid('unselectAll');
    WPosition.dtable.datagrid('load', WPosition.sform.serializeObject());
});