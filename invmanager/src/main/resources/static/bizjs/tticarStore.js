<!-- w_store.ftl 门店 -->
var WStore = {
    u: '/tticarStore'
    , uRegister: '/tticarStore/register'
    , uUpdate: '/tticarStore/update'
    , uList: '/tticarStore/list'
    , uEdit: '/tticarStore/edit'
    , uDel: '/tticarStore/delete',
    uAuthorization: '/tticarStore/authorization',
    uAuthorizationSubmit: '/tticarStore/authorizationSubmit',
};

WStore.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index, row) {
        WStore._edit(row['id']);
    };
    grid.url = WStore.uList;
    grid.singleSelect = true;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'name', title: '店铺名称', width: 80}
        , {field: 'tel', title: '店铺联系电话', width: 80}
        , {field: 'memo', title: '简单描述', width: 80}
        , {field: 'addr', title: '详细地址', width: 200}
        , {field: 'companyName', title: '企业名称', width: 80}
        , {field: 'taxId', title: '企业税号', width: 80}
        , {field: 'createtime', title: '创建时间', width: 80}
    ]];
    grid.loadGrid();
};
//
WStore.applyTTicar = function (mobile) {
    $yd.confirm("此操作会根据店铺联系电话匹配天天爱车账号，并且同步商品，确定继续?", function () {
        if (mobile) {
            $yd.post("/tticar/store", {mobile: mobile}, function (data) {
                WStore.refresh();
            })
        } else {
            $yd.alert("店铺联系电话为空");
        }
    });
};
WStore.dg.getOne = function () {
    var sels = WStore.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
WStore.dg.getSels = function () {
    return WStore.dtable.datagrid('getSelections');
};
// 处理按钮
WStore.btn = function () {
    var sb = new $yd.SearchBtn($(WStore.obtn));
    sb.create('remove', '取消店铺授权', WStore.remove);
    sb.create('refresh', '刷新', WStore.refresh);
    sb.create('edit', '编辑', WStore.edit);
    sb.create('plus', '新增', WStore.plus);
    sb.create('save green', '店铺授权', WStore.authorization);
    // sb.create('search', '查询', WStore.search);
};
//删除
WStore.remove = function () {
    var sels = WStore.dg.getSels();
    if (!sels ||sels.length <1) {
        $yd.alert('请选中记录 ');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(WStore.uDel, {ids: ids}, function () {
            window.location.reload();
        });
    })
};
//
WStore.edit = function () {
    var sel = WStore.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    WStore._edit(sel['id'])
};
// 表单预处理
WStore.preForm = function () {

    // 经营范围
    var scope = new $yd.combobox("mgrScope");
    scope.required =false;
    scope.multiple = true;
    scope.valueField = 'value';
    scope.textField = 'valueName';
    scope.load("/tticarStore/mgrScope");

    // 店铺说明
    var comment = new $yd.combobox("comment");
    comment.required =false;
    comment.multiple = true;
    comment.valueField = 'id';
    comment.textField = 'valueName';
    comment.load("/tticarStore/comment");

    // 优惠政策
    var preferential = new $yd.combobox("preferential")
    preferential.required =false;
    preferential.multiple = true;
    preferential.valueField = 'id';
    preferential.textField = 'valueName';
    preferential.load("/tticarStore/preferential");

    // 品牌
    new $yd.combogrid("#mgrBrands").addCheckbox("name").addCol("name", "品牌").load("/tticarStore/mgrBrands");

    // 展示图片
    var pictureUrlList = $("#pictureUrlList").val();
    if (!$yd.isEmpty(pictureUrlList)) {
        for (var i=0; i<pictureUrlList.length; i++) {
            var img = '<div class="form-group"> <img src="'
                + pictureUrlList[i]
                + '" alt=""> </div>';
            $("#companyPicture").append(img);
        }
    }
};
WStore._edit = function (id) {
    $yd.win.create('编辑店铺', WStore.uEdit + '?id=' + id).big().open(function (that, $win) {
            WStore.preForm();
            $yd.get(WStore.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(WStore.uUpdate).submit(function () {
                that.close();
                WStore.refresh();
            });
        }
    );
};
//新增
WStore.plus = function () {
    // var sel = WStore.dg.getOne();
    // var href = WStore.uEdit;
    // if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增店铺', WStore.uEdit).big().open(function (that, $win) {
        $("#smsCode").next("span").children("a").attr("href", "javascript:getSmsCodeClick();");
        WStore.preForm();
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(WStore.uRegister).submit(function () {
                that.close();
                WStore.refresh();
            });
        }
    );
};

//店铺授权authorization
WStore.authorization = function () {

    var dg = $yd.win3.create('店铺授权', WStore.uAuthorization);
    dg.width = "500px";
    dg.height = "250px";
    dg.open(function (that, $win) {

    }, function (that, $win) {
        $yd.form.create($win.find('form')).url(WStore.uAuthorizationSubmit).submit(function () {
            that.close();
            WStore.refresh();
        });
    })
};
//
WStore.refresh = function () {
    $('#wStore_table').datagrid('unselectAll');
    WStore.dtable.datagrid('reload');
};
//搜索
WStore.search = function () {
    WStore.dtable.datagrid('load', WStore.sform.serializeObject());
};

$(function () {
    WStore.sform = $('#wStore_form');
    WStore.dtable = $('#wStore_table');
    WStore.obtn = '#wStore_btn';

    WStore.dg(); // 数据表格
    WStore.btn(); // 初始化按钮
    WStore.search();
    $yd.bindEnter(WStore.search);
    $("#searchButton").click(WStore.search);

});

// 获取验证码
var timer = '';
var nums = 60;
// WStore.getSmsCode =function () {
//     // $(smsCodeA).click(getSmsCodeClick());
//     $("#smsCode").next("span").children("a").attr("href", "javascript:getSmsCodeClick();");
// }
function getSmsCodeClick() {
    var smsCodeA = $("#smsCode").next("span").children("a");
    var smsCodeSpan = smsCodeA.children("span").children("span");
    var tel = $("#registerTel").textbox('getValue')
    if(!tel){
        $yd.alert("请输入注册手机号");
        return;
    }
    if (!$yd.validateTel(tel)){
        $yd.alert("请输正确格式的手机号");
        return;
    };
    $yd.get("/tticarStore/getSmsCode", {tel: tel}, function () {
        $yd.alert("验证码发送成功，请在手机上查看");
        // $(smsCodeSpan).attr("style", "color:#aaa");
        $(smsCodeA).attr("href", "javascript:return false;"); //将按钮置为无效点击
        timer = setInterval(function () {
            if (nums > 0) {
                $(smsCodeSpan).text(nums-- + '秒后可重新获取');
            } else {
                nums = 60; //重置时间
                clearInterval(timer); //清除js定时器
                $(smsCodeA).attr("href", "javascript:getSmsCodeClick();");
                $(smsCodeSpan).text('发送验证码');
            }
        }, 1000); //倒计时一秒执行一次
    });
}

WStore.validate =function () {
    var reg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$/;
    var password = $("#password").textbox('getValue');
    var passwordSecond = $("#passwordSecond").textbox('getValue');
    if (!reg.test(password)) {
        $yd.alert("要求密码：6-12位数字加字母");
        return false;
    }
    if (password != passwordSecond) {
        $yd.alert("两次密码输入不一致");
        return false;
    }
}



