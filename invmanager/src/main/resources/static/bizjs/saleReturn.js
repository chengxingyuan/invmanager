<!-- sale_return.ftl 销售退货表 -->
var SaleReturn = {
    u: '/saleReturn'
    , uList: '/saleReturn/list'
    , uEdit: '/saleReturn/edit'
    , uDel: '/saleReturn/delete'
};

SaleReturn.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        SaleReturn.windowDetail(row['code']);
    };
    grid.onClickRow = function (index,row) {
        $(this).datagrid('unselectRow', index);
        SaleReturn.goodsTable.datagrid("reload", {
            code: row.code,

        });

    };
    grid.url = SaleReturn.uList;
    grid.fitColumns = true;
    grid.singleSelect = true;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'code', title: '退货单号', width: 80,halign:'center',align:'center'}
        , {field: 'orderTime', title: '单据时间', width: 70,halign:'center',align:'center', formatter: function (value) {
            return $yd.isEmpty(value) ? "" : value.substr(0, 10);
        }}
        , {field: 'nickname', title: '开单人', width: 70,halign:'center',align:'center'}
        , {field: 'customName', title: '客户', width: 70,halign:'center',align:'center'}
        , {field: 'count', title: '退货总数', width: 50,halign:'center',align:'center'}
        , {field: 'totalMoney', title: '商品总额', width: 50,halign:'center',align:'center'}
        , {field: 'realPay', title: '实退总额', width: 50,halign:'center',align:'center'}
        , {field: 'needPay', title: '待退欠款', width: 50,halign:'center',align:'center', formatter: function (value,row) {
            if (row['payStatus'] == 1) {
                return 0;
            }else {
                return (row['totalMoney'] - row['realPay']).toFixed(2);
            }
        }}
        // , {field: 'unitPrice', title: '退货单价', width: 80,halign:'center',align:'center'}
        // , {field: 'skuId', title: '所退商品', width: 80}


        // , {field: 'payStatus', title: '结算状态', width: 80,halign:'center',align:'center'}
        , {
            field: 'payStatus', title: '结算状态', width: 40, halign:'center',align:'center',formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-warning">未结清</label>';
                } else {
                    return '<label class="label label-default">已结清</label>';
                }
            }
        }
        , {field: 'repositoryStatus', title: '出库状态', width: 40,halign:'center',align:'center', formatter: function (value) {
            if (value == 0) {
                return '<label class="label label-warning">未入库</label>';
            } else {
                return '<label class="label label-default">已入库</label>';
            }
        }}
        , {field: 'remark', title: '备注', width: 80,halign:'center',align:'center'}
        , {field: 'ctime', title: '创建时间', width: 80,halign:'center',align:'center'}
    ]];
    grid.loadGrid();
};
SaleReturn.dg.getOne = function () {
    var sels = SaleReturn.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
SaleReturn.dg.getSels = function () {
    return SaleReturn.dtable.datagrid('getSelections');
};
// 处理按钮
SaleReturn.btn = function () {
    var sb = new $yd.SearchBtn($(SaleReturn.obtn));
    sb.create('remove', '删除', SaleReturn.remove);
    sb.create('refresh', '刷新', SaleReturn.refresh);
    // sb.create('edit', '编辑', SaleReturn.edit);
    sb.create('plus', '新增', SaleReturn.plus);
    // sb.create('search', '查询', SaleReturn.search);
};
//删除
SaleReturn.remove = function () {
    var sels = SaleReturn.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    for (var i = 0; i < sels.length; i++) {
        var n = sels[i];
        if (n.payStatus != 0) {
            $yd.alert('已结清不能删除');
            return;
        }
        if (n.realPay != 0) {
            $yd.alert('实退金额大于0不能删除');
            return;
        }
        if (n.repositoryStatus != 0) {
            $yd.alert('已入库不能删除');
            return;
        }
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['code']);
        });
        $yd.post(SaleReturn.uDel, {codes: ids}, function () {
            SaleReturn.refresh();
        });
    })
};
SaleReturn.windowDetail = function (saleReturnId) {
    var dg = $yd.win3.create('销售退货详情', "/saleReturn/detailPage?saleReturnCode=" + saleReturnId);
    dg.width = "70%";
    dg.height = "80%";
    dg.open(function (that, $win) {

    });
};
//
SaleReturn.edit = function () {
    var sel = SaleReturn.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    SaleReturn._edit(sel['id'])
};
SaleReturn._edit = function (id) {
    $yd.win.create('编辑销售退货表', SaleReturn.uEdit + '?id=' + id).open(function (that, $win) {
            $yd.get(SaleReturn.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(SaleReturn.u).submit(function () {
                that.close();
                SaleReturn.refresh();
            });
        }
    );
};
//新增
SaleReturn.plus = function () {
    parent.inv.OpenPage("/saleReturn/addSaleReturn",
        "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\"> 新增销售退货 </span>\n" +
        "<b class=\"arrow \"></b>", true)
};
//
SaleReturn.refresh = function () {
    $("form.search-form").form('clear');
    SaleReturn.search();
};
//搜索
SaleReturn.search = function () {
    SaleReturn.dtable.datagrid('load', SaleReturn.sform.serializeObject());
};

SaleReturn.goodsTableFun = function () {

    var grid = new $yd.DataGrid(SaleReturn.goodsTable);
    grid.url ='/saleReturn/detailGoodsList';
    grid.rownumbers = true;
    grid.singleSelect = true;
    grid.columns = [[
        {field: 'goodsName', title: '商品名称', width: 220},
        {field: 'skus', title: '规格', width: 220},
        {field: 'goodsId', title: '商品ID', width: 80, hidden: true},
        {field: 'goodsCode', title: '商品编号', width: 80},
        {field: 'OECode', title: 'OE编号', width: 120},
        {field: 'suitCar', title: '适用车型', width: 90},
        {field: 'wearhouseName', title: '仓库', width: 90},
        // {field: 'batchCode', title: '批号', width: 90},
        {field: 'unitPrice', title: '退货单价', width: 50},
        {
            field: 'count',
            title: '退货数量',
            width: 50
        },
        {
            field: 'shouldPay', title: '总额', width: 50, formatter: function (value, row, index) {
            return Number(row.count) * Number(row.unitPrice);
        }
        }
    ]]
    grid.loadGrid();
}

$(function () {
    SaleReturn.sform = $('#saleReturn_form');
    SaleReturn.dtable = $('#saleReturn_table');
    SaleReturn.obtn = '#saleReturn_btn';
    SaleReturn.goodsTable = $('#saleReturnGoods_table');
    SaleReturn.goodsTableFun();
    SaleReturn.dg(); // 数据表格
    SaleReturn.btn(); // 初始化按钮
    $yd.bindEnter(SaleReturn.search);
    $("#searchButton").click(SaleReturn.search);

    console.log($("#hidSearchText").val())
    if(!$yd.isEmpty($("#hidSearchText").val())) {
        $("#searchText").textbox("setValue", $("#hidSearchText").val());
        $('#hidSearchText').val('');
        setTimeout("SaleReturn.search()", 150);
    }

    $("input[name='payStatus']").combobox({
        url: "/sysDict/sales_pay_status",
        panelHeight: 'auto',
        valueField: 'id',
        textField: 'text',
        editable: false,
    });

    $("input[name='orderStatus']").combobox({
        url: "/sysDict/purchase_order_status",
        panelHeight: 'auto',
        valueField: 'id',
        textField: 'text',
        editable: false,
    });
});