<!-- in_repository_detail.ftl 入库单详情 -->
var OutRepositoryDetail = {
    uList: '/outRepositoryDetail/list'
    , typeArr: []
};

OutRepositoryDetail.dg = function () {
    $.ajax({
        type: "POST",
        url: "/sysDict/outRepository",
        dataType: "json",
        success: function (data) {
            OutRepositoryDetail.typeArr = data;
        }
    });

    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index, row) {
        // OutRepositoryDetail.detail(row.code);
        OutRepositoryDetail.printDetail(row.id);
    };
    grid.rownumbers = true;
    grid.singleSelect = true;
    grid.onClickRow = function (index,row) {
        OutRepositoryDetail.dtable.datagrid("unselectAll");
        OutRepositoryDetail.goodsTbale.datagrid("reload", {
            code:row.code,
        });
    }
    grid.url = OutRepositoryDetail.uList;
    grid.columns = [[
        {field: 'id', checkbox: false, hidden: true}
        , {
            field: 'code', title: '出库单号', width: 80,halign:'center',align:'center'
        }

        , {
            field: 'orderTime', title: '单据日期', width: 80,halign:'center',align:'center', formatter: function (value, row, index) {
                if (value == null) {
                    return '';
                } else {
                    value = value.substr(0, value.length - 9);
                    return value;
                }
            }
        }
        // , {field: 'wearhouseName', title: '仓库', width: 80, formatter: function (value) {
        //         return OutRepositoryDetail.formatDetail(value);
        //     }}
        // , {
        //     field: 'goodsName', title: '商品摘要', width: 160, formatter: function (value) {
        //         return OutRepositoryDetail.formatDetail(value);
        //     }
        // }
        , {field: 'count', title: '数量', width: 50,halign:'center',align:'center'}
        , {field: 'type', title: '出库类型', width: 80,halign:'center',align:'center', formatter: function (value, row, index) {
                for(var i=0; i<OutRepositoryDetail.typeArr.length; i++) {
                    if(OutRepositoryDetail.typeArr[i].value == value) {
                        return OutRepositoryDetail.typeArr[i].text;
                    }
                }
                return "";
            }
        }
        , {field: 'providerName', title: '往来单位', width: 50,halign:'center',align:'center'}
        , {field: 'username', title: '创建人', width: 50,halign:'center',align:'center'}
        , {field: 'remark', title: '备注', width: 120,halign:'center',align:'center'}
        , {
            field: 'rcode', title: '关联单号', width: 100,halign:'center',align:'center', formatter: function (value, row, index) {
                if (row['remark'] == '天天爱车平台:订单发货出库。') {
                    return row['busz_id'];
                }else {
                    return value;
                }
            }
        }
        , {field: 'ctime', title: '创建时间', width: 80,halign:'center',align:'center'}
    ]];
    grid.loadGrid();
};

OutRepositoryDetail.formatDetail = function (value) {
    if ($yd.isEmpty(value)) {
        return "";
    }
    var result = "";
    var first = "";
    var arr = value.split(",");
    if (arr.length == 1) {
        return "<a style='color:purple' title='" + value + "'>" + value + "</a>";
    }
    for (var i = 0; i < arr.length; i++) {
        result += arr[i] + "\n";
        if (i == 0) {
            first = result;
        }
    }
    var html = "<a style='color:purple' title='" + result + "'>" + first + "..." + "</a>";

    return html;
};
OutRepositoryDetail.dg.getOne = function () {
    var sels = OutRepositoryDetail.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
OutRepositoryDetail.dg.getSels = function () {
    return OutRepositoryDetail.dtable.datagrid('getSelections');
};
// 处理按钮
OutRepositoryDetail.btn = function () {
    var sb = new $yd.SearchBtn($(OutRepositoryDetail.obtn));
    sb.create('repeat red', '重置', OutRepositoryDetail.reset);
    sb.create('print green', '打印',OutRepositoryDetail.print);

    sb.create('refresh', '刷新', OutRepositoryDetail.refresh);
    // sb.create('plus', '新增', OutRepositoryDetail.plus);
};
OutRepositoryDetail.print = function () {
    var rows = OutRepositoryDetail.dtable.datagrid('getRows');
    var ids = [];
    for (var i=0;i<rows.length;i++) {
        ids.push(rows[i]['id'])
    }
    parent.inv.OpenPage("/outRepositoryDetail/printList?ids=" + ids, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\"> 打印出库列表</span>\n" +
        "<b class=\"arrow \"></b>", true)

};


OutRepositoryDetail.detail = function (code) {
    parent.inv.OpenPage("/outRepositoryDetail/detail?code=" + code,"<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\"> 出库单详情</span>\n" +
        "<b class=\"arrow \"></b>", true);
};

OutRepositoryDetail.printDetail = function (code) {
    parent.inv.OpenPage("/outRepositoryDetail/printDetail?code=" + code,"<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\"> 打印出库单</span>\n" +
        "<b class=\"arrow \"></b>", true);
};
OutRepositoryDetail.refresh = function () {
    window.location.reload();
};
//搜索
OutRepositoryDetail.search = function () {
    OutRepositoryDetail.dtable.datagrid('load', OutRepositoryDetail.sform.serializeObject());
};
//新增
OutRepositoryDetail.plus = function () {
    parent.inv.OpenPage("/outRepositoryDetail/addOutRepositoryOrder", "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\"> 新增出库单 </span>\n" +
        "<b class=\"arrow \"></b>",true)
};
//重置
OutRepositoryDetail.reset = function () {
    $(".search-form").form('clear');
    OutRepositoryDetail.search();
};
OutRepositoryDetail.loadGoodsTable = function () {

    // MMaterielTticar.relativeTable = $("#relativeGoods_table");
    var grid = new $yd.DataGrid(OutRepositoryDetail.goodsTbale);

    grid.url ='/outRepositoryDetail/getOne';
    grid.rownumbers = true;
    grid.singleSelect = true;
    grid.pagination=false,
        grid.columns= [[
            {field: 'id', checkbox: false, hidden:true},
            {field: 'goodsName', title: '商品名称', width: 150,halign:'center',align:'center'},
            {field: 'skus', title: '规格', width: 200,halign:'center',align:'center'},
            {field: 'code', title: '商品编号', width: 100,halign:'center',align:'center'},

            {
                field: 'unitName', title: '单位', width: 80,halign:'center',align:'center',
            }

            // , {field: 'providerName', title: '供应商', width: 80,halign:'center',align:'center'}
            , {field: 'houseName', title: '仓库', width: 80,halign:'center',align:'center'}
            , {field: 'positionName', title: '库位', width: 80,hidden:true}
            , {field: 'price', title: '单价',halign:'center',align:'center', width: 80}
            , {field: 'count', title: '数量', width: 50,halign:'center',align:'center'}
            , {field: 'remark', title: '总额', width: 80,halign:'center',align:'center',formatter:function (value,row) {
                return row['price'] * row['count'];
            }}
        ]]
    grid.loadGrid();
}

$(function () {
    OutRepositoryDetail.sform = $('#outRepositoryDetail_form');
    OutRepositoryDetail.dtable = $('#outRepositoryDetail_table');
    OutRepositoryDetail.obtn = '#outRepositoryDetail_btn';
    OutRepositoryDetail.goodsTbale = $('#outRepositoryGoods_table');
    OutRepositoryDetail.dg(); // 数据表格
    OutRepositoryDetail.btn(); // 初始化按钮
    OutRepositoryDetail.loadGoodsTable();
    new $yd.combotreegrid("#wearhouseId").addCol("name", "名称").addCol("code", "编码").load("/wWearhouse/list");
    // $("#wearhouseId").combobox({
    //     editable: false,
    //     valueField: 'id',
    //     textField: 'name',
    //     url:"/wWearhouse/list"
    // });
    $("input[name='outRepositoryType']").combobox({
        url: "/sysDict/outRepository",
        panelHeight: 'auto',
        valueField: 'id',
        textField: 'text',
        editable: false,
    });

    $yd.bindEnter(OutRepositoryDetail.search);

    if(!$yd.isEmpty($("#hidHouseId").val())) {
        $("#wearhouseId").combotreegrid("setValue", {id:$("#hidHouseId").val(),name:$("#hidHouseName").val()});
        $("#startTime").datebox("setValue", $("#hidStartTime").val());
        $("#endTime").datebox("setValue", $("#hidEndTime").val());
        OutRepositoryDetail.search();
    }
});

var btn1 = document.getElementById("searchButton");
btn1.addEventListener("click", function () {
    OutRepositoryDetail.dtable.datagrid('load', OutRepositoryDetail.sform.serializeObject());
});
