<!-- car_brand_materiel.ftl  -->
var CarBrandMateriel = {
    u: '/carBrandMateriel'
    , uList: '/carBrandMateriel/list'
    , uEdit: '/carBrandMateriel/edit'
    , uDel: '/carBrandMateriel/delete'
};

CarBrandMateriel.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        CarBrandMateriel._edit(row['id']);
    };
    grid.url = CarBrandMateriel.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'carBrandId', title: '车型id', width: 80}
        , {field: 'materielId', title: '商品id', width: 80}
        , {field: 'year', title: 'year', width: 80}
        , {field: 'car', title: '车型信息', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
CarBrandMateriel.dg.getOne = function () {
    var sels = CarBrandMateriel.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
CarBrandMateriel.dg.getSels = function () {
    return CarBrandMateriel.dtable.datagrid('getSelections');
};
// 处理按钮
CarBrandMateriel.btn = function () {
    var sb = new $yd.SearchBtn($(CarBrandMateriel.obtn));
    sb.create('remove', '删除', CarBrandMateriel.remove);
    sb.create('refresh', '刷新', CarBrandMateriel.refresh);
    sb.create('edit', '编辑', CarBrandMateriel.edit);
    sb.create('plus', '新增', CarBrandMateriel.plus);
    sb.create('search', '查询', CarBrandMateriel.search);
};
//删除
CarBrandMateriel.remove = function () {
    var sels = CarBrandMateriel.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(CarBrandMateriel.uDel, {ids: ids}, function () {
            CarBrandMateriel.refresh();
        });
    })
};
//
CarBrandMateriel.edit = function () {
    var sel = CarBrandMateriel.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    CarBrandMateriel._edit(sel['id'])
};
// 表单预处理
CarBrandMateriel.preForm = function () {
    new $yd.combobox("carBrandId").load("/carBrand/select");
    new $yd.combobox("materielId").load("/materiel/select");
};
CarBrandMateriel._edit = function (id) {
    $yd.win.create('编辑', CarBrandMateriel.uEdit + '?id=' + id).open(function (that, $win) {
            CarBrandMateriel.preForm();
            $yd.get(CarBrandMateriel.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(CarBrandMateriel.u).submit(function () {
                that.close();
                CarBrandMateriel.refresh();
            });
        }
    );
};
//新增
CarBrandMateriel.plus = function () {
    var sel = CarBrandMateriel.dg.getOne();
    var href = CarBrandMateriel.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增', href).open(function (that, $win){
            CarBrandMateriel.preForm();
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(CarBrandMateriel.u).submit(function () {
                that.close();
                CarBrandMateriel.refresh();
            });
        }
    );
};
//
CarBrandMateriel.refresh = function () {
    CarBrandMateriel.dtable.datagrid('reload');
};
//搜索
CarBrandMateriel.search = function () {
    CarBrandMateriel.dtable.datagrid('load', CarBrandMateriel.sform.serializeObject());
};

$(function () {
    CarBrandMateriel.sform = $('#carBrandMateriel_form');
    CarBrandMateriel.dtable = $('#carBrandMateriel_table');
    CarBrandMateriel.obtn = '#carBrandMateriel_btn';

    CarBrandMateriel.dg(); // 数据表格
    CarBrandMateriel.btn(); // 初始化按钮
    $yd.bindEnter(CarBrandMateriel.search);
});