<!-- goods_order.ftl 产品订单表 -->
var GoodsOrder = {
    u: '/tticar/goodsOrder'
    , uList: '/tticar/goodsOrder/list'
    , uEdit: '/tticar/goodsOrder/edit'
    , uDelivery: '/tticar/goodsOrder/delivery'
    ,uDetails:'/tticar/goodsOrder/details'
    , uDel: '/tticar/goodsOrder/delete'
    , uReceive: '/tticar/goodsOrder/receive'
    , uChange: '/tticar/goodsOrder/change'
    ,uReject: '/tticar/goodsOrder/reject'
    , uPlaceOrderPage: '/tticar/goodsOrder/placeOrderPage'
};

GoodsOrder.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.fitColumns = true;
    // grid.onDblClickRow = function (index, row) {
    //     GoodsOrder._edit(row['id']);
    // };
    grid.onClickRow=function (rowIndex, rowData) {
        $(this).datagrid('unselectRow', rowIndex);
    }

    grid.url = GoodsOrder.uList;
    grid.rownumbers=true,
        grid.singleSelect =true,
    grid.columns = [[
        {field: 'id', checkbox: true, hidden: true},
        {field: 'orderCode', title: '订单编号', width: 150,
        formatter: function (value, row) {
            var str = row.orderCode.substring(0,10) ;
            str = str+"";
            var str1 ="1"+ row.orderCode.substring(10,row.orderCode.length);
                return '<a style="color:deeppink;" href="javascript:void(0);" onclick="GoodsOrder._details('+str+','+str1+')">'+ value + '</a>';
            }}
        , {field: 'storeName', title: '门店/客户', width: 120}
        , {field: 'sellStoreName', title: '店铺', width: 120}
        , {field: 'totalMoney', title: '总金额', width:70}
        , {field: 'totalTransportFee', title: '总运费', width:70}
        , {field: 'totalNum', title: '总数量', width: 70}
        , {field: 'realPayFee', title: '实付金额', width: 70}
        , {field: 'buyerWantToTalk', title: '买家留言', width: 120}
        , {field: 'supplyDiscountFee', title: '优惠金额', width: 70, formatter: function (value, row, index) {
          return row.supplyDiscountFee + row.intergalDiscountFee + row.reductionFee;
        }}
        , {field: 'orderStatus', title: '状态', width: 120, formatter: function (value, row, index) {
            if (value == 1) {
                return '订单提交';
            }else if (value == 6){
                return '提醒接单';
            }else if (value == 12){
                return '订单关闭（卖家拒绝）';
            }else if (value == 15){
                return '订单关闭（未被卖家确认）';
            }else if (value == 22){
                return '订单确认';
            }else if (value == 25){
                return '付款超时';
            }else if (value == 30){
                return '订单支付（统一下单）';
            }else if (value == 31){
                return '订单支付（支付通知）';
            }else if (value == 32){
                return '订单确认（货到付款）';
            }else if (value == 33){
                return '支付成功';
            }else if (value == 34){
                return '订单支付（预计发货）';
            }else if (value == 36){
                return '提醒发货';
            }else if (value == 44){
                return '订单发货';
            }else if (value == 55 || value == 66){
                return '确认收货';
            }else if (value == 77){
                return '订单取消';
            }
        }}
        , {field: 'payTypeName', title: '支付方式', width: 70}
        , {field: 'sendType', title: '发货方式', width: 70}
        , {field: 'createTime', title: '下单时间', width: 150}
        , {field: 'operate', title: '操作',align:'center', width: 170,formatter: function (value, row, index) {
            var str = row.orderCode.substring(0,10) ;
            str = str+"";
            var str1 ="1"+ row.orderCode.substring(10,row.orderCode.length);

           if (row.orderStatus == 1) {
                // return '<a href="#" onclick="GoodsOrder._edit('+str+','+str1+')">修改</a> &nbsp;<a href="#" onclick="GoodsOrder._receiveOrder('+str+','+str1+','+1+')">接单</a> &nbsp;<a href="#" onclick="GoodsOrder._notReceiveOrder('+str+','+str1+','+0+')">拒绝接单</a>'
               return '<button style="background-color: red;border-radius:2px;color:white;border: 0px;" href="#" ' +
              'onclick="GoodsOrder._receiveOrder('+str+','+str1+')">接单</button>&nbsp;<button style="background-color: black;border-radius:2px;color:white;border: 0px;" href="#" ' +
              'onclick="GoodsOrder._notReceiveOrder('+str+','+str1+','+0+')">拒绝接单</button>'
           }else if (row.orderStatus ==22) {
               // return  '<a href="#" onclick="GoodsOrder._edit('+str+','+str1+')">修改</a>'

               return '<button style="background-color: red;border-radius:2px;color:white;border: 0px;" href="#" ' +
                   'onclick="GoodsOrder._edit('+str+','+str1+')">修改</button>'
           }else if (row.orderStatus ==30 || row.orderStatus ==31 || row.orderStatus ==32 || row.orderStatus==33
               || row.orderStatus ==34 || row.orderStatus ==36) {

               // return  '<a href="#" onclick="GoodsOrder._deliveryGoods('+str+','+str1+')">发货</a>'

               return  '<button style="background-color: green;border-radius:2px;color:white;border: 0px;" href="#" ' +
                   'onclick="GoodsOrder._deliveryGoods('+str+','+str1+')">发货</button>'
           }
        }}
    ]
  ];


    //
    grid.view = detailview;
    grid.detailFormatter = function (rowIndex, rowData) {
        return '<table></table>';
    };
    grid.onExpandRow = function (index, row) {
        var $td = $(this).datagrid('getRowDetail', index).find('table');
        $td.datagrid({
            url: GoodsOrder.uList + "/goods?orderId=" + row['orderCode'],
            method: 'get',
            // fitColumns: true,
            rownumbers: true,
            columns: [[
                {field: 'goodsName', title: '产品名称', width: 200},
                {field: 'standardName', title: '规格', width: 100},
                {field: 'count', title: '购买数量', width: 100},
                {field: 'price', title: '成交单价', width: 100, align: 'right'},
                {field: 'fee', title: '运费', width: 100, align: 'right'}
            ]],
            onResize: function () {
                GoodsOrder.dtable.datagrid('fixDetailRowHeight', index);
            },
            onLoadSuccess: function () {
                setTimeout(function () {
                    GoodsOrder.dtable.datagrid('fixDetailRowHeight', index);
                }, 0);
            }
        });
    };
    grid.loadGrid();
};
GoodsOrder.dg.getOne = function () {
    var sels = GoodsOrder.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
GoodsOrder.dg.getSels = function () {
    return GoodsOrder.dtable.datagrid('getSelections');
};
// 处理按钮
GoodsOrder.btn = function () {
    var sb = new $yd.SearchBtn($(GoodsOrder.obtn));
    // sb.create('remove', '删除', GoodsOrder.remove);
    sb.create('refresh', '刷新', GoodsOrder.refreshForButton);
    // sb.create('edit', '编辑', GoodsOrder.edit);
    sb.create('plus', '代下单', GoodsOrder.plus);
    // sb.create('search', '查询', GoodsOrder.search);
};
//删除
GoodsOrder.remove = function () {
    var sels = GoodsOrder.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(GoodsOrder.uDel, {ids: ids}, function () {
            GoodsOrder.refresh();
        });
    })
};
//
GoodsOrder.edit = function (index) {
    // var sel = GoodsOrder.dg.getOne();
    // if (!sel) {
    //     $yd.alert('请选择一条记录');
    //     return;
    // }
    GoodsOrder._edit(sel['id'])
};
// 表单预处理
GoodsOrder.preForm = function () {
    // new $yd.combobox("storeId").load("/store/select");
    // new $yd.combobox("sellStoreId").load("/sellStore/select");
};
//修改订单
GoodsOrder._edit = function (str,str1) {
    var str2 = str1+"";
    str2 = str2.substring(1, str2.length);
    var id = str+""+str2;
    $yd.win.create('修改订单', GoodsOrder.uEdit + '?id=' + id).open(function (that, $win) {
            GoodsOrder.preForm();
            $yd.get(GoodsOrder.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }
        , function (that, $win) {
            var orderCode = $("#orderCode").val();
            var changeMoney = $("#changeMoney").val();
            var totalFee = $("#fee").val();
            var sellStoreId = $("#sellStoreId").val();
            $yd.form.create($win.find('form')).url(GoodsOrder.uChange,{orderCode:orderCode,changeMoney:changeMoney,totalFee:totalFee,sellStoreId:sellStoreId}).submit(function () {
                that.close();
                GoodsOrder.refresh();
            });
        }
    );
};

//GoodsOrder_deliveryGoods 发货
GoodsOrder._deliveryGoods = function (str,str1) {
    var str2 = str1+"";
    str2 = str2.substring(1, str2.length);
    var id = str+""+str2;
    // $yd.win.createDelivery('订单发货', GoodsOrder.uDelivery + '?orderCode=' + id).open(function (that, $win) {
    //         GoodsOrder.preForm();
    //         $yd.get(GoodsOrder.u + "/" + id, function (data) {
    //             $win.find('form').form('load', data);
    //         })
    //     }
    //     , function (that, $win) {
    //         var rows = GoodsOrder.dtable("getRows");
    //         console.log(rows)
    //         console.log(11111111111111)
    //         $yd.form.create($win.find('form')).url(GoodsOrder.uDelivery).submit(function () {
    //             that.close();
    //             GoodsOrder.refresh();
    //         });
    //     }
    var dg = $yd.win3.create('订单发货', GoodsOrder.uDelivery+ '?orderCode=' + id);
    dg.width = "75%";
    dg.height = "40%";
    dg.open(function (that, $win) {

    }
    //     , function (that, $win) {
    //     $yd.form.create($win.find('form')).url(GoodsOrder.uReceive).submit(function () {
    //         that.close();
    //         GoodsOrder.refresh();
    //     });
    // })

    // var dg = $yd.win3.create('订单发货', GoodsOrder.uDelivery+ '?orderCode=' + id);
    // dg.width = "500px";
    // dg.height = "500px";
    // dg.open(function (that, $win) {
    //
    // }, function (that, $win) {
    //     $yd.form.create($win.find('form')).url(GoodsOrder.uReceive).submit(function () {
    //         that.close();
    //         GoodsOrder.refresh();
    //     });
    // }
        
    );
};
//查看详情
GoodsOrder._details = function (str,str1) {

    var str2 = str1+"";
    str2 = str2.substring(1, str2.length);
    var id = str+""+str2;
    $yd.win.create('查看详情', GoodsOrder.uDetails + '?id=' + id).open(function (that, $win) {
            GoodsOrder.preForm();
            $yd.get(GoodsOrder.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }
    );
};

//新增
GoodsOrder.plus = function () {

    var href = GoodsOrder.uPlaceOrderPage;

    var dg = $yd.win3.create('代下单', href);
    dg.width = "70%";
    dg.height = "80%";
    dg.open(function (that, $win) {

    });
};
//
GoodsOrder.refresh = function () {
    // $(".search-form").form('clear');
    GoodsOrder.dtable.datagrid('load', GoodsOrder.sform.serializeObject());
};
GoodsOrder.refreshForButton = function () {
    $(".search-form").form('clear');
    GoodsOrder.dtable.datagrid('load', GoodsOrder.sform.serializeObject());
    GoodsOrder.loadWstore();
};

//搜索
GoodsOrder.search = function () {
    GoodsOrder.dtable.datagrid('load', GoodsOrder.sform.serializeObject());
};
// 加载店铺
GoodsOrder.loadWstore = function () {
    var slist = new $yd.datalist(GoodsOrder.storeList);
    slist.singleSelect = true;
    slist.checkbox = false;
    slist.onClickRow = function (index, row) {
        $("#sellStoreId").val(row.id)
        GoodsOrder.dtable.datagrid('reload', {'t.sellStoreId': row.id});
    };
    slist.load('/wStore/select');
};

$(function () {
    GoodsOrder.sform = $('#goodsOrder_form');
    GoodsOrder.dtable = $('#goodsOrder_table');
    GoodsOrder.obtn = '#goodsOrder_btn';
    GoodsOrder.storeList = $('#wstore_list');

    GoodsOrder.dg(); // 数据表格
    GoodsOrder.btn(); // 初始化按钮
    $yd.bindEnter(GoodsOrder.search);
    GoodsOrder.loadWstore();//店铺

    if(!$yd.isEmpty($("#hidSearchText").val())) {
        $("#searchText").textbox("setValue", $("#hidSearchText").val());
        $("#startTime").datebox("setValue", $("#hidStartTime").val());
        $("#endTime").datebox("setValue", $("#hidEndTime").val());
        setTimeout("GoodsOrder.search()", 10);
    }
});

var btn1 = document.getElementById("searchButton");
btn1.addEventListener("click", function() {
    $('#goodsOrder_table').datagrid('unselectAll');
    GoodsOrder.dtable.datagrid('load', GoodsOrder.sform.serializeObject());
});

//接单
GoodsOrder._receiveOrder = function (str,str1) {
    var str2 = str1+"";
    str2 = str2.substring(1, str2.length);
    var id = str+""+str2;
    $yd.confirmReceive(function () {
        $yd.postReceive(GoodsOrder.uReceive, {orderId: id,status:22,reason :null}, function () {
            GoodsOrder.refresh();
        });
    })
};
GoodsOrder._notReceiveOrder = function (str,str1) {
    var str2 = str1+"";
    str2 = str2.substring(1, str2.length);
    var id = str+""+str2;
    // $yd.confirmNotReceive(function () {
    //     $yd.postNotReceive(GoodsOrder.uReceive, {id: id,status:status}, function () {
    //         GoodsOrder.refresh();
    //     });
    // })

    var dg = $yd.win3.create('拒绝接单', GoodsOrder.uReject+ '?id=' + id);
    dg.width = "500px";
    dg.height = "250px";
    dg.open(function (that, $win) {

    }, function (that, $win) {
        $yd.form.create($win.find('form')).url(GoodsOrder.uReceive).submit(function () {
            that.close();
            GoodsOrder.refresh();
        });
    })
};

// GoodsOrder.save = function () {
//     $yd.form.create(GoodsOrder.win.find('form')).url(GoodsOrder.u).submit(function () {
//         GoodsOrder.refresh();
//     });
// };
