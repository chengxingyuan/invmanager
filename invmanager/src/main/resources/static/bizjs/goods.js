<!-- goods.ftl 产品表 -->
var Goods = {
    u: '/goods'
    , uList: '/goods/list'
    , uEdit: '/goods/edit'
    , uDel: '/goods/delete'
};

Goods.dg = function () {
    var grid = new $yd.TreeGrid(this.dtable);
    grid.onDblClickRow = function (row) {
        Goods._edit(row['id']);
    };
    grid.toolbar = Goods.obtn;
    grid.url = Goods.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'storeId', title: '商铺id', width: 80}
        , {field: 'brandId', title: '品牌id', width: 80}
        , {field: 'categoryId', title: '分类id', width: 80}
        , {field: 'storeCategoryId', title: '店铺分类id', width: 80}
        , {field: 'name', title: '产品名称', width: 80}
        , {field: 'nameSpell', title: '产品名称拼音简写', width: 80}
        , {field: 'price', title: '游客价格，未登录80-100', width: 80}
        , {field: 'maxPrice', title: '游客价格，未登录80-100', width: 80}
        , {field: 'pricemember', title: '店铺已登录价格100-220', width: 80}
        , {field: 'maxPricemember', title: '店铺已登录价格100-220', width: 80}
        , {field: 'pricevip', title: '门店申请供应商会员价格200-20', width: 80}
        , {field: 'maxPricevip', title: '门店申请供应商会员价格200-20', width: 80}
        , {field: 'pricesell', title: '零售价低', width: 80}
        , {field: 'maxPricesell', title: '零售价高', width: 80}
        , {field: 'inventory', title: '库存货源充足／货源紧张／2000件', width: 80}
        , {field: 'placeOrigin', title: '产地', width: 80}
        , {field: 'minNum', title: '最低购买数量', width: 80}
        , {field: 'maxNum', title: '默认是0，表示不限购', width: 80}
        , {field: 'memo', title: '产品备注简单描述', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'uptime', title: '上架时间', width: 80}
        , {field: 'downtime', title: '下架时间', width: 80}
        , {field: 'standardCfg', title: '规格配置表颜色 ：', width: 80}
        , {field: 'description', title: '图文详情', width: 80}
        , {field: 'refGoodsId', title: '代运营产品id', width: 80}
        , {field: 'fee', title: '运费', width: 80}
        , {field: 'sort', title: '置顶排序', width: 80}
        , {field: 'createtime', title: '创建时间', width: 80}
        , {field: 'modifytime', title: '修改时间', width: 80}
        , {field: 'keyWords', title: '搜索关键字', width: 80}
        , {field: 'isOlder', title: '是否启用老客户价（0是1否）', width: 80}
        , {field: 'video', title: '视频地址', width: 80}
        , {field: 'videoDuration', title: '时长', width: 80}
        , {field: 'goodNo', title: '商品编号', width: 80}
        , {field: 'isAutoReceipt', title: '是否自动接单 0是 1否（手动接单）', width: 80}
        , {field: 'upSource', title: '商品来源 默认0app供应商 1总后台 2供应商后台', width: 80}
        , {field: 'showcase', title: '橱窗（0 上 1 下）', width: 80}
        , {field: 'showcaseTime', title: '上下橱窗操作时间', width: 80}
        , {field: 'recStatus', title: '推荐状态，0：不推荐，1：优先推荐，NULL和其他为正常推荐', width: 80}
        , {field: 'pricevip2', title: 'v2价格', width: 80}
        , {field: 'pricevip3', title: 'v3价格', width: 80}
        , {field: 'feeType', title: '0 免运费 1有运费  2运费到付', width: 80}
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
Goods.dg.getOne = function () {
    var sels = Goods.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
Goods.dg.getSels = function () {
    return Goods.dtable.treegrid('getSelections');
};
// 处理按钮
Goods.btn = function () {
    var sb = new $yd.SearchBtn($(Goods.obtn));
    sb.create('remove', '删除', Goods.remove);
    sb.create('refresh', '刷新', Goods.refresh);
    sb.create('edit', '编辑', Goods.edit);
    sb.create('plus', '新增', Goods.plus);
};
//删除
Goods.remove = function () {
    var sels = Goods.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(Goods.uDel, {ids: ids}, function () {
            Goods.refresh();
        });
    })
};
//
Goods.edit = function () {
    var sel = Goods.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    Goods._edit(sel['id'])
};
// 表单预处理
Goods.preForm = function () {
    new $yd.combobox("storeId").load("/store/select");
    new $yd.combobox("brandId").load("/brand/select");
    new $yd.combobox("categoryId").load("/category/select");
    new $yd.combobox("storeCategoryId").load("/storeCategory/select");
    new $yd.combobox("refGoodsId").load("/refGoods/select");
};
Goods._edit = function (id) {
    $yd.win.create('编辑产品表', Goods.uEdit + '?id=' + id).open(function (that, $win) {
            Goods.preForm();
            $yd.get(Goods.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(Goods.u).submit(function () {
                that.close();
                Goods.refresh();
            });
        }
    );
};
//新增
Goods.plus = function () {
    var sel = Goods.dg.getOne();
    var href = Goods.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增产品表', href).open(function (that, $win){
            Goods.preForm();
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(Goods.u).submit(function () {
                that.close();
                Goods.refresh();
            });
        }
    );
};
//
Goods.refresh = function () {
    Goods.dtable.treegrid('reload');
};

$(function () {
    Goods.sform = $('#goods_form');
    Goods.dtable = $('#goods_table');
    Goods.obtn = '#goods_btn';

    Goods.dg(); // 数据表格
    Goods.btn(); // 初始化按钮
});