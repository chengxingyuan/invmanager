<!-- m_sales_detail.ftl  -->
var MSalesDetail = {
    u: '/mSalesDetail'
    , uList: '/mSalesDetail/list'
    , uEdit: '/mSalesDetail/edit'
    , uDel: '/mSalesDetail/delete'
};

MSalesDetail.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        MSalesDetail._edit(row['id']);
    };
    grid.url = MSalesDetail.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'salesId', title: '销售订单', width: 80}
        , {field: 'skuId', title: '商品规格id', width: 80}
        , {field: 'batchCode', title: '批次', width: 80}
        , {field: 'price', title: '售价', width: 80}
        , {field: 'num', title: '销售数量', width: 80}
        , {field: 'fee', title: '总金额', width: 80}
        , {field: 'remark', title: '备注', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
MSalesDetail.dg.getOne = function () {
    var sels = MSalesDetail.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
MSalesDetail.dg.getSels = function () {
    return MSalesDetail.dtable.datagrid('getSelections');
};
// 处理按钮
MSalesDetail.btn = function () {
    var sb = new $yd.SearchBtn($(MSalesDetail.obtn));
    sb.create('remove', '删除', MSalesDetail.remove);
    sb.create('refresh', '刷新', MSalesDetail.refresh);
    sb.create('edit', '编辑', MSalesDetail.edit);
    sb.create('plus', '新增', MSalesDetail.plus);
    sb.create('search', '查询', MSalesDetail.search);
};
//删除
MSalesDetail.remove = function () {
    var sels = MSalesDetail.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(MSalesDetail.uDel, {ids: ids}, function () {
            MSalesDetail.refresh();
        });
    })
};
//
MSalesDetail.edit = function () {
    var sel = MSalesDetail.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    MSalesDetail._edit(sel['id'])
};
// 表单预处理
MSalesDetail.preForm = function () {
    new $yd.combobox("salesId").load("/sales/select");
    new $yd.combobox("skuId").load("/sku/select");
};
MSalesDetail._edit = function (id) {
    $yd.win.create('编辑', MSalesDetail.uEdit + '?id=' + id).open(function (that, $win) {
            MSalesDetail.preForm();
            $yd.get(MSalesDetail.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(MSalesDetail.u).submit(function () {
                that.close();
                MSalesDetail.refresh();
            });
        }
    );
};
//新增
MSalesDetail.plus = function () {
    var sel = MSalesDetail.dg.getOne();
    var href = MSalesDetail.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增', href).open(function (that, $win){
            MSalesDetail.preForm();
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(MSalesDetail.u).submit(function () {
                that.close();
                MSalesDetail.refresh();
            });
        }
    );
};
//
MSalesDetail.refresh = function () {
    MSalesDetail.dtable.datagrid('reload');
};
//搜索
MSalesDetail.search = function () {
    MSalesDetail.dtable.datagrid('load', MSalesDetail.sform.serializeObject());
};

$(function () {
    MSalesDetail.sform = $('#mSalesDetail_form');
    MSalesDetail.dtable = $('#mSalesDetail_table');
    MSalesDetail.obtn = '#mSalesDetail_btn';

    MSalesDetail.dg(); // 数据表格
    MSalesDetail.btn(); // 初始化按钮
    $yd.bindEnter(MSalesDetail.search);
});