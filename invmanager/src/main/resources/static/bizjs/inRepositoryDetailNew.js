<!-- in_repository_detail.ftl 入库单详情 -->
var InRepositoryDetail = {
    u: '/inRepositoryDetail'
    , uList: '/inRepositoryDetail/getOne',
    unitArr: []
};
InRepositoryDetail.code =0;
InRepositoryDetail.dg = function () {
    $.ajax({
        type: "POST",
        url: "/sysDict/materiel_MU",
        dataType: "json",
        success: function (data) {
            InRepositoryDetail.unitArr = data;
        }
    });

    InRepositoryDetail.dtable.datagrid({
        url: InRepositoryDetail.uList + "?code="+ InRepositoryDetail.code,
        fitColumns: true,
        autoRowHeight: true,
        rownumbers: true,
        singleSelect: false,
        pagination: false,
        columns: [[
            {field: 'id', checkbox: false, hidden:true},
            {field: 'goodsName', title: '商品名称', width: 150},
            {field: 'skus', title: '规格', width: 200},
            {
                field: 'unit', title: '单位', width: 80, formatter: function (value, row, index) {
                    for (var i = 0; i < InRepositoryDetail.unitArr.length; i++) {
                        if (value == InRepositoryDetail.unitArr[i].id) {
                            return InRepositoryDetail.unitArr[i].text;
                        }
                    }
                    return value;
                }
            }
            , {field: 'count', title: '入库数量', width: 50}
            , {field: 'providerName', title: '供应商', width: 80}
            , {field: 'houseName', title: '仓库', width: 80}
            , {field: 'positionName', title: '库位', width: 80}
            , {field: 'batchCode', title: '批号', width: 80}
            , {field: 'remark', title: '备注', width: 120}
        ]]
    });
};
// 处理按钮
InRepositoryDetail.btn = function () {
    var sb = new $yd.SearchBtn($(InRepositoryDetail.obtn));
    sb.create('print green', '打印', function () {
        InRepositoryDetail.dtable.datagrid('print', '入库表');
    });
};

$(function () {
    InRepositoryDetail.sform = $('#inRepositoryDetail_form');
    InRepositoryDetail.dtable = $('#inRepositoryDetail_table');
    InRepositoryDetail.obtn = '#inRepositoryDetail_btn';
    InRepositoryDetail.code = $("#code").val();
    InRepositoryDetail.dg(); // 数据表格
    InRepositoryDetail.btn(); // 初始化按钮
    $yd.bindEnter(InRepositoryDetail.search);
});


