<!-- w_store.ftl 门店 -->
var WStore = {
    u: '/wStore'
    , uList: '/wStore/list'
    , uEdit: '/wStore/edit'
    , uDel: '/wStore/delete'
};

WStore.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index, row) {
        WStore._edit(row['id']);
    };
    grid.url = WStore.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'name', title: '名称', width: 80}
        , {field: 'nickname', title: '店铺管理人', width: 80}
        , {field: 'mobile', title: '店铺联系电话', width: 80}
        , {field: 'addr', title: '详细地址', width: 80}
        , {field: 'memo', title: '简单描述', width: 80}
        , {field: 'company_name', title: '企业名称', width: 80}
        , {field: 'company_num', title: '企业税号', width: 80}
        , {
            field: 'ttstore_id', title: '开通天天爱车', width: 80, formatter: function (value, row) {
                if (value) {
                    return '<label class="label label-sm label-primary">已开通</label>';
                } else {
                    return '<button class="btn btn-xs btn-info" onclick="WStore.applyTTicar(' + row.mobile + ')">申请开通</button>';
                }
            }
        }
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: '创建时间', width: 80}
    ]];
    grid.loadGrid();
};
//
WStore.applyTTicar = function (mobile) {
    $yd.confirm("此操作会根据店铺联系电话匹配天天爱车账号，并且同步商品，确定继续?", function () {
        if (mobile) {
            $yd.post("/tticar/store", {mobile: mobile}, function (data) {
                WStore.refresh();
            })
        } else {
            $yd.alert("店铺联系电话为空");
        }
    });
};
WStore.dg.getOne = function () {
    var sels = WStore.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
WStore.dg.getSels = function () {
    return WStore.dtable.datagrid('getSelections');
};
// 处理按钮
WStore.btn = function () {
    var sb = new $yd.SearchBtn($(WStore.obtn));
    sb.create('remove', '删除', WStore.remove);
    sb.create('refresh', '刷新', WStore.refresh);
    sb.create('edit', '编辑', WStore.edit);
    sb.create('plus', '新增', WStore.plus);
    // sb.create('search', '查询', WStore.search);
};
//删除
WStore.remove = function () {
    var sels = WStore.dg.getSels();
    if (!sels ||sels.length <1) {
        $yd.alert('请选中记录 ');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(WStore.uDel, {ids: ids}, function () {
            WStore.refresh();
        });
    })
};
//
WStore.edit = function () {
    var sel = WStore.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    WStore._edit(sel['id'])
};
// 表单预处理
WStore.preForm = function () {
    var $userId = new $yd.combogrid("#userId");
    $userId.textField = 'nickname';
    $userId.onSelect = function () {
        var g = $('#userId').combogrid('grid').datagrid('getSelected');
        $('#mobile').textbox('setValue', g['mobile']);
    };
    $userId.addCol('nickname', "姓名").addCol('username', "用户名").load("/sysUser/list");
};
WStore._edit = function (id) {
    $yd.win.create('编辑门店', WStore.uEdit + '?id=' + id).open(function (that, $win) {
            WStore.preForm();
            $yd.get(WStore.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(WStore.u).submit(function () {
                that.close();
                WStore.refresh();
            });
        }
    );
};
//新增
WStore.plus = function () {
    var sel = WStore.dg.getOne();
    var href = WStore.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增门店', href).open(function (that, $win) {
            WStore.preForm();
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(WStore.u).submit(function () {
                that.close();
                WStore.refresh();
            });
        }
    );
};
//
WStore.refresh = function () {
    $('#wStore_table').datagrid('unselectAll');
    WStore.dtable.datagrid('reload');
};
//搜索
WStore.search = function () {
    WStore.dtable.datagrid('load', WStore.sform.serializeObject());
};

$(function () {
    WStore.sform = $('#wStore_form');
    WStore.dtable = $('#wStore_table');
    WStore.obtn = '#wStore_btn';

    WStore.dg(); // 数据表格
    WStore.btn(); // 初始化按钮
    $yd.bindEnter(WStore.search);
});
var btn1 = document.getElementById("searchButton");
btn1.addEventListener("click", function() {
    $('#wStore_table').datagrid('unselectAll');
    WStore.dtable.datagrid('load', WStore.sform.serializeObject());
});