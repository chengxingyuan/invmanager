<!-- 商品拆装 -->
var BsAdd = {
    u: '/bybs',
    uList: '/bybs/list',
    uEdit: '/bybs/add',
    editIndex: -1,
    unitArr: [],
    isDg: 0,
    winThat: undefined,
    dgType: undefined,
    isConfirm: 1
};

$.extend($.fn.datagrid.defaults.editors, {
    house: {
        init: function (container, options) {
            return $('<div style="text-align: center">编辑</div>').appendTo(container);
        },
        destroy: function (target) {
            $(target).remove();
        },
        getValue: function (target) {
            var rows = BsAdd.dtable.datagrid("getRows");
            var house = rows[BsAdd.editIndex].house;
            return house;
        },
        setValue: function (target, value) {
        },
        resize: function (target, width) {
            $(target).click(function () {
                BsAdd.isDg = 1;
                var rows = BsAdd.dtable.datagrid("getRows");
                var skuId = rows[BsAdd.editIndex].skuId;
                var house = rows[BsAdd.editIndex].house;
                var href = "/wWearhouse/treeGrid?skuId=" + skuId + "&house=" + house + "&type=0&houseId=" + $("#houseId").val();//type 0出库 1入库
                var $dg = $yd.win.create('仓库/库位/批次', href);
                $dg.width = "450px;";
                $dg.height = "520px;";
                $dg.open(function (that, $win) {
                    BsAdd.winThat = that;
                    BsAdd.dgType = "house";
                }, function () {
                    BsAdd.saveDgHouse();
                }, function () {
                    if (BsAdd.editIndex > -1) {
                        BsAdd.dtable.datagrid('endEdit', BsAdd.editIndex);
                    }
                    BsAdd.editIndex = -1;
                    BsAdd.isDg = 0;
                    BsAdd.winThat = undefined;
                    BsAdd.dgType = undefined;
                });
            });

            $(target)._outerWidth(width);
        }
    }
});
BsAdd.saveDgHouse = function () {
    var rows = BsAdd.dtable.datagrid("getRows");
    var data = KW.getResult();
    if (data != "error") {
        if (data != "") {
            //销售数量也要修改
            var arr = data.split(",");
            var total = 0;
            for (var i = 0; i < arr.length; i++) {
                total += Number(arr[i].split("_")[6]);
            }
            if (total == Number(rows[BsAdd.editIndex].count)) {
                BsAdd.dtable.datagrid('updateRow', {
                    index: BsAdd.editIndex,
                    row: {
                        house: data
                    }
                });
            } else {
                BsAdd.dtable.datagrid('updateRow', {
                    index: BsAdd.editIndex,
                    row: {
                        house: data,
                        num: total,
                        fee: Number(total) * Number(rows[BsAdd.editIndex].price)
                    }
                });
            }
        } else {
            BsAdd.dtable.datagrid('updateRow', {
                index: BsAdd.editIndex,
                row: {
                    house: data
                }
            });
        }

        BsAdd.winThat.close();
        BsAdd.isDg = 0;
        BsAdd.winThat = undefined;
        BsAdd.dgType = undefined;
    }
};
BsAdd.dg = function () {
    BsAdd.dtable = $('#part_table');
    BsAdd.dtable.datagrid({
        pagination: false,
        fitColumns: true,
        autoRowHeight: true,
        rownumbers: true,
        singleSelect: false,
        toolbar: [
            {
                text: '增加（Insert）', iconCls: 'icon-add', handler: function () {
                    BsAdd.plus();
                }
            },
            {
                text: '删除（Del）', iconCls: 'icon-remove', handler: function () {
                    BsAdd.remove();
                }
            }
        ],
        columns: [[
            {field: 'skuId', checkbox: true},//规格id
            {field: 'goodsName', title: '商品名称', width: 150},
            {field: 'goodsId', title: '商品ID', width: 80, hidden: true},
            {field: 'skus', title: '规格', width: 200},
            {field: 'code', title: '商品编号', width: 80},
            {
                field: 'unit', title: '单位', width: 80, formatter: function (value, row, index) {
                    for (var i = 0; i < BsAdd.unitArr.length; i++) {
                        if (value == BsAdd.unitArr[i].id) {
                            return BsAdd.unitArr[i].text;
                        }
                    }
                    return value;
                }
            },
            {field: 'partsCode', title: '配件编号', width: 80},
            {field: 'skusNum', title: '库存数量', width: 80, hidden: false},
            // {
            //     field: 'house',
            //     title: '<span style="color:blue">库位</span>',
            //     width: 150,
            //     editor: {type: 'house'},
            //     formatter: function (value, row, index) {
            //         if (BsAdd.isEmpty(value)) {
            //             return "";
            //         }
            //         var arr = value.split(",");
            //         var result = "";
            //         var first = "";
            //         for (var i = 0; i < arr.length; i++) {
            //             var arr2 = arr[i].split("_");
            //             result += arr2[3] + " " + arr2[4] + " X " + arr2[6] + "\n"
            //             if (first == "") {
            //                 first += arr2[3] + " " + arr2[4] + " X " + arr2[6];
            //             }
            //         }
            //         return "<a style='color:purple' title='" + result + "'>" + first + (arr.length > 1 ? "..." : "") + "</a>";
            //     }
            // },
            {
                field: 'price', title: '成本单价', width: 80, formatter: function (value, row, index) {
                    return Number(value).toFixed(2);
                }
            },
            {
                field: 'num',
                title: '<span style="color:blue">数量</span><span style="color:red">*</span>',
                width: 80,
                editor: {type: 'numberbox', options: {required: true, min: 1, precision: 0}}
            },
            {
                field: 'fee', title: '总金额', width: 80, formatter: function (value, row, index) {
                    return Number(value).toFixed(2);
                }
            },
            {
                field: 'remark',
                title: '<span style="color:blue">备注</span>',
                width: 100,
                editor: {type: 'textbox'}
            }
        ]],
        onClickCell: function (index, field) {
            if (BsAdd.editIndex != index) {
                if (BsAdd.editIndex > -1) {
                    BsAdd.dtable.datagrid('endEdit', BsAdd.editIndex);
                }
                BsAdd.editIndex = index;
            }
            if (field == "num" || field == "house" || field == "remark") {
                BsAdd.dtable.datagrid('beginEdit', index);
            }
        },
        onEndEdit: function (index, row, changes) {
            if (changes) {
                if (changes.num != undefined && row.skusNum != 0) {
                    if (row.num > row.skusNum) {
                        BsAdd.dtable.datagrid('updateRow', {
                            index: index,
                            row: {
                                num: row.skusNum
                            }
                        });
                    }
                    $.post("/inRepositoryDetail/getSkuHouseByNum", {
                        skuId: row.skuId,
                        num: row.num,
                        houseId: $("#houseId").val()
                    }, function (data) {
                        BsAdd.dtable.datagrid('updateRow', {
                            index: index,
                            row: {
                                house: data
                            }
                        });
                    });
                }

                BsAdd.dtable.datagrid('updateRow', {
                    index: index,
                    row: {
                        fee: Number(row.price) * Number(row.num)
                    }
                });
            }
        }
    });

    BsAdd.dtable.datagrid('enableCellEditing');
};
BsAdd.formatDetail = function (value) {
    if ($yd.isEmpty(value)) {
        return "";
    }
    var result = "";
    var first = "";
    var arr = value.split(",");
    if (arr.length == 1) {
        return "<a style='color:purple' title='" + value + "'>" + value + "</a>";
    }
    for (var i = 0; i < arr.length; i++) {
        result += arr[i] + "\n";
        if (i == 0) {
            first = result;
        }
    }
    var html = "<a style='color:purple' title='" + result + "'>" + first + "..." + "</a>";

    return html;
};
BsAdd.refresh = function () {
    window.location.reload();
};
BsAdd.clear = function () {
    var rows = BsAdd.dtable.datagrid("getRows");
    if(rows.length > 0) {
        BsAdd.dtable.datagrid("deleteRow", 0);
    }
};
BsAdd.plus = function () {
    var href = "/mMateriel/searchDg?houseId=" + $("#houseId").val();
    var $dg = $yd.win.create('商品', href);
    // var $dg = $yd.win.create('商品（当前仓库中有库存的才会被查出）', href);
    $dg.width = "1050px;";
    $dg.height = "400px;";
    BsAdd.isDg = 1;
    $dg.open(function (that, $win) {
            BsAdd.winThat = that;
            BsAdd.dgType = "goods";
        }, function () {
            BsAdd.saveDgGoods();
        }, function () {
            BsAdd.isDg = 0;
            BsAdd.winThat = undefined;
            BsAdd.dgType = undefined;
            document.onkeydown = BsAdd.keyDown;
        }
    );
};
BsAdd.isExist = function (rows, skuId) {
    for (var i = 0; i < rows.length; i++) {
        if (rows[i].skuId == skuId) {
            return true;
        }
    }
    return false;
};
BsAdd.saveDgGoods = function () {
    document.onkeydown = BsAdd.keyDown;

    var data = Goods.getChecked();
    if (data.length == 0) {
        BsAdd.winThat.close();
        BsAdd.isDg = 0;
        BsAdd.winThat = undefined;
        BsAdd.dgType = undefined;
    }

    var rows = BsAdd.dtable.datagrid("getData").rows;
    if(rows.length > 0) {
        BsAdd.dtable.datagrid("deleteRow", 0);
    }
    for (var i = 0; i < data.length; i++) {
        if (!BsAdd.isExist(rows, data[i].skuId)) {
            var dt = data[i];
            //获取成本单价
            $.ajax({
                type: "POST",
                url: "/inRepositoryDetail/getSkuAvgCost2House",
                data: {
                    skuId: data[i].skuId,
                    houseId: $("#houseId").val(),
                },
                dataType: "json",
                async: false,
                success: function (result) {
                    BsAdd.dtable.datagrid('appendRow', {
                        skuId: dt.skuId,
                        goodsName: dt.goodsName,
                        goodsId: dt.goodsId,
                        skus: dt.skusName,
                        code: dt.code,
                        partsCode: dt.partsCode,
                        unit: dt.unit,
                        skusNum: dt.skusNum,
                        price: Number(result.avgCost),
                        num: 1,
                        fee: Number(result.avgCost) * Number(1),
                        house: result.house,
                    }).datagrid('clearSelections');
                }
            });
        }
    }
    BsAdd.winThat.close();
    BsAdd.isDg = 0;
    BsAdd.winThat = undefined;
    BsAdd.dgType = undefined;
};
Array.prototype.contains = function (needle) {
    for (var i in this) {
        if (this[i] == needle) return true;
    }
    return false;
};

//删除
BsAdd.remove = function () {
    var sels = BsAdd.dtable.datagrid('getChecked')
    if (sels.length < 1) {
        $yd.alert('选中记录');
        return;
    }
    var ids = [];
    $.each(sels, function (i, n) {
        ids.push(n['skuId']);
    });

    $.each(ids, function (i, n) {
        var rows = BsAdd.dtable.datagrid('getRows');
        for (var ii = 0; ii < rows.length; ii++) {
            if (rows[ii].skuId == n) {
                BsAdd.dtable.datagrid('deleteRow', ii);
                break;
            }
        }
    });
};

BsAdd.isEmpty = function (value) {
    if (value == undefined || value == null || value == "") {
        return true;
    }
    return false;
};
BsAdd.save = function () {
    //结束编辑状态
    if (BsAdd.editIndex != undefined && BsAdd.editIndex != -1) {
        BsAdd.dtable.datagrid('endEdit', BsAdd.editIndex);
    }
    if (BsAdd.editIndex != undefined && BsAdd.editIndex != -1) {
        BsAdd.dtable.datagrid('endEdit', BsAdd.editIndex);
    }

    $('#myForm').form('submit', {
        url: BsAdd.uEdit,
        onSubmit: function () {
            var isValid = $(this).form('validate');//只能校验处于编辑状态的栏位
            if (!isValid) {
                return isValid;
            }

            //校验数据
            var rows = BsAdd.dtable.datagrid("getRows");
            if (rows.length == 0) {
                $yd.alert("请添加商品");
                return false;
            }

            // for (var i = 0; i < rows.length; i++) {
            //     var row = rows[i];
            //     var house = row.house;
            //     if (BsAdd.isEmpty(house)) {
            //         $yd.alert("商品未配置库位");
            //         return false;
            //     }
            // }

            //封装数据
            $("#goods").val(JSON.stringify(rows));
            return isValid;
        },
        success: function (data) {
            var d = $.parseJSON(data);
            if (d.code == 0) {
                $yd.alert("操作成功", function () {
                    window.location.reload();
                });
            }
        }
    });
};

// 处理按钮
BsAdd.btn = function () {
    var sb = new $yd.SearchBtn($(BsAdd.obtn));
    sb.create('repeat red', '重置（Shift+R）', BsAdd.refresh);
    sb.create('save green', '保存（Shift+S）', BsAdd.save);
};
BsAdd.keyDown = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (!e) {
        return;
    }
};
BsAdd.keyUp = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (!e) {
        return;
    }

    if (BsAdd.isDg == 0) {
        if (e.keyCode == 45) {//insert
            BsAdd.plus();
        }
        if (e.keyCode == 46) {//del
            BsAdd.remove();
        }
        if (e.shiftKey && e.keyCode == 83) {
            BsAdd.save();//生单: Shift + S
        }
        if (e.shiftKey && e.keyCode == 82) {
            BsAdd.refresh();
        }
    } else {
        if (e.keyCode == 13) {
            if(BsAdd.dgType == "goods") {
                BsAdd.saveDgGoods();
            } else if(BsAdd.dgType == "house") {
                BsAdd.saveDgHouse();
            }
        } else if (e.keyCode == 27 && BsAdd.winThat) {//ESC
            BsAdd.winThat.close();
        }
    }
};
BsAdd.init = function () {
    $.ajax({
        type: "POST",
        url: "/sysDict/materiel_MU",
        dataType: "json",
        success: function (data) {
            BsAdd.unitArr = data;
        }
    });

    //默认值
    $("input[name='orderTime']").datebox({
        required: true,
        editable: false,
        value: new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()
    });

    $("#orderType").combobox({
        url: "/sysDict/bybs_type",
        panelHeight:'auto',
        editable:false,
        required:true,
        valueField:'id',
        textField:'text',
        onChange: function (newValue, oldValue) {
            if(BsAdd.isConfirm == 0) {
                return;
            }
            if(!$yd.isEmpty(oldValue)) {
                $.messager.confirm({
                    title: '确认框',
                    border: false,
                    top: 0,
                    msg: "切换单据类型页面会重置，确定切换？",
                    fn: function (r) {
                        if (r) {
                            window.location.href = "/bybs/add?orderType=0";
                        } else {
                            BsAdd.isConfirm = 0;
                            $("#orderType").combobox("setValue", oldValue);
                            BsAdd.isConfirm = 1;
                        }
                    }
                })
            } else {
                window.location.href = "/bybs/add?orderType=0";
            }
        }
    });

    $("#houseId").combobox({
        prompt:'仓库',
        url: "/wWearhouse/list",
        panelHeight:'auto',
        editable:false,
        required:true,
        valueField:'id',
        textField:'name',
        onLoadSuccess: function () {
            var d = $(this).combobox("getData");
            $(this).combobox("setValue", d[0].id);
        },
        onChange: function (newValue, oldValue) {
            var rows1 = BsAdd.dtable.datagrid("getRows");
            if(rows1.length == 0) {
                return;
            }
            if(BsAdd.isConfirm == 0) {
                return;
            }
            if(!$yd.isEmpty(oldValue)) {
                $.messager.confirm({
                    title: '确认框',
                    border: false,
                    top: 0,
                    msg: "切换仓库清除当前仓库已添加的商品，确定切换？",
                    fn: function (r) {
                        if (r) {
                            BsAdd.clear();
                        } else {
                            BsAdd.isConfirm = 0;
                            $("#houseId").combobox("setValue", oldValue);
                            BsAdd.isConfirm = 1;
                        }
                    }
                })
            } else {
                BsAdd.clear();
            }
        }
    });

    //鼠标点击页面任何地方，表格编辑结束编辑状态
    document.onclick = BsAdd.mouseClick;

    //快捷键
    document.onkeydown = BsAdd.keyDown;
    document.onkeyup = BsAdd.keyUp;

    setTimeout("$('#orderTime').focus()", 500);
    $("a").attr("tabindex", -1);
    $(":checkbox").attr("tabindex", -1);
};
BsAdd.mouseClick = function (e) {
    e = window.event || e; // 兼容IE7
    var obj = $(e.srcElement || e.target);

    if (BsAdd.isDg == 0 && !$(obj).hasClass("datagrid-cell") && !$(obj).hasClass("datagrid-row-over") && !$(obj).hasClass("datagrid-row-selected")) {
        if (BsAdd.editIndex != -1) {
            BsAdd.dtable.datagrid('endEdit', BsAdd.editIndex);
            BsAdd.editIndex = -1;
        }
    }
};
$(function () {
    BsAdd.obtn = '#part_btn';
    BsAdd.dg(); // 数据表格
    BsAdd.btn(); // 初始化按钮
    BsAdd.init(); // 数据表格
});