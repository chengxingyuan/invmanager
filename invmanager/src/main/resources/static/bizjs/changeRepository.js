<!-- change_repository.ftl 调拨单 -->
var ChangeRepository = {
    u: '/changeRepository'
    , uList: '/changeRepository/list'
    , uEdit: '/changeRepository/edit'
    , uDel: '/changeRepository/delete'
    , checkStatus: 0
};

ChangeRepository.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        // ChangeRepository._edit(row['id']);
    };
    grid.rownumbers = true;
    grid.checkOnSelect = false;
    grid.onClickRow= function (rowIndex,rowData) {
        $(this).datagrid('unselectRow', rowIndex);
        var isShow =rowData['status'];

        if (isShow =='未审核') {
            document.getElementById("checkGoodsBtn").style.display="block";
        }else {
            document.getElementById("checkGoodsBtn").style.display="none";
        }
        var code = rowData['code'];
        $('#changeCode').val(code);
        $('#changeGoods_table').datagrid("reload", {
            code: code
        });
    };
    grid.singleSelect = true;
    grid.url = ChangeRepository.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'code', title: '调拨单号', width: 80,halign:'center',align:'center'}
        // , {field: 'skuId', title: '商品sku_id', width: 80}
        , {field: 'fromWearhouseName', title: '调出仓库', width: 80,halign:'center',align:'center'}
        , {field: 'toWearhouseName', title: '调入仓库', width: 80,halign:'center',align:'center'}
        , {field: 'count', title: '调拨数量', width: 80,halign:'center',align:'center'}
        , {field: 'username', title: '调拨人', width: 80,halign:'center',align:'center'}
        , {field: 'status', title: '审核状态', width: 80,halign:'center',align:'center', formatter: function (value) {
            if (value == 0) {
                return '<label class="label label-success">未审核</label>';
            } else if(value == 1) {
                return '<label class="label label-default">已审核</label>';
            }
        }}

        // , {field: 'money', title: '调拨成本', width: 80}
        , {field: 'remark', title: '备注', width: 80,halign:'center',align:'center'}
        // , {
        //     field: 'status', title: '状态', width: 50, formatter: function (value) {
        //         if (value == 0) {
        //             return '<label class="label label-success">启用</label>';
        //         } else {
        //             return '<label class="label label-default">禁用</label>';
        //         }
        //     }
        // }
        , {field: 'ctime', title: '创建时间', width: 80,halign:'center',align:'center'}
    ]];

    grid.loadGrid();
};
ChangeRepository.dg.getOne = function () {
    var sels = ChangeRepository.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
ChangeRepository.dg.getSels = function () {
    return ChangeRepository.dtable.datagrid('getSelections');
};
// 处理按钮
ChangeRepository.btn = function () {
    var sb = new $yd.SearchBtn($(ChangeRepository.obtn));
    sb.create('remove', '删除', ChangeRepository.remove);
    sb.create('refresh', '刷新', ChangeRepository.refresh);
    // sb.create('edit', '编辑', ChangeRepository.edit);
    sb.create('plus', '新增', ChangeRepository.plus);
    // sb.create('search', '查询', ChangeRepository.search);
};
//删除
ChangeRepository.remove = function () {
    var sels = ChangeRepository.dg.getSels();
    console.log(sels)
    if (!sels || sels.length < 1) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var id = 0;
        $.each(sels, function (i, n) {
            id= n['id'];
        });
        $yd.post(ChangeRepository.uDel, {ids: id}, function () {
            ChangeRepository.refresh();
        });
    })
};
//
ChangeRepository.edit = function () {
    var sel = ChangeRepository.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    ChangeRepository._edit(sel['id'])
};
ChangeRepository._edit = function (id) {
    $yd.win.create('编辑调拨单', ChangeRepository.uEdit + '?id=' + id).open(function (that, $win) {
            $yd.get(ChangeRepository.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(ChangeRepository.u).submit(function () {
                that.close();
                ChangeRepository.refresh();
            });
        }
    );
};
//新增
ChangeRepository.plus = function () {
    // var sel = ChangeRepository.dg.getOne();
    var href = ChangeRepository.uEdit;
    // if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    // $yd.win.create('新增调拨单', href).open(function (that, $win){
    //     }, function (that, $win) {
    //         $yd.form.create($win.find('form')).url(ChangeRepository.u).submit(function () {
    //             that.close();
    //             ChangeRepository.refresh();
    //         });
    //     }
    // );
    var dg = $yd.win3.create('新增调拨单', href);
    dg.width = "70%";
    dg.height = "60%";
    dg.open(function (that, $win) {

    });
};
//
ChangeRepository.refresh = function () {
    // window.location.reload();
    $(".search-form").form('clear');
    ChangeRepository.dtable.datagrid('load', ChangeRepository.sform.serializeObject());
};
//搜索
ChangeRepository.search = function () {
    ChangeRepository.dtable.datagrid('load', ChangeRepository.sform.serializeObject());
};


ChangeRepository.initGoodsTable = function(){


    var grid = new $yd.DataGrid($('#changeGoods_table'));
    // grid.onDblClickRow = function (index,row) {
    //     CCheckDetail._edit(row['id']);
    // };
    grid.rownumbers = true;
    grid.pagination = false;
    grid.url = '/changeRepository/changeGoodsList';
    grid.columns = [[
        {field: 'id', checkbox: false, hidden: true}
        , {field: 'skuId', hidden: true}
        , {field: 'goodsName', title: '商品名称', width: 120,halign:'center',align:'center'}
        , {field: 'skus', title: '规格', width: 120,halign:'center',align:'center'}
        , {field: 'count', title: '数量', width: 80,halign:'center',align:'center'}

    ]];
    // grid.onLoadSuccess = function (data) {
    //     $('#remark').textbox("setValue",data.rows[0].remark);
    // }
    grid.loadGrid();

};
ChangeRepository.goodsTableBtn = function () {
    var sb = new $yd.SearchBtn($('#checkGoodsBtn'));
    if(ChangeRepository.checkStatus == 0) {
        sb.create('check green', '调拨审核', ChangeRepository.correct);
    }
};
ChangeRepository.correct = function () {
    $yd.confirm("确认审核通过该调拨单？",function () {
        var dbCode = $('#changeCode').val();
        $.post("/changeRepository/changeCorrect", {
            dbCode: dbCode
        }, function (data) {
            if (data.code == 0) {
                $yd.alert("操作成功", function () {
                    window.location.reload();
                });
            } else {
                $yd.alert(data.message);
            }
        });
    });


}

$(function () {
    ChangeRepository.sform = $('#changeRepository_form');
    ChangeRepository.dtable = $('#changeRepository_table');
    ChangeRepository.obtn = '#changeRepository_btn';

    ChangeRepository.dg(); // 数据表格
    ChangeRepository.btn(); // 初始化按钮
    ChangeRepository.init();
    ChangeRepository.initGoodsTable();
    ChangeRepository.goodsTableBtn();
    $yd.bindEnter(ChangeRepository.search);
});

ChangeRepository.init = function () {
    $("input[id='repository']").combobox({
        url: "/wWearhouse/list",
        panelHeight: 'auto',
        required: true,
        valueField: 'id',
        method:'post',
        textField: 'text',
        editable: false,
        // onLoadSuccess: function () {
        //     var d = $(this).combobox("getData");
        //     $(this).combobox("setValue", d[0].value);
        // }
    });
    $("input[name='changeTime']").datebox({
        required: true,
        editable: false,
        value: new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()
    }); 
}
var btn1 = document.getElementById("searchButton");
btn1.addEventListener("click", function () {
    ChangeRepository.dtable.datagrid('load', ChangeRepository.sform.serializeObject());
});
