<!-- car_brand_tmp.ftl 车品牌 -->
var CarBrandTmp = {
    u: '/carBrandTmp'
    , uList: '/carBrandTmp/list'
    , uEdit: '/carBrandTmp/edit'
    , uDel: '/carBrandTmp/delete'
};

CarBrandTmp.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        CarBrandTmp._edit(row['id']);
    };
    grid.url = CarBrandTmp.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'brandName', title: '品牌名称', width: 80}
        , {field: 'subName', title: '子品牌名称', width: 80}
        , {field: 'seriesName', title: '系列名称', width: 80}
        , {field: 'modelName', title: '车型名称', width: 80}
        , {field: 'skuName', title: '车款名称', width: 80}
        , {field: 'src', title: '图片地址', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
CarBrandTmp.dg.getOne = function () {
    var sels = CarBrandTmp.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
CarBrandTmp.dg.getSels = function () {
    return CarBrandTmp.dtable.datagrid('getSelections');
};
// 处理按钮
CarBrandTmp.btn = function () {
    var sb = new $yd.SearchBtn($(CarBrandTmp.obtn));
    sb.create('remove', '删除', CarBrandTmp.remove);
    sb.create('refresh', '刷新', CarBrandTmp.refresh);
    sb.create('edit', '编辑', CarBrandTmp.edit);
    sb.create('plus', '新增', CarBrandTmp.plus);
    sb.create('search', '查询', CarBrandTmp.search);
};
//删除
CarBrandTmp.remove = function () {
    var sels = CarBrandTmp.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(CarBrandTmp.uDel, {ids: ids}, function () {
            CarBrandTmp.refresh();
        });
    })
};
//
CarBrandTmp.edit = function () {
    var sel = CarBrandTmp.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    CarBrandTmp._edit(sel['id'])
};
CarBrandTmp._edit = function (id) {
    $yd.win.create('编辑车品牌', CarBrandTmp.uEdit + '?id=' + id).open(function (that, $win) {
            $yd.get(CarBrandTmp.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(CarBrandTmp.u).submit(function () {
                that.close();
                CarBrandTmp.refresh();
            });
        }
    );
};
//新增
CarBrandTmp.plus = function () {
    var sel = CarBrandTmp.dg.getOne();
    var href = CarBrandTmp.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增车品牌', href).open(function (that, $win){
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(CarBrandTmp.u).submit(function () {
                that.close();
                CarBrandTmp.refresh();
            });
        }
    );
};
//
CarBrandTmp.refresh = function () {
    CarBrandTmp.dtable.datagrid('reload');
};
//搜索
CarBrandTmp.search = function () {
    CarBrandTmp.dtable.datagrid('load', CarBrandTmp.sform.serializeObject());
};

$(function () {
    CarBrandTmp.sform = $('#carBrandTmp_form');
    CarBrandTmp.dtable = $('#carBrandTmp_table');
    CarBrandTmp.obtn = '#carBrandTmp_btn';

    CarBrandTmp.dg(); // 数据表格
    CarBrandTmp.btn(); // 初始化按钮
    $yd.bindEnter(CarBrandTmp.search);
});