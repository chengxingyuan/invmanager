<!-- m_cateogry_tmp.ftl 存货分类表 -->
var MCateogryTmp = {
    u: '/mCateogryTmp'
    , uList: '/mCateogryTmp/list'
    , uEdit: '/mCateogryTmp/edit'
    , uDel: '/mCateogryTmp/delete'
};

MCateogryTmp.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        MCateogryTmp._edit(row['id']);
    };
    grid.url = MCateogryTmp.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'name', title: '分类名称', width: 80}
        , {field: 'groupName', title: 'groupName', width: 80}
        , {field: 'code', title: '分类编码', width: 80}
        , {field: 'categoryType', title: '分类类型：1级分类，2级分类，3级分类', width: 80}
        , {field: 'isLeaf', title: '是否叶子节点', width: 80}
        , {field: 'sort', title: '排序', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
MCateogryTmp.dg.getOne = function () {
    var sels = MCateogryTmp.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
MCateogryTmp.dg.getSels = function () {
    return MCateogryTmp.dtable.datagrid('getSelections');
};
// 处理按钮
MCateogryTmp.btn = function () {
    var sb = new $yd.SearchBtn($(MCateogryTmp.obtn));
    sb.create('remove', '删除', MCateogryTmp.remove);
    sb.create('refresh', '刷新', MCateogryTmp.refresh);
    sb.create('edit', '编辑', MCateogryTmp.edit);
    sb.create('plus', '新增', MCateogryTmp.plus);
    sb.create('search', '查询', MCateogryTmp.search);
};
//删除
MCateogryTmp.remove = function () {
    var sels = MCateogryTmp.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(MCateogryTmp.uDel, {ids: ids}, function () {
            MCateogryTmp.refresh();
        });
    })
};
//
MCateogryTmp.edit = function () {
    var sel = MCateogryTmp.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    MCateogryTmp._edit(sel['id'])
};
MCateogryTmp._edit = function (id) {
    $yd.win.create('编辑存货分类表', MCateogryTmp.uEdit + '?id=' + id).open(function (that, $win) {
            $yd.get(MCateogryTmp.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(MCateogryTmp.u).submit(function () {
                that.close();
                MCateogryTmp.refresh();
            });
        }
    );
};
//新增
MCateogryTmp.plus = function () {
    var sel = MCateogryTmp.dg.getOne();
    var href = MCateogryTmp.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增存货分类表', href).open(function (that, $win){
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(MCateogryTmp.u).submit(function () {
                that.close();
                MCateogryTmp.refresh();
            });
        }
    );
};
//
MCateogryTmp.refresh = function () {
    MCateogryTmp.dtable.datagrid('reload');
};
//搜索
MCateogryTmp.search = function () {
    MCateogryTmp.dtable.datagrid('load', MCateogryTmp.sform.serializeObject());
};

$(function () {
    MCateogryTmp.sform = $('#mCateogryTmp_form');
    MCateogryTmp.dtable = $('#mCateogryTmp_table');
    MCateogryTmp.obtn = '#mCateogryTmp_btn';

    MCateogryTmp.dg(); // 数据表格
    MCateogryTmp.btn(); // 初始化按钮
    $yd.bindEnter(MCateogryTmp.search);
});