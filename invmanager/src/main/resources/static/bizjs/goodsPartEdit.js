<!-- 商品拆装 -->
var Part = {
    u: '/goodsPart',
    uList: '/goodsPart/list',
    uEdit: '/goodsPart/edit',
    editIndex: -1,
    editIndex2: -1,
    unitArr: [],
    isDg: 0,
    winThat: undefined,
    dgType: undefined,
    isConfirm: 1
};

$.extend($.fn.datagrid.defaults.editors, {
    house: {
        init: function (container, options) {
            return $('<div style="text-align: center">编辑</div>').appendTo(container);
        },
        destroy: function (target) {
            $(target).remove();
        },
        getValue: function (target) {
            var rows = Part.dtable.datagrid("getRows");
            var house = rows[Part.editIndex].house;
            return house;
        },
        setValue: function (target, value) {
        },
        resize: function (target, width) {
            $(target).click(function () {
                Part.isDg = 1;
                var rows = Part.dtable.datagrid("getRows");
                var skuId = rows[Part.editIndex].skuId;
                var house = rows[Part.editIndex].house;
                var href = "/wWearhouse/treeGrid?skuId=" + skuId + "&house=" + house + "&type=0&houseId=" + $("#houseId").val();//type 0出库 1入库
                var $dg = $yd.win.create('仓库/库位/批次', href);
                $dg.width = "450px;";
                $dg.height = "520px;";
                $dg.open(function (that, $win) {
                    Part.winThat = that;
                    Part.dgType = "house";
                }, function (that, $win) {
                    Part.saveDgHouse();
                }, function () {
                    if (Part.editIndex > -1) {
                        Part.dtable.datagrid('endEdit', Part.editIndex);
                    }
                    Part.editIndex = -1;
                    Part.isDg = 0;
                    Part.winThat = undefined;
                    Part.dgType = undefined;
                });
            });

            $(target)._outerWidth(width);
        }
    }
});
$.extend($.fn.datagrid.defaults.editors, {
    house2: {
        init: function (container, options) {
            return $('<div style="text-align: center">编辑</div>').appendTo(container);
        },
        destroy: function (target) {
            $(target).remove();
        },
        getValue: function (target) {
            var rows = Part.dtable2.datagrid("getRows");
            var house = rows[Part.editIndex2].house;
            return house;
        },
        setValue: function (target, value) {
        },
        resize: function (target, width) {
            $(target).click(function () {
                Part.isDg = 1;
                var rows = Part.dtable2.datagrid("getRows");
                var skuId = rows[Part.editIndex2].skuId;
                var house = rows[Part.editIndex2].house;
                var href = "/wWearhouse/treeGrid?skuId=" + skuId + "&house=" + house + "&type=1&houseId=" + $("#houseId").val();//type 0出库 1入库
                var $dg = $yd.win.create('仓库/库位', href);
                $dg.width = "450px;";
                $dg.height = "520px;";
                $dg.open(function (that, $win) {
                    Part.winThat = that;
                    Part.dgType = "house2";
                }, function (that, $win) {
                    Part.saveDgHouse2();
                }, function () {
                    if (Part.editIndex2 > -1) {
                        Part.dtable2.datagrid('endEdit', Part.editIndex2);
                    }
                    Part.editIndex2 = -1;
                    Part.isDg = 0;
                    Part.winThat = undefined;
                    Part.dgType = undefined;
                });
            });

            $(target)._outerWidth(width);
        }
    }
});
Part.saveDgHouse = function () {
    var rows = Part.dtable.datagrid("getRows");
    var data = KW.getResult();
    if (data != "error") {
        if (data != "") {
            //销售数量也要修改
            var arr = data.split(",");
            var total = 0;
            for (var i = 0; i < arr.length; i++) {
                total += Number(arr[i].split("_")[6]);
            }
            if (total == Number(rows[Part.editIndex].count)) {
                Part.dtable.datagrid('updateRow', {
                    index: Part.editIndex,
                    row: {
                        house: data
                    }
                });
            } else {
                Part.dtable.datagrid('updateRow', {
                    index: Part.editIndex,
                    row: {
                        house: data,
                        num: total,
                        fee: Number(total) * Number(rows[Part.editIndex].price)
                    }
                });
            }
        } else {
            Part.dtable.datagrid('updateRow', {
                index: Part.editIndex,
                row: {
                    house: data
                }
            });
        }

        Part.winThat.close();
        Part.isDg = 0;
        Part.winThat = undefined;
        Part.dgType = undefined;
    }
};
Part.saveDgHouse2 = function () {
    var data = KW.getResult();
    var positionId = "";
    if (!$yd.isEmpty(data)) {
        var arr = data.split("_");
        positionId = arr[2];
    }
    Part.dtable2.datagrid('updateRow', {
        index: Part.editIndex2,
        row: {
            house: data,
            positionId: positionId
        }
    });

    Part.winThat.close();
    Part.isDg = 0;
    Part.winThat = undefined;
    Part.dgType = undefined;
};
Part.dg = function () {
    Part.dtable = $('#part_table');
    Part.dtable.datagrid({
        url: "/goodsPart/getInfo?id=" + $("#id").val(),
        pagination: false,
        fitColumns: true,
        autoRowHeight: true,
        rownumbers: true,
        singleSelect: true,
        columns: [[
            {field: 'skuId', checkbox: false, hidden: true},//规格id
            {field: 'goodsName', title: '商品名称', width: 150},
            {field: 'goodsId', title: '商品ID', width: 80, hidden: true},
            {field: 'skuName', title: '规格', width: 200},
            {field: 'code', title: '商品编号', width: 80},
            {
                field: 'unit', title: '单位', width: 80, formatter: function (value, row, index) {
                    for (var i = 0; i < Part.unitArr.length; i++) {
                        if (value == Part.unitArr[i].id) {
                            return Part.unitArr[i].text;
                        }
                    }
                    return value;
                }
            },
            {field: 'partsCode', title: '配件编号', width: 80},
            {field: 'skusNum', title: '库存数量', width: 80, hidden: true},
            // {
            //     field: 'house',
            //     title: '<span style="color:blue">库位</span>',
            //     width: 150,
            //     editor: {type: 'house'},
            //     formatter: function (value, row, index) {
            //         if (Part.isEmpty(value)) {
            //             return "";
            //         }
            //         var arr = value.split(",");
            //         var result = "";
            //         var first = "";
            //         for (var i = 0; i < arr.length; i++) {
            //             var arr2 = arr[i].split("_");
            //             result += arr2[3] + " " + arr2[4] + " X " + arr2[6] + "\n"
            //             if (first == "") {
            //                 first += arr2[3] + " " + arr2[4] + " X " + arr2[6];
            //             }
            //         }
            //         return "<a style='color:purple' title='" + result + "'>" + first + (arr.length > 1 ? "..." : "") + "</a>";
            //     }
            // },
            {
                field: 'price', title: '成本单价', width: 80, formatter: function (value, row, index) {
                    return Number(value).toFixed(2);
                }
            },
            {
                field: 'num',
                title: '<span style="color:blue">拆装数量</span><span style="color:red">*</span>',
                width: 80,
                editor: {type: 'numberbox', options: {required: true, min: 1, precision: 0}}
            },
            {
                field: 'fee', title: '拆分总额', width: 80, formatter: function (value, row, index) {
                    return Number(value).toFixed(2);
                }
            }
        ]],
        onClickCell: function (index, field) {
            if($("#status").val() == 1) {
                return;
            }
            if (Part.editIndex != index) {
                if (Part.editIndex > -1) {
                    Part.dtable.datagrid('endEdit', Part.editIndex);
                }
                Part.editIndex = index;
            }
            if (field == "num" || field == "house") {
                Part.dtable.datagrid('beginEdit', index);
            }
        },
        onEndEdit: function (index, row, changes) {
            if (changes) {
                if (changes.num != undefined && row.skusNum != 0) {
                    if (row.num > row.skusNum) {
                        Part.dtable.datagrid('updateRow', {
                            index: index,
                            row: {
                                num: row.skusNum
                            }
                        });
                    }
                    $.post("/inRepositoryDetail/getSkuHouseByNum", {
                        skuId: row.skuId,
                        num: row.num,
                        houseId: $("#houseId").val()
                    }, function (data) {
                        Part.dtable.datagrid('updateRow', {
                            index: index,
                            row: {
                                house: data
                            }
                        });
                    });
                }

                Part.dtable.datagrid('updateRow', {
                    index: index,
                    row: {
                        fee: Number(row.price) * Number(row.num)
                    }
                });
            }
        }
    });

    if($("#status").val() == 0) {
        Part.dtable.datagrid('enableCellEditing');
    }
};
Part.dg2 = function () {
    Part.dtable2 = $('#part_table2');
    Part.dtable2.datagrid({
        url: "/goodsPartDetail/getDetailList?partId=" + $("#id").val(),
        pagination: false,
        fitColumns: true,
        autoRowHeight: true,
        rownumbers: true,
        singleSelect: false,
        columns: [[
            {field: 'id', hidden: true},
            {field: 'positionId', hidden: true},//库位
            {field: 'skuId', checkbox: false, hidden:true},//规格id
            {field: 'goodsName', title: '商品名称', width: 150},
            {field: 'goodsId', title: '商品ID', width: 80, hidden: true},
            {field: 'skuName', title: '规格', width: 200},
            {field: 'code', title: '商品编号', width: 80},
            {
                field: 'unit', title: '单位', width: 80, formatter: function (value, row, index) {
                    for (var i = 0; i < Part.unitArr.length; i++) {
                        if (value == Part.unitArr[i].id) {
                            return Part.unitArr[i].text;
                        }
                    }
                    return value;
                }
            },
            {field: 'partsCode', title: '配件编号', width: 80},
            // {
            //     field: 'house',
            //     title: '<span style="color:blue">库位</span>',
            //     width: 150,
            //     editor: {type: 'house2'},
            //     formatter: function (value, row, index) {
            //         if ($yd.isEmpty(value)) {
            //             return "";
            //         }
            //         var arr = value.split("_");
            //         return "<a style='color:purple'>" + arr[1] + "：" + arr[3] + "</a>";
            //     }
            // },
            {
                field: 'price', title: '拆装单价', width: 80, formatter: function (value, row, index) {
                    return Number(value).toFixed(2);
                }
            },
            {
                field: 'num',
                title: '<span style="color:blue">拆装数量</span><span style="color:red">*</span>',
                width: 80,
                editor: {type: 'numberbox', options: {required: true, min: 1, precision: 0}}
            },
            {
                field: 'fee', title: '拆分总额', width: 80, formatter: function (value, row, index) {
                    return Number(value).toFixed(2);
                }
            }
        ]],
        onClickCell: function (index, field) {
            if($("#status").val() == 1) {
                return;
            }
            if (Part.editIndex2 != index) {
                if (Part.editIndex2 > -1) {
                    Part.dtable2.datagrid('endEdit', Part.editIndex2);
                }
                Part.editIndex2 = index;
            }
            if (field == "num" || field == "house") {
                Part.dtable2.datagrid('beginEdit', index);
            }
        },
        onEndEdit: function (index, row, changes) {
            if (changes) {
                Part.dtable2.datagrid('updateRow', {
                    index: index,
                    row: {
                        fee: Number(row.price) * Number(row.num)
                    }
                });
            }
        }
    });

    if($("#status").val() == 0) {
        Part.dtable2.datagrid('enableCellEditing');
    }
};
Part.formatDetail = function (value) {
    if ($yd.isEmpty(value)) {
        return "";
    }
    var result = "";
    var first = "";
    var arr = value.split(",");
    if (arr.length == 1) {
        return "<a style='color:purple' title='" + value + "'>" + value + "</a>";
    }
    for (var i = 0; i < arr.length; i++) {
        result += arr[i] + "\n";
        if (i == 0) {
            first = result;
        }
    }
    var html = "<a style='color:purple' title='" + result + "'>" + first + "..." + "</a>";

    return html;
};
Part.refresh = function () {
    window.location.reload();
};
Part.clear = function () {
    var rows = Part.dtable.datagrid("getRows");
    if(rows.length > 0) {
        Part.dtable.datagrid("deleteRow", 0);
    }
    rows = Part.dtable2.datagrid("getRows");
    while(rows.length > 0) {
        Part.dtable2.datagrid("deleteRow", 0);
        rows = Part.dtable2.datagrid("getRows");
    }
};
Part.changeTotalFee = function (newValue, oldValue) {
    if (newValue == "") {
        $("#totalFee").numberbox("setValue", 0);
        return;
    }
    if (Number(newValue) > Number($("#goodsFee").val())) {
        $yd.alert("应付总额不能大于商品总额！", function () {
            $("#totalFee").numberbox("setValue", $("#goodsFee").val());
        });
    } else {
        $("#discountFee").numberbox("setValue", Number($("#goodsFee").val()) - Number($("#totalFee").val()));
        if (Number(newValue) < Number($("#payFee").val())) {
            $yd.alert("本次付款金额不能大于应付总额！", function () {
                $("#payFee").numberbox("setValue", newValue);
                $("#payedFee").numberbox("setValue", newValue);
                $("#payingFee").numberbox("setValue", 0);
            });
        } else {
            $("#payedFee").numberbox("setValue", $("#payFee").val());
            $("#payingFee").numberbox("setValue", Number(newValue) - Number($("#payFee").val()));
        }
    }
};
Part.changePayFee = function (newValue, oldValue) {
    if (newValue == "") {
        $("#payFee").numberbox("setValue", 0);
        return;
    }
    if (Number(newValue) > Number($("#totalFee").val())) {
        $yd.alert("本次付款金额不能大于应付总额！", function () {
            $("#payFee").numberbox("setValue", $("#totalFee").val());
        });
    } else {
        $("#payedFee").numberbox("setValue", newValue);
        $("#payingFee").numberbox("setValue", Number($("#totalFee").val()) - Number(newValue));
    }
};
Part.plus = function () {
    var href = "/mMateriel/searchDg?isSingle=1&houseId=" + $("#houseId").val();
    // var $dg = $yd.win.create('商品（当前仓库中有库存的才会被查出）', href);
    var $dg = $yd.win.create('商品', href);
    $dg.width = "1050px;";
    $dg.height = "400px;";
    Part.isDg = 1;
    $dg.open(function (that, $win) {
            Part.winThat = that;
            Part.dgType = "goods";
        }, function (that, $win) {
            Part.saveDgGoods();
        }, function () {
            Part.isDg = 0;
            Part.winThat = undefined;
            Part.dgType = undefined;
            document.onkeydown = Part.keyDown;
        }
    );
};
Part.plus2 = function () {
    var href = "/mMateriel/searchDg?isSingle=1&bindHouseId=" + $("#houseId").val();
    // var $dg = $yd.win.create('商品（绑定当前仓库的才会被查出）', href);
    var $dg = $yd.win.create('商品', href);
    $dg.width = "1050px;";
    $dg.height = "400px;";
    Part.isDg = 1;
    $dg.open(function (that, $win) {
            Part.winThat = that;
            Part.dgType = "goods2";
        }, function (that, $win) {
            Part.saveDgGoods2();
        }, function () {
            Part.isDg = 0;
            Part.winThat = undefined;
            Part.dgType = undefined;
            document.onkeydown = Part.keyDown;
        }
    );
};
Part.isExist = function (rows, skuId) {
    for (var i = 0; i < rows.length; i++) {
        if (rows[i].skuId == skuId) {
            return true;
        }
    }
    return false;
};
Part.saveDgGoods = function () {
    document.onkeydown = Part.keyDown;

    var data = Goods.getChecked();
    if (data.length == 0) {
        Part.winThat.close();
        Part.isDg = 0;
        Part.winThat = undefined;
        Part.dgType = undefined;
    }

    var rows = Part.dtable.datagrid("getData").rows;
    if(rows.length > 0) {
        Part.dtable.datagrid("deleteRow", 0);
    }
    for (var i = 0; i < data.length; i++) {
        if (!Part.isExist(rows, data[i].skuId)) {
            var dt = data[i];
            //获取成本单价
            $.ajax({
                type: "POST",
                url: "/inRepositoryDetail/getSkuAvgCost2House",
                data: {
                    skuId: data[i].skuId,
                    houseId: $("#houseId").val(),
                },
                dataType: "json",
                success: function (result) {
                    Part.dtable.datagrid('appendRow', {
                        skuId: dt.skuId,
                        goodsName: dt.goodsName,
                        goodsId: dt.goodsId,
                        skus: dt.skusName,
                        code: dt.code,
                        partsCode: dt.partsCode,
                        unit: dt.unit,
                        skusNum: dt.skusNum,
                        price: Number(result.avgCost),
                        num: 1,
                        fee: Number(result.avgCost) * Number(1),
                        house: result.house,
                    }).datagrid('clearSelections');
                }
            });
        }
    }
    Part.winThat.close();
    Part.isDg = 0;
    Part.winThat = undefined;
    Part.dgType = undefined;
};
Part.saveDgGoods2 = function () {
    document.onkeydown = Part.keyDown;

    var data = Goods.getChecked();
    if (data.length == 0) {
        Part.winThat.close();
        Part.isDg = 0;
        Part.winThat = undefined;
        Part.dgType = undefined;
    }

    var rows = Part.dtable2.datagrid("getData").rows;
    for (var i = 0; i < data.length; i++) {
        if (!Part.isExist(rows, data[i].skuId) && !Part.isExist(Part.dtable.datagrid("getData").rows, data[i].skuId)) {
            var dt = data[i];
            $.ajax({
                type: "POST",
                url: "/inRepositoryDetail/getMaterielHouseByHouseId",
                data: {
                    materielId: dt.goodsId,
                    houseId: $("#houseId").val(),
                },
                dataType: "json",
                success: function (result) {
                    var positionId = $yd.isEmpty(result.house) ? 0 : result.house.split("_")[2];
                    Part.dtable2.datagrid('appendRow', {
                        skuId: dt.skuId,
                        goodsName: dt.goodsName,
                        goodsId: dt.goodsId,
                        skus: dt.skusName,
                        code: dt.code,
                        partsCode: dt.partsCode,
                        unit: dt.unit,
                        price: dt.buyPrice,
                        amountActual: dt.buyPrice,
                        num: 1,
                        fee: Number(dt.buyPrice) * Number(1),
                        house: result.house,
                        positionId: positionId
                    }).datagrid('clearSelections');
                }
            });
        }
    }
    Part.winThat.close();
    Part.isDg = 0;
    Part.winThat = undefined;
    Part.dgType = undefined;
};

Array.prototype.contains = function (needle) {
    for (var i in this) {
        if (this[i] == needle) return true;
    }
    return false;
};

//删除
Part.remove = function () {
    var rows = Part.dtable.datagrid("getRows");
    if(rows.length == 0) {
        $yd.alert("无记录");
        return;
    }
    Part.dtable.datagrid("deleteRow", 0);
};
Part.remove2 = function () {
    var sels = Part.dtable2.datagrid('getChecked')
    if (sels.length < 1) {
        $yd.alert('选中记录');
        return;
    }
    var ids = [];
    $.each(sels, function (i, n) {
        ids.push(n['skuId']);
    });

    $.each(ids, function (i, n) {
        var rows = Part.dtable2.datagrid('getRows');
        for (var ii = 0; ii < rows.length; ii++) {
            if (rows[ii].skuId == n) {
                Part.dtable2.datagrid('deleteRow', ii);
                break;
            }
        }
    });
};

Part.isEmpty = function (value) {
    if (value == undefined || value == null || value == "") {
        return true;
    }
    return false;
};
Part.save0 = function(){
    Part.save(0);
};
Part.save1 = function(){
    $yd.confirm("审核后将生成相应的出入库单，确认信息无误？", function () {
        Part.save(1);
    });
};
Part.save = function (type) {
    //结束编辑状态
    if (Part.editIndex != undefined && Part.editIndex != -1) {
        Part.dtable.datagrid('endEdit', Part.editIndex);
    }
    if (Part.editIndex2 != undefined && Part.editIndex2 != -1) {
        Part.dtable2.datagrid('endEdit', Part.editIndex2);
    }

    $('#myForm').form('submit', {
        url: Part.uEdit + "?type=" + type,
        onSubmit: function () {
            var isValid = $(this).form('validate');//只能校验处于编辑状态的栏位
            if (!isValid) {
                return isValid;
            }

            //校验数据
            var rows = Part.dtable.datagrid("getRows");
            if (rows.length == 0) {
                $yd.alert("请添加拆分商品");
                return false;
            }

            // for (var i = 0; i < rows.length; i++) {
            //     var row = rows[i];
            //     var house = row.house;
            //     if (Part.isEmpty(house)) {
            //         $yd.alert("拆分商品未配置库位");
            //         return false;
            //     }
            // }

            var rows2 = Part.dtable2.datagrid("getRows");
            if (rows2.length == 0) {
                $yd.alert("请添加拆分后商品");
                return false;
            }

            //封装数据
            $("#goodsFrom").val(JSON.stringify(rows[0]));
            $("#goodsTo").val(JSON.stringify(rows2));
            return isValid;
        },
        success: function (data) {
            var d = $.parseJSON(data);
            if (d.code == 0) {
                $yd.alert("操作成功", function () {
                    window.location.reload();
                });
            } else {
                $yd.alert(d.message);
            }
        }
    });
};
// 处理按钮
Part.btn = function () {
    var sb = new $yd.SearchBtn($(Part.obtn));
    sb.create('repeat red', '重置（Shift+R）', Part.refresh);
    if($("#status").val() == 0) {
        sb.create('edit', '审核(Shift+V)', Part.save1);
        sb.create('save green', '保存（Shift+S）', Part.save0);
    }
};
Part.keyDown = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (!e) {
        return;
    }
    // console.log("add.js")
};
Part.keyUp = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (!e) {
        return;
    }
    if (Part.isDg == 0) {
        // if (e.keyCode == 45) {//insert
        //     Part.plus2();
        // }
        // if (e.keyCode == 46) {//del
        //     Part.remove2();
        // }
        if (e.shiftKey && e.keyCode == 83) {
            Part.save(0);//生单: Shift + S
        }
        if (e.shiftKey && e.keyCode == 86) {
            Part.save1();//生单: Shift + V
        }
        if (e.shiftKey && e.keyCode == 82) {
            Part.refresh();
        }
    } else {
        if (e.keyCode == 13) {
            if(Part.dgType == "goods") {
                Part.saveDgGoods();
            } else if(Part.dgType == "goods2") {
                Part.saveDgGoods2();
            } else if(Part.dgType == "house") {
                Part.saveDgHouse();
            } else if(Part.dgType == "house2") {
                Part.saveDgHouse2();
            }
        } else if (e.keyCode == 27 && Part.winThat) {//ESC
            Part.winThat.close();
        }
    }
};
Part.init = function () {
    $.ajax({
        type: "POST",
        url: "/sysDict/materiel_MU",
        async:false,
        dataType: "json",
        success: function (data) {
            Part.unitArr = data;
        }
    });

    $("#houseId").combobox({
        prompt:'仓库',
        url: "/wWearhouse/list",
        panelHeight:'auto',
        editable:false,
        required:true,
        valueField:'id',
        textField:'name',
        onChange: function (newValue, oldValue) {
            var rows1 = Part.dtable.datagrid("getRows");
            var rows2 = Part.dtable2.datagrid("getRows");
            if(rows1.length == 0 && rows2.length == 0) {
                return;
            }
            if(Part.isConfirm == 0) {
                return;
            }
            if(!$yd.isEmpty(oldValue)) {
                $.messager.confirm({
                    title: '确认框',
                    border: false,
                    top: 0,
                    msg: "切换仓库清除当前仓库已添加的商品，确定切换？",
                    fn: function (r) {
                        if (r) {
                            Part.clear();
                        } else {
                            Part.isConfirm = 0;
                            $("#houseId").combobox("setValue", oldValue);
                            Part.isConfirm = 1;
                        }
                    }
                })
            } else {
                Part.clear();
            }
        }
    });

    //鼠标点击页面任何地方，表格编辑结束编辑状态
    document.onclick = Part.mouseClick;

    //快捷键
    document.onkeydown = Part.keyDown;
    document.onkeyup = Part.keyUp;

    setTimeout("$('#orderTime').focus()", 500);
    $("a").attr("tabindex", -1);
    $(":checkbox").attr("tabindex", -1);
};
Part.mouseClick = function (e) {
    e = window.event || e; // 兼容IE7
    var obj = $(e.srcElement || e.target);

    if (Part.isDg == 0 && !$(obj).hasClass("datagrid-cell") && !$(obj).hasClass("datagrid-row-over")) {
        if (Part.editIndex != -1) {
            Part.dtable.datagrid('endEdit', Part.editIndex);
            Part.editIndex = -1;
        }
    }
};
$(function () {
    Part.obtn = '#part_btn';
    Part.dg(); // 数据表格
    Part.dg2(); // 数据表格
    Part.btn(); // 初始化按钮
    Part.init(); // 数据表格
});