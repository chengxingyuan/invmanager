<!-- 商品查询 -->
var Goods = {
    u: '/mMateriel',
    uList: '/mMateriel/search',
    searchText: "",
    searchCar: "",
};
Goods.search = function() {
    Goods.dtable.datagrid('load', {
        searchText: $("#searchText").val(),
        searchCar: $("#searchCar").val(),
        houseId: $("#hidHouseId").val(),
        bindHouseId: $("#hidBindHouseId").val()
    });
};
Goods.getChecked = function() {
    return Goods.dtable.datagrid("getSelections");
};
Goods.dg = function () {
    Goods.dtable = $('#goodsTb');
    var grid = new $yd.DataGrid(Goods.dtable);
    grid.checkOnSelect = true; // 选择行不选中复选框
    grid.url = Goods.uList;

    var clm = {field: 'skuId', checkbox: true};
    if(Goods.isSingle == 1) {
        grid.singleSelect = true;
        clm = {field: 'skuId', checkbox: false, hidden: true};
    }
    grid.queryParams = {
        houseId: $("#hidHouseId").val(),
        bindHouseId: $("#hidBindHouseId").val()
    };
    grid.columns = [[
        clm
        , {field: 'goodsName', title: '商品名称', width: 100}
        , {field: 'skusName', title: '规格', width: 100}
        , {field: 'code', title: '商品编码', width: 100}
        , {field: 'partsCode', title: '配件编码', width: 100}
        , {field: 'car', title: '适用车型', width: 100, formatter: function (value, row, index) {
            return $yd.formatDetail(value);
        }}
        , {field: 'skusNum', title: '库存数量', width: 100}
        // , {field: 'code', title: '规格编码', hidden: true}
        , {field: 'buyPrice', title: '进价', hidden: true}
        , {field: 'sellPrice', title: '售价', hidden: true}
        , {field: 'goodsId', title: '商品id', hidden: true}
        , {field: 'unit', title: '商品单位', hidden: true}
        , {field: 'ralativeId', title: '供应商ID', hidden: true}
        , {field: 'ralativeName', title: '供应商名称', hidden: true}
        , {field: 'wearhouseId', title: '仓库id', hidden: true}
        , {field: 'wearhouseName', title: '仓库名称', hidden: true}
        , {field: 'positionId', title: '库位id', hidden: true}
        , {field: 'positionName', title: '库位名称', hidden: true}
    ]];
    grid.loadGrid();
};

Goods.keydown = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (!e) {
        return;
    }
    switch (e.keyCode) {
        case 9: //tab
            if (e.preventDefault) {
                e.preventDefault();
            }
            else {
                e.returnValue = false;
            }
        case 32:
            GoodsTree.space();
            break;
        case 37:
            GoodsTree.left();
            break;
        case 38:
            GoodsTree.up();
            break;
        case 39:
            GoodsTree.right();
            break;
        case 40:
            GoodsTree.down();
            break;
    }
};

Goods.init = function () {
    /*
    $("#searchText").keyup(function () {
        searchGoods();
    });
    $("#searchCar").keyup(function () {
        searchGoods();
    });

    function searchGoods () {
        var content = $("#searchText").val();
        var car = $("#searchCar").val();
        if (car == Goods.searchCar && content == Goods.searchText) {
            return;
        }
        Goods.searchText = content;
        Goods.searchCar = car;
        Goods.search();
    }*/

    //快捷键
    // document.onkeydown = Goods.keydown;

    $("#aCar").click(function () {
        var href = "/carBrand/tree?isCheck=0";
        var $dg = $yd.win.create('车型', href);
        $dg.width = "450px;";
        $dg.height = "520px;";
        $dg.open(function (that, $win) {
            }, function (that, $win) {
                var result = CarTree.getChecked();
                var tmp = "";
                var carBrandId = result.length == 0 ? 0 : result[result.length-1].id;
                for(var i=0; i<result.length; i++) {
                    tmp += " -> " + result[i].name;
                }
                if(tmp != "") {
                    tmp = tmp.substr(4);
                }
                $("#car").val(tmp);
                $("#carBrandId").val(carBrandId);
                that.close();
                Goods.search();
            }
        );
    });
};

$(function () {
    Goods.isSingle = $("#isSingle").val();
    Goods.dg(); // 数据表格
    Goods.init(); // 初始化
    $("#searchButton").click(Goods.search);
    // $("#searchText").bind('input',function () {
    //     Goods.search();
    // });
    // $('#searchText').textbox('textbox').bind('click', function() {
    //     alert(1);
    // });
    // $('input', $('#searchText').next('span')).click(function() {
    // alert(3);
    // });

});

function checkInput() {
    Goods.search();
}