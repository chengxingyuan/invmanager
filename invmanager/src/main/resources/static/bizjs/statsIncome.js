var Income = {
    uList: '/stats/incomeList'
};

Income.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    // grid.onDblClickRow = function (index,row) {
    //     Income._edit(row['id']);
    // };
    grid.nowarp = false;
    grid.url = Income.uList;
    grid.rownumbers = true;
    grid.columns = [[
        {field: 'goodsName', title: '商品名称', width: 80}
        , {field: 'skuName', title: '规格属性', width: 80}
        , {field: 'totalNum', title: '销售量', width: 80}
        , {field: 'totalFee', title: '销售总额', width: 80, formatter:function (value) {
                return Number(value).toFixed(2);
            }}
    ]];
    grid.onLoadSuccess = function(data) {
        //汇总
        $.post("/stats/getSumInfo2",$("#mSales_form").serialize(),function (data) {
            $("#totalFee").textbox("setValue", data.totalFee);
            $("#totalNum").textbox("setValue", data.totalNum);
        });
    };
    grid.loadGrid();
};
//重置
Income.reset = function () {
    $(".search-form").form('clear');
    Income.search();
}
// 处理按钮
Income.btn = function () {
    var sb = new $yd.SearchBtn($(Income.obtn));
    sb.create('repeat red', '重置', Income.reset);
    // sb.create('refresh', '刷新', Income.refresh);
    // sb.create('search', '查询', Income.search);
};
Income._edit = function (id) {
    parent.inv.OpenPage("/mSales/edit?id=" + id, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "                                <span class=\"menu-text\"> 销售单详情 </span>\n" +
        "                                <b class=\"arrow \"></b>",true)
};
Income.refresh = function () {
    $('#mSales_table').datagrid('unselectAll');
    Income.dtable.datagrid('reload');
};
//搜索
Income.search = function () {
    Income.dtable.datagrid('load', Income.sform.serializeObject());
};

$(function () {
    Income.sform = $('#mSales_form');
    Income.dtable = $('#mSales_table');
    Income.obtn = '#mSales_btn';

    Income.dg(); // 数据表格
    Income.btn(); // 初始化按钮
    $yd.bindEnter(Income.search);
});
var btn1 = document.getElementById("searchButton");
btn1.addEventListener("click", function() {
    $('#mSales_table').datagrid('unselectAll');
    Income.dtable.datagrid('load', Income.sform.serializeObject());
});