var Busz = {
    uList: '/stats/buszList?orderStatus=2'
};

Date.prototype.format = function (format) {
    var o = {
        "M+": this.getMonth() + 1, //month
        "d+": this.getDate(),    //day
        "h+": this.getHours(),   //hour
        "m+": this.getMinutes(), //minute
        "s+": this.getSeconds(), //second
        "q+": Math.floor((this.getMonth() + 3) / 3),  //quarter
        "S": this.getMilliseconds() //millisecond
    }
    if (/(y+)/.test(format)) format = format.replace(RegExp.$1,
        (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o) if (new RegExp("(" + k + ")").test(format))
        format = format.replace(RegExp.$1,
            RegExp.$1.length == 1 ? o[k] :
                ("00" + o[k]).substr(("" + o[k]).length));
    return format;
};
Busz.isEmpty = function (value) {
    if (value == undefined || value == null || value == "") {
        return true;
    }
    return false;
}

Busz.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index, row) {
        Busz._edit(row['id']);
    };
    grid.nowarp = false;
    grid.url = Busz.uList;
    grid.rownumbers = true;
    grid.columns = [[
        {field: 'id', checkbox: false, hidden: true}
        , {field: 'code', title: '订单编号', width: 80}
        , {
            field: 'orderTime', title: '订单日期', width: 80, formatter: function (value) {
                return new Date(value).format('yyyy-MM-dd');
            }
        }
        , {
            field: 'totalFee', title: '订单总额', width: 80, formatter: function (value, row, index) {
                return Number(value).toFixed(2);
            }
        }
        , {field: 'username', title: '操作人', width: 80}
    ]];
    grid.onLoadSuccess = function () {
        //汇总
        $.post("/stats/getSumInfo?orderStatus=2", $("#mSales_form").serialize(), function (data) {
            $("#totalFee").textbox("setValue", Number(data.totalFee).toFixed(2));
            $("#totalNum").textbox("setValue", data.totalNum);
        });
    };
    grid.loadGrid();
};
Busz.dg.getOne = function () {
    var sels = Busz.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
Busz.dg.getSels = function () {
    return Busz.dtable.datagrid('getSelections');
};
//重置
Busz.reset = function () {
    $(".search-form").form('clear');
    Busz.search();
};
// 处理按钮
Busz.btn = function () {
    var sb = new $yd.SearchBtn($(Busz.obtn));
    // sb.create('remove', '', Busz.remove);
    // sb.create('remove', '发货', Busz.remove);
    sb.create('repeat red', '重置', Busz.reset);
    // sb.create('refresh', '刷新', Busz.refresh);
    // sb.create('edit', '编辑', Busz.edit);
    // sb.create('plus', '新增', Busz.plus);
    // sb.create('search', '查询', Busz.search);
};
//删除
Busz.remove = function () {
    var sels = Busz.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(Busz.uDel, {ids: ids}, function () {
            Busz.refresh();
        });
    })
};
//
Busz.edit = function () {
    var sel = Busz.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    Busz._edit(sel['id'])
};
// 表单预处理
Busz.preForm = function () {
    new $yd.combobox("customerId").load("/customer/select");
};
Busz._edit = function (id) {
    // $yd.win.create('编辑', Busz.uEdit + '?id=' + id).big().open(function (that, $win) {
    //         Busz.preForm();
    //         $yd.get(Busz.u + "/" + id, function (data) {
    //             $win.find('form').form('load', data);
    //         })
    //     }, function (that, $win) {
    //         $yd.form.create($win.find('form')).url(Busz.u).submit(function () {
    //             that.close();
    //             Busz.refresh();
    //         });
    //     }
    // );
    parent.inv.OpenPage("/mSales/edit?id=" + id, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "                                <span class=\"menu-text\"> 销售单详情 </span>\n" +
        "                                <b class=\"arrow \"></b>", true)
};
//新增
Busz.plus = function () {
    parent.inv.OpenPage("/mSales/addSales", "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "                                <span class=\"menu-text\"> 新增销售单 </span>\n" +
        "                                <b class=\"arrow \"></b>", true)

};
//
Busz.refresh = function () {
    $('#mSales_table').datagrid('unselectAll');
    Busz.dtable.datagrid('reload');
};
//搜索
Busz.search = function () {
    Busz.dtable.datagrid('load', Busz.sform.serializeObject());
};

$(function () {
    Busz.sform = $('#mSales_form');
    Busz.dtable = $('#mSales_table');
    Busz.obtn = '#mSales_btn';

    Busz.dg(); // 数据表格
    Busz.btn(); // 初始化按钮
    $yd.bindEnter(Busz.search);
    $("#searchButton").click(Busz.search);
});