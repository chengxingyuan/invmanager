<!-- bybs_detail.ftl 报溢报损详情 -->
var BybsDetail = {
    u: '/bybsDetail'
    , uList: '/bybsDetail/list'
    , uEdit: '/bybsDetail/edit'
    , uDel: '/bybsDetail/delete'
};

BybsDetail.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        BybsDetail._edit(row['id']);
    };
    grid.url = BybsDetail.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'bybsId', title: '总单id', width: 80}
        , {field: 'wearhouseId', title: '仓库', width: 80}
        , {field: 'positionId', title: '库位id', width: 80}
        , {field: 'skuId', title: '商品skuid', width: 80}
        , {field: 'batchNumber', title: '批号', width: 80}
        , {field: 'num', title: '数量', width: 80}
        , {field: 'price', title: '成本单价', width: 80}
        , {field: 'fee', title: '总额', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'username', title: '没用到', width: 80}
        , {field: 'remark', title: '备注', width: 80}
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
BybsDetail.dg.getOne = function () {
    var sels = BybsDetail.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
BybsDetail.dg.getSels = function () {
    return BybsDetail.dtable.datagrid('getSelections');
};
// 处理按钮
BybsDetail.btn = function () {
    var sb = new $yd.SearchBtn($(BybsDetail.obtn));
    sb.create('remove', '删除', BybsDetail.remove);
    sb.create('refresh', '刷新', BybsDetail.refresh);
    sb.create('edit', '编辑', BybsDetail.edit);
    sb.create('plus', '新增', BybsDetail.plus);
    sb.create('search', '查询', BybsDetail.search);
};
//删除
BybsDetail.remove = function () {
    var sels = BybsDetail.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(BybsDetail.uDel, {ids: ids}, function () {
            BybsDetail.refresh();
        });
    })
};
//
BybsDetail.edit = function () {
    var sel = BybsDetail.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    BybsDetail._edit(sel['id'])
};
// 表单预处理
BybsDetail.preForm = function () {
    new $yd.combobox("bybsId").load("/bybs/select");
    new $yd.combobox("wearhouseId").load("/wearhouse/select");
    new $yd.combobox("positionId").load("/position/select");
    new $yd.combobox("skuId").load("/sku/select");
};
BybsDetail._edit = function (id) {
    $yd.win.create('编辑报溢报损详情', BybsDetail.uEdit + '?id=' + id).open(function (that, $win) {
            BybsDetail.preForm();
            $yd.get(BybsDetail.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(BybsDetail.u).submit(function () {
                that.close();
                BybsDetail.refresh();
            });
        }
    );
};
//新增
BybsDetail.plus = function () {
    var sel = BybsDetail.dg.getOne();
    var href = BybsDetail.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增报溢报损详情', href).open(function (that, $win){
            BybsDetail.preForm();
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(BybsDetail.u).submit(function () {
                that.close();
                BybsDetail.refresh();
            });
        }
    );
};
//
BybsDetail.refresh = function () {
    BybsDetail.dtable.datagrid('reload');
};
//搜索
BybsDetail.search = function () {
    BybsDetail.dtable.datagrid('load', BybsDetail.sform.serializeObject());
};

$(function () {
    BybsDetail.sform = $('#bybsDetail_form');
    BybsDetail.dtable = $('#bybsDetail_table');
    BybsDetail.obtn = '#bybsDetail_btn';

    BybsDetail.dg(); // 数据表格
    BybsDetail.btn(); // 初始化按钮
    $yd.bindEnter(BybsDetail.search);
});