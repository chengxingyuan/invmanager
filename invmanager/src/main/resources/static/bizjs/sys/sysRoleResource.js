<!-- sys_role_resource.ftl 角色资源表 -->
var SysRoleResource = {
    u: '/sysRoleResource'
    , uList: '/sysRoleResource/list'
    , uEdit: '/sysRoleResource/edit'
    , uDel: '/sysRoleResource/delete'
};

SysRoleResource.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        SysRoleResource._edit(row['id']);
    };
    grid.url = SysRoleResource.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'roleId', title: 'roleId', width: 80}
        , {field: 'resourceId', title: 'resourceId', width: 80}
        , {field: 'resourcePer', title: 'resourcePer', width: 80}
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
SysRoleResource.dg.getOne = function () {
    var sels = SysRoleResource.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
SysRoleResource.dg.getSels = function () {
    return SysRoleResource.dtable.datagrid('getSelections');
};
// 处理按钮
SysRoleResource.btn = function () {
    var sb = new $yd.SearchBtn($(SysRoleResource.obtn));
    sb.create('remove', '删除', SysRoleResource.remove);
    sb.create('refresh', '刷新', SysRoleResource.refresh);
    sb.create('edit', '编辑', SysRoleResource.edit);
    sb.create('plus', '新增', SysRoleResource.plus);
    sb.create('search', '查询', SysRoleResource.search);
};
//删除
SysRoleResource.remove = function () {
    var sels = SysRoleResource.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(SysRoleResource.uDel, {ids: ids}, function () {
            SysRoleResource.refresh();
        });
    })
};
//
SysRoleResource.edit = function () {
    var sel = SysRoleResource.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    SysRoleResource._edit(sel['id'])
};
SysRoleResource._edit = function (id) {
    $yd.win.create('编辑角色资源表', SysRoleResource.uEdit + '?id=' + id).open(function (that, $win) {
            $yd.get(SysRoleResource.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysRoleResource.u).submit(function () {
                that.close();
                SysRoleResource.refresh();
            });
        }
    );
};
//新增
SysRoleResource.plus = function () {
    var sel = SysRoleResource.dg.getOne();
    var href = SysRoleResource.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name']
    $yd.win.create('新增角色资源表', href).open(null, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysRoleResource.u).submit(function () {
                that.close();
                SysRoleResource.refresh();
            });
        }
    );
};
//
SysRoleResource.refresh = function () {
    SysRoleResource.dtable.datagrid('reload');
};
//搜索
SysRoleResource.search = function () {
    SysRoleResource.dtable.datagrid('load', SysRoleResource.sform.serializeObject());
};

$(function () {
    SysRoleResource.sform = $('#sysRoleResource_form');
    SysRoleResource.dtable = $('#sysRoleResource_table');
    SysRoleResource.obtn = '#sysRoleResource_btn';

    SysRoleResource.dg(); // 数据表格
    SysRoleResource.btn(); // 初始化按钮
    $yd.bindEnter(SysRoleResource.search);
});