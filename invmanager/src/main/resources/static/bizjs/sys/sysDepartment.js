<!-- sys_department.ftl 部门表 -->
var SysDepartment = {
    u: '/sysDepartment'
    , uList: '/sysDepartment/list'
    , uEdit: '/sysDepartment/edit'
    , uDel: '/sysDepartment/delete'
};

SysDepartment.dg = function () {
    var grid = new $yd.TreeGrid(this.dtable);
    grid.onDblClickRow = function (row) {
        SysDepartment._edit(row['id']);
    };
    grid.onClickRow = function (row) {
        SysUser.dtable.datagrid('load', {deptId: row.id});
    };
    grid.toolbar = SysDepartment.obtn;
    grid.url = SysDepartment.uList;
    grid.pagination = false;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'name', title: '部门名称', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
    ]];
    grid.loadGrid();
};
SysDepartment.dg.getOne = function () {
    var sels = SysDepartment.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
SysDepartment.dg.getSels = function () {
    return SysDepartment.dtable.treegrid('getSelections');
};
// 处理按钮
SysDepartment.btn = function () {
    var sb = new $yd.SearchBtn($(SysDepartment.obtn));
    sb.create('remove', '删除', SysDepartment.remove);
    sb.create('edit', '编辑', SysDepartment.edit);
    sb.create('plus', '新增', SysDepartment.plus);
};
//删除
SysDepartment.remove = function () {
    var sels = SysDepartment.dg.getSels();
    if (!sels ||sels.length <1) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(SysDepartment.uDel, {ids: ids}, function () {
            SysDepartment.refresh();
        });
    })
};
//
SysDepartment.edit = function () {
    var sel = SysDepartment.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    SysDepartment._edit(sel['id'])
};
SysDepartment._edit = function (id) {
    $yd.win.create('编辑部门表', SysDepartment.uEdit + '?id=' + id).open(function (that, $win) {
            $yd.get(SysDepartment.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysDepartment.u).submit(function () {
                that.close();
                SysDepartment.refresh();
            });
        }
    );
};
//新增
SysDepartment.plus = function () {
    var sel = SysDepartment.dg.getOne();
    var href = SysDepartment.uEdit;
    // if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name']
    $yd.win.create('新增部门表', href).open(null, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysDepartment.u).submit(function () {
                that.close();
                SysDepartment.refresh();
            });
        }
    );
};
//
SysDepartment.refresh = function () {
    SysDepartment.dtable.treegrid('reload');
};

$(function () {
    SysDepartment.sform = $('#sysDepartment_form');
    SysDepartment.dtable = $('#sysDepartment_table');
    SysDepartment.obtn = '#sysDepartment_btn';

    SysDepartment.dg(); // 数据表格
    SysDepartment.btn(); // 初始化按钮
});