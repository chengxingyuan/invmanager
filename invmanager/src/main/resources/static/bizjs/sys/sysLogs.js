<!-- sys_logs.ftl 请求日志 -->
var SysLogs = {
    u: '/sysLogs'
    , uList: '/sysLogs/list'
    , uEdit: '/sysLogs/edit'
    , uDel: '/sysLogs/delete'
};

SysLogs.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.url = SysLogs.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'name', title: '用户名', width: 80}
        , {field: 'rargs', title: '请求参数', width: 80}
        , {field: 'ruri', title: '请求地址', width: 80}
        , {field: 'desc', title: '请求描述', width: 80}
        , {field: 'rmethod', title: '请求方法', width: 80}
        , {field: 'ipAddr', title: '请求ip', width: 80}
        , {field: 'statuCode', title: '请求码', width: 80}
        , {field: 'sessionId', title: '请求标示', width: 80}
        , {field: 'rtime', title: '请求耗时', width: 80}
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
SysLogs.dg.getOne = function () {
    var sels = SysLogs.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
SysLogs.dg.getSels = function () {
    return SysLogs.dtable.datagrid('getSelections');
};
// 处理按钮
SysLogs.btn = function () {
    var sb = new $yd.SearchBtn($(SysLogs.obtn));
    sb.create('search', '查询', SysLogs.search);
};
//
SysLogs.refresh = function () {
    SysLogs.dtable.datagrid('reload');
};
//搜索
SysLogs.search = function () {
    SysLogs.dtable.datagrid('load', SysLogs.sform.serializeObject());
};

$(function () {
    SysLogs.sform = $('#sysLogs_form');
    SysLogs.dtable = $('#sysLogs_table');
    SysLogs.obtn = '#sysLogs_btn';

    SysLogs.dg(); // 数据表格
    SysLogs.btn(); // 初始化按钮
    $yd.bindEnter(SysLogs.search);
});