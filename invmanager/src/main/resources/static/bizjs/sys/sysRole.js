<!-- sys_role.ftl 角色表 -->
var SysRole = {
    u: '/sysRole'
    , uList: '/sysRole/list'
    , uEdit: '/sysRole/edit'
    , uDel: '/sysRole/delete'
    , uResTree: '/sysResource/tree'
    , uResource: '/sysRole/resource'
};

SysRole.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index, row) {
        SysRole._edit(row['id']);
    };
    grid.onClickRow = function (index, row) {
        SysRole.loadResources(row);
    };
    grid.url = SysRole.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'name', title: '角色名称', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: '创建时间', width: 80}
    ]];
    grid.loadGrid();
};
SysRole.dg.getOne = function () {
    var sels = SysRole.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
SysRole.dg.getSels = function () {
    return SysRole.dtable.datagrid('getSelections');
};
// 处理按钮
SysRole.btn = function () {
    var sb = new $yd.SearchBtn($(SysRole.obtn));
    sb.create('remove', '删除', SysRole.remove);
    sb.create('refresh', '刷新', SysRole.refresh);
    sb.create('edit', '编辑', SysRole.edit);
    sb.create('plus', '新增', SysRole.plus);
    // sb.create('search', '查询', SysRole.search);
};
//删除
SysRole.remove = function () {
    var sels = SysRole.dg.getSels();
    if (!sels || sels.length < 1) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(SysRole.uDel, {ids: ids}, function () {
            SysRole.refresh();
        });
    })
};
//
SysRole.edit = function () {
    var sel = SysRole.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    SysRole._edit(sel['id'])
};
SysRole._edit = function (id) {
    $yd.win.create('编辑角色表', SysRole.uEdit + '?id=' + id).open(function (that, $win) {
            $yd.get(SysRole.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysRole.u).submit(function () {
                that.close();
                SysRole.refresh();
            });
        }
    );
};
//新增
SysRole.plus = function () {
    var sel = SysRole.dg.getOne();
    var href = SysRole.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增角色表', href).open(function () {

        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysRole.u).submit(function () {
                that.close();
                SysRole.refresh();
            });
        }
    );
};
//
SysRole.refresh = function () {
    $('#SysRole_table').datagrid('unselectAll');
    SysRole.dtable.datagrid('reload');
};
//搜索
SysRole.search = function () {
    $('#SysRole_table').datagrid('unselectAll');
    SysRole.dtable.datagrid('load', SysRole.sform.serializeObject());
};

//加载权限
SysRole.loadResources = function (row) {
    SysRole.roleSelected = row['id'];
    SysRole.rtree.tree('options').queryParams.roleId = row['id'];
    SysRole.rtree.tree('reload');
};

SysRole.role = function () {
    SysRole.rtree.tree({
        checkbox: true,
        url: SysRole.uResTree,
        method: 'get',
        onBeforeLoad: function () {
            $(this).tree('options').cascadeCheck = false;
        },
        onLoadSuccess: function () {
            $(this).tree('options').cascadeCheck = true;
        },
        onClick: function (node) {
            var o = node.checkState == 'checked' ? 'uncheck' : 'check';
            $(this).tree(o, node.target);
        },
        onCheck: function (node, checked) {
            if (SysRole.roleSelected) {
                var list = [];
                ilist(node);
                recu('getChildren', node);
                recu('getParent', node);
                $yd.post(SysRole.uResource, JSON.stringify(list));
            }

            function ilist(n) {
                var j = {roleId: SysRole.roleSelected, resourceId: n['id'], resourcePer: n['permission']};
                j.id = n.checkState === 'unchecked' ? -1 : 0;

                // console.info(n.checkState);
                // console.info(j.id);
                list.push(j);
            }

            function recu(opt, node) {
                var pp = SysRole.rtree.tree(opt, node.target);
                if (pp) {
                    if (Array.isArray(pp)) {
                        $.each(pp, function (i, n) {
                            ilist(n);
                            recu(opt, n);
                        })
                    } else {
                        ilist(pp);
                        recu(opt, pp);
                    }
                }
            }
        }
    }).removeClass('hide');
};

$(function () {
    SysRole.sform = $('#sysRole_form');
    SysRole.dtable = $('#sysRole_table');
    SysRole.obtn = '#sysRole_btn';
    SysRole.rtree = $('#role_tree');

    SysRole.btn(); // 初始化按钮
    SysRole.dg(); // 数据表格
    SysRole.role();
    $yd.bindEnter(SysRole.search);
});
var btn1 = document.getElementById("searchButton");
btn1.addEventListener("click", function() {
    $('#sysRole_table').datagrid('unselectAll');
    SysRole.dtable.datagrid('load', SysRole.sform.serializeObject());
});