<!-- sys_tenant.ftl 租户 -->
var SysTenant = {
    u: '/sysTenant'
    , uList: '/sysTenant/list'
    , uEdit: '/sysTenant/edit'
    , uDel: '/sysTenant/delete'
    , relativeUrl: '/sysTenant/isNeedRelative'

};

SysTenant.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index, row) {
        SysTenant._edit(row['id']);
    };
    grid.onClickRow=function (rowIndex, rowData) {
        $(this).datagrid('unselectRow', rowIndex);
    }
    grid.url = SysTenant.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'name', title: '租户名称', width: 80}
        , {field: 'memo', title: '描述', width: 80}
        , {field: 'addr', title: '详细地址', width: 80}
        , {field: 'mobile', title: '手机号', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: '创建时间', width: 80}
        , {field: 'needRelativeInv', title: '是否需要关联库存', width: 80,formatter:function (value,row) {
            var idStr = row.id;
            if (value == 0) {
                return '<button onmouseover="this.style.backgroundColor=\'#E39300\'" onmouseout="this.style.backgroundColor=\'#FF3300\'" style="width:82px;background-color: #FF3300;border-radius:2px;color:white;border: 0px;" href="#" onclick="SysTenant.needRelative('+idStr+','+1+')">是</button>&nbsp;&nbsp;' ;
            }
            if (value ==1) {
                return '<button onmouseover="this.style.backgroundColor=\'#E39300\'" onmouseout="this.style.backgroundColor=\'grey\'" style="width:82px;background-color: grey;border-radius:2px;color:white;border: 0px;" href="#" onclick="SysTenant.needRelative('+idStr+','+0+')">否</button>&nbsp;&nbsp;' ;
            }
        }}
    ]];
    grid.loadGrid();
};
SysTenant.dg.getOne = function () {
    var sels = SysTenant.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
SysTenant.dg.getSels = function () {
    return SysTenant.dtable.datagrid('getSelections');
};
// 处理按钮
SysTenant.btn = function () {
    var sb = new $yd.SearchBtn($(SysTenant.obtn));
    sb.create('remove', '删除', SysTenant.remove);
    sb.create('refresh', '刷新', SysTenant.refresh);
    sb.create('edit', '编辑', SysTenant.edit);
    sb.create('plus', '新增', SysTenant.plus);
    sb.create('search', '查询', SysTenant.search);
};
//删除
SysTenant.remove = function () {
    var sels = SysTenant.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(SysTenant.uDel, {ids: ids}, function () {
            SysTenant.refresh();
        });
    })
};
//是否需要关联库存
SysTenant.needRelative = function (id,status) {
    var msg = "";
    if (status == 1) {
        msg = '确认不需要关联库存？';
    }else{
        msg = '确认需要关联库存？';
    }

    $yd.confirm(msg,function () {
        $yd.post(SysTenant.relativeUrl, {id: id,status:status}, function () {
            SysTenant.refresh();
        });
    })
}
//
SysTenant.edit = function () {
    var sel = SysTenant.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    SysTenant._edit(sel['id'])
};
SysTenant._edit = function (id) {
    $yd.win.create('编辑租户', SysTenant.uEdit + '?id=' + id).open(function (that, $win) {
            $yd.get(SysTenant.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysTenant.u).submit(function () {
                that.close();
                SysTenant.refresh();
            });
        }
    );
};
//新增
SysTenant.plus = function () {
    var sel = SysTenant.dg.getOne();
    var href = SysTenant.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name']
    $yd.win.create('新增租户', href).open(null, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysTenant.u).submit(function () {
                that.close();
                SysTenant.refresh();
            });
        }
    );
};
//
SysTenant.refresh = function () {
    SysTenant.dtable.datagrid('reload');
};
//搜索
SysTenant.search = function () {
    SysTenant.dtable.datagrid('load', SysTenant.sform.serializeObject());
};

$(function () {
    SysTenant.sform = $('#sysTenant_form');
    SysTenant.dtable = $('#sysTenant_table');
    SysTenant.obtn = '#sysTenant_btn';

    SysTenant.dg(); // 数据表格
    SysTenant.btn(); // 初始化按钮
    $yd.bindEnter(SysTenant.search);
});