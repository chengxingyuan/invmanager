<!-- sys_resource.ftl 资源管理 -->
var SysResource = {
    u: '/sysResource'
    , uList: '/sysResource/list'
    , uEdit: '/sysResource/edit'
    , uDel: '/sysResource/delete'
};

SysResource.dg = function () {
    var grid = new $yd.TreeGrid(SysResource.dtable);
    grid.onDblClickRow = function (row) {
        SysResource._edit(row['id']);
    };
    grid.url = SysResource.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'name', title: '资源名称', width: 80}
        , {field: 'src', title: '路径', width: 80}
        , {field: 'permission', title: '权限', width: 80}
        , {field: 'icon', title: '图标', width: 80}
        , {field: 'sort', title: '排序', width: 80}
        , {field: 'type', title: '资源类型', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
SysResource.dg.getOne = function () {
    var sels = SysResource.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
SysResource.dg.getSels = function () {
    return SysResource.dtable.treegrid('getSelections');
};
// 处理按钮
SysResource.btn = function () {
    var sb = new $yd.SearchBtn($(SysResource.obtn));
    sb.create('remove', '删除', SysResource.remove);
    sb.create('refresh', '刷新', SysResource.refresh);
    sb.create('edit', '编辑', SysResource.edit);
    sb.create('plus', '新增', SysResource.plus);
    sb.create('search', '查询', SysResource.search);
};
//删除
SysResource.remove = function () {
    var sels = SysResource.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(SysResource.uDel, {ids: ids}, function () {
            SysResource.refresh();
        });
    })
};
//
SysResource.edit = function () {
    var sel = SysResource.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    SysResource._edit(sel['id'])
};
SysResource._edit = function (id) {
    $yd.win.create('编辑资源管理', SysResource.uEdit + '?id=' + id).open(function (that, $win) {
            $yd.get(SysResource.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysResource.u).submit(function () {
                that.close();
                SysResource.refresh();
            });
        }
    );
};
//新增
SysResource.plus = function () {
    var sel = SysResource.dg.getOne();
    var href = SysResource.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name']
    $yd.win.create('新增资源管理', href).open(null, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysResource.u).submit(function () {
                that.close();
                SysResource.refresh();
            });
        }
    );
};
//
SysResource.refresh = function () {
    SysResource.dtable.treegrid('reload');
};
//搜索
SysResource.search = function () {
    SysResource.dtable.treegrid('load', SysResource.sform.serializeObject());
};

$(function () {
    SysResource.sform = $('#sysResource_form');
    SysResource.dtable = $('#sysResource_table');
    SysResource.obtn = '#sysResource_btn';

    SysResource.dg(); // 数据表格
    SysResource.btn(); // 初始化按钮
    $yd.bindEnter(SysResource.search);
});