<!-- sys_dict.ftl 字典表 -->
var SysDict = {
    u: '/sysDict'
    , uList: '/sysDict/list'
    , uEdit: '/sysDict/edit'
    , uDel: '/sysDict/delete'
};

SysDict.dg = function () {
    var grid = new $yd.TreeGrid(this.dtable);
    grid.onDblClickRow = function (row) {
        if (row.editable == 1) {
            $yd.alert("不能修改", "warning");
            return;
        }
        SysDict._edit(row['id']);
    };
    grid.url = SysDict.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'code', title: '编码', width: 80}
        , {field: 'name', title: '显示值', width: 80}
        , {field: 'value', title: '值', width: 80}
        , {field: 'sort', title: '排序', width: 80}
        , {
            field: 'editable', title: '来源', width: 80, formatter: function (value) {
                if (value == 1) {
                    return '<label class="label label-warning">系统</label>';
                } else {
                    return '<label class="label label-primary">自建</label>';
                }
            }
        }
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'memo', title: '备注', width: 80}
        , {field: 'ctime', title: '创建时间', width: 80}
    ]];
    grid.loadGrid();
};
SysDict.dg.getOne = function () {
    var sels = SysDict.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
SysDict.dg.getSels = function () {
    return SysDict.dtable.treegrid('getSelections');
};
// 处理按钮
SysDict.btn = function () {
    var sb = new $yd.SearchBtn($(SysDict.obtn));
    sb.create('remove', '删除', SysDict.remove);
    sb.create('refresh', '刷新', SysDict.refresh);
    sb.create('edit', '编辑', SysDict.edit);
    sb.create('plus', '新增', SysDict.plus);
    sb.create('search', '查询', SysDict.search);
};
//删除
SysDict.remove = function () {
    var sels = SysDict.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(SysDict.uDel, {ids: ids}, function () {
            SysDict.refresh();
        });
    })
};
//
SysDict.edit = function () {
    var sel = SysDict.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    if (sel.editable == 1) {
        $yd.alert("不能修改", "warning");
        return;
    }
    SysDict._edit(sel['id'])
};
SysDict._edit = function (id) {
    $yd.win.create('编辑字典表', SysDict.uEdit + '?id=' + id).open(function (that, $win) {
            $yd.get(SysDict.u + "/id/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysDict.u).submit(function () {
                that.close();
                SysDict.refresh();
            });
        }
    );
};
//新增
SysDict.plus = function () {
    var sel = SysDict.dg.getOne();
    var href = SysDict.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增字典表', href).open(function (that, $win) {
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysDict.u).submit(function () {
                that.close();
                SysDict.refresh();
            });
        }
    );
};
//
SysDict.refresh = function () {
    SysDict.dtable.treegrid('reload');
};
//搜索
SysDict.search = function () {
    SysDict.dtable.treegrid('load', SysDict.sform.serializeObject());
};

$(function () {
    SysDict.sform = $('#sysDict_form');
    SysDict.dtable = $('#sysDict_table');
    SysDict.obtn = '#sysDict_btn';

    SysDict.dg(); // 数据表格
    SysDict.btn(); // 初始化按钮
    $yd.bindEnter(SysDict.search);
});