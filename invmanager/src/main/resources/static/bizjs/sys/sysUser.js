<!-- sys_user.ftl 管理员账号 -->
var SysUser = {
    u: '/sysUser'
    , uList: '/sysUser/list'
    , uEdit: '/sysUser/edit'
    , uDel: '/sysUser/delete'
};

SysUser.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index, row) {
        SysUser._edit(row['id']);
    };
    grid.url = SysUser.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'roleName', title: '角色名称', width: 80}
        , {field: 'deptName', title: '所在部门', width: 80}
        , {field: 'username', title: '用户名', width: 80}
        , {field: 'nickname', title: '姓名', width: 80}
        , {field: 'mobile', title: '手机号', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'lastLoginTime', title: '上次登录时间', width: 80}
        , {field: 'loginTime', title: '登录时间', width: 80}
        , {field: 'loginTimes', title: '登陆次数', width: 80}
        , {field: 'ctime', title: '创建时间', width: 80}
    ]];
    grid.loadGrid();
};
SysUser.dg.getOne = function () {
    var sels = SysUser.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
SysUser.dg.getSels = function () {
    return SysUser.dtable.datagrid('getSelections');
};
// 处理按钮
SysUser.btn = function () {
    var sb = new $yd.SearchBtn($(SysUser.obtn));
    sb.create('remove', '删除', SysUser.remove);
    sb.create('refresh', '刷新', SysUser.refresh);
    sb.create('edit', '编辑', SysUser.edit);
    sb.create('plus', '新增', SysUser.plus);
    // sb.create('search', '查询', SysUser.search);
};
//删除
SysUser.remove = function () {
    var sels = SysUser.dg.getSels();
    if (!sels | sels.length < 1) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(SysUser.uDel, {ids: ids}, function () {
            SysUser.refresh();
        });
    })
};
//
SysUser.edit = function () {
    var sel = SysUser.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    SysUser._edit(sel['id'])
};
// 表单预处理
SysUser.preForm = function () {
    new $yd.combobox("roleId").load("/sysRole/select");
    new $yd.comboboxFalse("deptId").load("/sysDepartment/select");
};
SysUser._edit = function (id) {
    $yd.win.create('编辑管理员账号', SysUser.uEdit + '?id=' + id).open(function (that, $win) {
            SysUser.preForm();
            // $win.find('#password').textbox('readonly');
            // $win.find('#pwd_div').hide();
            $win.find('#pwd_div').css("display", "none");
            $yd.get(SysUser.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysUser.u).submit(function () {
                that.close();
                SysUser.refresh();
            });
        }
    );
};
//新增
SysUser.plus = function () {
    var sel = SysUser.dg.getOne();
    var href = SysUser.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增管理员账号', href).open(function (that, $win) {
            SysUser.preForm();
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysUser.u).submit(function () {
                that.close();
                SysUser.refresh();
            });
        }
    );
};
//
SysUser.refresh = function () {
    $('#sysUser_table').datagrid('unselectAll');
    SysUser.dtable.datagrid('reload');
};
//搜索
SysUser.search = function () {
    $('#sysUser_table').datagrid('unselectAll');
    SysUser.dtable.datagrid('load', SysUser.sform.serializeObject());
};

$(function () {
    SysUser.sform = $('#sysUser_form');
    SysUser.dtable = $('#sysUser_table');
    SysUser.obtn = '#sysUser_btn';

    SysUser.dg(); // 数据表格
    SysUser.btn(); // 初始化按钮
    $yd.bindEnter(SysUser.search);
});
var btn1 = document.getElementById("searchButton");
btn1.addEventListener("click", function() {
    $('#sysUser_table').datagrid('unselectAll');
    SysUser.dtable.datagrid('load', SysUser.sform.serializeObject());
});