<!-- purchase_return.ftl 退货单表 -->
var PurchaseReturn = {
    u: '/purchaseReturn'
    , uList: '/purchaseReturn/list'
    , uEdit: '/purchaseReturn/edit'
    , uDel: '/purchaseReturn/delete'
};

PurchaseReturn.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        PurchaseReturn.windowDetail(row['code']);
    };
    grid.url = PurchaseReturn.uList;
    grid.rownumbers = true;
    grid.onClickRow = function (index,row) {
        $(this).datagrid('unselectRow', index);
        PurchaseReturn.goodsTable.datagrid("reload", {
            code: row.code,

        });

    };
    grid.checkOnSelect = false;
    grid.singleSelect = true;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'code', title: '退货单号', width: 80,halign:'center',align:'center'}
        , {field: 'orderTime', title: '单据时间', width: 70,halign:'center',align:'center', formatter: function (value) {
            return $yd.isEmpty(value) ? "" : value.substr(0, 10);
        }}
        , {field: 'nickname', title: '开单人', width: 70,halign:'center',align:'center'}
        , {field: 'relativeName', title: '供应商', width: 70,halign:'center',align:'center'}
        , {field: 'count', title: '退货总数', width: 50,halign:'center',align:'center'}
        , {field: 'totalMoney', title: '商品总额', width: 50,halign:'center',align:'center'}
        , {field: 'realPay', title: '实退总额', width: 50,halign:'center',align:'center'}
        , {field: 'needPay', title: '待退欠款', width: 50,halign:'center',align:'center', formatter: function (value,row) {
            if (row['payStatus'] == 1) {
                return 0;
            }else {
                return (row['totalMoney'] - row['realPay']).toFixed(2);
            }
        }}
        // , {field: 'unitPrice', title: '退货单价', width: 80,halign:'center',align:'center'}
        // , {field: 'skuId', title: '所退商品', width: 80}


        // , {field: 'payStatus', title: '结算状态', width: 80,halign:'center',align:'center'}
        , {
            field: 'payStatus', title: '结算状态', width: 40, halign:'center',align:'center',formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">未结清</label>';
                } else {
                    return '<label class="label label-default">已结清</label>';
                }
            }
        }
        , {field: 'repositoryStatus', title: '出库状态', width: 40,halign:'center',align:'center', formatter: function (value) {
            if (value == 0) {
                return '<label class="label label-success">未出库</label>';
            } else {
                return '<label class="label label-default">已出库</label>';
            }
        }}
        , {field: 'remark', title: '备注', width: 80,halign:'center',align:'center'}
        , {field: 'ctime', title: '创建时间', width: 80,halign:'center',align:'center'}
    ]];
    grid.loadGrid();
};

PurchaseReturn.windowDetail = function (purchaseReturnId) {
    var dg = $yd.win3.create('采购退货详情', "/purchaseReturn/detailPage?purchaseReturnCode=" + purchaseReturnId);
    dg.width = "70%";
    dg.height = "80%";
    dg.open(function (that, $win) {

    });
};
PurchaseReturn.dg.getOne = function () {
    var sels = PurchaseReturn.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
PurchaseReturn.dg.getSels = function () {
    return PurchaseReturn.dtable.datagrid('getSelections');
};
// 处理按钮
PurchaseReturn.btn = function () {
    var sb = new $yd.SearchBtn($(PurchaseReturn.obtn));
    sb.create('remove', '删除', PurchaseReturn.remove);
    sb.create('refresh', '刷新', PurchaseReturn.refresh);
    // sb.create('edit', '编辑', PurchaseReturn.edit);
    sb.create('plus', '新增', PurchaseReturn.plus);
    // sb.create('search', '查询', PurchaseReturn.search);
};
//删除
PurchaseReturn.remove = function () {
    var sels = PurchaseReturn.dg.getSels();
    if (sels.length == 0) {
        $yd.alert('请选中记录');
        return;
    }
    for (var i = 0; i < sels.length; i++) {
        var n = sels[i];
        if (n.payStatus != 0) {
            $yd.alert('已结清不能删除');
            return;
        }
        if (n.realPay != 0) {
            $yd.alert('实退金额大于0不能删除');
            return;
        }
        if (n.repositoryStatus != 0) {
            $yd.alert('已出库不能删除');
            return;
        }
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['code']);
        });
        $yd.post(PurchaseReturn.uDel, {codes: ids}, function () {
            PurchaseReturn.refresh();
        });
    })
};
//
PurchaseReturn.edit = function () {
    var sel = PurchaseReturn.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    PurchaseReturn._edit(sel['id'])
};
PurchaseReturn._edit = function (id) {
    $yd.win.create('编辑退货单表', PurchaseReturn.uEdit + '?id=' + id).open(function (that, $win) {
            $yd.get(PurchaseReturn.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(PurchaseReturn.u).submit(function () {
                that.close();
                PurchaseReturn.refresh();
            });
        }
    );
};
//新增
PurchaseReturn.plus = function () {
    parent.inv.OpenPage("/purchaseReturn/addPurchaseReturn",
        "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\"> 新增采购退货 </span>\n" +
        "<b class=\"arrow \"></b>", true)
};
//
PurchaseReturn.refresh = function () {
    $("form.search-form").form('clear');
    PurchaseReturn.search();
};
//搜索
PurchaseReturn.search = function () {
    PurchaseReturn.dtable.datagrid('load', PurchaseReturn.sform.serializeObject());
};

PurchaseReturn.goodsTableFun = function () {

    var grid = new $yd.DataGrid(PurchaseReturn.goodsTable);
    grid.url ='/purchaseReturn/detailGoodsList';
    grid.rownumbers = true;
    grid.singleSelect = true;
    grid.columns = [[
        {field: 'goodsName', title: '商品名称', width: 220},
        {field: 'skus', title: '规格', width: 220},
        {field: 'goodsId', title: '商品ID', width: 80, hidden: true},
        {field: 'goodsCode', title: '商品编号', width: 80},
        {field: 'OECode', title: 'OE编号', width: 120},
        {field: 'suitCar', title: '适用车型', width: 90},
        {field: 'wearhouseName', title: '仓库', width: 90},
        // {field: 'batchCode', title: '批号', width: 90},
        {field: 'unitPrice', title: '退货单价', width: 50},
        {
            field: 'count',
            title: '退货数量',
            width: 50
        },
        {
            field: 'shouldPay', title: '总额', width: 50, formatter: function (value, row, index) {
            return Number(row.count) * Number(row.unitPrice);
        }
        }
    ]]
    grid.loadGrid();
}

$(function () {
    PurchaseReturn.sform = $('#purchaseReturn_form');
    PurchaseReturn.dtable = $('#purchaseReturn_table');
    PurchaseReturn.goodsTable = $('#purchaseReturnGoods_table');
    PurchaseReturn.obtn = '#purchaseReturn_btn';
    PurchaseReturn.goodsTableFun();
    PurchaseReturn.dg(); // 数据表格
    PurchaseReturn.btn(); // 初始化按钮
    $yd.bindEnter(PurchaseReturn.search);
    $("#searchButton").click(PurchaseReturn.search);

    if(!$yd.isEmpty($("#hidSearchText").val())) {
        $("#searchText").textbox("setValue", $("#hidSearchText").val());
        setTimeout("PurchaseReturn.search()", 150);
        $('#hidSearchText').val('');
    }
    $("input[name='payStatus']").combobox({
        url: "/sysDict/sales_pay_status",
        panelHeight: 'auto',
        valueField: 'id',
        textField: 'text',
        editable: false,
    });

    $("input[name='orderStatus']").combobox({
        url: "/sysDict/sales_order_status",
        panelHeight: 'auto',
        valueField: 'id',
        textField: 'text',
        editable: false,
    });
});