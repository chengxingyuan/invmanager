var BsDetail = {
    u: '/bybs',
    uList: '/bybs/list',
    uEdit: '/bybs/edit',
    editIndex: -1,
    unitArr: [],
    isDg: 0,
    winThat: undefined,
    dgType: undefined,
    isConfirm: 1
};

$.extend($.fn.datagrid.defaults.editors, {
    house: {
        init: function (container, options) {
            return $('<div style="text-align: center">编辑</div>').appendTo(container);
        },
        destroy: function (target) {
            $(target).remove();
        },
        getValue: function (target) {
            var rows = BsDetail.dtable.datagrid("getRows");
            var house = rows[BsDetail.editIndex].house;
            return house;
        },
        setValue: function (target, value) {
        },
        resize: function (target, width) {
            $(target).click(function () {
                BsDetail.isDg = 1;
                var rows = BsDetail.dtable.datagrid("getRows");
                var skuId = rows[BsDetail.editIndex].skuId;
                var house = rows[BsDetail.editIndex].house;
                var href = "/wWearhouse/treeGrid?skuId=" + skuId + "&house=" + house + "&type=0&houseId=" + $("#houseId").val();//type 0出库 1入库
                var $dg = $yd.win.create('仓库/库位/批次', href);
                $dg.width = "450px;";
                $dg.height = "520px;";
                $dg.open(function (that, $win) {
                    BsDetail.winThat = that;
                    BsDetail.dgType = "house";
                }, function () {
                    BsDetail.saveDgHouse();
                }, function () {
                    if (BsDetail.editIndex > -1) {
                        BsDetail.dtable.datagrid('endEdit', BsDetail.editIndex);
                    }
                    BsDetail.editIndex = -1;
                    BsDetail.isDg = 0;
                    BsDetail.winThat = undefined;
                    BsDetail.dgType = undefined;
                });
            });

            $(target)._outerWidth(width);
        }
    }
});
BsDetail.saveDgHouse = function () {
    var rows = BsDetail.dtable.datagrid("getRows");
    var data = KW.getResult();
    if (data != "error") {
        if (data != "") {
            //销售数量也要修改
            var arr = data.split(",");
            var total = 0;
            for (var i = 0; i < arr.length; i++) {
                total += Number(arr[i].split("_")[6]);
            }
            if (total == Number(rows[BsDetail.editIndex].count)) {
                BsDetail.dtable.datagrid('updateRow', {
                    index: BsDetail.editIndex,
                    row: {
                        house: data
                    }
                });
            } else {
                BsDetail.dtable.datagrid('updateRow', {
                    index: BsDetail.editIndex,
                    row: {
                        house: data,
                        num: total,
                        fee: Number(total) * Number(rows[BsDetail.editIndex].price)
                    }
                });
            }
        } else {
            BsDetail.dtable.datagrid('updateRow', {
                index: BsDetail.editIndex,
                row: {
                    house: data
                }
            });
        }

        BsDetail.winThat.close();
        BsDetail.isDg = 0;
        BsDetail.winThat = undefined;
        BsDetail.dgType = undefined;
    }
};
BsDetail.dg = function () {
    BsDetail.dtable = $('#part_table');
    BsDetail.dtable.datagrid({
        url: "/bybsDetail/getDetailList?id=" + $("#id").val(),
        pagination: false,
        fitColumns: true,
        autoRowHeight: true,
        rownumbers: true,
        singleSelect: false,
        columns: [[
            {field: 'positionId', hidden: true},//库位
            {field: 'skuId', checkbox: false, hidden: true},//规格id
            {field: 'goodsName', title: '商品名称', width: 150},
            {field: 'goodsId', title: '商品ID', width: 80, hidden: true},
            {field: 'skuName', title: '规格', width: 200},
            {field: 'code', title: '商品编号', width: 80},
            {
                field: 'unit', title: '单位', width: 80, formatter: function (value, row, index) {
                    for (var i = 0; i < BsDetail.unitArr.length; i++) {
                        if (value == BsDetail.unitArr[i].id) {
                            return BsDetail.unitArr[i].text;
                        }
                    }
                    return value;
                }
            },
            {field: 'partsCode', title: '配件编号', width: 80},
            // {
            //     field: 'house',
            //     title: '<span style="color:blue">库位</span>',
            //     width: 150,
            //     editor: {type: 'house'},
            //     formatter: function (value, row, index) {
            //         if ($yd.isEmpty(value)) {
            //             return "";
            //         }
            //         var arr = value.split(",");
            //         var result = "";
            //         var first = "";
            //         for (var i = 0; i < arr.length; i++) {
            //             var arr2 = arr[i].split("_");
            //             result += arr2[3] + " " + arr2[4] + " X " + arr2[6] + "\n"
            //             if (first == "") {
            //                 first += arr2[3] + " " + arr2[4] + " X " + arr2[6];
            //             }
            //         }
            //         return "<a style='color:purple' title='" + result + "'>" + first + (arr.length > 1 ? "..." : "") + "</a>";
            //     }
            // },
            {
                field: 'price', title: '成本单价', width: 80, formatter: function (value, row, index) {
                    return Number(value).toFixed(2);
                }
            },
            {
                field: 'num',
                title: '<span style="color:blue">数量</span><span style="color:red">*</span>',
                width: 80,
                editor: {type: 'numberbox', options: {required: true, min: 1, precision: 0}}
            },
            {
                field: 'fee', title: '总金额', width: 80, formatter: function (value, row, index) {
                    return Number(value).toFixed(2);
                }
            },
            {
                field: 'remark',
                title: '<span style="color:blue">备注</span>',
                width: 100,
                editor: {type: 'textbox'}
            }
        ]],
        onClickCell: function (index, field) {
            if($("#status").val() == 1) {
                return;
            }
            if (BsDetail.editIndex != index) {
                if (BsDetail.editIndex > -1) {
                    BsDetail.dtable.datagrid('endEdit', BsDetail.editIndex);
                }
                BsDetail.editIndex = index;
            }
            if (field == "num" || field == "house" || field == "remark") {
                BsDetail.dtable.datagrid('beginEdit', index);
            }
        },
        onEndEdit: function (index, row, changes) {
            if (changes) {
                BsDetail.dtable.datagrid('updateRow', {
                    index: index,
                    row: {
                        fee: Number(row.price) * Number(row.num)
                    }
                });
            }
        }
    });

    if($("#status").val() == 0) {
        BsDetail.dtable.datagrid('enableCellEditing');
    }
};
BsDetail.formatDetail = function (value) {
    if ($yd.isEmpty(value)) {
        return "";
    }
    var result = "";
    var first = "";
    var arr = value.split(",");
    if (arr.length == 1) {
        return "<a style='color:purple' title='" + value + "'>" + value + "</a>";
    }
    for (var i = 0; i < arr.length; i++) {
        result += arr[i] + "\n";
        if (i == 0) {
            first = result;
        }
    }
    var html = "<a style='color:purple' title='" + result + "'>" + first + "..." + "</a>";

    return html;
};
BsDetail.refresh = function () {
    window.location.reload();
};
BsDetail.clear = function () {
    var rows = BsDetail.dtable.datagrid("getRows");
    while(rows.length > 0) {
        BsDetail.dtable.datagrid("deleteRow", 0);
        rows = BsDetail.dtable.datagrid("getRows");
    }
};
BsDetail.plus = function () {
    var href = "/mMateriel/searchDg?bindHouseId=" + $("#houseId").val();
    // var $dg = $yd.win.create('商品（绑定当前仓库的才会被查出）', href);
    var $dg = $yd.win.create('商品', href);
    $dg.width = "1050px;";
    $dg.height = "400px;";
    BsDetail.isDg = 1;
    $dg.open(function (that, $win) {
            BsDetail.winThat = that;
            BsDetail.dgType = "goods";
        }, function (that, $win) {
            BsDetail.saveDgGoods();
        }, function () {
            BsDetail.isDg = 0;
            BsDetail.winThat = undefined;
            BsDetail.dgType = undefined;
            document.onkeydown = BsDetail.keyDown;
        }
    );
};
BsDetail.isExist = function (rows, skuId) {
    for (var i = 0; i < rows.length; i++) {
        if (rows[i].skuId == skuId) {
            return true;
        }
    }
    return false;
};
BsDetail.saveDgGoods = function () {
    document.onkeydown = BsDetail.keyDown;

    var data = Goods.getChecked();
    if (data.length == 0) {
        BsDetail.winThat.close();
        BsDetail.isDg = 0;
        BsDetail.winThat = undefined;
        BsDetail.dgType = undefined;
    }

    var rows = BsDetail.dtable.datagrid("getData").rows;
    for (var i = 0; i < data.length; i++) {
        if (!BsDetail.isExist(rows, data[i].skuId)) {
            var dt = data[i];

            //获取成本单价
            $.ajax({
                type: "POST",
                url: "/inRepositoryDetail/getSkuAvgCost2House",
                data: {
                    skuId: data[i].skuId,
                    houseId: $("#houseId").val(),
                },
                dataType: "json",
                async: false,
                success: function (res) {
                    var avgCost = res.avgCost;

                    $.ajax({
                        type: "POST",
                        url: "/inRepositoryDetail/getMaterielHouseByHouseId",
                        data: {
                            materielId: dt.goodsId,
                            houseId: $("#houseId").val(),
                        },
                        dataType: "json",
                        async: false,
                        success: function (result) {
                            var positionId = $yd.isEmpty(result.house) ? 0 : result.house.split("_")[2];
                            BsDetail.dtable.datagrid('appendRow', {
                                skuId: dt.skuId,
                                goodsName: dt.goodsName,
                                goodsId: dt.goodsId,
                                skus: dt.skusName,
                                code: dt.code,
                                partsCode: dt.partsCode,
                                unit: dt.unit,
                                price: avgCost,
                                num: 1,
                                fee: Number(avgCost) * Number(1),
                                house: result.house,
                                positionId: positionId
                            }).datagrid('clearSelections');
                        }
                    });
                }
            });
        }
    }
    BsDetail.winThat.close();
    BsDetail.isDg = 0;
    BsDetail.winThat = undefined;
    BsDetail.dgType = undefined;
};

Array.prototype.contains = function (needle) {
    for (var i in this) {
        if (this[i] == needle) return true;
    }
    return false;
};
BsDetail.remove = function () {
    var sels = BsDetail.dtable.datagrid('getChecked')
    if (sels.length < 1) {
        $yd.alert('选中记录');
        return;
    }
    var ids = [];
    $.each(sels, function (i, n) {
        ids.push(n['skuId']);
    });

    $.each(ids, function (i, n) {
        var rows = BsDetail.dtable.datagrid('getRows');
        for (var ii = 0; ii < rows.length; ii++) {
            if (rows[ii].skuId == n) {
                BsDetail.dtable.datagrid('deleteRow', ii);
                break;
            }
        }
    });
};

BsDetail.isEmpty = function (value) {
    if (value == undefined || value == null || value == "") {
        return true;
    }
    return false;
};
BsDetail.save0 = function(){
    BsDetail.save(0);
};
BsDetail.save1 = function(){
    $yd.confirm("审核后将生成相应的出入库单，确认信息无误？", function () {
        BsDetail.save(1);
    });
};
BsDetail.save = function (type) {
    //结束编辑状态
    if (BsDetail.editIndex != undefined && BsDetail.editIndex != -1) {
        BsDetail.dtable.datagrid('endEdit', BsDetail.editIndex);
    }
    if (BsDetail.editIndex != undefined && BsDetail.editIndex != -1) {
        BsDetail.dtable.datagrid('endEdit', BsDetail.editIndex);
    }

    $('#myForm').form('submit', {
        url: BsDetail.uEdit + "?type=" + type,
        onSubmit: function () {
            var isValid = $(this).form('validate');//只能校验处于编辑状态的栏位
            if (!isValid) {
                return isValid;
            }

            //校验数据
            var rows = BsDetail.dtable.datagrid("getRows");
            if (rows.length == 0) {
                $yd.alert("请添加商品");
                return false;
            }

            // for (var i = 0; i < rows.length; i++) {
            //     var row = rows[i];
            //     var house = row.house;
            //     if (BsDetail.isEmpty(house)) {
            //         $yd.alert("商品未配置库位");
            //         return false;
            //     }
            // }

            //封装数据
            $("#goods").val(JSON.stringify(rows));
            return isValid;
        },
        success: function (data) {
            var d = $.parseJSON(data);
            if (d.code == 0) {
                $yd.alert("操作成功", function () {
                    window.location.reload();
                });
            }
        }
    });
};

// 处理按钮
BsDetail.btn = function () {
    var sb = new $yd.SearchBtn($(BsDetail.obtn));
    sb.create('repeat red', '重置（Shift+R）', BsDetail.refresh);
    if($("#status").val() == 0) {
        sb.create('edit', '审核(Shift+V)', BsDetail.save1);
        sb.create('save green', '保存（Shift+S）', BsDetail.save0);
    }
};
BsDetail.keyDown = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (!e) {
        return;
    }
    // console.log("add.js")
};
BsDetail.keyUp = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (!e) {
        return;
    }

    if (BsDetail.isDg == 0) {
        // if (e.keyCode == 45) {//insert
        //     BsDetail.plus();
        // }
        // if (e.keyCode == 46) {//del
        //     BsDetail.remove();
        // }
        if (e.shiftKey && e.keyCode == 83) {
            BsDetail.save0();//生单: Shift + S
        }
        if (e.shiftKey && e.keyCode == 86) {
            BsDetail.save1();//生单: Shift + V
        }
        if (e.shiftKey && e.keyCode == 82) {
            BsDetail.refresh();
        }
    } else {
        if (e.keyCode == 13) {
            if(BsDetail.dgType == "goods") {
                BsDetail.saveDgGoods();
            } else if(BsDetail.dgType == "house") {
                BsDetail.saveDgHouse();
            }
        } else if (e.keyCode == 27 && BsDetail.winThat) {//ESC
            BsDetail.winThat.close();
        }
    }
};
BsDetail.init = function () {
    $.ajax({
        type: "POST",
        url: "/sysDict/materiel_MU",
        dataType: "json",
        async: false,
        success: function (data) {
            BsDetail.unitArr = data;
        }
    });

    //默认值
    $("input[name='orderTime']").datebox({
        required: true,
        editable: false,
        value: new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()
    });

    $("#orderType").combobox({
        url: "/sysDict/bybs_type",
        panelHeight:'auto',
        editable:false,
        required:true,
        valueField:'id',
        textField:'text',
        onChange: function (newValue, oldValue) {
            if(BsDetail.isConfirm == 0) {
                return;
            }
            if(!$yd.isEmpty(oldValue)) {
                $.messager.confirm({
                    title: '确认框',
                    border: false,
                    top: 0,
                    msg: "切换仓库清除当前仓库已添加的商品，确定切换？",
                    fn: function (r) {
                        if (r) {
                            window.location.href = "/bybs/add?orderType=1";
                        } else {
                            BsDetail.isConfirm = 0;
                            $("#orderType").combobox("setValue", oldValue);
                            BsDetail.isConfirm = 1;
                        }
                    }
                })
            } else {
                window.location.href = "/bybs/add?orderType=1";
            }
        }
    });


    $("#houseId").combobox({
        prompt:'仓库',
        url: "/wWearhouse/list",
        panelHeight:'auto',
        editable:false,
        required:true,
        valueField:'id',
        textField:'name',
        onChange: function (newValue, oldValue) {
            var rows = BsDetail.dtable.datagrid("getRows");
            if(rows.length == 0) {
                return;
            }
            if(BsDetail.isConfirm == 0) {
                return;
            }
            if(!$yd.isEmpty(oldValue)) {
                $.messager.confirm({
                    title: '确认框',
                    border: false,
                    top: 0,
                    msg: "切换仓库清除当前仓库已添加的商品，确定切换？",
                    fn: function (r) {
                        if (r) {
                            BsDetail.clear();
                        } else {
                            BsDetail.isConfirm = 0;
                            $("#houseId").combobox("setValue", oldValue);
                            BsDetail.isConfirm = 1;
                        }
                    }
                })
            } else {
                BsDetail.clear();
            }
        }
    });

    //鼠标点击页面任何地方，表格编辑结束编辑状态
    document.onclick = BsDetail.mouseClick;

    //快捷键
    document.onkeydown = BsDetail.keyDown;
    document.onkeyup = BsDetail.keyUp;

    setTimeout("$('#orderTime').focus()", 500);
    $("a").attr("tabindex", -1);
    $(":checkbox").attr("tabindex", -1);
};
BsDetail.mouseClick = function (e) {
    e = window.event || e; // 兼容IE7
    var obj = $(e.srcElement || e.target);

    if (BsDetail.isDg == 0 && !$(obj).hasClass("datagrid-cell") && !$(obj).hasClass("datagrid-row-over") && !$(obj).hasClass("datagrid-row-selected")) {
        if (BsDetail.editIndex != -1) {
            BsDetail.dtable.datagrid('endEdit', BsDetail.editIndex);
            BsDetail.editIndex = -1;
        }
    }
};
$(function () {
    BsDetail.obtn = '#part_btn';
    BsDetail.dg(); // 数据表格
    BsDetail.btn(); // 初始化按钮
    BsDetail.init(); // 数据表格
});