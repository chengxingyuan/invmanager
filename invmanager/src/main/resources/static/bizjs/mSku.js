<!-- m_sku.ftl 物料规格 -->
var MSku = {
    u: '/mSku'
    , uList: '/mSku/list'
    , uEdit: '/mSku/edit'
    , uDel: '/mSku/delete'
    , dgType: undefined
    , winThat: undefined
    , win: undefined
};

MSku.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index, row) {
        MSku._edit(row['id']);
    };
    grid.onClickRow = function (index, row) {
        MSkuValue.dtable.datagrid('load', {skuId: row.id});
    };
    grid.onUnselect = function (index, row) {
        MSkuValue.dtable.datagrid('load', {skuId: 0});
    };
    grid.rownumbers = true;
    grid.url = MSku.uList;
    grid.columns = [[
        {field: 'id', checkbox: false, hidden:true}
        , {field: 'name', title: '规格名称', width: 80}
        // , {field: 'code', title: '编码', width: 80}
        , {field: 'sort', title: '排序', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else if (value == 1) {
                    return '<label class="label label-default">禁用</label>';
                }
            }, editor: {
                type: 'combobox'
                , options: {data: [{valueField: 0, textField: '启用'}, {valueField: 1, textField: '禁用'}]}
            }
        }
        , {field: 'ctime', title: '创建时间', width: 80}
    ]];
    grid.onLoadSuccess = function() {
        MSku.dtable.datagrid("unselectAll");
        var rows = MSku.dtable.datagrid("getRows");
        if(rows.length == 0) {
            MSkuValue.dtable.datagrid('load', {skuId: 0});
        } else {
            MSku.dtable.datagrid("selectRow", 0);
            MSkuValue.dtable.datagrid('load', {skuId: rows[0].id});
        }
    };
    grid.loadGrid();
};
MSku.dg.getOne = function () {
    var sels = MSku.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
MSku.dg.getSels = function () {
    return MSku.dtable.datagrid('getSelections');
};
// 处理按钮
MSku.btn = function () {
    var sb = new $yd.SearchBtn($(MSku.obtn));
    sb.create('remove', '删除(DEL)', MSku.remove);
    sb.create('refresh', '刷新(F2)', MSku.refresh);
    sb.create('edit', '编辑(Shift+E)', MSku.edit);
    sb.create('plus', '新增(Shift+N)', MSku.plus);
    // sb.create('search', '查询', MSku.search);
};
//删除
MSku.remove = function () {
    var sels = MSku.dg.getSels();
    if (sels.length == 0) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(MSku.uDel, {ids: ids}, function () {
            MSku.refresh();
        });
    })
};
//
MSku.edit = function () {
    var sel = MSku.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    MSku._edit(sel['id'])
};
MSku._edit = function (id) {
    MSku.dgType = "sku";
    var dg = $yd.win2.create('编辑存货规格', MSku.uEdit + '?id=' + id);
    dg.width = "500px";
    dg.height = "400px";
    dg.open(function (that, $win) {
            MSku.win = $win;
            MSku.winThat = that;
            $yd.get(MSku.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
                MSku.preForm(data);
            })
        }, function (that, $win) {
            MSku.saveSku();
        }, function (that, $win) {
            MSku.closeDg(1);
        }
    );
};
//新增
MSku.plus = function () {
    var sel = MSku.dg.getOne();
    var href = MSku.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];

    MSku.dgType = "sku";
    var dg = $yd.win2.create('新增存货规格', href);
    dg.width = "500px";
    dg.height = "400px";
    dg.open(function (that, $win) {
            MSku.win = $win;
            MSku.winThat = that;
            MSku.preForm();
        }, function (that, $win) {
            MSku.saveSku();
        }, function (that, $win) {
            MSku.closeDg(1);
        } , function (that, $win) {
            MSku.saveSkuNext();
        }
    );
};
MSku.closeDg = function (closeEd) {
    if(closeEd == 0) {
        MSku.winThat.close();
    }

    MSku.dgType = undefined;
    MSku.winThat = undefined;
    MSku.win = undefined;
};
MSku.saveSku = function () {
    $yd.form.create(MSku.win.find('form')).url(MSku.u).submit(function () {
        MSku.refresh();
        MSku.closeDg(0);
    });
};
MSku.saveSkuNext = function () {
    $yd.form.create(MSku.win.find('form')).url(MSku.u).submit(function () {
        MSkuEdit.reset();
        MSku.refresh();
    });
};
//
MSku.refresh = function () {
    $('#mSku_table').datagrid('unselectAll');
    MSku.dtable.datagrid('reload');
    MSkuValue.refresh();
};
//搜索
MSku.search = function () {
    MSku.dtable.datagrid('load', MSku.sform.serializeObject());
};

//表单预处理
MSku.preForm = function (data) {
    // var category = new $yd.combotreegrid("#categoryId", true).addCol('name', '名称').addCol('code', '编码');
    // category.load("/mCateogry/list");
    // MSkuEdit.addValid();
};

MSku.keyUp = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (!e) {
        return;
    }
    // console.log(e.keyCode);
    if($yd.isForbidCutKey == 1) {
        return;
    }
    if($yd.isEmpty(MSku.dgType)) { //未弹框
        if (e.shiftKey && e.keyCode == 78) { //Shift+N
            MSku.plus();
        }
        if (e.shiftKey && e.keyCode == 69) { //Shift+E
            MSku.edit();
        }
        if(e.keyCode == 113) {//F2
            MSku.refresh();
        }
        if(e.keyCode == 46) {//DEL
            MSku.remove();
        }
        if(e.keyCode == 13) {
            MSku.search();
        }
    } else{ //弹框
        if(e.keyCode == 27) {//ESC
            MSku.closeDg(0);
        }
        if(e.keyCode == 13) {//Enter
            if(e.ctrlKey) {//Ctrl+Enter
                if(MSku.dgType == "sku") {
                    MSku.saveSkuNext();
                } else if(MSku.dgType == "skuValue") {
                    MSkuValue.saveSkuNext();
                }
            } else if(MSku.dgType == "sku") {
                MSku.saveSku();
            } else if(MSku.dgType == "skuValue") {
                MSkuValue.saveSku();
            }
        }
    }
};
MSku.init = function () {
    //快捷键
    document.onkeyup = MSku.keyUp;

    //先获取焦点再失去焦点，否则打开页面后若不点击页面，快捷键无效
    $("#searchName").next().find(".textbox-text").focus()
    $("#searchName").next().find(".textbox-text").blur()
};

$(function () {
    MSku.sform = $('#mSku_form');
    MSku.dtable = $('#mSku_table');
    MSku.obtn = '#mSku_btn';

    MSku.dg(); // 数据表格
    MSku.btn(); // 初始化按钮
    $yd.bindEnter(MSku.search);
    MSku.init();
});
var btn1 = document.getElementById("searchButton");
btn1.addEventListener("click", function() {
    $('#mSku_table').datagrid('unselectAll');
    MSku.dtable.datagrid('load', MSku.sform.serializeObject());
});