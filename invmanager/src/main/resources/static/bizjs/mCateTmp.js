<!-- m_cate_tmp.ftl 存货分类表 -->
var MCateTmp = {
    u: '/mCateTmp'
    , uList: '/mCateTmp/list'
    , uEdit: '/mCateTmp/edit'
    , uDel: '/mCateTmp/delete'
};

MCateTmp.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index,row) {
        MCateTmp._edit(row['id']);
    };
    grid.url = MCateTmp.uList;
    grid.columns = [[
        {field: 'id', checkbox: true}
        , {field: 'name', title: '分类名称', width: 80}
        , {field: 'groupName', title: 'groupName', width: 80}
        , {field: 'code', title: '分类编码', width: 80}
        , {field: 'categoryType', title: '分类类型：1级分类，2级分类，3级分类', width: 80}
        , {field: 'isLeaf', title: '是否叶子节点', width: 80}
        , {field: 'sort', title: '排序', width: 80}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: 'ctime', width: 80}
    ]];
    grid.loadGrid();
};
MCateTmp.dg.getOne = function () {
    var sels = MCateTmp.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
MCateTmp.dg.getSels = function () {
    return MCateTmp.dtable.datagrid('getSelections');
};
// 处理按钮
MCateTmp.btn = function () {
    var sb = new $yd.SearchBtn($(MCateTmp.obtn));
    sb.create('remove', '删除', MCateTmp.remove);
    sb.create('refresh', '刷新', MCateTmp.refresh);
    sb.create('edit', '编辑', MCateTmp.edit);
    sb.create('plus', '新增', MCateTmp.plus);
    sb.create('search', '查询', MCateTmp.search);
};
//删除
MCateTmp.remove = function () {
    var sels = MCateTmp.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(MCateTmp.uDel, {ids: ids}, function () {
            MCateTmp.refresh();
        });
    })
};
//
MCateTmp.edit = function () {
    var sel = MCateTmp.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    MCateTmp._edit(sel['id'])
};
MCateTmp._edit = function (id) {
    $yd.win.create('编辑存货分类表', MCateTmp.uEdit + '?id=' + id).open(function (that, $win) {
            $yd.get(MCateTmp.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(MCateTmp.u).submit(function () {
                that.close();
                MCateTmp.refresh();
            });
        }
    );
};
//新增
MCateTmp.plus = function () {
    var sel = MCateTmp.dg.getOne();
    var href = MCateTmp.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增存货分类表', href).open(function (that, $win){
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(MCateTmp.u).submit(function () {
                that.close();
                MCateTmp.refresh();
            });
        }
    );
};
//
MCateTmp.refresh = function () {
    MCateTmp.dtable.datagrid('reload');
};
//搜索
MCateTmp.search = function () {
    MCateTmp.dtable.datagrid('load', MCateTmp.sform.serializeObject());
};

$(function () {
    MCateTmp.sform = $('#mCateTmp_form');
    MCateTmp.dtable = $('#mCateTmp_table');
    MCateTmp.obtn = '#mCateTmp_btn';

    MCateTmp.dg(); // 数据表格
    MCateTmp.btn(); // 初始化按钮
    $yd.bindEnter(MCateTmp.search);
});