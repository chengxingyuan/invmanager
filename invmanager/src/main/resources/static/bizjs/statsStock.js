var Income = {
    uList: '/stats/stockList',
    unitArr: []
};

Income.dg = function () {
    $.ajax({
        type: "POST",
        url: "/sysDict/materiel_MU",
        dataType: "json",
        success: function (data) {
            Income.unitArr = data;
        }
    });

    var grid = new $yd.DataGrid(this.dtable);
    // grid.onDblClickRow = function (index,row) {
    //     Income._edit(row);
    // };
    grid.nowarp = false;
    grid.url = Income.uList;
    grid.rownumbers = true;
    grid.columns = [[
          {field: 'id', hidden: true}
        , {field: 'skuId', hidden: true}
        , {field: 'buszId', hidden: true}
        , {field: 'count', hidden: true}
        , {field: 'opType', hidden: true}
        // , {field: 'orderTime', title: '单据日期', width: 40,formatter:function(value,row,index){
        //         if (value == null){
        //             return '';
        //         }else {
        //             value = value.substr(0, value.length - 9);
        //             return value;
        //         }
        //     }}
        // , {field: 'code', title: '单据编码', width: 80}
        // , {field: 'type', title: '出入库类型', width: 80,formatter:function(value,row,index){
        //         if(row.opType == 0) {
        //             if(value == 0) {
        //                 return "采购入库";
        //             } else if(value == 1) {
        //                 return "盘点入库";
        //             }
        //         } else if(value == 0) {
        //             return "销售出库";
        //         } else if(value == 1) {
        //             return "盘点出库";
        //         }
        //         return "";
        //     }}
        , {field: 'goodsName', title: '商品名称', width: 80}
        // , {field: 'goodsCode', title: '商品编码', width: 80}
        , {field: 'skuName', title: '规格', width: 120}
        , {field: 'unit', title: '单位', width: 80, formatter: function (value, row, index) {
                for (var i = 0; i < Income.unitArr.length; i++) {
                    if (value == Income.unitArr[i].id) {
                        return Income.unitArr[i].text;
                    }
                }
                return value;
            }
        }
        , {field: 'houseName', title: '仓库', width: 80}
        , {field: 'rkNum', title: '入库数量', width: 80}
        , {field: 'ckNum', title: '出库数量', width: 80}
    ]];
    grid.onLoadSuccess = function(data) {
        //汇总
        $.post("/stats/getSumInfo3",$("#mSales_form").serialize(),function (data) {
            $("#ckNum").textbox("setValue", data.ckNum);
            $("#rkNum").textbox("setValue", data.rkNum);
        });
    };
    grid.loadGrid();
};
//重置
Income.reset = function () {
    $("#mSales_form").form('clear');
    Income.search();
}
// 处理按钮
Income.btn = function () {
    var sb = new $yd.SearchBtn($(Income.obtn));
    sb.create('repeat red', '重置', Income.reset);
    // sb.create('refresh', '刷新', Income.refresh);
    // sb.create('search', '查询', Income.search);
};
Income._edit = function (row) {
    debugger
    if(row.buszId == 0) {
        $yd.alert("错误数据");
        return;
    }
    if(row.opType == 0) {
        if(row.type == 0) {
                $.ajax({
                    type: "GET",
                    url: "/pPurchaseDetail/" + row.buszId,
                    dataType: "json",
                    success: function (data) {
                        debugger
                        var d = data.data;
                        if(data.code == 0) {
                            parent.inv.OpenPage("/pPurchase/detail?purchaseId=" + d.purchaseId, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
                                "<span class=\"menu-text\"> 采购单详情</span>\n" +
                                "<b class=\"arrow \"></b>",true)
                        }
                    }
                });
        } else if(row.type == 1) {
            return "盘点入库";
        }
    } else if(row.type == 0) {
        $.ajax({
            type: "GET",
            url: "/mSalesDetail/" + row.buszId,
            dataType: "json",
            success: function (data) {
                debugger
                var d = data.data;
                if(data.code == 0) {
                    parent.inv.OpenPage("/mSales/edit?id=" + d.salesId, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
                        "                                <span class=\"menu-text\"> 销售单详情 </span>\n" +
                        "                                <b class=\"arrow \"></b>",true)
                }
            }
        });

    } else if(row.type == 1) {
        return "盘点出库";
    }
};
Income.refresh = function () {
    $('#mSales_table').datagrid('unselectAll');
    Income.dtable.datagrid('reload');
};
//搜索
Income.search = function () {
    Income.dtable.datagrid('load', Income.sform.serializeObject());
};

$(function () {
    Income.sform = $('#mSales_form');
    Income.dtable = $('#mSales_table');
    Income.obtn = '#mSales_btn';

    Income.dg(); // 数据表格
    Income.btn(); // 初始化按钮
    $yd.bindEnter(Income.search);

    new $yd.combotreegrid("#wearhouseId").addCol("name", "名称").load("/wWearhouse/list");
});
var btn1 = document.getElementById("searchButton");
btn1.addEventListener("click", function() {
    $('#mSales_table').datagrid('unselectAll');
    Income.dtable.datagrid('load', Income.sform.serializeObject());
});