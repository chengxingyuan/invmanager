<!-- w_relative_unit.ftl 往来客户 -->
var WRelativeUnit = {
    u: '/wRelativeUnit'
    , uList: '/wRelativeUnit/list'
    , uEdit: '/wRelativeUnit/edit'
    , uDel: '/wRelativeUnit/delete'
    , uExcel: '/wRelativeUnit/importExcel'
    , uSaveExcel: '/wRelativeUnit/saveExcel'
    , downLoadExcel: '/wRelativeUnit/downLoadExcel'
    , dgType: undefined
    , winThat: undefined
    , win: undefined
};

WRelativeUnit.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index, row) {
        WRelativeUnit._edit(row['id']);
    };
    grid.url = WRelativeUnit.uList;
    grid.rownumbers = true;
    grid.columns = [[
        {field: 'id', checkbox: false, hidden: true}
        , {field: 'name', title: '名称', width: 50}
        , {field: 'code', title: '编码', width: 50}
        , {field: 'helpCode', title: '助记码', width: 50}
        , {field: 'rproperty', title: '性质', width: 50, hidden : true}
        // , {field: 'rpropertyName', title: '性质', width: 50}
        , {field: 'contactor', title: '联系人', width: 50}
        , {field: 'tel', title: '联系电话', width: 50}
        , {
            field: 'status', title: '状态', width: 50, formatter: function (value) {
                if (value == 0) {
                    return '<label class="label label-success">启用</label>';
                } else {
                    return '<label class="label label-default">禁用</label>';
                }
            }
        }
        , {field: 'ctime', title: '创建时间', width: 50}
    ]];
    grid.loadGrid();
};
WRelativeUnit.dg.getOne = function () {
    var sels = WRelativeUnit.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
WRelativeUnit.dg.getSels = function () {
    return WRelativeUnit.dtable.datagrid('getSelections');
};
WRelativeUnit.lcxxdd = function() {//线下订单
    var sel = WRelativeUnit.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    parent.inv.OpenPage("/mSales?searchText=" + sel.contactor, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\">销售单列表</span>\n" +
        "<b class=\"arrow \"></b>", true)
};
WRelativeUnit.lcptdd = function() {//平台订单
    var sel = WRelativeUnit.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    parent.inv.OpenPage("/tticar/goodsOrder?searchText=" + sel.contactor, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\">订单管理</span>\n" +
        "<b class=\"arrow \"></b>", true)
};
WRelativeUnit.lcyskd = function() {//应收款单
    var sel = WRelativeUnit.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    parent.inv.OpenPage("/finance/receivable?cusName=" + sel.contactor, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\">应收款单</span>\n" +
        "<b class=\"arrow \"></b>", true)
};
WRelativeUnit.lccgd = function() {//采购单
    var sel = WRelativeUnit.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    parent.inv.OpenPage("/pPurchase?relativeId=" + sel.id, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\">采购单列表</span>\n" +
        "<b class=\"arrow \"></b>", true)
};
WRelativeUnit.lcyfkd = function() {//应付款单
    var sel = WRelativeUnit.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    parent.inv.OpenPage("/finance/payable?relativeId=" + sel.id, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "<span class=\"menu-text\">应付款单</span>\n" +
        "<b class=\"arrow \"></b>", true)
};
// 处理按钮
WRelativeUnit.btn = function () {
    var sb = new $yd.SearchBtn($(WRelativeUnit.obtn));
    sb.create('remove', '删除(DEL)', WRelativeUnit.remove);
    sb.create('refresh', '刷新(F2)', WRelativeUnit.refresh);

    sb.create('file-excel-o green', '导入', WRelativeUnit.importExcel);
    sb.create('file-excel-o green', '下载导入模板', WRelativeUnit.downExcel);
    sb.create('edit', '编辑(Shift+E)', WRelativeUnit.edit);
    sb.create('plus', '新增(Shift+N)', WRelativeUnit.plus);
    // sb.create('search', '查询', WRelativeUnit.search);

    $("#lcxxdd").click(WRelativeUnit.lcxxdd);
    $("#lcptdd").click(WRelativeUnit.lcptdd);
    $("#lcyskd").click(WRelativeUnit.lcyskd);
    $("#lccgd").click(WRelativeUnit.lccgd);
    $("#lcyfkd").click(WRelativeUnit.lcyfkd);
};
//删除
WRelativeUnit.remove = function () {
    var sels = WRelativeUnit.dg.getSels();
    if (!sels || sels.length < 1) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(WRelativeUnit.uDel, {ids: ids}, function () {
            WRelativeUnit.refresh();
        });
    })
};

//下载模板
WRelativeUnit.downExcel = function () {
    location.href="wRelativeUnit/downLoadExcel";

}
// 导入
WRelativeUnit.importExcel = function () {
    $yd.win.create('导入Excel', WRelativeUnit.uExcel).open(function (that, $win) {

        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(WRelativeUnit.uSaveExcel).submit(function () {
                that.close();
                WRelativeUnit.refresh();
            });
        }
    );
}

//
WRelativeUnit.edit = function () {
    var sel = WRelativeUnit.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    WRelativeUnit._edit(sel['id'])
};
WRelativeUnit._edit = function (id) {
    WRelativeUnit.dgType = "unit";
    var dg = $yd.win2.create('编辑', WRelativeUnit.uEdit + '?id=' + id);
    dg.width = "700px";
    dg.height = "400px";
    dg.open(function (that, $win) {
            WRelativeUnit.win = $win;
            WRelativeUnit.winThat = that;
            $yd.get(WRelativeUnit.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function () {
            WRelativeUnit.save();
        }, function () {
            WRelativeUnit.closeDg(1);
        }
    );
};
//新增
WRelativeUnit.plus = function () {
    var sel = WRelativeUnit.dg.getOne();
    var href = WRelativeUnit.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];

    WRelativeUnit.dgType = "house";
    var dg = $yd.win2.create('新增', href);
    dg.width = "700px";
    dg.height = "400px";
    dg.open(function (that, $win) {
            WRelativeUnit.win = $win;
            WRelativeUnit.winThat = that;
            $("#unitRproperty").combobox("setValue", WRelativeUnit.pTree.tree("getSelected").id);
        }, function () {
            WRelativeUnit.save();
        }, function () {
            WRelativeUnit.closeDg(1);
        } , function () {
            WRelativeUnit.saveNext();
        }
    );
};
WRelativeUnit.closeDg = function (closeEd) {
    if(closeEd == 0) {
        WRelativeUnit.winThat.close();
    }

    WRelativeUnit.dgType = undefined;
    WRelativeUnit.winThat = undefined;
    WRelativeUnit.win = undefined;
};
WRelativeUnit.save = function () {
    $yd.form.create(WRelativeUnit.win.find('form')).url(WRelativeUnit.u).submit(function () {
        WRelativeUnit.refresh();
        WRelativeUnit.closeDg(0);
    });
};
WRelativeUnit.saveNext = function () {
    $yd.form.create(WRelativeUnit.win.find('form')).url(WRelativeUnit.u).submit(function () {
        WRelativeUnitEdit.reset();
        WRelativeUnit.refresh();
    });
};
WRelativeUnit.keyUp = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (!e) {
        return;
    }
    // console.log(e.keyCode);
    if($yd.isForbidCutKey == 1) {
        return;
    }
    if($yd.isEmpty(WRelativeUnit.dgType)) { //未弹框
        if (e.shiftKey && e.keyCode == 78) { //Shift+N
            WRelativeUnit.plus();
        }
        if (e.shiftKey && e.keyCode == 69) { //Shift+E
            WRelativeUnit.edit();
        }
        if(e.keyCode == 113) {//F2
            WRelativeUnit.refresh();
        }
        if(e.keyCode == 46) {//DEL
            WRelativeUnit.remove();
        }
        if(e.keyCode == 13) {
            WRelativeUnit.search();
        }
    } else{ //弹框
        if(e.keyCode == 27) {//ESC
            WRelativeUnit.closeDg(0);
        }
        if(e.keyCode == 13) {//Enter
            if(e.ctrlKey) {//Ctrl+Enter
                WRelativeUnit.saveNext();
            } else {
                WRelativeUnit.save();
            }
        }
    }
};
WRelativeUnit.init = function () {
    //快捷键
    document.onkeyup = WRelativeUnit.keyUp;

    //先获取焦点再失去焦点，否则打开页面后若不点击页面，快捷键无效
    $("#searchName").next().find(".textbox-text").focus()
    $("#searchName").next().find(".textbox-text").blur()
};
//
WRelativeUnit.refresh = function () {

    $('#wRelativeUnit_table').datagrid('unselectAll');

    WRelativeUnit.dtable.datagrid('reload');
};
//搜索
WRelativeUnit.search = function () {

    $('#wRelativeUnit_table').datagrid('unselectAll');
    WRelativeUnit.dtable.datagrid('load', WRelativeUnit.sform.serializeObject());
};
// 属性树
WRelativeUnit.iProTree = function () {
    WRelativeUnit.pTree.tree({
        url: "/sysDict/relativeNuit_PR/tree",
        method: 'get',
        onClick: function (node) {
            if(!WRelativeUnit.pTree.tree("isLeaf", node.target)) {
                return;
            }
            if(node.id==1) {
                $("#mb").show();
                $("#mb2").hide();
            } else {
                $("#mb2").show();
                $("#mb").hide();
            }
            $('#wRelativeUnit_table').datagrid('unselectAll');
            $('#rproperty').textbox('setValue',node.id);
            WRelativeUnit.dtable.datagrid('load', {rproperty: node.id});
        },
        onLoadSuccess: function () {
            var node = WRelativeUnit.pTree.tree("find", 1);
            if(node) {
                WRelativeUnit.pTree.tree("select", node.target);
            }
        },
        onBeforeSelect: function (node) {
            if(WRelativeUnit.pTree.tree("isLeaf", node.target)) {
                return true;
            }
            return false;
        }
    })
};

$(function () {
    WRelativeUnit.sform = $('#wRelativeUnit_form');
    WRelativeUnit.dtable = $('#wRelativeUnit_table');
    WRelativeUnit.obtn = '#wRelativeUnit_btn';
    WRelativeUnit.pTree = $('#rpropertyTree');

    WRelativeUnit.dg(); // 数据表格
    WRelativeUnit.btn(); // 初始化按钮
    $yd.bindEnter(WRelativeUnit.search);
    WRelativeUnit.iProTree(); // pTree
    WRelativeUnit.init();
});
var btn1 = document.getElementById("searchButton");
btn1.addEventListener("click", function() {
    // $('#wRelativeUnit_table').datagrid('unselectAll');
    // WRelativeUnit.dtable.datagrid('load', WRelativeUnit.sform.serializeObject());
    $('#wRelativeUnit_table').datagrid('unselectAll');
    WRelativeUnit.dtable.datagrid('load', WRelativeUnit.sform.serializeObject());
});