var Part = {
    u: '/goodsPart'
    , uList: '/goodsPart/list'
    , uEdit: '/goodsPart/edit'
    , uDel: '/goodsPart/delete'
};

Date.prototype.format = function (format) {
    var o = {
        "M+": this.getMonth() + 1, //month
        "d+": this.getDate(),    //day
        "h+": this.getHours(),   //hour
        "m+": this.getMinutes(), //minute
        "s+": this.getSeconds(), //second
        "q+": Math.floor((this.getMonth() + 3) / 3),  //quarter
        "S": this.getMilliseconds() //millisecond
    }
    if (/(y+)/.test(format)) format = format.replace(RegExp.$1,
        (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o) if (new RegExp("(" + k + ")").test(format))
        format = format.replace(RegExp.$1,
            RegExp.$1.length == 1 ? o[k] :
                ("00" + o[k]).substr(("" + o[k]).length));
    return format;
};
Part.isEmpty = function (value) {
    if (value == undefined || value == null || value == "") {
        return true;
    }
    return false;
}

Part.dg = function () {
    var grid = new $yd.DataGrid(this.dtable);
    grid.onDblClickRow = function (index, row) {
        Part._edit(row['id']);
    };
    grid.onLoadSuccess = function () {
        var rows = Part.dtable.datagrid("getRows");
        if(rows.length > 0) {
            Part.dtable.datagrid("selectRow", 0);
        }
    };
    grid.onSelect = function (index, row) {
        Part.ptable.datagrid("load", {
            partId : row.id
        });
    };
    grid.pageSize = 10;
    grid.singleSelect = true;
    grid.rownumbers = true;
    grid.nowarp = false;
    grid.url = Part.uList;
    grid.columns = [[
        {field: 'id', checkbox: false, hidden: true}
        , {field: 'code', title: '拆装单号', width: 80}
        , {
            field: 'order_time', title: '单据日期', width: 80, formatter: function (value) {
                return new Date(value).format('yyyy-MM-dd');
            }
        }
        , {
            field: 'houseName', title: '仓库', width: 120
        }
        , {
            field: 'goodsName', title: '货品名称', width: 120
        }
        , {field: 'skuName', title: '货品规格', width: 80}
        , {
            field: 'unit', title: '单位', width: 50, formatter: function (value, row, index) {
                for (var i = 0; i < Part.unitArr.length; i++) {
                    if (value == Part.unitArr[i].id) {
                        return Part.unitArr[i].text;
                    }
                }
                return value;
            }
        }
        , {field: 'num', title: '货品数', width: 80}
        , {field: 'fee', title: '拆装总额', width: 80, formatter: function (value, row, index) {
                return Number(value).toFixed(2);
            }}
        , {
            field: 'status', title: '状态', width: 80, formatter: function (value) {
                return value == 0 ? "待审核" : "已审核";
            }
        }
        , {field: 'cname', title: '创建人', width: 80}
        , {field: 'ctime', title: '创建时间', width: 80}
        , {field: 'utime', title: '审核时间', width: 80}
    ]];
    grid.loadGrid();
};
Part.dg2 = function () {
    Part.ptable = $('#goodsPart_table2');
    Part.ptable.datagrid({
        url: "/goodsPartDetail/getDetailList",
        fitColumns: true,
        autoRowHeight: true,
        rownumbers: true,
        singleSelect: false,
        columns: [[
            {field: 'goodsName', title: '货品名称', width: 80},
            {field: 'skuName', title: '规格', width: 80},
            {field: 'code', title: '商品编号', width: 80},
            {
                field: 'unit', title: '单位', width: 50, formatter: function (value, row, index) {
                    for (var i = 0; i < Part.unitArr.length; i++) {
                        if (value == Part.unitArr[i].id) {
                            return Part.unitArr[i].text;
                        }
                    }
                    return value;
                }
            },
            {field: 'partsCode', title: '配件编号', width: 80},
            {field: 'price', title: '拆装单价', width: 80, formatter: function (value, row, index) {
                    return Number(value).toFixed(2);
                }},
            {field: 'num', title: '拆装数量', width: 80},
            {field: 'fee', title: '拆装总额', width: 80, formatter: function (value, row, index) {
                    return Number(value).toFixed(2);
                }},
            {field: 'houseName', hidden: true},
            {field: 'positionName', hidden: true},
            {field: 'house', title: '仓库/库位', width: 100, formatter: function (value, row, index) {
                    return row.houseName + " " + row.positionName;
                }}
        ]]
    });
};
Part.formatDetail = function (value) {
    if (Part.isEmpty(value)) {
        return "";
    }
    var result = "";
    var first = "";
    var arr = value.split(",");
    if (arr.length == 1) {
        return "<a style='color:purple' title='" + value + "'>" + value + "</a>";
    }
    for (var i = 0; i < arr.length; i++) {
        result += arr[i] + "\n";
        if (i == 0) {
            first = result;
        }
    }
    var html = "<a style='color:purple' title='" + result + "'>" + first + "..." + "</a>";

    return html;
};
Part.dg.getOne = function () {
    var sels = Part.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
Part.dg.getSels = function () {
    return Part.dtable.datagrid('getSelections');
};
// 重置
Part.reset = function () {
    $("form.search-form").form('clear');
    Part.search();
};
Part.audit = function () {
    var row = Part.dtable.datagrid('getSelected');
    if(!row) {
        $yd.alert("选择记录");
        return;
    }
    if(row.status == 1) {
        $yd.alert("无法重复审核");
        return;
    }
    $yd.confirm("审核后将生成相应的出入库单，确认信息无误？", function () {
        $yd.post("/goodsPart/audit",{id:row.id}, function (data) {
            if($yd.isEmpty(data.message) || data.message == "操作成功") {
                $yd.alert("操作成功", function () {
                    Part.search();
                })
            } else {
                $yd.alert(data.message);
            }
        });
    });
};
// 处理按钮
Part.btn = function () {
    var sb = new $yd.SearchBtn($(Part.obtn));
    sb.create('remove', '删除(DEL)', Part.remove);
    sb.create('edit', '审核(Shift+V)', Part.audit);
    sb.create('refresh', '刷新(F2)', Part.refresh);
    sb.create('edit', '编辑(Shift+E)', Part.edit);
    sb.create('plus', '新建(Shift+N)', Part.plus);
};
//删除
Part.remove = function () {
    var sel = Part.dg.getOne();
    if (!sel) {
        $yd.alert('选中记录');
        return;
    }
    if(sel.status == 1) {
        $yd.alert('已审核无法删除');
        return;
    }
    $yd.confirm(function () {
        $.post(Part.uDel, {ids: [sel.id]}, function (data) {
            if($yd.isEmpty(data.message) || data.message == "操作成功") {
                $yd.alert("操作成功", function () {
                    Part.search();
                })
            } else {
                $yd.alert(data.message);
            }
        });
    })
};

Part.edit = function () {
    var sel = Part.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    Part._edit(sel['id'])
};
// 表单预处理
Part.preForm = function () {
    new $yd.combobox("customerId").load("/customer/select");
};
Part._edit = function (id) {
    parent.inv.OpenPage("/goodsPart/edit?id=" + id, "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "                                <span class=\"menu-text\"> 拆装单详情 </span>\n" +
        "                                <b class=\"arrow \"></b>", true)
};
//新增
Part.plus = function () {
    parent.inv.OpenPage("/goodsPart/add", "<i class=\"menu-icon fa fa-flag\"></i>\n" +
        "                                <span class=\"menu-text\"> 新建拆装单 </span>\n" +
        "                                <b class=\"arrow \"></b>", true)
};
//
Part.refresh = function () {
    window.location.reload();
};
//搜索
Part.search = function () {
    Part.dtable.datagrid('load', Part.sform.serializeObject());
};
Part.keyDown = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (!e) {
        return;
    }
    // console.log("add.js")
};
Part.keyUp = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (!e) {
        return;
    }
    if (e.keyCode == 46) {//del
        Part.remove();
    }
    if (e.keyCode == 113) {//F2
        Part.refresh();
    }
    if (e.shiftKey && e.keyCode == 78) {
        Part.plus();//生单: Shift + N
    }
    if (e.shiftKey && e.keyCode == 69) {
        Part.edit();//生单: Shift + E
    }
    if (e.shiftKey && e.keyCode == 86) {
        Part.audit();//生单: Shift + V
    }
};
Part.init = function () {
    $.ajax({
        type: "POST",
        url: "/sysDict/materiel_MU",
        dataType: "json",
        success: function (data) {
            Part.unitArr = data;
        }
    });
    $("#houseId").combobox({
        prompt:'仓库',
        url: "/wWearhouse/list",
        panelHeight:'auto',
        editable:false,
        valueField:'id',
        textField:'name'
    });

    //快捷键
    document.onkeydown = Part.keyDown;
    document.onkeyup = Part.keyUp;

    //先获取焦点再失去焦点，否则打开页面后若不点击页面，快捷键无效
    $("#startTime").next().find(".textbox-text").focus()
    $("#startTime").next().find(".textbox-text").blur()
};
$(function () {
    Part.sform = $('#goodsPart_form');
    Part.dtable = $('#goodsPart_table');
    Part.obtn = '#goodsPart_btn';

    Part.init();
    Part.dg(); // 数据表格
    Part.btn(); // 初始化按钮
    $yd.bindEnter(Part.search);
    $("#searchButton").click(Part.search);

    Part.dg2(); // 数据表格
});