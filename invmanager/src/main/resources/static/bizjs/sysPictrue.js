<!-- sys_pictrue.ftl 图片资源 -->
var SysPictrue = {
    u: '/sysPictrue'
    , uList: '/sysPictrue/list'
    , uEdit: '/sysPictrue/edit'
    , uDel: '/sysPictrue/delete'
};

SysPictrue.picdg = function () {
    var grid1 = new $yd.DataGrid(this.pdtable);
    grid1.url = SysPictrue.uList + "?type=1";
    grid1.toolbar = '#picture_table_tb';
    grid1.columns = [[
        {field: 'id', checkbox: true}
        , {
            field: 'path', title: '图片路径', align: 'center', width: 100, formatter: function (value, row, index) {
                return '<img src="' + $.urlPre + value + '" style="width: auto;height: 60px;">';
            }
        }
        , {field: 'name', title: '图片名称', width: 80}
        , {field: 'size', title: '大小', width: 80}
        , {field: 'bsize', title: '尺寸/时长', width: 80}
        , {field: 'cuser', title: '创建人', width: 80}
        , {field: 'memo', title: '描述', width: 80}
        , {field: 'ctime', title: '创建时间', width: 80}
    ]];
    grid1.loadGrid();
    SysPictrue.picdg.initFileinput();
};
//图片上传
SysPictrue.picdg.initFileinput = function (id) {
    var ele = '#picture_input';
    $(ele).fileinput('destroy');
    var fileinput = new $yd.fileinput(ele);
    if (id) {
        fileinput.uploadUrl = fileinput.uploadUrl + "?pid=" + id;
    }
    fileinput.showPreview = false;
    fileinput.render();
    $(ele).on('filebatchuploadcomplete', function () {
        SysPictrue.pdtable.datagrid('load', {pid: id});
    });
};
SysPictrue.dg = function () {
    var grid = new $yd.TreeGrid(this.dtable);
    grid.onClickRow = function (row) {
        SysPictrue.pdtable.datagrid('load', {pid: row.id});
        SysPictrue.picdg.initFileinput(row.id);
    };
    grid.toolbar = SysPictrue.obtn;
    grid.url = SysPictrue.uList + "?type=0";
    grid.pagination = false;
    grid.columns = [[{field: 'name', title: '文件夹名称', width: 120}]];
    grid.loadGrid();
};
SysPictrue.dg.getOne = function () {
    var sels = SysPictrue.dg.getSels();
    if (sels.length === 1) {
        return sels[0];
    }
    return null;
};
SysPictrue.dg.getSels = function () {
    return SysPictrue.dtable.treegrid('getSelections');
};
// 处理按钮
SysPictrue.btn = function () {
    var sb = new $yd.SearchBtn($(SysPictrue.obtn));
    // sb.create('remove', '删除', SysPictrue.remove);
    sb.create('refresh', '刷新', SysPictrue.refresh);
    sb.create('edit', '编辑', SysPictrue.edit);
    sb.create('plus', '新增', SysPictrue.plus);
};
//删除
SysPictrue.remove = function () {
    var sels = SysPictrue.dg.getSels();
    if (!sels) {
        $yd.alert('请选中记录');
        return;
    }
    $yd.confirm(function () {
        var ids = [];
        $.each(sels, function (i, n) {
            ids.push(n['id']);
        });
        $yd.post(SysPictrue.uDel, {ids: ids}, function () {
            SysPictrue.refresh();
        });
    })
};
//
SysPictrue.edit = function () {
    var sel = SysPictrue.dg.getOne();
    if (!sel) {
        $yd.alert('请选择一条记录');
        return;
    }
    SysPictrue._edit(sel['id'])
};
SysPictrue._edit = function (id) {
    $yd.win.create('编辑图片资源', SysPictrue.uEdit + '?id=' + id).open(function (that, $win) {
            $yd.get(SysPictrue.u + "/" + id, function (data) {
                $win.find('form').form('load', data);
            })
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysPictrue.u).submit(function () {
                that.close();
                SysPictrue.refresh();
            });
        }
    );
};
//新增
SysPictrue.plus = function () {
    var sel = SysPictrue.dg.getOne();
    var href = SysPictrue.uEdit;
    if (sel) href += '?pid=' + sel['id'] + '&name=' + sel['name'];
    $yd.win.create('新增图片资源', href).open(function (that, $win) {
        }, function (that, $win) {
            $yd.form.create($win.find('form')).url(SysPictrue.u).submit(function () {
                that.close();
                SysPictrue.refresh();
            });
        }
    );
};
//
SysPictrue.refresh = function () {
    SysPictrue.dtable.treegrid('reload');
};

$(function () {
    SysPictrue.sform = $('#sysPictrue_form');
    SysPictrue.dtable = $('#sysPictrue_table');
    SysPictrue.pdtable = $('#picture_table');
    SysPictrue.obtn = '#sysPictrue_btn';

    SysPictrue.dg(); // 数据表格
    SysPictrue.btn(); // 初始化按钮
    SysPictrue.picdg(); // 数据表格
});