package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.CarBrandMateriel;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-09-20
 */
public interface ICarBrandMaterielService extends IService<CarBrandMateriel> {

    Page selectPage(CarBrandMateriel carBrandMateriel, MyPage<CarBrandMateriel> page);

    void delete(List<Long> ids);

}
