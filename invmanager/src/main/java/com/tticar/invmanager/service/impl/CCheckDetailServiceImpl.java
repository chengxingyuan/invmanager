package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.entity.CCheck;
import com.tticar.invmanager.entity.CCheckDetail;
import com.tticar.invmanager.entity.RepositoryCount;
import com.tticar.invmanager.mapper.CCheckDetailMapper;
import com.tticar.invmanager.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 盘点单详情 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-13
 */
@Service
public class CCheckDetailServiceImpl extends ServiceImpl<CCheckDetailMapper, CCheckDetail> implements ICCheckDetailService {

    @Autowired
    ICCheckService checkService;
    @Autowired
    IMMaterielService materielService;
    @Autowired
    IMMaterielSkuService materielSkuService;
    @Autowired
    private IInRepositoryDetailService inRepositoryDetailService;
    @Autowired
    private IRepositoryCountService repositoryCountService;

    @Override
    public List selectMapList(Long checkId) {
        Wrapper wrapper = new EntityWrapper();
        wrapper.eq(CCheckDetail.CHECK_ID, checkId);
        return this.selectMaps(wrapper);
    }

    @Override
    public List<Map> getDetailListById(Long checkId) {
        CCheck check = this.checkService.selectById(checkId);
        List<Map> list = this.baseMapper.getDetailListById(checkId);
        List<Long> skuIdList = new ArrayList<>();
        for (Map map : list) {
            skuIdList.add(Long.valueOf(map.get("skuId").toString()));
        }
        Wrapper wrapper = new EntityWrapper<>();
        wrapper.eq("wearhouse_id", check.getHouseId());
        wrapper.in("sku_id", skuIdList);
        List<RepositoryCount> numList = this.repositoryCountService.selectList(wrapper);

        Map<String, Integer> numMap = new HashMap<>();
        for (RepositoryCount rc : numList) {
            String key = rc.getSkuId().toString();
            numMap.put(key, rc.getCount());
        }

        for (Map map : list) {
            String key = map.get("skuId").toString();
            map.put("curNum", numMap.get(key) == null ? 0 : numMap.get(key));
        }
        return list;
    }

}
