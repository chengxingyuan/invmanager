package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.MMaterielTticar;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.RelativeGoodsDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 天天爱车产品 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-23
 */
public interface IMMaterielTticarService extends IService<MMaterielTticar> {

    Page selectPage(MMaterielTticar mMaterielTticar, Map param, MyPage<MMaterielTticar> page);

    void delete(List<Long> ids);

    List<RelativeGoodsDTO> queryRelativeGoodsByTticarGoods(Long tticarGoodsId,String skuValue);

    void cancelRelative(Long skuId,String tticarGoodsId,String skuValue);
}
