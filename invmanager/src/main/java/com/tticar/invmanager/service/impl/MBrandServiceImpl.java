package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.common.Enum.Status;
import com.tticar.invmanager.entity.MBrand;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.MBrandMapper;
import com.tticar.invmanager.service.IMBrandService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 存货品牌 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
@Service
public class MBrandServiceImpl extends ServiceImpl<MBrandMapper, MBrand> implements IMBrandService {

    @Override
    public Page selectPage(MBrand mBrand, MyPage<MBrand> page) {
        Wrapper wrapper = new EntityWrapper();
        if (mBrand.getStatus() == null) {
            wrapper.ne(Status.STATUS.getDesc(), Status.DELETE.getCode());
        } else {
            wrapper.eq(Status.STATUS.getDesc(), mBrand.getStatus());
        }
        wrapper.like(Strings.isNotEmpty(mBrand.getName()), MBrand.NAME, mBrand.getName());
        wrapper.orderBy("ctime", false);
        return this.selectMapsPage(page, wrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            MBrand mBrand = new MBrand();
            mBrand.setId(id);
            //删除为-1，区分删除和禁用
            mBrand.setStatus(-1);
            list.add(mBrand);
        }
        this.updateBatchById(list);
    }

}
