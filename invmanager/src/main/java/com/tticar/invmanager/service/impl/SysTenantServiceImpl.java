package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.entity.Columns;
import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.common.utils.MD5Utils;
import com.tticar.invmanager.entity.SysTenant;
import com.tticar.invmanager.entity.SysUser;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.SysTenantMapper;
import com.tticar.invmanager.service.ISysTenantService;
import com.tticar.invmanager.service.ISysUserService;
import org.apache.commons.collections.MapUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 租户 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
@Service
public class SysTenantServiceImpl extends ServiceImpl<SysTenantMapper, SysTenant> implements ISysTenantService {

    @Autowired
    private ISysUserService sysUserService;

    @Override
    public Page selectPage(SysTenant sysTenant, MyPage<SysTenant> page) {
        Wrapper wrapper = new EntityWrapper();
        wrapper.like(Strings.isNotEmpty(sysTenant.getName()), SysTenant.NAME, sysTenant.getName());
        return this.selectMapsPage(page, wrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            SysTenant sysTenant = new SysTenant();
            sysTenant.setId(id);
            //删除为-1，区分删除和禁用
            sysTenant.setStatus(-1);
            list.add(sysTenant);
        }
        this.updateBatchById(list);
    }

    @Override
    public List select() {
        Wrapper wrapper = new EntityWrapper();
        wrapper.setSqlSelect(Columns.create().column("id").column("name"));
        return this.selectMaps(wrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(SysTenant sysTenant, SysUser sysUser, String newpassword) {
        this.insertOrUpdate(sysTenant);
        sysUser.setId(sysTenant.getSysUserId());
        sysUser.setRoleId(2l);
        sysUser.setRoleName("管理员");

        if (StringUtils.hasText(newpassword)) {
            sysUser.setPassword(MD5Utils.generatePasswordMD5(newpassword, null));
        } else {
            if (sysUser.getId() == null) {
                String mobile = sysUser.getMobile();
                newpassword = mobile.substring(mobile.length() - 6, mobile.length());
                sysUser.setPassword(MD5Utils.generatePasswordMD5(newpassword, null));
            }
        }
        if (sysUser.getId() != null) {
            sysUserService.update(sysUser, Condition.create().isNotNull(SysUser.TENANT_ID).eq(SysUser.ID, sysUser.getId()));
        } else {
            sysUserService.insertOrUpdate(sysUser);
            sysUser.setPassword(null);
            sysUser.setTenantId(sysTenant.getId());
            sysUserService.updateById(sysUser);
            sysTenant.setSysUserId(sysUser.getId());
            this.insertOrUpdate(sysTenant);
        }
    }

    @Override
    public Map getById(Long id) {
        Map map = this.selectMap(Condition.create().eq(SysTenant.ID, id));
        Long userId = MapUtils.getLong(map, "sysUserId");

        if (userId != null) {
            Wrapper wrapper = Condition.create().isNotNull(SysUser.TENANT_ID).eq(SysUser.ID, userId);
            SysUser sysUser = sysUserService.selectOne(wrapper);
            map.put(SysUser.USERNAME, sysUser.getUsername());
            map.put(SysUser.NICKNAME, sysUser.getNickname());
        }
        return map;
    }

}
