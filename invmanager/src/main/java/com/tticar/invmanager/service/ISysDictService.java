package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.SysDict;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.vo.DictCodes;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-08
 */
public interface ISysDictService extends IService<SysDict> {

    Page selectPage(SysDict sysDict, MyPage<SysDict> page);

    void delete(List<Long> ids);

    Map selMap(DictCodes dictCode);

    List selList(DictCodes dictCode);

    List selTree(DictCodes dictCode);

    void edit(SysDict sysDict);

    void formSubmit(Map map);

    Map loadForm(DictCodes id);

    void loadToLocal();
}
