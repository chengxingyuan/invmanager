package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.SaleReturn;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.PurchaseAddDto;
import com.tticar.invmanager.entity.vo.PurchaseGoodsDto;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 销售退货表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2019-01-04
 */
public interface ISaleReturnService extends IService<SaleReturn> {

    Page selectPage(SaleReturn saleReturn, MyPage<SaleReturn> page);

    void delete(List<Long> ids);

    List<Map> detailGoodsList(String code);

    List<Map> getSaleDetailList(String saleReturnCode);

    void inRepositoryForSaleReturn(String saleReturnCode, List<PurchaseGoodsDto> goodsList, BigDecimal realPay, BigDecimal totalMoney, Integer payStatus, String remark);

    void endCountSaleReturn(String saleReturnCode,BigDecimal realPay,BigDecimal totalMoney,Integer payStatus,String remark);

    void cancelInForSaleReturn(String saleReturnCode);

    void cancelEndCount(String saleReturnCode);

    void addSaleReturn(PurchaseAddDto dto) throws ParseException;

    String allReturnForSale(Long id);
}
