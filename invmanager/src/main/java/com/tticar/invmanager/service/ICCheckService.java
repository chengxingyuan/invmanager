package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.CCheck;
import com.tticar.invmanager.entity.vo.CheckAddDto;
import com.tticar.invmanager.entity.vo.CheckDetailDto;
import com.tticar.invmanager.entity.vo.MyPage;

import java.text.ParseException;
import java.util.List;

/**
 * <p>
 * 盘点单 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-13
 */
public interface ICCheckService extends IService<CCheck> {

    Page selectPage(CCheck check, MyPage<CCheck> page);

    void delete(List<Long> ids);

    void addCheck(Long houseId, List<CheckAddDto> list);

    String correct(Long checkId, List<CheckDetailDto> detailList) throws ParseException;
}
