package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.shiro.LoginUser;
import com.tticar.invmanager.entity.SysUser;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
/**
 * <p>
 * 管理员账号 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
public interface ISysUserService extends IService<SysUser> {

    Page selectPage(SysUser sysUser, MyPage<SysUser> page);

    List selectList(SysUser sysUser);

    void delete(List<Long> ids);

    LoginUser getLoginUser(String username);

    void logLogin(LoginUser loginUser);

    void edit(SysUser sysUser, String newPassword);
}
