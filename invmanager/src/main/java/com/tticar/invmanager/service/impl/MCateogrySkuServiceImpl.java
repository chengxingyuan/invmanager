package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.entity.Column;
import com.baomidou.mybatisplus.entity.Columns;
import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.common.Enum.Status;
import com.tticar.invmanager.common.exception.MyServiceException;
import com.tticar.invmanager.entity.MCateogry;
import com.tticar.invmanager.entity.MCateogrySku;
import com.tticar.invmanager.entity.MSku;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.MCateogryMapper;
import com.tticar.invmanager.mapper.MCateogrySkuMapper;
import com.tticar.invmanager.service.IMCateogryService;
import com.tticar.invmanager.service.IMCateogrySkuService;
import com.tticar.invmanager.service.IMSkuService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 存货分类sku表 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-03
 */
@Service
public class MCateogrySkuServiceImpl extends ServiceImpl<MCateogrySkuMapper, MCateogrySku> implements IMCateogrySkuService {

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateList(List<MCateogrySku> cateogrySkuList, Long cateogryId) {
        if (cateogryId != null && cateogryId != 0) {
            Wrapper wrapper = new EntityWrapper<>();
            wrapper.eq(MCateogrySku.CATEGORY_ID, cateogryId);
            this.delete(wrapper);
        }
        if (CollectionUtils.isNotEmpty(cateogrySkuList)) {
            for (MCateogrySku m : cateogrySkuList) {
                    this.insert(m);
            }
        }

    }
}
