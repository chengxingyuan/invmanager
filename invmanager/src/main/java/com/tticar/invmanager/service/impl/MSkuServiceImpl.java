package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.common.Enum.CodeType;
import com.tticar.invmanager.common.Enum.Status;
import com.tticar.invmanager.common.exception.MyServiceException;
import com.tticar.invmanager.entity.MCateogrySku;
import com.tticar.invmanager.entity.MSku;
import com.tticar.invmanager.entity.MSkuValue;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.MSkuMapper;
import com.tticar.invmanager.service.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 物料规格 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-05
 */
@Service
public class MSkuServiceImpl extends ServiceImpl<MSkuMapper, MSku> implements IMSkuService {

    @Autowired
    IMSkuValueService skuValueService;
    @Autowired
    IMCateogrySkuService cateogrySkuService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void insertOrUpdateOne(MSku mSku) {
        if (mSku.getId() != null && mSku.getId() != 0) {
            // 校验：如果此数据已被使用，不可禁用或删除
            checkIsUsed(mSku.getId(), mSku.getStatus());
            // 更新sku值的状态
            Wrapper wrapper = new EntityWrapper();
            wrapper.eq(MSkuValue.SKU_ID, mSku.getId());
            MSkuValue skuValue = new MSkuValue();
            skuValue.setStatus(mSku.getStatus());
            skuValueService.update(skuValue, wrapper);
        }
        this.insertOrUpdate(mSku);

        // 新增分类sku
//        if (mSku.getCategoryIds() != null && mSku.getCategoryIds().length > 0) {
//            for (Long categoryId : mSku.getCategoryIds()) {
//                MCateogrySku cateogrySku = new MCateogrySku();
//                cateogrySku.setCategoryId(categoryId);
//                cateogrySku.setSkuId(mSku.getId());
//                cateogrySkuService.insert(cateogrySku);
//            }
//        }

    }

    @Override
    public Page selectPage(MSku mSku, MyPage<MSku> page) {
        Wrapper wrapper = new EntityWrapper();
        wrapper.ne(MSku.STATUS, Status.DELETE.getCode());
        wrapper.like(Strings.isNotEmpty(mSku.getName()), MSku.NAME, mSku.getName());
        wrapper.orderBy("sort", true);
        return this.selectMapsPage(page, wrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<Long> ids) throws Exception {
        if (CollectionUtils.isEmpty(ids)) {
            return;
        }

        Wrapper wrapper = new EntityWrapper();
        wrapper.in("sku_id", ids);
        int count = this.cateogrySkuService.selectCount(wrapper);
        if(count > 0) {
            throw new MyServiceException("规格已关联分类，无法删除");
        }

        List list = new ArrayList();
        for (Long id : ids) {
            // 校验：如果此数据已被使用，不可禁用或删除
            checkIsUsed(id, Status.DELETE.getCode());
            MSku mSku = new MSku();
            mSku.setId(id);
            //删除为-1，区分删除和禁用
            mSku.setStatus(-1);
            list.add(mSku);
        }
        this.updateBatchById(list);

        // 删除关联数据
        wrapper = new EntityWrapper();
        wrapper.in(MSkuValue.SKU_ID, ids);
        MSkuValue skuValue = new MSkuValue();
        skuValue.setStatus(-1);
        skuValueService.update(skuValue, wrapper);
    }

    private void checkIsUsed(Long id, int status) {
        if (status == Status.DELETE.getCode() || status == Status.INVALID.getCode()) {
            Wrapper wrapper = new EntityWrapper<>();
            wrapper.eq("sku_id", id);
            List list = cateogrySkuService.selectList(wrapper);
            if (CollectionUtils.isNotEmpty(list)) {
                MSku sku = this.selectById(id);
                String msg = "此数据已被使用，不可禁用或删除";
                if (sku != null) {
                    msg = "（"+ sku.getName() + ")" + msg;
                }
               throw new MyServiceException(msg);
            }
        }
    }

}
