package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.entity.Column;
import com.baomidou.mybatisplus.entity.Columns;
import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.common.Enum.Status;
import com.tticar.invmanager.common.exception.MyServiceException;
import com.tticar.invmanager.entity.SysRole;
import com.tticar.invmanager.entity.SysRoleResource;
import com.tticar.invmanager.entity.SysUser;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.SysRoleMapper;
import com.tticar.invmanager.mapper.SysRoleResourceMapper;
import com.tticar.invmanager.service.ISysRoleService;
import com.tticar.invmanager.service.ISysUserService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Autowired
    private SysRoleResourceMapper roleResourceMapper;

    @Autowired
    private ISysUserService sysUserService;

    @Override
    public Page selectPage(SysRole sysRole, MyPage<SysRole> page) {
        Wrapper wrapper = new EntityWrapper();
        wrapper.ne(Status.STATUS.getDesc(), Status.DELETE.getCode());
        wrapper.like(Strings.isNotEmpty(sysRole.getName()), SysRole.NAME, sysRole.getName());
        return this.selectMapsPage(page, wrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            Wrapper wrapper = new EntityWrapper<>();
            wrapper.ne("status", -1);
            wrapper.eq("role_id", id);
            List<SysUser> sysUsers = sysUserService.selectList(wrapper);
            if (sysUsers != null && sysUsers.size() > 0) {
                throw new MyServiceException("角色已经被使用，禁止删除");
            }
            SysRole sysRole = new SysRole();
            sysRole.setId(id);
            //删除为-1，区分删除和禁用
            sysRole.setStatus(-1);
            list.add(sysRole);
        }
        this.updateBatchById(list);
    }

    @Override
    public List getResourceByRoleId(Long roleId) {
        Wrapper<SysRoleResource> wrapper = Condition.create();
        wrapper.eq(SysRoleResource.ROLE_ID, roleId);
        wrapper.setSqlSelect(Column.create().column(SysRoleResource.RESOURCE_ID));
        return roleResourceMapper.selectObjs(wrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertOrUpdate(List<SysRoleResource> roleResources) {
        if (!roleResources.isEmpty()) {
            for (SysRoleResource roleResource : roleResources) {
                Long id = roleResource.getId();
                if (id == -1) {
                    roleResource.setId(null);
                    roleResourceMapper.delete(new EntityWrapper(roleResource));
                } else {
                    roleResource.setId(null);
                    Integer count = roleResourceMapper.selectCount(new EntityWrapper(roleResource));
                    if (count == 0) {
                        roleResourceMapper.insert(roleResource);
                    }
                }
            }
        }
    }

    @Override
    public List select() {
        Wrapper wrapper = Condition.wrapper();
        wrapper.eq("status", 0);
        wrapper.setSqlSelect(Columns.create().column("id").column("name"));
        return this.selectMaps(wrapper);
    }

}
