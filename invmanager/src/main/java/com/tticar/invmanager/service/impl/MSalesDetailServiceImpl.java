package com.tticar.invmanager.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.entity.MSales;
import com.tticar.invmanager.entity.MSalesDetail;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.MSalesDetailMapper;
import com.tticar.invmanager.service.IInRepositoryDetailService;
import com.tticar.invmanager.service.IMSalesDetailService;
import com.tticar.invmanager.service.IMSalesService;
import com.tticar.invmanager.service.IRepositoryCountService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-15
 */
@Service
public class MSalesDetailServiceImpl extends ServiceImpl<MSalesDetailMapper, MSalesDetail> implements IMSalesDetailService {

    @Autowired
    private IRepositoryCountService repositoryCountService;

    @Autowired
    private IMSalesService salesService;

    @Autowired
    private IInRepositoryDetailService inRepositoryDetailService;

    @Override
    public Page selectPage(MSalesDetail mSalesDetail, MyPage<MSalesDetail> page) {
        Wrapper wrapper = new EntityWrapper();
        //wrapper.like(Strings.isNotEmpty(mSalesDetail.getName()), MSalesDetail.NAME, mSalesDetail.getName());
        return this.selectMapsPage(page, wrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            MSalesDetail mSalesDetail = new MSalesDetail();
            mSalesDetail.setId(id);
            mSalesDetail.setStatus(1);
            list.add(mSalesDetail);
        }
        this.updateBatchById(list);
    }

    @Override
    public List<Map> getSalesGoods(String salesIds) {
        return this.baseMapper.getSalesGoods(salesIds);
    }

    @Override
    public List<Map> getSalesDetailList(Long salesId) {
        MSales sales = this.salesService.selectById(salesId);
        List<Map> list = this.baseMapper.getSalesDetailList(salesId);
        String skuIds = "";
        for(Map map : list) {
            skuIds += "," + map.get("skuId");
        }
        skuIds = skuIds.substring(1);
        List<Map> skuList = repositoryCountService.getSkuNum(skuIds);
        Map<Long, BigDecimal> skuMap = new HashMap<>();
        for(Map<String, Object> tmp : skuList) {
            skuMap.put(Long.valueOf(tmp.get("skuId")+""), (BigDecimal) tmp.get("skuNum"));
        }

        for(Map map : list) {
            map.put("skuNum", skuMap.get(map.get("skuId")));
        }

        //仓库、库位、批次：未入库时取默认，已入库取出库记录
        if(sales.getOrderStatus() != 0) {
            List<Map> houseList = this.baseMapper.getSalesHouseList(salesId);
            Map<String, String> houseMap = new HashMap<>();
            for(Map<String, Object> tmp : houseList) {
                houseMap.put(tmp.get("skuId").toString(), (String) tmp.get("house"));
            }
            for(Map map : list) {
                map.put("house", houseMap.get(map.get("skuId").toString()));
            }
        }
//        List<Map> houseList;
//        if(sales.getOrderStatus() == 0) {
//            houseList = inRepositoryDetailService.getSkuBatchCodeForOut(skuIds);
//        } else {
//            houseList = this.baseMapper.getSalesHouseList(salesId);
//        }
        return list;
    }
}
