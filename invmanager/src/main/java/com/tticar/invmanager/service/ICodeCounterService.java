package com.tticar.invmanager.service;

import com.tticar.invmanager.common.Enum.CodeType;
import com.tticar.invmanager.entity.CodeCounter;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;

/**
 * <p>
 * 单号计数器 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-01
 */
public interface ICodeCounterService extends IService<CodeCounter> {

    /**
     * 生成编码
     * @param codeType 编码类型前缀
     * @param middle 编码中间部分
     * @param length 序号长度
     * @return
     */
    String getCode(CodeType codeType, String middle, int length);

    /**
     * 生成单号
     * @param codeType 编码类型前缀
     * @return
     */
    String getOrderCode(CodeType codeType);

    String getOrderCodeByTenantId(CodeType codeType,Long tenantId);

    /**
     * 生成批号
     * @return
     */
    String getBatchCode();

}
