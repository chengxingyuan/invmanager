package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.BybsDetail;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 报溢报损详情 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-18
 */
public interface IBybsDetailService extends IService<BybsDetail> {

    Page selectPage(BybsDetail bybsDetail, MyPage<BybsDetail> page);

    void delete(List<Long> ids);

    List<Map> getDetailList(Long id);
}
