package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.CarBrandMateriel;
import com.tticar.invmanager.entity.MMateriel;
import com.tticar.invmanager.entity.MMaterielSku;
import com.tticar.invmanager.entity.vo.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 存货表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-09
 */
public interface IMMaterielService extends IService<MMateriel> {

    Page selectPage(MMateriel mMateriel, MyPage<MMateriel> page);

    void delete(List<Long> ids);

    void edit(MMateriel mMateriel, List<MMaterielSku> mMaterielSkus, String house, List<CarBrandMateriel> carBrandList);

    Object getById(Long id);

    void edit(MMaterielSku mMaterielSku);

    Page list(MMateriel mMateriel, MyPage<MMateriel> page);

    List<Tree> tree(Long pid, Integer type);

    List<Tree> tree(String searchText, Integer type);

    /**
     * 根据商品名字（模糊查询）查出包含该名字的 skuId集合
     * */
    List<Long> selectMaterielIdByName(String materielName);

    List<GoodsTreeGrid> treegridForCheck(GoodsTreeGridSearchDto searchDto);

    /**
     * 导入存货商品excel
     * @param list
     */
    void importExcel(List<List<String>> list);

    Page search(MyPage<MMateriel> page, String searchText, String searchCar, Long houseId, Long bindHouseId);

    Page searchForRelative(MyPage<MMateriel> page, String searchText, String searchCar, Long houseId, Long bindHouseId);

    List queryInvMaterielList(Map map);

    MMateriel selectNotTenantId(Long id,Long tenantId);
}
