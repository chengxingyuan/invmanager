package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.CarBrandTmp;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;

/**
 * <p>
 * 车品牌 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-09-19
 */
public interface ICarBrandTmpService extends IService<CarBrandTmp> {

    Page selectPage(CarBrandTmp carBrandTmp, MyPage<CarBrandTmp> page);

    void delete(List<Long> ids);

}
