package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.WRelativeUnit;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
/**
 * <p>
 * 往来客户 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-08
 */
public interface IWRelativeUnitService extends IService<WRelativeUnit> {

    Page selectPage(WRelativeUnit wRelativeUnit, MyPage<WRelativeUnit> page);

    void delete(List<Long> ids);

    List<Long> queryOrderIdByRelativeId(Long relativeId);

    void insertExcel( List<List<String>> list);

    void isExist(WRelativeUnit wRelativeUnit);

}
