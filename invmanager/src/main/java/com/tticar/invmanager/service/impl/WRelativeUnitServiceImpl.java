package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.common.Enum.CodeType;
import com.tticar.invmanager.common.exception.MyServiceException;
import com.tticar.invmanager.entity.WRelativeUnit;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.WRelativeUnitMapper;
import com.tticar.invmanager.service.ICodeCounterService;
import com.tticar.invmanager.service.IWRelativeUnitService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 往来客户 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-08
 */
@Service
public class WRelativeUnitServiceImpl extends ServiceImpl<WRelativeUnitMapper, WRelativeUnit> implements IWRelativeUnitService {

    @Autowired
    private ICodeCounterService codeCounterService;

    @Override
    public Page selectPage(WRelativeUnit wRelativeUnit, MyPage<WRelativeUnit> page) {
        return page.setRecords(this.baseMapper.search(page, wRelativeUnit));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            //如果供应商或者客户存在未完结的订单，则禁止删除
            List<Long> longs = queryOrderIdByRelativeId(id);
            if (longs.size() > 0) {
                throw new MyServiceException("该客户/供应商存在为完成的订单，禁止删除");
            }
            WRelativeUnit wRelativeUnit = new WRelativeUnit();
            wRelativeUnit.setId(id);
            //删除为-1，区分删除和禁用
            wRelativeUnit.setStatus(-1);
            list.add(wRelativeUnit);
        }
        this.updateBatchById(list);
    }

    /**
     * 查询供应商/客户 是否有未完结的订单
     * */
    @Override
    public List<Long> queryOrderIdByRelativeId(Long relativeId) {
        List<Long> longs = this.baseMapper.queryOrderIdByRelativeId(relativeId);
        return longs;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertExcel(List<List<String>> listByExcel) {
        for (int i = 0; i < listByExcel.size(); i++) {
            WRelativeUnit wRelativeUnit = new WRelativeUnit();
            //第一列名称
            if (StringUtils.isNotBlank( listByExcel.get(i).get(0))) {
                wRelativeUnit.setName((listByExcel.get(i).get(0)).trim());
            }else {
                throw new MyServiceException("第" + (i+2) +"行，名称列不能为空");
            }
            //第二列简称
            wRelativeUnit.setSimpleName(listByExcel.get(i).get(1));
            //第三列合作性质：客户/供应商
            if (StringUtils.isNotBlank( listByExcel.get(i).get(2))) {
                String code = "";
                if ("客户".equals(( listByExcel.get(i).get(2)).trim())) {
                    wRelativeUnit.setRproperty(1);
                    code = codeCounterService.getCode(CodeType.KH, "", 6);
                }else if ("供应商".equals((listByExcel.get(i).get(2)).trim())){
                    wRelativeUnit.setRproperty(2);
                    code = codeCounterService.getCode(CodeType.GYS, "", 4);
                }else {
                    throw new MyServiceException("第" + (i+2) +"行，客户/供应商列请正确填写");
                }
                wRelativeUnit.setCode(code);
            }
            //第四列：会员类型
            if (StringUtils.isNotBlank(listByExcel.get(i).get(3))){
                if ("一级会员".equals((listByExcel.get(i).get(3)).trim())){
                    wRelativeUnit.setMemberType(1);
                }else if ("二级会员".equals((listByExcel.get(i).get(3)).trim())){
                    wRelativeUnit.setMemberType(2);
                }
            }
            //第五列：联系人
            wRelativeUnit.setContactor((listByExcel.get(i).get(4)).trim());
            //第六列：联系人电话
            wRelativeUnit.setTel((listByExcel.get(i).get(5)).trim());
            //第七列：地址
            wRelativeUnit.setAddr((listByExcel.get(i).get(6)).trim());
            isExistForExcel(wRelativeUnit,i);
            wRelativeUnit.setStatus(0);
            insert(wRelativeUnit);
        }
    }
    @Override
    public void isExist(WRelativeUnit wRelativeUnit) {
        Wrapper wrapper = new EntityWrapper<>();
        wrapper.ne("status", -1);
        wrapper.eq("rproperty", wRelativeUnit.getRproperty());
        List<WRelativeUnit> wRelativeUnits = selectList(wrapper);
        if (wRelativeUnit.getId() == null) {
            if (wRelativeUnits != null && wRelativeUnits.size() > 0) {
                for (WRelativeUnit wRelativeUnit1 : wRelativeUnits) {
                    if (wRelativeUnit1.getName().equals(wRelativeUnit.getName())) {
                        throw new MyServiceException("该客户/供应商已存在");
                    }
                }
            }
        }else {
            if (wRelativeUnits != null && wRelativeUnits.size() > 0) {
                for (WRelativeUnit wRelativeUnit1 : wRelativeUnits) {
                    if (wRelativeUnit1.getName().equals(wRelativeUnit.getName()) && !wRelativeUnit1.getId().equals(wRelativeUnit.getId())) {
                        throw new MyServiceException("该客户/供应商已存在");
                    }
                }
            }
        }
    }

    public void isExistForExcel(WRelativeUnit wRelativeUnit,int i) {
        Wrapper wrapper = new EntityWrapper<>();
        wrapper.ne("status", -1);
        wrapper.eq("rproperty", wRelativeUnit.getRproperty());
        List<WRelativeUnit> wRelativeUnits = selectList(wrapper);
            if (wRelativeUnits != null && wRelativeUnits.size() > 0) {
                for (WRelativeUnit wRelativeUnit1 : wRelativeUnits) {
                    if (wRelativeUnit1.getName().equals(wRelativeUnit.getName())) {
                        throw new MyServiceException("第"+(i+2)+"行客户/供应商已存在");
                    }
                }
            }
    }


}
