package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.MSalesDetail;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-15
 */
public interface IMSalesDetailService extends IService<MSalesDetail> {

    Page selectPage(MSalesDetail mSalesDetail, MyPage<MSalesDetail> page);

    void delete(List<Long> ids);

    List<Map> getSalesGoods(String salesIds);

    List<Map> getSalesDetailList(Long salesId);
}
