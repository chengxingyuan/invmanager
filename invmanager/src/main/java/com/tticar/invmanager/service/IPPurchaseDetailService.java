package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.PPurchaseDetail;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 采购单详情 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-03
 */
public interface IPPurchaseDetailService extends IService<PPurchaseDetail> {

    List selectMapList(Long purchaseId);

    Page selectPage(PPurchaseDetail pPurchaseDetail, MyPage<PPurchaseDetail> page);

    void delete(List<Long> ids);

    void detailInRepository(List<PPurchaseDetail> details, Long purchaseId);

}
