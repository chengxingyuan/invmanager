package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.common.Enum.Status;
import com.tticar.invmanager.entity.WPositionMateriel;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.WPositionMaterielMapper;
import com.tticar.invmanager.service.IWPositionMaterielService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 库位对应的商品 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-23
 */
@Service
public class WPositionMaterielServiceImpl extends ServiceImpl<WPositionMaterielMapper, WPositionMateriel> implements IWPositionMaterielService {

    @Override
    public Page selectPage(WPositionMateriel wPositionMateriel, MyPage<WPositionMateriel> page) {
        Wrapper wrapper = new EntityWrapper();
        wrapper.ne(Status.STATUS.getDesc(), Status.DELETE.getCode());
        //wrapper.like(Strings.isNotEmpty(wPositionMateriel.getName()), WPositionMateriel.NAME, wPositionMateriel.getName());
        return this.selectMapsPage(page, wrapper);
    }
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            WPositionMateriel wPositionMateriel = new WPositionMateriel();
            wPositionMateriel.setId(id);
            //删除为-1，区分删除和禁用
            wPositionMateriel.setStatus(-1);
            list.add(wPositionMateriel);
        }
        this.updateBatchById(list);
    }
    @Override
    public String getBindInfo(Long materielId) {
        Map map = this.baseMapper.getBindInfo(materielId);
        if(map == null || map.get("info") == null) {
            return "";
        }
        return map.get("info").toString();
    }

}
