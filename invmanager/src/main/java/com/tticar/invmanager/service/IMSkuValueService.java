package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.MSkuValue;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
/**
 * <p>
 * 物料规格值 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-05
 */
public interface IMSkuValueService extends IService<MSkuValue> {

    Page selectPage(MSkuValue mSkuValue, MyPage<MSkuValue> page);

    List selectList(MSkuValue mSkuValue);

    void delete(List<Long> ids);

    void insertOrUpdateOne(MSkuValue mSkuValue);

}
