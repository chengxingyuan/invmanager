package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.WStore;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;

/**
 * <p>
 * 门店 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-24
 */
public interface IWStoreService extends IService<WStore> {

    Page selectPage(WStore wStore, MyPage<WStore> page);

    void delete(List<Long> ids);

    List select(WStore wStore);
}
