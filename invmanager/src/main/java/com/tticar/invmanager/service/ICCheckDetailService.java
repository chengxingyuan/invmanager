package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.CCheckDetail;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 盘点单详情 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-13
 */
public interface ICCheckDetailService extends IService<CCheckDetail> {

    List selectMapList(Long checkId);

    List<Map> getDetailListById(Long checkId);
}
