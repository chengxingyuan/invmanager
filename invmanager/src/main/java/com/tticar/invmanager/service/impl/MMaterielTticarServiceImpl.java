package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.common.shiro.MySecurityUtils;
import com.tticar.invmanager.entity.MMateriel;
import com.tticar.invmanager.entity.MMaterielTticar;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.RelativeGoodsDTO;
import com.tticar.invmanager.mapper.MMaterielTticarMapper;
import com.tticar.invmanager.service.IMMaterielTticarService;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 天天爱车产品 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-23
 */
@Service
public class MMaterielTticarServiceImpl extends ServiceImpl<MMaterielTticarMapper, MMaterielTticar> implements IMMaterielTticarService {
    @Autowired
    private MMaterielTticarMapper mMaterielTticarMapper;

    @Override
    public Page selectPage(MMaterielTticar mTticar, Map param, MyPage page) {
        Wrapper wrapper = Condition.create();
        wrapper.eq(mTticar.getTtstoreId() != null, "t." + MMaterielTticar.TTSTORE_ID, mTticar.getTtstoreId());
        wrapper.like(param.containsKey(MMateriel.NAME), "t1." + MMateriel.NAME, param.get(MMateriel.NAME) + "");
        wrapper.like(param.containsKey(MMateriel.CODE), "t1." + MMateriel.CODE, param.get(MMateriel.CODE) + "");
        wrapper.orderBy("t." + MMaterielTticar.CTIME, false);
        List list = baseMapper.get(page, wrapper);
        page.setRecords(list);
        return page;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            deleteById(id);
        }

    }
    /**
     * 根据天天爱车的商品查询其关联的仓库存货
     * */
    @Override
    public List<RelativeGoodsDTO> queryRelativeGoodsByTticarGoods(Long tticarGoodsId,String skuValue) {
        return mMaterielTticarMapper.queryRelativeGoodsByTticarGoods(tticarGoodsId.toString(),skuValue);
    }

    @Override
    public void cancelRelative(Long skuId,String tticarGoodsId,String skuValue) {
        mMaterielTticarMapper.cancelRelative(skuId, MySecurityUtils.currentUser().getTenantId(),tticarGoodsId,skuValue);
    }

}
