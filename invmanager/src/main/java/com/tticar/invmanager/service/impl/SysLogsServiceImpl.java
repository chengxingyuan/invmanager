package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.tticar.invmanager.common.Enum.Operate;
import com.tticar.invmanager.entity.SysLogs;
import com.tticar.invmanager.entity.SysLogsOperate;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.SysLogsMapper;
import com.tticar.invmanager.mapper.SysLogsOperateMapper;
import com.tticar.invmanager.service.ISysLogsService;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 请求日志 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-29
 */
@Service
public class SysLogsServiceImpl extends ServiceImpl<SysLogsMapper, SysLogs> implements ISysLogsService {

    @Autowired
    SysLogsOperateMapper sysLogsOperateMapper;

    @Override
    public Page selectPage(SysLogs sysLogs, MyPage<SysLogs> page) {
        Wrapper wrapper = new EntityWrapper();
        wrapper.orderBy(SysLogs.CTIME, false);
        wrapper.like(Strings.isNotEmpty(sysLogs.getName()), SysLogs.NAME, sysLogs.getName());
        wrapper.like(Strings.isNotEmpty(sysLogs.getName()), SysLogs.RURI, sysLogs.getRuri());
        Page<SysLogs> sysLogsPage = this.selectPage(page, wrapper);
        List<SysLogs> sysLogsList = sysLogsPage.getRecords();
        // 为请求日志添加描述
        if (sysLogsPage != null && CollectionUtils.isNotEmpty(sysLogsList)) {
            for (SysLogs log : sysLogsList) {
                try {
                    if (StringUtils.isBlank(log.getRuri()))  continue;
                    String[] urls = log.getRuri().split("/");
                    String  desc = "";
                    for (int i=0; i<urls.length; i++) {
                        // 根据URL匹配功能描述
                        if (i == 1) {
                            SysLogsOperate logOperate = new SysLogsOperate();
                            logOperate.setRuri(urls[i]);
                            SysLogsOperate operate = sysLogsOperateMapper.selectOne(logOperate);
                            desc += operate == null ? "" : operate.getOperate();
                        } else {
                            // 根据URL匹配详细功能描述
                            desc += Operate.getdesc(urls[i]);
                        }
                    }
                    if (urls.length == 2 && log.getRmethod().equals("GET")) {
                        desc += "查询";
                    } else if (urls.length == 2 && log.getRmethod().equals("POST")) {
                        desc += "保存新增或编辑";
                    }
                    log.setDesc(desc);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        sysLogsPage.setRecords(sysLogsList);
        return sysLogsPage;
    }

    @Override
    @Async
    public void insertLog(SysLogs log) {
        this.insert(log);
    }

}
