package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.entity.CarBrandMateriel;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.CarBrandMaterielMapper;
import com.tticar.invmanager.service.ICarBrandMaterielService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-09-20
 */
@Service
public class CarBrandMaterielServiceImpl extends ServiceImpl<CarBrandMaterielMapper, CarBrandMateriel> implements ICarBrandMaterielService {

    @Override
    public Page selectPage(CarBrandMateriel carBrandMateriel, MyPage<CarBrandMateriel> page) {
        Wrapper wrapper = new EntityWrapper();
        //wrapper.like(Strings.isNotEmpty(carBrandMateriel.getName()), CarBrandMateriel.NAME, carBrandMateriel.getName());
        return this.selectMapsPage(page, wrapper);
    }
    @Override
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            CarBrandMateriel carBrandMateriel = new CarBrandMateriel();
            carBrandMateriel.setId(id);
            carBrandMateriel.setStatus(1);
            list.add(carBrandMateriel);
        }
        this.updateBatchById(list);
    }

}
