package com.tticar.invmanager.service;

import com.tticar.invmanager.common.Enum.PayBusinessType;
import com.tticar.invmanager.entity.PPay;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 采购单支付流水 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-06
 */
public interface IPPayService extends IService<PPay> {

    Page selectPage(PPay pPay, MyPage<PPay> page);

    List selectMapList(Long businessId, PayBusinessType businessType);

    void delete(List<Long> ids);
}
