package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.entity.CarBrandTmp;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.CarBrandTmpMapper;
import com.tticar.invmanager.service.ICarBrandTmpService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 车品牌 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-09-19
 */
@Service
public class CarBrandTmpServiceImpl extends ServiceImpl<CarBrandTmpMapper, CarBrandTmp> implements ICarBrandTmpService {

    @Override
    public Page selectPage(CarBrandTmp carBrandTmp, MyPage<CarBrandTmp> page) {
        Wrapper wrapper = new EntityWrapper();
        //wrapper.like(Strings.isNotEmpty(carBrandTmp.getName()), CarBrandTmp.NAME, carBrandTmp.getName());
        return this.selectMapsPage(page, wrapper);
    }
    @Override
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            CarBrandTmp carBrandTmp = new CarBrandTmp();
            carBrandTmp.setId(id);
            carBrandTmp.setStatus(1);
            list.add(carBrandTmp);
        }
        this.updateBatchById(list);
    }

}
