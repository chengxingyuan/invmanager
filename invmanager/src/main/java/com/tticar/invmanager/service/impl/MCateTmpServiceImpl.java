package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.entity.MCateTmp;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.MCateTmpMapper;
import com.tticar.invmanager.service.IMCateTmpService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 存货分类表 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-10
 */
@Service
public class MCateTmpServiceImpl extends ServiceImpl<MCateTmpMapper, MCateTmp> implements IMCateTmpService {

    @Override
    public Page selectPage(MCateTmp mCateTmp, MyPage<MCateTmp> page) {
        Wrapper wrapper = new EntityWrapper();
        //wrapper.like(Strings.isNotEmpty(mCateTmp.getName()), MCateTmp.NAME, mCateTmp.getName());
        return this.selectMapsPage(page, wrapper);
    }
    @Override
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            MCateTmp mCateTmp = new MCateTmp();
            mCateTmp.setId(id);
            mCateTmp.setStatus(1);
            list.add(mCateTmp);
        }
        this.updateBatchById(list);
    }

}
