package com.tticar.invmanager.service;

import com.alibaba.fastjson.JSONArray;
import com.tticar.invmanager.entity.InRepositoryDetail;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.InRepositoryAddDTO;
import com.tticar.invmanager.entity.vo.InRepositoryDetailSearchDto;
import com.tticar.invmanager.entity.vo.MyPage;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 入库单详情 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-02
 */
public interface IInRepositoryDetailService extends IService<InRepositoryDetail> {

    Page selectPage(InRepositoryDetailSearchDto searchDto, MyPage<InRepositoryDetail> page);

    void delete(List<Long> ids);

    void batchInsert(JSONArray list);

    void addInRepositoryDetail(InRepositoryAddDTO dto);

    Integer getBatchCodeIsExsit(String batchCode);

    List<Map> batchCodeGrid(Long skuId);

    List<Map> getSkuBatchCodeForOut(String skuIds);

    List<Map> getDetailByCode(String code);

    List<Map> getDetailBySku(Long houseId, Long goodsId, Long skuId);

    String getSkuHouseByNum(Long skuId, Integer num, Long houseId);

    String getSkuHouseByNumAndTenantId(Long skuId, Integer num, Long houseId,Long tenantId);

    Long getSkuNumByBatchCode(Long skuId, String batchCode);

    List<Map> getBatchListByHouseId2SkuId(Long houseId, Long skuId);

    Double getSkuAvgCostByHouseId(Long houseId, Long skuId);

    String getSkuHouseByHouseId(Long houseId, Long skuId);

    String getMaterielHouseByHouseId(Long houseId, Long materielId);
}
