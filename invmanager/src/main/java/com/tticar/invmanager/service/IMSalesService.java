package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.MSales;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.PurchaseGoodsDto;
import com.tticar.invmanager.entity.vo.SalesAddDto;
import com.tticar.invmanager.entity.vo.SalesSearchDto;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-13
 */
public interface IMSalesService extends IService<MSales> {

    Page selectPage(MSales mSales, MyPage<MSales> page);

    Page buszStats(MSales mSales, MyPage<MSales> page);

    Map getSumInfo(MSales mSales);

    void delete(List<Long> ids);

    void addSales(SalesAddDto dto) throws ParseException;

    String outRepo(SalesAddDto dto) throws ParseException;

    void pay(Long salesId, BigDecimal payFee, Integer payType, String payRemark);

    Page search(SalesSearchDto searchDto, MyPage<MSales> page);

    List<Map> getSaleDetailList(Long saleId);

    void outRepositoryFromWindow(Long saleId, List<PurchaseGoodsDto> goodsDtoList,BigDecimal realPay,BigDecimal totalMoney,Integer payStatus,String remark);

    void endCountSale(Long saleId,BigDecimal realPay,BigDecimal totalMoney,Integer payStatus,String remark);

    void cancelOutSale(Long saleId);

    void cancelSaleEnd(Long saleId);

    void endCountForInCome(List<String> codes);



}
