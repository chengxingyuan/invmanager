package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.GoodsPartDetail;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 盘点单详情 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-15
 */
public interface IGoodsPartDetailService extends IService<GoodsPartDetail> {

    Page selectPage(GoodsPartDetail goodsPartDetail, MyPage<GoodsPartDetail> page);

    void delete(List<Long> ids);

    List<Map> getDetailList(Long partId);
}
