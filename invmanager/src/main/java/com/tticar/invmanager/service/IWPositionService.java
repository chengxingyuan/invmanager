package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.WPosition;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 库位表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-31
 */
public interface IWPositionService extends IService<WPosition> {

    Page selectPage(WPosition wPosition, MyPage<WPosition> page);

    Page selectPageByWearhouseId(WPosition wPosition, MyPage<WPosition> page,Long wearhouseId);

    void delete(List<Long> ids);

    List<Map> getForTree(Long skuId, Long materielId);

    void edit(WPosition wPosition);

    List<Map> getForTreeBind(Long materielId);

    /**
     * 根据仓库Id查询出仓库下属的库位Id集合
     * */
    List<Long> selectPositionIdsByWearhouseId(Long wearhouseId);


}
