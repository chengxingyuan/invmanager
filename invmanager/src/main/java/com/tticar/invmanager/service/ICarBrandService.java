package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.CarBrand;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.Tree;

import java.util.List;

/**
 * <p>
 * 车品牌 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-09-19
 */
public interface ICarBrandService extends IService<CarBrand> {

    Page selectPage(CarBrand carBrand, MyPage<CarBrand> page);

    void delete(List<Long> ids);

    List<Tree> tree(Long id);

    List<Tree> tree(String searchText);

}
