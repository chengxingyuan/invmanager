package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.common.Enum.Status;
import com.tticar.invmanager.common.exception.MyServiceException;
import com.tticar.invmanager.entity.MSku;
import com.tticar.invmanager.entity.MSkuValue;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.MSkuValueMapper;
import com.tticar.invmanager.service.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 物料规格值 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-05
 */
@Service
public class MSkuValueServiceImpl extends ServiceImpl<MSkuValueMapper, MSkuValue> implements IMSkuValueService {

    @Autowired
    IMSkuService mSkuService;
//    @Autowired
//    IMMaterielSkuService cateogrySkuService;

    @Override
    public Page selectPage(MSkuValue mSkuValue, MyPage<MSkuValue> page) {
        Wrapper wrapper = new EntityWrapper();
        if (mSkuValue.getStatus() != null) {
            wrapper.eq(MSkuValue.STATUS, mSkuValue.getStatus());
        } else {
            wrapper.ne(MSkuValue.STATUS, Status.DELETE.getCode());
        }
        wrapper.eq(mSkuValue.getSkuId() != null, MSkuValue.SKU_ID, mSkuValue.getSkuId());
        wrapper.like(Strings.isNotEmpty(mSkuValue.getName()), MSkuValue.NAME, mSkuValue.getName());
        wrapper.orderBy("ctime", false);
        return this.selectMapsPage(page, wrapper);
    }

    @Override
    public List selectList(MSkuValue mSkuValue) {
        Wrapper wrapper = new EntityWrapper();
        if (mSkuValue.getStatus() != null) {
            wrapper.eq(MSkuValue.STATUS, mSkuValue.getStatus());
        } else {
            wrapper.ne(MSkuValue.STATUS, Status.DELETE.getCode());
        }
        wrapper.eq(mSkuValue.getSkuId() != null, MSkuValue.SKU_ID, mSkuValue.getSkuId());
        wrapper.orderBy("ctime", false);
        return this.selectMaps(wrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
//            // 校验：如果此数据已被使用，不可禁用或删除
//            checkIsUsed(id, Status.DELETE.getCode());
            MSkuValue mSkuValue = new MSkuValue();
            mSkuValue.setId(id);
            //删除为-1，区分删除和禁用
            mSkuValue.setStatus(-1);
            list.add(mSkuValue);
        }
        this.updateBatchById(list);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void insertOrUpdateOne(MSkuValue mSkuValue) {
        // 校验：如果此数据已被使用，不可禁用或删除
        if (mSkuValue.getId() != null && mSkuValue.getId() != 0) {
//            checkIsUsed(mSkuValue.getId(), mSkuValue.getStatus());
        }
        this.insertOrUpdate(mSkuValue);
        if (mSkuValue.getIsDefault() == 1) {
            // 只能有一个默认值,把其他同一规格的置为不是默认
            Wrapper wrapper = new EntityWrapper();
            wrapper.ne(MSkuValue.ID, mSkuValue.getId());
            wrapper.eq(MSkuValue.SKU_ID, mSkuValue.getSkuId());
            MSkuValue skuValue = new MSkuValue();
            skuValue.setIsDefault(0);
            this.update(skuValue, wrapper);

        }
    }

//    private void checkIsUsed(Long id, int status) {
//        if (status == Status.DELETE.getCode() || status == Status.INVALID.getCode()) {
//            MSkuValue skuValue = this.selectById(id);
//            Wrapper wrapper = new EntityWrapper<>();
//            wrapper.eq("sku_id", skuValue.getSkuId());
//            List list = cateogrySkuService.selectList(wrapper);
//            if (CollectionUtils.isNotEmpty(list)) {
//                String msg = "此数据已被使用，不可禁用或删除";
//                msg = "（"+ skuValue.getName() + ")" + msg;
//                throw new MyServiceException(msg);
//            }
//        }
//    }

}
