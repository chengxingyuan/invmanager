package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.Bybs;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.BybsDto;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-18
 */
public interface IBybsService extends IService<Bybs> {

    Page selectPage(Bybs bybs, MyPage<Bybs> page);

    void delete(List<Long> ids);

    void addBybs(BybsDto bybsDto);

    void editBybs(BybsDto bybsDto);

    void audit(Long id);

}
