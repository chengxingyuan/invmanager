package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.SysLogs;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
/**
 * <p>
 * 请求日志 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-29
 */
public interface ISysLogsService extends IService<SysLogs> {

    Page selectPage(SysLogs sysLogs, MyPage<SysLogs> page);

    void insertLog(SysLogs log);
}
