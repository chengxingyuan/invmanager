package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.SysDepartment;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
public interface ISysDepartmentService extends IService<SysDepartment> {

    Page selectPage(SysDepartment sysDepartment, MyPage<SysDepartment> page);

    void delete(List<Long> ids);

    List select();
}
