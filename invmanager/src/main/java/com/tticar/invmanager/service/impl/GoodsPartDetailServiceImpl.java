package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.entity.GoodsPartDetail;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.GoodsPartDetailMapper;
import com.tticar.invmanager.service.IGoodsPartDetailService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 盘点单详情 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-15
 */
@Service
public class GoodsPartDetailServiceImpl extends ServiceImpl<GoodsPartDetailMapper, GoodsPartDetail> implements IGoodsPartDetailService {

    @Override
    public Page selectPage(GoodsPartDetail goodsPartDetail, MyPage<GoodsPartDetail> page) {
        Wrapper wrapper = new EntityWrapper();
        //wrapper.like(Strings.isNotEmpty(goodsPartDetail.getName()), GoodsPartDetail.NAME, goodsPartDetail.getName());
        return this.selectMapsPage(page, wrapper);
    }
    @Override
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            GoodsPartDetail goodsPartDetail = new GoodsPartDetail();
            goodsPartDetail.setId(id);
            goodsPartDetail.setStatus(1);
            list.add(goodsPartDetail);
        }
        this.updateBatchById(list);
    }

    @Override
    public List<Map> getDetailList(Long partId) {
        return this.baseMapper.getDetailList(partId);
    }

}
