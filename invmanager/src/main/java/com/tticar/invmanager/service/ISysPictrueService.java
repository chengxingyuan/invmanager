package com.tticar.invmanager.service;

import com.tticar.invmanager.common.shiro.LoginUser;
import com.tticar.invmanager.entity.SysPictrue;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 图片资源 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-16
 */
public interface ISysPictrueService extends IService<SysPictrue> {
    List upload(List<MultipartFile> file, LoginUser user, SysPictrue pictrue) throws Exception;

    String upload(MultipartFile file, String basePath) throws IOException, Exception;

    List getByIds(String ids);

    Page selectPage(SysPictrue sysPictrue, MyPage<SysPictrue> page);

    void delete(List<Long> ids);

}
