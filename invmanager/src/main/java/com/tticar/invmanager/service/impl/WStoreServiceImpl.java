package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.entity.Columns;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.common.Enum.Status;
import com.tticar.invmanager.entity.WStore;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.WStoreMapper;
import com.tticar.invmanager.service.IWStoreService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 门店 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-24
 */
@Service
public class WStoreServiceImpl extends ServiceImpl<WStoreMapper, WStore> implements IWStoreService {

    @Override
    public Page selectPage(WStore wStore, MyPage<WStore> page) {
        Wrapper wrapper = new EntityWrapper();
        wrapper.ne(Status.STATUS.getDesc(), Status.DELETE.getCode());
        Columns column = Columns.create().column("*");
        column.column("(select nickname from sys_user where id=user_id)", "nickname");
        wrapper.setSqlSelect(column);
        wrapper.like(Strings.isNotEmpty(wStore.getName()), WStore.NAME, wStore.getName());
        wrapper.orderBy("ctime", false);
        return this.selectMapsPage(page, wrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            WStore wStore = new WStore();
            wStore.setId(id);
            //删除为-1，区分删除和禁用
            wStore.setStatus(-1);
            list.add(wStore);
        }
        this.updateBatchById(list);
    }

    @Override
    public List select(WStore wStore) {
        Wrapper wrapper = new EntityWrapper().isNotNull(WStore.TTSTORE_ID);
        Columns column = Columns.create().column(WStore.TTSTORE_ID, WStore.ID).column(WStore.NAME);
        wrapper.setSqlSelect(column);
        wrapper.eq(Status.STATUS.getDesc(), Status.VALID.getCode());
        return this.selectMaps(wrapper);
    }

}
