package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.WPositionMateriel;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;

/**
 * <p>
 * 库位对应的商品 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-23
 */
public interface IWPositionMaterielService extends IService<WPositionMateriel> {

    Page selectPage(WPositionMateriel wPositionMateriel, MyPage<WPositionMateriel> page);

    void delete(List<Long> ids);

    String getBindInfo(Long materielId);

}
