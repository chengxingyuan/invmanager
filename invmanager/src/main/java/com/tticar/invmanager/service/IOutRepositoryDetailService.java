package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.OutRepositoryDetail;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.OutRepositoryDetailAddDTO;
import com.tticar.invmanager.entity.vo.OutRepositoryDetailSearchDto;
import com.tticar.invmanager.tticar.entity.DeliveryGoodsDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 出库单详情 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-06
 */
public interface IOutRepositoryDetailService extends IService<OutRepositoryDetail> {

    Page selectPage(OutRepositoryDetailSearchDto searchDto, MyPage<OutRepositoryDetail> page);

    void delete(List<Long> ids);

    void addOutRepositoryDetail(OutRepositoryDetailAddDTO dto);

    List<Map> getDetailByCode(String code);

    /**
     * 天天爱车订单发货出库
     */
    void deliveryGoods(List<DeliveryGoodsDTO> deliveryGoodsDTOs,Long id);

    /**
     * 员工端发货 扣除仓储库存
     */
//    void deliveryGoodsByStaffClient(String goodsOrderId);
}
