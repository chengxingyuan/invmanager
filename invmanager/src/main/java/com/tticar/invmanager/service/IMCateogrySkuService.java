package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.MCateogrySku;

import java.util.List;


/**
 * <p>
 * 存货分类sku表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-03
 */
public interface IMCateogrySkuService extends IService<MCateogrySku> {

    void  updateList (List<MCateogrySku> cateogrySkuList, Long cateogryId);

}
