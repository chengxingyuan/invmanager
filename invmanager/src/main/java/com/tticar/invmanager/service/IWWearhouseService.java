package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.WWearhouse;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.vo.KwTree;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.Tree;

import java.util.List;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-02
 */
public interface IWWearhouseService extends IService<WWearhouse> {

    Page selectPage(WWearhouse wWearhouse, MyPage<WWearhouse> page);

    Page selectPageForAvailable(WWearhouse wWearhouse, MyPage<WWearhouse> page);

    Page selectPageForSearch(WWearhouse wWearhouse, MyPage<WWearhouse> page);

    void delete(List<Long> ids);

    List<Tree> tree();

    List<KwTree> treeGrid(Long skuId, Integer type, Long houseId);

    List<KwTree> treeGridForBind(Long skuId);
}
