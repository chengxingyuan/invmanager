package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.MBrand;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
/**
 * <p>
 * 存货品牌 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
public interface IMBrandService extends IService<MBrand> {

    Page selectPage(MBrand mBrand, MyPage<MBrand> page);

    void delete(List<Long> ids);

}
