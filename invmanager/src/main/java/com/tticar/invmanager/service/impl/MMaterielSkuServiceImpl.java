package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.common.shiro.MySecurityUtils;
import com.tticar.invmanager.entity.MMaterielSku;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.MMaterielSkuMapper;
import com.tticar.invmanager.service.IMMaterielSkuService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-11
 */
@Service
public class MMaterielSkuServiceImpl extends ServiceImpl<MMaterielSkuMapper, MMaterielSku> implements IMMaterielSkuService {

    @Override
    public Page incomeList(MyPage<MMaterielSku> page, String startTime, String endTime, String searchText) {
        Map<String, Object> params = new HashMap<>();
        params.put("startTime", StringUtils.isNotEmpty(startTime) ? startTime : null);
        params.put("endTime", StringUtils.isNotEmpty(endTime) ? endTime : null);
        params.put("tenantId", MySecurityUtils.currentUser().getTenantId());
        params.put("searchText", StringUtils.isNotEmpty(searchText) ? searchText : null);

        page.setRecords(baseMapper.incomeList(page, params));
        return page;
    }

    @Override
    public Map getSumInfo(String startTime, String endTime, String searchText) {
        Map<String, Object> params = new HashMap<>();
        params.put("startTime", StringUtils.isNotEmpty(startTime) ? startTime : null);
        params.put("endTime", StringUtils.isNotEmpty(endTime) ? endTime : null);
        params.put("tenantId", MySecurityUtils.currentUser().getTenantId());
        params.put("searchText", StringUtils.isNotEmpty(searchText) ? searchText : null);

        return baseMapper.getSumInfo(params);
    }

    @Override
    public Page stockList(MyPage<MMaterielSku> page, String startTime, String endTime, String searchText, String userName, Long wearhouseId) {
        Map<String, Object> params = new HashMap<>();
        params.put("startTime", StringUtils.isNotEmpty(startTime) ? startTime : null);
        params.put("endTime", StringUtils.isNotEmpty(endTime) ? endTime : null);
        params.put("tenantId", MySecurityUtils.currentUser().getTenantId());
        params.put("searchText", StringUtils.isNotEmpty(searchText) ? searchText : null);
        params.put("userName", StringUtils.isNotEmpty(userName) ? userName : null);
        params.put("wearhouseId", wearhouseId);

        page.setRecords(baseMapper.stockList(page, params));
        return page;
    }

    @Override
    public Map getSumInfoForStock(String startTime, String endTime, String searchText, String userName, Long wearhouseId) {
        Map<String, Object> params = new HashMap<>();
        params.put("startTime", StringUtils.isNotEmpty(startTime) ? startTime : null);
        params.put("endTime", StringUtils.isNotEmpty(endTime) ? endTime : null);
        params.put("tenantId", MySecurityUtils.currentUser().getTenantId());
        params.put("searchText", StringUtils.isNotEmpty(searchText) ? searchText : null);
        params.put("userName", StringUtils.isNotEmpty(userName) ? userName : null);
        params.put("wearhouseId", wearhouseId);

        return baseMapper.getSumInfoForStock(params);
    }

    @Override
    public MMaterielSku selectNotTenantId(Long id,Long tenantId) {
        return baseMapper.selectNotTenantId(id,tenantId);
    }
}
