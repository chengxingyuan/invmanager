package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.ChangeRepository;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 调拨单 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-12-10
 */
public interface IChangeRepositoryService extends IService<ChangeRepository> {

    Page selectPage(ChangeRepository changeRepository, MyPage<ChangeRepository> page);

    void delete(Long ids);

    void insertNew(String rows, Long fromWearhouseId, Long toWearhouseId, Date orderTime,String remark);

    void correctChangeOrder(String dbCode);
}
