package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.GoodsPart;
import com.tticar.invmanager.entity.vo.GoodsPartDto;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-15
 */
public interface IGoodsPartService extends IService<GoodsPart> {

    Page selectPage(GoodsPart goodsPart, MyPage<GoodsPart> page);

    void delete(List<Long> ids);

    void addGoodsPart(GoodsPartDto dto);

    void editGoodsPart(GoodsPartDto dto);

    List<Map> getInfo(Long id);

    void audit(Long id);
}
