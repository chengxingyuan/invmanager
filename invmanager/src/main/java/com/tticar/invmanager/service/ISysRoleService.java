package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.SysRole;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.SysRoleResource;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
public interface ISysRoleService extends IService<SysRole> {

    Page selectPage(SysRole sysRole, MyPage<SysRole> page);

    void delete(List<Long> ids);

    List getResourceByRoleId(Long roleId);

    void insertOrUpdate(List<SysRoleResource> roleResources);

    List select();

}
