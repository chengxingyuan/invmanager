package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.RepositoryCount;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.RepositoryCountSearchDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品库存数量表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-10
 */
public interface IRepositoryCountService extends IService<RepositoryCount> {

    Page selectPage(RepositoryCountSearchDto searchDto, MyPage<RepositoryCount> page);

    List<RepositoryCount>  selectDetailAll(long houseId, List<Long> skuIdList);

    void delete(List<Long> ids);

    RepositoryCount selectDataBySkuIdAndWearhouseId(Long skuId, Long wearhouseId);

    RepositoryCount selectDataBySkuIdAndWearhouseIdByTenantId(Long skuId, Long wearhouseId,Long tenantId);

    RepositoryCount selectDataBySkuIdAndWearhouseIdAndPositionId(Long skuId, Long wearhouseId,Long positionId);

    RepositoryCount selectDataBySkuIdAndWearhouseIdAndPositionIdAndTenantId(Long skuId, Long wearhouseId,Long positionId,Long tenantId);

    List<Map> getSkuNum(String skuIds);

    List<Map> getSkuNumByHouseId(String skuIds, Long houseId);


    void updateRepositoryCount(Long skuId, Long wearhouseId,Integer count);

    void updateRepositoryCountByTenantId(Long skuId, Long wearhouseId,Integer count,Long tenantId);

    void updateRepositoryCountByPositionId(Long skuId, Long wearhouseId,Long positionId,Integer count);

    void updateRepositoryCountByPositionIdAndTenantId(Long skuId, Long wearhouseId,Long positionId,Integer count,Long tenantId);

    List<RepositoryCount> queryOutRepository(Long skuId);
    List<RepositoryCount> queryOutRepositoryByTenantId(Long skuId,Long tenantId);

    List<RepositoryCount> queryOutRepositoryByHouseId(Long skuId,Long houseId);

    List<Map> getRepositoryListByStoreId(Long storeId);

    Map queryGoodsMessageForSupplierApp(Long storeId,Long repositoryId,String searchText,Long page,Long pageSize);



}
