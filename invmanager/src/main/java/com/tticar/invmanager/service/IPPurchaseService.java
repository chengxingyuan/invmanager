package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.PPurchase;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.PurchaseAddDto;
import com.tticar.invmanager.entity.vo.PurchaseGoodsDto;
import com.tticar.invmanager.entity.vo.PurchaseSearchDto;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 采购单 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-03
 */
public interface IPPurchaseService extends IService<PPurchase> {

    Page selectPage(PPurchase pPurchase, MyPage<PPurchase> page);

    Page selectExpendPage(PPurchase pPurchase, MyPage<PPurchase> page);

    void delete(List<Long> ids);

    void addPurchase(PurchaseAddDto dto) throws ParseException;

    List<Map> getPurchaseDetailList(Long purchaseId);

    void pay(Long purchaseId, BigDecimal payFee, Integer payType, String payRemark);

    void inRepository(Long purchaseId);

    void inRepositoryNewVersion(Long purchaseId, List<PurchaseGoodsDto> goodsList,BigDecimal realPay,BigDecimal totalMoney,Integer payStatus,String remark);

    void endCountPurchase(Long purchaseId,BigDecimal realPay,BigDecimal totalMoney,Integer payStatus,String remark);

    void cancelInPurchase(Long purchaseId);

    void cancelPurchaseEnd(Long purchaseId);

    Page search(PurchaseSearchDto searchDto, MyPage<PPurchase> page);

    void endCountForExpend(List<String> codes);
}
