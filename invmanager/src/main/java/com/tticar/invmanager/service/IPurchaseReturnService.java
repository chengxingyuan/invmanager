package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.PurchaseReturn;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.PurchaseAddDto;
import com.tticar.invmanager.entity.vo.PurchaseGoodsDto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 退货单表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-12-21
 */
public interface IPurchaseReturnService extends IService<PurchaseReturn> {

    Page selectPage(PurchaseReturn purchaseReturn, MyPage<PurchaseReturn> page);

    void delete(List<Long> ids);

    List<Map> detailGoodsList(String code);

    void addPurchaseReturn(PurchaseAddDto dto);

    List<Map> getPurchaseDetailList(String purchaseReturnCode);

    void outRepositoryNewVersion(String purchaseReturnCode, List<PurchaseGoodsDto> goodsList, BigDecimal realPay, BigDecimal totalMoney, Integer payStatus, String remark);

    void cancelOutRepository(String purchaseReturnCode);

    void cancelEndCount(String purchaseReturnCode);

    void endCountPurchaseReturn(String purchaseReturnCode,BigDecimal realPay,BigDecimal totalMoney,Integer payStatus,String remark);

    String allReturn(Long id);
}
