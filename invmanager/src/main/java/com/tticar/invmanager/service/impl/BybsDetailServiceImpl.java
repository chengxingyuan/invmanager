package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.entity.BybsDetail;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.BybsDetailMapper;
import com.tticar.invmanager.service.IBybsDetailService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 报溢报损详情 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-18
 */
@Service
public class BybsDetailServiceImpl extends ServiceImpl<BybsDetailMapper, BybsDetail> implements IBybsDetailService {

    @Override
    public Page selectPage(BybsDetail bybsDetail, MyPage<BybsDetail> page) {
        Wrapper wrapper = new EntityWrapper();
        //wrapper.like(Strings.isNotEmpty(bybsDetail.getName()), BybsDetail.NAME, bybsDetail.getName());
        return this.selectMapsPage(page, wrapper);
    }
    @Override
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            BybsDetail bybsDetail = new BybsDetail();
            bybsDetail.setId(id);
            bybsDetail.setStatus(1);
            list.add(bybsDetail);
        }
        this.updateBatchById(list);
    }

    @Override
    public List<Map> getDetailList(Long id) {
        return this.baseMapper.getDetailList(id);
    }

}
