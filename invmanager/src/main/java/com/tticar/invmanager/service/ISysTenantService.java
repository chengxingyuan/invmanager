package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.SysTenant;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.SysUser;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 租户 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
public interface ISysTenantService extends IService<SysTenant> {

    Page selectPage(SysTenant sysTenant, MyPage<SysTenant> page);

    void delete(List<Long> ids);

    List select();

    void save(SysTenant sysTenant, SysUser sysUser, String newpassword);

    Map getById(Long id);
}
