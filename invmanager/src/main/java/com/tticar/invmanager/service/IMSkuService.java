package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.MSku;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
/**
 * <p>
 * 物料规格 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-05
 */
public interface IMSkuService extends IService<MSku> {

    void insertOrUpdateOne(MSku mSku);

    Page selectPage(MSku mSku, MyPage<MSku> page);

    void delete(List<Long> ids) throws Exception;

}
