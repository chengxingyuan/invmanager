package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.MCateogry;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.MCateogrySku;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.Tree;

import java.util.List;
/**
 * <p>
 * 存货分类表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-03
 */
public interface IMCateogryService extends IService<MCateogry> {

    Page selectPage(MCateogry mCateogry, MyPage<MCateogry> page);

    void delete(List<Long> ids);

    List msku(MCateogrySku cateogrySku);

    List<MCateogrySku> getCateogrySkuList(List<Long> cateogryIdList);

    void updateSku(List<MCateogrySku> cateogrySku);

    List getTree(Long id);

    void edit(MCateogry mCateogry);

    List<Tree> tree();

    void synchronizationFromTticar();
}
