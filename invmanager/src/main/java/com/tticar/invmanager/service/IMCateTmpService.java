package com.tticar.invmanager.service;

import com.tticar.invmanager.entity.MCateTmp;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;

/**
 * <p>
 * 存货分类表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-10
 */
public interface IMCateTmpService extends IService<MCateTmp> {

    Page selectPage(MCateTmp mCateTmp, MyPage<MCateTmp> page);

    void delete(List<Long> ids);

}
