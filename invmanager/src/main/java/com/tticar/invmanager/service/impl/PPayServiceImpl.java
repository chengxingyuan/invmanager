package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.entity.Columns;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.common.Enum.PayBusinessType;
import com.tticar.invmanager.common.Enum.Status;
import com.tticar.invmanager.entity.PPay;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.mapper.PPayMapper;
import com.tticar.invmanager.service.IPPayService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 采购单支付流水 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-06
 */
@Service
public class PPayServiceImpl extends ServiceImpl<PPayMapper, PPay> implements IPPayService {

    @Override
    public Page selectPage(PPay pPay, MyPage<PPay> page) {
        Wrapper wrapper = new EntityWrapper();
        //wrapper.like(Strings.isNotEmpty(pPay.getName()), PPay.NAME, pPay.getName());
        return this.selectMapsPage(page, wrapper);
    }

    @Override
    public List selectMapList(Long businessId, PayBusinessType businessType) {
        Wrapper wrapper = new EntityWrapper();
        Columns column = Columns.create().column("*");
        column.column("(SELECT name from sys_dict where pid=(select id from sys_dict where code='pay_type') and value=type )", "type_name");
        wrapper.setSqlSelect(column);
        wrapper.eq(PPay.BUSINESS_ID, businessId);
        wrapper.eq(PPay.BUSINESS_TYPE, businessType.getCode());
        wrapper.eq("status", Status.VALID.getCode());
        return this.selectMaps(wrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            PPay pPay = new PPay();
            pPay.setId(id);
            pPay.setStatus(1);
            list.add(pPay);
        }
        this.updateBatchById(list);
    }
}
