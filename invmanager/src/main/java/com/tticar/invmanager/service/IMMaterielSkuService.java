package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.MMaterielSku;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-11
 */
public interface IMMaterielSkuService extends IService<MMaterielSku> {
    Page incomeList(MyPage<MMaterielSku> page, String startTime, String endTime, String searchText);

    Map getSumInfo(String startTime, String endTime, String searchText);

    Page stockList(MyPage<MMaterielSku> page, String startTime, String endTime, String searchText, String userName, Long wearhouseId);

    Map getSumInfoForStock(String startTime, String endTime, String searchText, String userName, Long wearhouseId);

    MMaterielSku selectNotTenantId(Long id,Long tenantId);
}
