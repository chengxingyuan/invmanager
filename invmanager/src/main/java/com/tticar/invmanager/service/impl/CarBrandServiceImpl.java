package com.tticar.invmanager.service.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.entity.CarBrand;
import com.tticar.invmanager.entity.MMaterielSku;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.Tree;
import com.tticar.invmanager.mapper.CarBrandMapper;
import com.tticar.invmanager.service.ICarBrandService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车品牌 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-09-19
 */
@Service
public class CarBrandServiceImpl extends ServiceImpl<CarBrandMapper, CarBrand> implements ICarBrandService {

    @Override
    public Page selectPage(CarBrand carBrand, MyPage<CarBrand> page) {
        Wrapper wrapper = new EntityWrapper();
        //wrapper.like(Strings.isNotEmpty(carBrand.getName()), CarBrand.NAME, carBrand.getName());
        return this.selectMapsPage(page, wrapper);
    }
    @Override
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            CarBrand carBrand = new CarBrand();
            carBrand.setId(id);
            carBrand.setStatus(1);
            list.add(carBrand);
        }
        this.updateBatchById(list);
    }

    @Override
    public List<Tree> tree(Long pid) {
        List<Tree> resultList = new ArrayList<>();
        if(pid == null) {
            pid = 0l;
        }
        List<Map> list = baseMapper.getForTree(pid);
        for(Map map : list) {
            Tree tree = new Tree((Long) map.get("id"), (String) map.get("name"));
            tree.setPid(pid);
            if((Integer) map.get("level") == 5) {
                tree.setState("open");
            } else {
                tree.setState("closed");
            }
            Map<String, Object> attr = new HashMap<>();
            attr.put("level", map.get("level"));
            tree.setAttributes(attr);
//            tree.setIconCls("icon-goods");

            resultList.add(tree);
        }

        return resultList;
    }

    @Override
    public List<Tree> tree(String searchText) {
//        List<Tree> resultList = new ArrayList<>();
//        List<Map> list = baseMapper.getForTree(searchText);
//        for(Map map : list) {
//            Tree tree = new Tree((Long) map.get("id"), (String) map.get("name"));
//            tree.setState("open");
//            tree.setPid(0l);
//            tree.setIconCls("icon-goods");
//
//            Map<String, Object> attr = new HashMap<>();
//            attr.put("unit", map.get("unit"));
//            attr.put("ralativeId", map.get("ralativeId"));
//            attr.put("ralativeName", map.get("ralativeName"));
//            attr.put("wearhouseId", map.get("wearhouseId"));
//            attr.put("wearhouseName", map.get("wearhouseName"));
//            attr.put("positionId", map.get("positionId"));
//            attr.put("positionName", map.get("positionName"));
//            tree.setAttributes(attr);
//
//            List<Tree> children = tree((Long) map.get("id"), type);
//            if(children != null && children.size() > 0) {
//                tree.setChildren(children);
//                resultList.add(tree);
//            }
//        }
        List<Tree> resultList = new ArrayList<>();
        List<Map> list = baseMapper.getForTreeSearchText(searchText);
        for(Map map : list) {
            Tree tree = new Tree((Long) map.get("id"), (String) map.get("name"));
            if((Integer) map.get("level") == 5) {
                tree.setState("open");
            } else {
                tree.setState("closed");
            }
            Map<String, Object> attr = new HashMap<>();
            attr.put("level", map.get("level"));
            tree.setAttributes(attr);
//            tree.setIconCls("icon-goods");

            resultList.add(tree);
        }

        return resultList;
    }
}
