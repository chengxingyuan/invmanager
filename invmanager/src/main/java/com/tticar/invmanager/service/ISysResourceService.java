package com.tticar.invmanager.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.SysResource;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;

/**
 * <p>
 * 资源管理 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-17
 */
public interface ISysResourceService extends IService<SysResource> {

    List getTree(Long roleId, Long id);

    Page selectPage(SysResource sysResource, MyPage<SysResource> page);

    void delete(List<Long> ids);
}
