package com.tticar.invmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication
@EnableAsync
@ImportResource(value = "dubbo-bd-config-${config.dubbo.env}.xml")
public class InvmanagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(InvmanagerApplication.class, args);
    }

}
