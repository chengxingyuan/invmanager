package com.tticar.invmanager.common.shiro;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yandou on 2018/6/26.
 */
public class LoginUser implements Serializable {

    private Long id;
    private Long roleId;
    private Long tenantId;

    private String username;
    private String nickname;
    private String deptName;
    private String password;
    private Integer status;

    private String tenantName;
    private List<String> pers;


    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public List<String> getPers() {
        return pers;
    }

    public void setPers(List<String> pers) {
        this.pers = pers;
    }

    @Override
    public String toString() {
        return this.getNickname();
    }
}
