package com.tticar.invmanager.common.utils;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.tticar.invmanager.common.Enum.Status;
import com.tticar.invmanager.common.shiro.LoginUser;
import com.tticar.invmanager.entity.MCateogry;
import com.tticar.invmanager.entity.SysDict;
import com.tticar.invmanager.entity.WWearhouse;
import com.tticar.invmanager.entity.vo.DictCodes;
import com.tticar.invmanager.service.IMCateogryService;
import com.tticar.invmanager.service.ISysDictService;
import com.tticar.invmanager.service.IWWearhouseService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import java.util.ArrayList;
import java.util.List;


/**
 * <p>
 *  service服务工具类
 * </p>
 *
 * @author jiwenbin
 * @since 2018-07-27
 */
public class ServiceUtil {

    private static final String STR_FORMAT = "00000";

    public static LoginUser getLoginUser() {
        //获取当前登录用户
        Subject subject = SecurityUtils.getSubject();
        LoginUser loginUser = (LoginUser) subject.getPrincipal();
        return  loginUser;
    }

    public static Long getLoginUserId() {
        return  getLoginUser().getId();
    }

    public static String getLoginUserName() {
        return  getLoginUser().getUsername();
    }

    /**
     * 根据字典类型查询字典列表
     * @param sysDictService 字典服务接口
     * @param dictCodes 字典类型枚举
     * @return
     */
    public static List<SysDict> selectSysDictList(ISysDictService sysDictService, DictCodes dictCodes) {
        Wrapper wrapper = new EntityWrapper();
        wrapper.eq("code", dictCodes.name());
        SysDict parentDict = sysDictService.selectOne(wrapper);
        if (parentDict == null) {
            return null;
        }
        Wrapper dictWrapper = new EntityWrapper();
        dictWrapper.eq("status", Status.VALID.getCode());
        dictWrapper.eq("pid", parentDict.getId());
        dictWrapper.orderBy("sort", true);
        return sysDictService.selectList(dictWrapper);
    }

    /**
     * 获取商品分类id集合
     * 递归查询子集
     * @param cateogryIdList
     * @return
     */
    public static List<Long> getCateogryIdList(List<Long> cateogryIdList, IMCateogryService cateogryService) {
        return getCateogryIdList(cateogryIdList, cateogryService, false);
    }

    /**
     * 获取商品分类id集合
     * 递归查询子集
     * @param cateogryIdList
     * @param status 是否只查询有效数据
     * @return
     */
    public static List<Long> getCateogryIdList(List<Long> cateogryIdList, IMCateogryService cateogryService, boolean status) {
        if (CollectionUtils.isEmpty(cateogryIdList)) {
            return null;
        }

        Wrapper wrapper = new EntityWrapper<>();
        wrapper.in(MCateogry.PID, cateogryIdList);
        wrapper.eq(status, MCateogry.STATUS, 0);
        List<MCateogry> list = cateogryService.selectList(wrapper);
        if (CollectionUtils.isEmpty(list)) {
            return cateogryIdList;
        }
        List<Long> pIdList = new ArrayList<>();
        for (MCateogry cateogry : list) {
            pIdList.add(cateogry.getId());
        }
        return getCateogryIdList(pIdList, cateogryService, status);
    }

    /**
     * 获取仓库id集合
     * 递归查询子集
     * @param houseIdList
     * @return
     */
    public static List<Long> getHouseIdList(List<Long> houseIdList, IWWearhouseService wearhouseService) {
        if (CollectionUtils.isEmpty(houseIdList)) {
            return null;
        }
        Wrapper wrapper = new EntityWrapper<>();
        wrapper.in(WWearhouse.PID, houseIdList);
        List<WWearhouse> list = wearhouseService.selectList(wrapper);
        if (CollectionUtils.isEmpty(list)) {
            return houseIdList;
        }
        List<Long> pIdList = new ArrayList<>();
        for (WWearhouse wearhouse : list) {
            pIdList.add(wearhouse.getId());
        }
        return getHouseIdList(pIdList, wearhouseService);
    }
}
