package com.tticar.invmanager.common.Enum;

/**
 * 支付业务类型
 * @author jiwenbin
 * @date 2018/05/15
 */
public enum PayBusinessType {

    PURCHASE(0, "采购"),

    SALE(1, "销售")
    ;

    private int  code;
    private String desc;

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    PayBusinessType(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static String getdesc(int code) {
    	PayBusinessType[] codes = PayBusinessType.values();
    	for (PayBusinessType o : codes) {
    		if (o.code == code) {
    			return o.desc;
    		}
    	}
    	return "";
    }
}
