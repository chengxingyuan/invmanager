package com.tticar.invmanager.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

public class StringTticarUtils extends StringUtils {
    private static final String KM = "km";

    public StringTticarUtils() {
    }

    public static String concats(String... args) {
        String result = " ";
        String[] var2 = args;
        int var3 = args.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            String str = var2[var4];
            if(StringUtils.isNotBlank(str)) {
                result = result.concat(str);
            }
        }

        return result;
    }

    public static String getCallBack(String callback) {
        if(StringUtils.isBlank(callback)) {
            callback = "success_jsonpCallback";
        }

        return callback;
    }

    public static String format(String str, String... args) {
        return StringUtils.isBlank(str)?str:(args == null?str:(args.length <= 0?str:String.format(str, args)));
    }

    public static String formatDistanceKm(String distance) {
        if(StringUtils.isBlank(distance)) {
            return null;
        } else {
            BigDecimal decimal = new BigDecimal(distance);
            return decimal.setScale(1, 1).toString().concat("km");
        }
    }

    public static String cardNo(String cardno) {
        return StringUtils.isBlank(cardno)?null:(cardno.length() <= 8?cardno:StringUtils.substring(cardno, 0, 3).concat("**** ****").concat(StringUtils.substring(cardno, cardno.length() - 4)));
    }

    public static String cardNoEnd(String cardno) {
        return StringUtils.isBlank(cardno)?null:(cardno.length() <= 4?cardno:StringUtils.substring(cardno, cardno.length() - 4));
    }

    public static String getU(String nameSpell) {
        return StringUtils.isBlank(nameSpell)?null:StringUtils.substring(nameSpell, 0, 1).toUpperCase();
    }

    public static String getLimitString(String str, int len) {
        return StringUtils.isBlank(str)?str:(str.length() <= len?str:StringUtils.join(new String[]{left(str, len - 3), "..."}));
    }

    public static String valueOf(Long id) {
        return id != null?String.valueOf(id):"";
    }

    public static String getZeroIfNull(String inventory) {
        return (String)defaultIfBlank(inventory, "0");
    }

    public static Object getZeroIfNull(Object val) {
        return val == null?Integer.valueOf(0):val;
    }
}
