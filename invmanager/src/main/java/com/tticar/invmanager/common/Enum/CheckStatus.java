package com.tticar.invmanager.common.Enum;

/**
 * 盘点单状态
 * @author jiwenbin
 * @date 2018/05/15
 */
public enum CheckStatus {

    WAIT_CONFIRM(0, "待校正库存"),

    CONFIRM(1, "已校正库存"),

    INVALID(2, "作废"),
    ;

    private int  code;
    private String desc;

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    CheckStatus(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static String getdesc(int code) {
    	CheckStatus[] codes = CheckStatus.values();
    	for (CheckStatus o : codes) {
    		if (o.code == code) {
    			return o.desc;
    		}
    	}
    	return "";
    }
}
