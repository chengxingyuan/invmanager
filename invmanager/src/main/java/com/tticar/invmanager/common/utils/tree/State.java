package com.tticar.invmanager.common.utils.tree;

public class State {
    private Boolean expanded;

    public State() {
        super();
    }

    public State(Boolean expanded) {
        this.expanded = expanded;
    }

    public Boolean getExpanded() {
        return expanded;
    }

    public void setExpanded(Boolean expanded) {
        this.expanded = expanded;
    }
}
