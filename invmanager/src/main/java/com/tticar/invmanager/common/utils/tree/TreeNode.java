package com.tticar.invmanager.common.utils.tree;

import java.io.Serializable;
import java.util.List;

public class TreeNode implements Serializable {
    private String id;
    private String text;
    private String pid;
    private List<TestTree> nodes;

    private String degree;

    private State state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public List<TestTree> getNodes() {
        return nodes;
    }

    public void setNodes(List<TestTree> nodes) {
        this.nodes = nodes;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
