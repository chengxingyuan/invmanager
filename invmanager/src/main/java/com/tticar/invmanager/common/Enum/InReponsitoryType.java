package com.tticar.invmanager.common.Enum;

/**
 * 入库类型
 * @author jiwenbin
 * @date 2018/05/15
 */
public enum InReponsitoryType {

    PURCHASE(0, "采购入库"),

    CHECK(1, "盘点入库"),

    ;

    private int  code;
    private String desc;

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    InReponsitoryType(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static String getdesc(int code) {
    	InReponsitoryType[] codes = InReponsitoryType.values();
    	for (InReponsitoryType o : codes) {
    		if (o.code == code) {
    			return o.desc;
    		}
    	}
    	return "";
    }
}
