package com.tticar.invmanager.common.Enum;

/**
 * 单号编码
 * @author jiwenbin
 * @date 2018/07/30
 */
public enum CodeType {

    CGLX01("CGLX01", "普通采购"),
    CGLX02("CGLX02", "补货采购"),
    RK("RK", "入库单"),
    CK("CK", "出库单"),
    PD("PD", "盘点单"),
    XS("XS", "销售单"),
    DB("DB", "调拨单"),
    CK20("CK", "仓库编码"),
    KW("KW", "库位编码"),
    KH("KH", "客户"),
    GYS("GYS", "供应商"),
    SP("SP", "存货商品"),
    FL("FL", "存货分类"),
    BM("BM", "部门"),
    CZ("CZ", "拆装"),
    BY("BY", "报溢"),
    BS("BS", "报损"),
    CT("CT", "采购退货"),
    XT("XT", "销售退货"),
    ;

    private String  code;
    private String desc;

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    CodeType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static String getdesc(String code) {
    	CodeType[] codes = CodeType.values();
    	for (CodeType o : codes) {
    		if (o.code.equals(code)) {
    			return o.desc;
    		}
    	}
    	return "";
    }
    public static CodeType getType(String code) {
    	CodeType[] codes = CodeType.values();
    	for (CodeType o : codes) {
    		if (o.code.equals(code)) {
    			return o;
    		}
    	}
    	return null;
    }
}
