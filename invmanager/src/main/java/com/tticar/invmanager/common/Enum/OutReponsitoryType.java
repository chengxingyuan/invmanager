package com.tticar.invmanager.common.Enum;

/**
 * 出库类型
 * @author jiwenbin
 * @date 2018/05/15
 */
public enum OutReponsitoryType {

    SALE(0, "销售出库"),

    CHECK(1, "盘点出库"),

    ;

    private int  code;
    private String desc;

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    OutReponsitoryType(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static String getdesc(int code) {
    	OutReponsitoryType[] codes = OutReponsitoryType.values();
    	for (OutReponsitoryType o : codes) {
    		if (o.code == code) {
    			return o.desc;
    		}
    	}
    	return "";
    }
}
