package com.tticar.invmanager.common.Enum;

/**
 * 日志操作描述
 * @author jiwenbin
 * @date 2018/09/15
 */
public enum Operate {
    SAVE("save","保存"),
    DELETE("delete","删除"),
    LIST("list","列表"),
    EDIT("edit","编辑"),
    DOEDIT("doEdit","编辑"),
    DETAIL("detail","详情"),
    SELECT("select","查询"),
    ADD("add","新增"),
    ADDSALES("addSales","新增"),
    CORRECT("correct","校正库存"),
    PAYDETAILLIST("payDetailList","结算记录"),

    ;

    private String  code;
    private String desc;


    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    Operate(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static String getdesc(String code) {
    	Operate[] codes = Operate.values();
    	for (Operate o : codes) {
    		if (o.code.equals(code)) {
    			return o.desc;
    		}
    	}
    	return "";
    }
}
