package com.tticar.invmanager.common.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

	private static DateUtils instance = null;


	public final static String yyyyMMdd = "yyyy-MM-dd";
	public final static String yyyyMMdd1 = "yyyyMMdd";
	public final static String yyyyMMdd2 = "yyyyMMddHH";
	public final static String yyyyMMdd3 = "yyyy年MM月dd日";
	public final static String yyyyMMddHHmm = "yyyy-MM-dd HH:mm";
	public final static String yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss";
	public final static String yyyyMMdd_HHmmss = "yyyy-MM-dd-HHmmss";

	private final SimpleDateFormat longHourSdf;
    private final SimpleDateFormat longSdf;
    private final SimpleDateFormat shortSdf;

	
	public synchronized static DateUtils getInstance(){
		if(instance == null){
			instance = new DateUtils();
		}
		return instance;
	}
	    
    public DateUtils(){
        this.shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        this.longHourSdf = new SimpleDateFormat("yyyy-MM-dd HH");
        this.longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

	public String dateToString(Date date, String format) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		return simpleDateFormat.format(date);
	}
	    
	    /**
	     * 获得本周的第一天，周一
	     * 
	     * @return
	     */
	    public   Date getCurrentWeekDayStartTime() {
	        Calendar c = Calendar.getInstance();
	        try {
	            int weekday = c.get(Calendar.DAY_OF_WEEK) - 2;
	            c.add(Calendar.DATE, -weekday);
	            c.setTime(longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00"));
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return c.getTime();
	    }

	    /**
	     * 获得本周的最后一天，周日
	     * 
	     * @return
	     */
	    public   Date getCurrentWeekDayEndTime() {
	        Calendar c = Calendar.getInstance();
	        try {
	            int weekday = c.get(Calendar.DAY_OF_WEEK);
	            c.add(Calendar.DATE, 8 - weekday);
	            c.setTime(longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59"));
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return c.getTime();
	    }

	    /**
	     * 获得今天的开始时间，即2012-01-01 00:00:00
	     * 
	     * @return
	     */
	    public   Date getCurrentDayStartTime() {
	        Date now = new Date();
	        try {
	            now = shortSdf.parse(shortSdf.format(now));
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return now;
	    }

	/**
	 * 获得一个日期的结束时间，如2012-01-01 23:59:59
	 *
	 * @return
	 */
	public   Date getDateEndTime(Date date) {
		if (date == null) {
			return null;
		}
		try {
			date = longSdf.parse(shortSdf.format(date) + " 23:59:59");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	    /**
	     * 获得今天的结束时间，即2012-01-01 23:59:59
	     * 
	     * @return
	     */
	    public   Date getCurrentDayEndTime() {
	        Date now = new Date();
	        try {
	            now = longSdf.parse(shortSdf.format(now) + " 23:59:59");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return now;
	    }

	    /**
	     * 获得本小时的开始时间，即2012-01-01 23:59:59
	     * 
	     * @return
	     */
	    public   Date getCurrentHourStartTime() {
	        Date now = new Date();
	        try {
	            now = longHourSdf.parse(longHourSdf.format(now));
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return now;
	    }

	    /**
	     * 获得本小时的结束时间，即2012-01-01 23:59:59
	     * 
	     * @return
	     */
	    public   Date getCurrentHourEndTime() {
	        Date now = new Date();
	        try {
	            now = longSdf.parse(longHourSdf.format(now) + ":59:59");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return now;
	    }

	    /**
	     * 获得本月的开始时间，即2012-01-01 00:00:00
	     * 
	     * @return
	     */
	    public   Date getCurrentMonthStartTime() {
	        Calendar c = Calendar.getInstance();
	        Date now = null;
	        try {
	            c.set(Calendar.DATE, 1);
	            now = shortSdf.parse(shortSdf.format(c.getTime()));
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return now;
	    }
	    
	    /**
	     * 获得本月的开始时间，即2012-01-01 00:00:00
	     *  时间戳
	     * @return
	     */
	    public  Long getCurrentMonthStartTimeStamp(String timestamp ) {
	        if(timestamp.length()==10){
	        	timestamp=timestamp+"000";
	        }
	        long lt = new Long(timestamp);
	        Date date = new Date(lt);
	        Calendar c = Calendar.getInstance();
	        c.setTime(date);
	        Date timeMouth = null;
	        try {
	            c.set(Calendar.DATE, 1);
	            timeMouth = longSdf.parse(shortSdf.format(c.getTime())+ " 00:00:00");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return Long.valueOf(String.valueOf(timeMouth.getTime()).substring(0, 10));
	    }

	    /**
	     * 当前月的结束时间，即2012-01-31 23:59:59
	     * 
	     * @return
	     */
	    public   Date getCurrentMonthEndTime() {
	        Calendar c = Calendar.getInstance();
	        Date now = null;
	        try {
	            c.set(Calendar.DATE, 1);
	            c.add(Calendar.MONTH, 1);
	            c.add(Calendar.DATE, -1);
	            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return now;
	    }
	    
	    /**
	     * 当前月的结束时间，即2012-01-31 23:59:59
	     * 时间戳
	     * @return
	     */
	    public   Long getCurrentMonthEndTimeStamp(String timestamp) {
	    	if(timestamp.length()==10){
	        	timestamp=timestamp+"000";
	        }
	        long lt = new Long(timestamp);
	        Date date = new Date(lt);
	        Calendar c = Calendar.getInstance();
	        c.setTime(date);
	        Date timeMouth = null;
	        try {
	            c.set(Calendar.DATE, 1);
	            c.add(Calendar.MONTH, 1);
	            c.add(Calendar.DATE, -1);
	            timeMouth = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return Long.valueOf(String.valueOf(timeMouth.getTime()).substring(0, 10));
	    }

	    /**
	     * 当前年的开始时间，即2012-01-01 00:00:00
	     * 
	     * @return
	     */
	    public   Date getCurrentYearStartTime() {
	        Calendar c = Calendar.getInstance();
	        Date now = null;
	        try {
	            c.set(Calendar.MONTH, 0);
	            c.set(Calendar.DATE, 1);
	            now = shortSdf.parse(shortSdf.format(c.getTime()));
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return now;
	    }

	    /**
	     * 当前年的结束时间，即2012-12-31 23:59:59
	     * 
	     * @return
	     */
	    public   Date getCurrentYearEndTime() {
	        Calendar c = Calendar.getInstance();
	        Date now = null;
	        try {
	            c.set(Calendar.MONTH, 11);
	            c.set(Calendar.DATE, 31);
	            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return now;
	    }

	    /**
	     * 当前季度的开始时间，即2012-01-1 00:00:00
	     * 
	     * @return
	     */
	    public   Date getCurrentQuarterStartTime() {
	        Calendar c = Calendar.getInstance();
	        int currentMonth = c.get(Calendar.MONTH) + 1;
	        Date now = null;
	        try {
	            if (currentMonth >= 1 && currentMonth <= 3)
	                c.set(Calendar.MONTH, 0);
	            else if (currentMonth >= 4 && currentMonth <= 6)
	                c.set(Calendar.MONTH, 3);
	            else if (currentMonth >= 7 && currentMonth <= 9)
	                c.set(Calendar.MONTH, 4);
	            else if (currentMonth >= 10 && currentMonth <= 12)
	                c.set(Calendar.MONTH, 9);
	            c.set(Calendar.DATE, 1);
	            now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return now;
	    }

	    /**
	     * 当前季度的结束时间，即2012-03-31 23:59:59
	     * 
	     * @return
	     */
	    public   Date getCurrentQuarterEndTime() {
	        Calendar c = Calendar.getInstance();
	        int currentMonth = c.get(Calendar.MONTH) + 1;
	        Date now = null;
	        try {
	            if (currentMonth >= 1 && currentMonth <= 3) {
	                c.set(Calendar.MONTH, 2);
	                c.set(Calendar.DATE, 31);
	            } else if (currentMonth >= 4 && currentMonth <= 6) {
	                c.set(Calendar.MONTH, 5);
	                c.set(Calendar.DATE, 30);
	            } else if (currentMonth >= 7 && currentMonth <= 9) {
	                c.set(Calendar.MONTH, 8);
	                c.set(Calendar.DATE, 30);
	            } else if (currentMonth >= 10 && currentMonth <= 12) {
	                c.set(Calendar.MONTH, 11);
	                c.set(Calendar.DATE, 31);
	            }
	            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return now;
	    }
	    /**
	     * 获取前/后半年的开始时间
	     * @return
	     */
	    public   Date getHalfYearStartTime(){
	        Calendar c = Calendar.getInstance();
	        int currentMonth = c.get(Calendar.MONTH) + 1;
	        Date now = null;
	        try {
	            if (currentMonth >= 1 && currentMonth <= 6){
	                c.set(Calendar.MONTH, 0);
	            }else if (currentMonth >= 7 && currentMonth <= 12){
	                c.set(Calendar.MONTH, 6);
	            }
	            c.set(Calendar.DATE, 1);
	            now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return now;
	        
	    }
	    /**
	     * 获取前/后半年的结束时间
	     * @return
	     */
	    public   Date getHalfYearEndTime(){
	        Calendar c = Calendar.getInstance();
	        int currentMonth = c.get(Calendar.MONTH) + 1;
	        Date now = null;
	        try {
	            if (currentMonth >= 1 && currentMonth <= 6){
	                c.set(Calendar.MONTH, 5);
	                c.set(Calendar.DATE, 30);
	            }else if (currentMonth >= 7 && currentMonth <= 12){
	                c.set(Calendar.MONTH, 11);
	                c.set(Calendar.DATE, 31);
	            }
	            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return now;
	    }
	    public static void main(String[] args) throws Exception {
	    	System.out.println(DateUtils.getInstance().getCurrentMonthStartTimeStamp("1485878400"));
	    	System.out.println(DateUtils.getInstance().getCurrentMonthEndTimeStamp("1485878400"));
	    	Calendar c = Calendar.getInstance();
	    	Date date1 = new Date(DateUtils.getInstance().getCurrentMonthStartTimeStamp("1461727331"));
	    	Date date2 = new Date(DateUtils.getInstance().getCurrentMonthEndTimeStamp("1461727331"));
	    	DateUtils.getInstance().getCurrentMonthStartTimeStamp("1461727331");
	    	
	    }
}
