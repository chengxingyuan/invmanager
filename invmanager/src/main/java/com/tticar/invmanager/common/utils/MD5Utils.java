package com.tticar.invmanager.common.utils;

import com.tticar.invmanager.common.exception.MyServiceException;
import org.springframework.util.StringUtils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class MD5Utils {
	/**
	 * 对字符串进行Md5加密
	 * 
	 * @param input 原文
	 * @return md5后的密文
	 */
	public static String md5(String input) {
		byte[] code = null;
        try {
            code = MessageDigest.getInstance("md5").digest(input.getBytes());
        } catch (NoSuchAlgorithmException e) {
            code = input.getBytes();
        }
        BigInteger bi = new BigInteger(code);
        return bi.abs().toString(32).toUpperCase();
	}


	public static String md5ToLower(String str) {
		try {
			MessageDigest msgDigest = MessageDigest.getInstance("md5");
			msgDigest.update(str.getBytes());
			byte[] enbyte = msgDigest.digest();
			return byte2hex(enbyte).toLowerCase();
		} catch (NoSuchAlgorithmException e) {
			throw new MyServiceException(e.getMessage());
		}
	}
	public static String byte2hex(byte[] bytes) {
		StringBuffer retString = new StringBuffer();
		for (int i = 0; i < bytes.length; ++i) {
			retString.append(Integer.toHexString(0x0100 + (bytes[i] & 0x00FF))
					.substring(1).toUpperCase());
		}
		return retString.toString();
	}
	
	/**
	 * 对字符串进行Md5加密
	 * 
	 * @param input 原文
	 * @param salt 随机数
	 * @return string
	 */
	public static String generatePasswordMD5(String input, String salt) {
		if(StringUtils.isEmpty(salt)) {
			salt = "";
		}
		return md5(salt + md5(input));
	}
	
	public static void main(String[] args) {
		System.out.println(md5("111111"));;
	}
	
}
