package com.tticar.invmanager.common.config;

import com.baomidou.mybatisplus.plugins.parser.tenant.TenantSqlParser;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.update.Update;

/**
 * Created by yandou on 2018/7/31.
 */
public class MyTenantSqlParser extends TenantSqlParser {

    @Override
    public void processSelectBody(SelectBody selectBody) {
        if (selectBody instanceof PlainSelect) {
            PlainSelect plainSelect = (PlainSelect) selectBody;
            if (plainSelect.getWhere() != null && plainSelect.getWhere().toString().contains("tenant_id")) {
                return;
            }
            if (plainSelect.getJoins() != null && plainSelect.getJoins().toString().contains("tenant_id")) {
                return;
            }
        }
        super.processSelectBody(selectBody);
    }
    @Override
    public void processInsert(Insert insert) {
        if (insert != null && insert.toString().contains("tenant_id")) {
            return;
        }
        super.processInsert(insert);
    }

    @Override
    public void processUpdate(Update update) {
        if (update != null && update.toString().contains("tenant_id")) {
            return;
        }
        super.processUpdate(update);
    }

    @Override
    public void processDelete(Delete delete) {
        if (delete.getWhere() != null && delete.getWhere().toString().contains("tenant_id")) {
            return;
        }
        super.processDelete(delete);
    }

}
