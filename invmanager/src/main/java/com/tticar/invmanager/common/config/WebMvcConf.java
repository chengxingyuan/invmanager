package com.tticar.invmanager.common.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.tticar.invmanager.common.Interceptor.LoggerInterceptor;
import com.tticar.invmanager.common.utils.MyBeanUtils;
import com.tticar.invmanager.service.ISysDictService;
import nl.justobjects.pushlet.servlet.Pushlet;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yandou on 2018/6/13.
 */
@Configuration
public class WebMvcConf implements ApplicationRunner, ServletContextInitializer, WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoggerInterceptor())
                .excludePathPatterns(
                        "/",
                        "/login**",
                        "/bizjs/**",
                        "/public/**",
                        "/error",
                        "/sysLogs/**",
                        "/home/images/**",
                        "/home/css/**"
                )
                .addPathPatterns("/**");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(fastJsonConverter());
    }

    /**
     * fastJson相关设置
     */
    private FastJsonHttpMessageConverter fastJsonConverter() {
        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
        //
        List<MediaType> supportedMediaTypes = new ArrayList();
//        supportedMediaTypes.add(MediaType.parseMediaType("text/html;charset=UTF-8"));
        supportedMediaTypes.add(MediaType.APPLICATION_JSON);
        converter.setSupportedMediaTypes(supportedMediaTypes);
        //
        FastJsonConfig jsonConfig = new FastJsonConfig();
        List<SerializerFeature> serializerFeatureList = new ArrayList<SerializerFeature>();
        serializerFeatureList.add(SerializerFeature.PrettyFormat);
        serializerFeatureList.add(SerializerFeature.WriteDateUseDateFormat);
        serializerFeatureList.add(SerializerFeature.WriteMapNullValue);
        serializerFeatureList.add(SerializerFeature.WriteNullStringAsEmpty);
        serializerFeatureList.add(SerializerFeature.WriteNullListAsEmpty);
        serializerFeatureList.add(SerializerFeature.DisableCircularReferenceDetect);

        SerializerFeature[] serializerFeatures = serializerFeatureList.toArray(new SerializerFeature[serializerFeatureList.size()]);
        jsonConfig.setSerializerFeatures(serializerFeatures);
        jsonConfig.setCharset(Charset.forName("UTF-8"));
        converter.setFastJsonConfig(jsonConfig);
        return converter;
    }

    @Bean
    public Pushlet pushlet() {
        return new Pushlet();
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        Map<String, String> params = new HashMap<String, String>();
        params.put("org.atmosphere.servlet", "org.springframework.web.servlet.DispatcherServlet");
        params.put("contextClass", "org.springframework.web.context.support.AnnotationConfigWebApplicationContext");
        params.put("contextConfigLocation", "net.org.selector.animals.config.ComponentConfiguration");

        ServletRegistration.Dynamic pushletSevlet = servletContext.addServlet("pushlet", pushlet());
        pushletSevlet.getMappings().clear();
        pushletSevlet.addMapping("/pushlet/pushlet.srv");
        pushletSevlet.setInitParameters(params);
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        ISysDictService sysDictService = MyBeanUtils.getBean(ISysDictService.class);
        sysDictService.loadToLocal();
    }
}
