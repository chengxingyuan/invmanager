package com.tticar.invmanager.common.exception;

/**
 * Created by yandou on 17/11/29.
 */
public class MyServiceException extends RuntimeException {

    public static MyServiceException getInstance(String msg) {
        return new MyServiceException(msg);
    }

    public MyServiceException(String message) {
        super(message);
    }

}
