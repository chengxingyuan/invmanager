package com.tticar.invmanager.common.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ThreadContext;

import java.util.Objects;

/**
 * Created by yandou on 16/7/22.
 */
public abstract class MySecurityUtils extends SecurityUtils {

    private static final String USER_KEY = "current_user";

    public MySecurityUtils() {
        super();
    }

    public static void setCurrentUser(LoginUser loginUser) {
        Subject subject = ThreadContext.getSubject();
        subject.getSession().setAttribute(USER_KEY, loginUser);
    }

    public static LoginUser currentUser() {
        Subject subject = ThreadContext.getSubject();
        if (Objects.isNull(subject)) {
            return null;
        }
        Object user = subject.getSession().getAttribute(USER_KEY);
        return user == null ? null : (LoginUser) user;
    }

}
