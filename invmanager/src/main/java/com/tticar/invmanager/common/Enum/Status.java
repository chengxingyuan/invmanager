package com.tticar.invmanager.common.Enum;

/**
 * 通用状态
 * @author jiwenbin
 * @date 2018/05/15
 */
public enum Status {
    DELETE(-1,"删除"),

    VALID(0, "未删除/有效/展示/未处理"),

    INVALID(1, "禁用"),

    STATUS(2,"status")
    ;

    private int  code;
    private String desc;


    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    Status(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static String getdesc(int code) {
    	Status[] codes = Status.values();
    	for (Status o : codes) {
    		if (o.code == code) {
    			return o.desc;
    		}
    	}
    	return "";
    }
}
