package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.IWPositionMaterielService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tticar.invmanager.entity.WPositionMateriel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.POST;
import java.util.List;

/**
 * <p>
 * 库位对应的商品 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-23
 */
@Controller
@RequestMapping("/wPositionMateriel")
public class WPositionMaterielController extends BaseController {

    @Autowired
    private IWPositionMaterielService wPositionMaterielService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(WPositionMateriel wPositionMateriel) {
        wPositionMaterielService.insertOrUpdate(wPositionMateriel);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        wPositionMaterielService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(wPositionMaterielService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(WPositionMateriel wPositionMateriel, ModelMap modelMap) {
        modelMap.put("id", wPositionMateriel.getId());
        return "/wPositionMateriel/wPositionMateriel_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(WPositionMateriel wPositionMateriel, MyPage<WPositionMateriel> page) {
        return wPositionMaterielService.selectPage(wPositionMateriel, page);
    }

    @GetMapping
    public String index() {
        return "/wPositionMateriel/wPositionMateriel";
    }


    @PostMapping("getBindInfo")
    @ResponseBody
    public MyResponseBody getBindInfo(Long materielId) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(wPositionMaterielService.getBindInfo(materielId));
        return responseBody;
    }
}
