package com.tticar.invmanager.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.exception.MyServiceException;
import com.tticar.invmanager.common.shiro.LoginUser;
import com.tticar.invmanager.common.shiro.MySecurityUtils;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.ISysPictrueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tticar.invmanager.entity.SysPictrue;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 图片资源 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-16
 */
@Controller
@RequestMapping("/sysPictrue")
public class SysPictrueController extends BaseController {

    @Autowired
    private ISysPictrueService sysPictrueService;

    /**
     * 获取图片详情.
     *
     * @param ids
     * @return
     */
    @GetMapping("/ids/{ids}")
    @ResponseBody
    public MyResponseBody getByIds(@PathVariable("ids") String ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(sysPictrueService.getByIds(ids));
        return responseBody;
    }

    @RequestMapping("upload/delete")
    @ResponseBody
    public JSONObject delete() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("error", "");
        return jsonObject;
    }

    @RequestMapping("upload")
    @ResponseBody
    public JSONObject upload(@RequestParam("file_data") List<MultipartFile> file, SysPictrue pictrue) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("error", "");
        LoginUser user = MySecurityUtils.currentUser();
        try {
            jsonObject.put("ids", sysPictrueService.upload(file, user, pictrue));
        } catch (Exception e) {
            e.printStackTrace();
            jsonObject.put("error", e.getMessage());
        }
        return jsonObject;
    }

    @RequestMapping("uploadPicture")
    @ResponseBody
    public List uploadPicture(@RequestParam("file") List<MultipartFile> files) {
        List<String> urlList = new ArrayList<>();
        try {
            for (MultipartFile file : files) {
                String url = sysPictrueService.upload(file, "tticar/store/");
                urlList.add(url);
            }
        } catch (Exception e) {
            e.printStackTrace();
           throw new MyServiceException("图片上传失败");
        }
        return urlList;
    }

    @PostMapping
    @ResponseBody
    public MyResponseBody post(SysPictrue sysPictrue) {
        sysPictrue.setType(SysPictrue.ZERO);
        sysPictrueService.insertOrUpdate(sysPictrue);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        sysPictrueService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(sysPictrueService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(SysPictrue sysPictrue, ModelMap modelMap) {
        if (java.util.Objects.isNull(sysPictrue.getPid())) {
            sysPictrue.setPid(0l);
            sysPictrue.setName("顶级目录");
        }
        modelMap.put("parent", sysPictrue);
        modelMap.put("id", sysPictrue.getId());
        return "/sys/sysPictrue/sysPictrue_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(SysPictrue sysPictrue, MyPage<SysPictrue> page) {
        return sysPictrueService.selectPage(sysPictrue, page);
    }

    @GetMapping
    public String index(String from, ModelMap modelMap) {
        modelMap.put("from", from);
        return "/sys/sysPictrue/sysPictrue";
    }

}
