package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.ICarBrandMaterielService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tticar.invmanager.entity.CarBrandMateriel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-09-20
 */
@Controller
@RequestMapping("/carBrandMateriel")
public class CarBrandMaterielController extends BaseController {

    @Autowired
    private ICarBrandMaterielService carBrandMaterielService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(CarBrandMateriel carBrandMateriel) {
        carBrandMaterielService.insertOrUpdate(carBrandMateriel);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        carBrandMaterielService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(carBrandMaterielService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(CarBrandMateriel carBrandMateriel, ModelMap modelMap) {
        modelMap.put("id", carBrandMateriel.getId());
        return "/carBrandMateriel/carBrandMateriel_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(CarBrandMateriel carBrandMateriel, MyPage<CarBrandMateriel> page) {
        return carBrandMaterielService.selectPage(carBrandMateriel, page);
    }

    @GetMapping
    public String index() {
        return "/carBrandMateriel/carBrandMateriel";
    }

    @PostMapping("getDetailList")
    @ResponseBody
    public List<CarBrandMateriel> getDetailList(Long materielId) {
        if(materielId == null || materielId == 0) {
            return new ArrayList<>();
        }
        Wrapper wrapper = new EntityWrapper();
        wrapper.eq("materiel_id", materielId);
        return this.carBrandMaterielService.selectList(wrapper);
    }

}
