package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.exception.MyServiceException;
import com.tticar.invmanager.common.shiro.MySecurityUtils;
import com.tticar.invmanager.common.utils.MD5Utils;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.SysUser;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 管理员账号 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
@Controller
@RequestMapping("/sysUser")
public class SysUserController extends BaseController {

    @Autowired
    private ISysUserService sysUserService;

    /**
     * @param modelMap
     * @return
     */
    @GetMapping("profile")
    public String profile(ModelMap modelMap) {
        modelMap.put("id", MySecurityUtils.currentUser().getId());
        return "/sys/sysUser/sysUser_profile";
    }

    @PostMapping
    @ResponseBody
    public MyResponseBody post(SysUser sysUser, String newPassword) {
        if (sysUser != null && sysUser.getDeptId() == null) {
            throw new MyServiceException("部门不能为空");
        }
        sysUserService.edit(sysUser, newPassword);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        sysUserService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(sysUserService.selectById(id));
        return responseBody;
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(SysUser sysUser, MyPage<SysUser> page) {
        return sysUserService.selectPage(sysUser, page);
    }

    @PostMapping("userList")
    @ResponseBody
    public List<SysUser> userList(SysUser sysUser) {
        sysUser.setTenantId(MySecurityUtils.currentUser().getTenantId());
        List<SysUser> list = sysUserService.selectList(sysUser);
        return list;
    }

    @GetMapping("edit")
    public String edit(SysUser sysUser, ModelMap modelMap) {
        modelMap.put("id", sysUser.getId());
        return "/sys/sysUser/sysUser_edit";
    }

    @GetMapping
    public String index() {
        return "/sys/sysUser/sysUser";
    }

}
