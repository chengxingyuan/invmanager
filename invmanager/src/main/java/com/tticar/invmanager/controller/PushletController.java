package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.entity.Columns;
import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.WStore;
import com.tticar.invmanager.service.IWStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by yandou on 2018/8/9.
 */
@Controller
@RequestMapping("pushlet")
public class PushletController extends BaseController {

    @Autowired
    private IWStoreService wStoreService;

    @RequestMapping("tt/{storeId}")
    public String ttdashbord(@PathVariable("storeId") Long storeId, ModelMap modelMap) {
        modelMap.put("storeId", storeId);
        return "/sys/pushlet/ttdashbord";
    }

    @RequestMapping("tt")
    public String tt() {
        return "/sys/pushlet/tt";
    }

    @RequestMapping("info")
    @ResponseBody
    public MyResponseBody info() {
        MyResponseBody responseBody = MyResponseBody.success();
        Wrapper w = Condition.create().isNotNull(WStore.TTSTORE_ID);
        w.setSqlSelect(Columns.create().column(WStore.TTSTORE_ID).column(WStore.NAME));
        List maps = wStoreService.selectMaps(w);
        if (!maps.isEmpty()) {
            responseBody.setData(maps);
        }
        return responseBody;
    }

}
