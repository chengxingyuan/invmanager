package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.common.utils.date.DateStyle;
import com.tticar.invmanager.common.utils.date.DateUtil;
import com.tticar.invmanager.entity.RepositoryCount;
import com.tticar.invmanager.entity.WWearhouse;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.RepositoryCountSearchDto;
import com.tticar.invmanager.mapper.RepositoryCountMapper;
import com.tticar.invmanager.service.IRepositoryCountService;
import com.tticar.invmanager.service.IWWearhouseService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * <p>
 * 商品库存数量表 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-10
 */
@Controller
@RequestMapping("/repositoryCount")
public class RepositoryCountController extends BaseController {

    @Autowired
    private IRepositoryCountService repositoryCountService;

    @Autowired
    private IWWearhouseService wearhouseService;
    @Autowired
    private RepositoryCountMapper repositoryCountMapper;


    @PostMapping
    @ResponseBody
    public MyResponseBody post(RepositoryCount repositoryCount) {
        repositoryCountService.insertOrUpdate(repositoryCount);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        repositoryCountService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(repositoryCountService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(RepositoryCount repositoryCount, ModelMap modelMap) {
        modelMap.put("id", repositoryCount.getId());
        return "/repositoryCount/repositoryCount_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(RepositoryCountSearchDto searchDto, MyPage<RepositoryCount> page) {
        return repositoryCountService.selectPage(searchDto, page);
    }

    @GetMapping
    public String index(Long houseId,String searchText, ModelMap map) {
        if(houseId != null && houseId != 0) {
            map.put("houseId", houseId);
            WWearhouse house = this.wearhouseService.selectById(houseId);
            map.put("houseName", house.getName());
        }
        if (StringUtils.isNotBlank(searchText)) {
            map.put("searchText", searchText);
        }

        return "/repositoryCount/repositoryCount";
    }

    @GetMapping("getSurplusCount")
    @ResponseBody
    public int getSurplusCount(@RequestParam("skuId")Long skuId,@RequestParam("wearhouseId")Long wearhouseId,
                               @RequestParam("positionId")Long positionId){
        if (skuId == null || wearhouseId == null || positionId == null) {
            return 0;
        }
        RepositoryCount repositoryCount = repositoryCountService.selectDataBySkuIdAndWearhouseIdAndPositionId(skuId, wearhouseId,positionId);
        if (repositoryCount == null) {
            return 0;
        }
        return repositoryCount.getCount();
    }

    @GetMapping("print")
    public String print(@RequestParam("ids") Long[] ids, ModelMap modelMap) {
        modelMap.put("thisTime", DateUtil.DateToString(new Date(), DateStyle.YYYY_MM_DD_HH_MM_SS));
        String idsData = "(";
        for (int i = 0; i < ids.length; i++) {
            idsData += ids[i] + ",";
        }
        idsData = idsData.substring(0, idsData.length() - 1);
        idsData = idsData + ")";
        modelMap.put("ids",idsData);
        return "/repositoryCount/repositoryCount_print";
    }

    @RequestMapping("printData")
    @ResponseBody
    public List printData(@RequestParam("ids") String ids){
        Map map = new HashMap<>(2);
        map.put("ids", ids);
        return repositoryCountMapper.searchPrintData(map);
    }

    @RequestMapping("getSpecific")
    @ResponseBody
    public List getSpecific(Long skuId) {
        return repositoryCountMapper.getSpecific(skuId);
    }


    /**
     * APP端根据供应商的店铺id查询旗下仓库
     * */
    @RequestMapping("getRepositoryByStoreId")
    @ResponseBody
    public MyResponseBody getRepositoryByApp(Long storeId) {
        MyResponseBody responseBody = MyResponseBody.success();
        List<Map> repositoryListByStoreId = repositoryCountService.getRepositoryListByStoreId(storeId);
        responseBody.setData(repositoryListByStoreId);
        return responseBody;
    }

    /**
     * APP端根据供应商的店铺id/仓库id/模糊搜索等条件查询商品信息
     * */
    @RequestMapping("queryGoodsMessage")
    @ResponseBody
    public MyResponseBody queryGoodsMessage(Long storeId,Long repositoryId,String searchText,Long page,Long pageSize) {
        MyResponseBody responseBody = MyResponseBody.success();
        Map map = repositoryCountService.queryGoodsMessageForSupplierApp(storeId,repositoryId,searchText,page,pageSize);
        responseBody.setData(map);
        return responseBody;
    }



}
