package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.IGoodsPartDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tticar.invmanager.entity.GoodsPartDetail;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 盘点单详情 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-15
 */
@Controller
@RequestMapping("/goodsPartDetail")
public class GoodsPartDetailController extends BaseController {

    @Autowired
    private IGoodsPartDetailService goodsPartDetailService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(GoodsPartDetail goodsPartDetail) {
        goodsPartDetailService.insertOrUpdate(goodsPartDetail);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        goodsPartDetailService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(goodsPartDetailService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(GoodsPartDetail goodsPartDetail, ModelMap modelMap) {
        modelMap.put("id", goodsPartDetail.getId());
        return "/goodsPartDetail/goodsPartDetail_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(GoodsPartDetail goodsPartDetail, MyPage<GoodsPartDetail> page) {
        return goodsPartDetailService.selectPage(goodsPartDetail, page);
    }

    @GetMapping
    public String index() {
        return "/goodsPartDetail/goodsPartDetail";
    }

    @PostMapping("getDetailList")
    @ResponseBody
    public List<Map> getDetailList(Long partId) {
        if(partId == null || partId == 0) {
            return new ArrayList<>();
        }
        return this.goodsPartDetailService.getDetailList(partId);
    }

}
