package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.MSku;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.IMSkuValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tticar.invmanager.entity.MSkuValue;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 物料规格值 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-05
 */
@Controller
@RequestMapping("/mSkuValue")
public class MSkuValueController extends BaseController {

    @Autowired
    private IMSkuValueService mSkuValueService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(MSkuValue mSkuValue) {
        Wrapper wrapper = new EntityWrapper();
        wrapper.ne("status", -1);
        wrapper.eq("name", mSkuValue.getName());
        wrapper.eq("sku_id", mSkuValue.getSkuId());
        if(mSkuValue.getId() != null) {
            wrapper.ne("id", mSkuValue.getId());
        }
        MSkuValue sku = this.mSkuValueService.selectOne(wrapper);
        if(sku != null) {
            return MyResponseBody.failure("名称已存在");
        }

        if(mSkuValue.getSort() == null) {
            mSkuValue.setSort(0);
        }
        mSkuValueService.insertOrUpdateOne(mSkuValue);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        mSkuValueService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(mSkuValueService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(MSkuValue mSkuValue, ModelMap modelMap) {
        modelMap.put("id", mSkuValue.getId());
        return "/mSkuValue/mSkuValue_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(MSkuValue mSkuValue, MyPage<MSkuValue> page) {
        return mSkuValueService.selectPage(mSkuValue, page);
    }

    @GetMapping("allList")
    @ResponseBody
    public MyResponseBody allList(MSkuValue mSkuValue) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(mSkuValueService.selectList(mSkuValue));
        return responseBody;
    }

    @GetMapping
    public String index() {
        return "/mSkuValue/mSkuValue";
    }

}
