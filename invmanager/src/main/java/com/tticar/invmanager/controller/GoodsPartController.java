package com.tticar.invmanager.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.common.utils.date.DateStyle;
import com.tticar.invmanager.common.utils.date.DateUtil;
import com.tticar.invmanager.entity.GoodsPartDetail;
import com.tticar.invmanager.entity.vo.GoodsPartDetailDto;
import com.tticar.invmanager.entity.vo.GoodsPartDto;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.PurchaseGoodsDto;
import com.tticar.invmanager.service.IGoodsPartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tticar.invmanager.entity.GoodsPart;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-15
 */
@Controller
@RequestMapping("/goodsPart")
public class GoodsPartController extends BaseController {

    @Autowired
    private IGoodsPartService goodsPartService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(GoodsPart goodsPart) {
        goodsPartService.insertOrUpdate(goodsPart);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        goodsPartService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(goodsPartService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(GoodsPart goodsPart, ModelMap modelMap) {
        modelMap.put("id", goodsPart.getId());
        goodsPart = this.goodsPartService.selectById(goodsPart.getId());
        modelMap.put("part", goodsPart);
        modelMap.put("orderTime", DateUtil.DateToString(goodsPart.getOrderTime(), DateStyle.YYYY_MM_DD));
        modelMap.put("ctime", DateUtil.DateToString(goodsPart.getCtime(), DateStyle.YYYY_MM_DD));
        if(goodsPart.getOtime() != null) {
            modelMap.put("otime", DateUtil.DateToString(goodsPart.getOtime(), DateStyle.YYYY_MM_DD));
        }
        return "/goodsPart/goodsPart_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(GoodsPart goodsPart, MyPage<GoodsPart> page) {
        return goodsPartService.selectPage(goodsPart, page);
    }

    @GetMapping
    public String index() {
        return "/goodsPart/goodsPart";
    }

    @GetMapping("add")
    public String add(GoodsPart goodsPart, ModelMap modelMap) {
        modelMap.put("id", goodsPart.getId());
        return "/goodsPart/goodsPartAdd";
    }

    @PostMapping("add")
    @ResponseBody
    public MyResponseBody add(GoodsPartDto goodsPartDto, String goodsFrom, String goodsTo) {
        MyResponseBody responseBody = MyResponseBody.success();
        goodsPartDto.setFrom(JSONObject.parseObject(goodsFrom, GoodsPartDetailDto.class));
        goodsPartDto.setList(JSONArray.parseArray(goodsTo, GoodsPartDetailDto.class));
        goodsPartService.addGoodsPart(goodsPartDto);
        return responseBody;
    }

    @PostMapping("edit")
    @ResponseBody
    public MyResponseBody edit(GoodsPartDto goodsPartDto, String goodsFrom, String goodsTo) {
        MyResponseBody responseBody = MyResponseBody.success();
        goodsPartDto.setFrom(JSONObject.parseObject(goodsFrom, GoodsPartDetailDto.class));
        goodsPartDto.setList(JSONArray.parseArray(goodsTo, GoodsPartDetailDto.class));
        goodsPartService.editGoodsPart(goodsPartDto);
        return responseBody;
    }

    @PostMapping("getInfo")
    @ResponseBody
    public List<Map> getInfo(Long id) {
        return goodsPartService.getInfo(id);
    }

    @PostMapping("audit")
    @ResponseBody
    public MyResponseBody audit(Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        goodsPartService.audit(id);
        return responseBody;
    }

}
