package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.ISysLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tticar.invmanager.entity.SysLogs;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 请求日志 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-29
 */
@Controller
@RequestMapping("/sysLogs")
public class SysLogsController extends BaseController {

    @Autowired
    private ISysLogsService sysLogsService;

    @GetMapping("list")
    @ResponseBody
    public Page list(SysLogs sysLogs, MyPage<SysLogs> page) {
        return sysLogsService.selectPage(sysLogs, page);
    }

    @GetMapping
    public String index() {
        return "/sys/sysLogs/sysLogs";
    }

}
