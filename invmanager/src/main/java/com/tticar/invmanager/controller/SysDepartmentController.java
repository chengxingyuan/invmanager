package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.Enum.CodeType;
import com.tticar.invmanager.common.exception.MyServiceException;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.SysUser;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.ICodeCounterService;
import com.tticar.invmanager.service.ISysDepartmentService;
import com.tticar.invmanager.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tticar.invmanager.entity.SysDepartment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 部门表 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
@Controller
@RequestMapping("/sysDepartment")
public class SysDepartmentController extends BaseController {

    @Autowired
    private ISysDepartmentService sysDepartmentService;
    @Autowired
    private ICodeCounterService codeCounterService;
    @Autowired
    private ISysUserService sysUserService;
    @GetMapping("select")
    @ResponseBody
    public List select(){
        return sysDepartmentService.select();
    }


    @PostMapping
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public MyResponseBody post(SysDepartment sysDepartment) {
        Wrapper wrapper1 = new EntityWrapper<>();
        wrapper1.ne("status", -1);
        List<SysDepartment> sysDepartments = sysDepartmentService.selectList(wrapper1);
        if (sysDepartment.getId() == null) {
            if (sysDepartments != null && sysDepartments.size() > 0) {
                for (SysDepartment sysDepartment1 : sysDepartments) {
                    if (sysDepartment1.getName().equals(sysDepartment.getName())) {
                        throw new MyServiceException("该部门已经存在，请重新命名");
                    }
                }
            }
            String code = codeCounterService.getCode(CodeType.BM, "", 3);
            sysDepartment.setCode(code);
        }else {
            if (sysDepartments != null && sysDepartments.size() > 0) {
                for (SysDepartment sysDepartment1 : sysDepartments) {
                    if (sysDepartment1.getName().equals(sysDepartment.getName()) && !sysDepartment.getId().equals(sysDepartment1.getId())) {
                        throw new MyServiceException("该部门已经存在，请重新命名");
                    }
                }
            }
        }
        if (sysDepartment.getId() != null && sysDepartment.getStatus() != 0) {
            Wrapper wrapper = new EntityWrapper<>();
            wrapper.eq("dept_id", sysDepartment.getId());
            wrapper.ne("status", -1);
            List<SysUser> sysUsers = sysUserService.selectList(wrapper);
            if (sysUsers != null && sysUsers.size() > 0) {
                throw new MyServiceException("该部门下有未删除的用户，禁止禁用");
            }

        }
        sysDepartmentService.insertOrUpdate(sysDepartment);

        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        sysDepartmentService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(sysDepartmentService.selectById(id));
        return responseBody;
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(SysDepartment sysDepartment, MyPage<SysDepartment> page) {
        return sysDepartmentService.selectPage(sysDepartment, page);
    }

    @GetMapping("edit")
    public String edit(SysDepartment sysDepartment, ModelMap modelMap) {
        if (java.util.Objects.isNull(sysDepartment.getPid())) {
            sysDepartment.setPid(0l);
            sysDepartment.setName("顶级目录");
        }
        modelMap.put("parent", sysDepartment);
        modelMap.put("id", sysDepartment.getId());
        return "/sys/sysDepartment/sysDepartment_edit";
    }

    @GetMapping
    public String index() {
        return "/sys/sysDepartment/sysDepartment";
    }

}
