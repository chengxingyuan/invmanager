package com.tticar.invmanager.controller;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.Enum.CheckStatus;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.common.utils.ServiceUtil;
import com.tticar.invmanager.entity.CCheck;
import com.tticar.invmanager.entity.MCateogrySku;
import com.tticar.invmanager.entity.RepositoryCount;
import com.tticar.invmanager.entity.vo.CheckAddDto;
import com.tticar.invmanager.entity.vo.CheckDetailDto;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.ICCheckService;
import com.tticar.invmanager.service.IMCateogryService;
import com.tticar.invmanager.service.IRepositoryCountService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 盘点单 前端控制器
 * </p>
 *
 * @author jiwenbin
 * @since 2018-08-13
 */
@Controller
@RequestMapping("/cCheck")
public class CCheckController extends BaseController {

    @Autowired
    private ICCheckService cCheckService;
    @Autowired
    private IRepositoryCountService repositoryCountService;
    @Autowired
    private IMCateogryService cateogryService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(CCheck cCheck) {
        cCheckService.insertOrUpdate(cCheck);
        return MyResponseBody.success();
    }

    /**
     * 保存新增盘点单
     *
     * @param details
     * @param houseId
     * @return
     * @throws Exception
     */
    @PostMapping("save")
    @ResponseBody
    public MyResponseBody save(@RequestParam("details") String details, @RequestParam("houseId") Long houseId) {
        MyResponseBody responseBody = MyResponseBody.success();
        List<CheckAddDto> list = JSONArray.parseArray(details, CheckAddDto.class);
        this.cCheckService.addCheck(houseId, list);
        return responseBody;
    }

    @GetMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        Wrapper wrapper = new EntityWrapper();
        wrapper.in("id", ids);
        List<CCheck> list = cCheckService.selectList(wrapper);
        for (CCheck check : list) {
            if (check.getStatus() == CheckStatus.CONFIRM.getCode()) {
                responseBody.setMessage("已校正不能删除");
                return responseBody;
            }
        }
        for (CCheck check : list) {
            CCheck checkUpdate = new CCheck();
            checkUpdate.setId(check.getId());
            checkUpdate.setStatus(CheckStatus.INVALID.getCode());
            cCheckService.updateById(checkUpdate);
        }
        return MyResponseBody.success();
    }

    /**
     * 校正库存
     * 并且生成出/入库单
     *
     * @param checkId
     * @return
     */
    @PostMapping("correct")
    @ResponseBody
    public MyResponseBody correct(@RequestParam("checkId") Long checkId, String details) throws ParseException {
        MyResponseBody responseBody = MyResponseBody.success();
        List<CheckDetailDto> detailList = JSONArray.parseArray(details, CheckDetailDto.class);
        responseBody.setMessage(this.cCheckService.correct(checkId, detailList));
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(cCheckService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(CCheck cCheck, ModelMap modelMap) {
        modelMap.put("id", cCheck.getId());
        return "/cCheck/cCheck_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(CCheck check, MyPage<CCheck> page) {
        return cCheckService.selectPage(check, page);
    }

    /**
     * 库存信息
     *
     * @param houseId 仓库id
     * @param houseId 商品分类id
     * @return
     */
    @GetMapping("repositoryList")
    @ResponseBody
    public List repositoryList(@RequestParam("houseId") long houseId, @RequestParam("cateogryId") long cateogryId) {
        List<Long> cateogryIdList = null;
        if (cateogryId != 0) {
            List<Long> idList = new ArrayList<>();
            idList.add(cateogryId);
            // 商品分类是多层级的 cateogryId不知道是第几层的，这里需要递归得到最终的子集
            cateogryIdList = ServiceUtil.getCateogryIdList(idList, cateogryService);
        }
        List<MCateogrySku> list = cateogryService.getCateogrySkuList(cateogryIdList);
        List<Long> skuIdList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(list)) {
            for (MCateogrySku cateogrySku : list) {
                skuIdList.add(cateogrySku.getSkuId());
            }
        }
        List<RepositoryCount> countList = repositoryCountService.selectDetailAll(houseId, skuIdList);
        return countList;
    }

    @GetMapping
    public String index() {
        return "/cCheck/cCheck";
    }

    @GetMapping("checkAdd")
    public String checkAdd() {
        return "/cCheck/checkAdd";
    }

}
