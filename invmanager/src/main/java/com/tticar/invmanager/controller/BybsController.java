package com.tticar.invmanager.controller;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.common.utils.date.DateStyle;
import com.tticar.invmanager.common.utils.date.DateUtil;
import com.tticar.invmanager.entity.Bybs;
import com.tticar.invmanager.entity.vo.BybsDetailDto;
import com.tticar.invmanager.entity.vo.BybsDto;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.IBybsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-18
 */
@Controller
@RequestMapping("/bybs")
public class BybsController extends BaseController {

    @Autowired
    private IBybsService bybsService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(Bybs bybs) {
        bybsService.insertOrUpdate(bybs);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        bybsService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(bybsService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(Bybs bybs, ModelMap modelMap) {
        modelMap.put("id", bybs.getId());
        bybs = this.bybsService.selectById(bybs.getId());
        modelMap.put("bybs", bybs);
        modelMap.put("orderTime", DateUtil.DateToString(bybs.getOrderTime(), DateStyle.YYYY_MM_DD));
        modelMap.put("ctime", DateUtil.DateToString(bybs.getCtime(), DateStyle.YYYY_MM_DD));
        if(bybs.getOtime() != null) {
            modelMap.put("otime", DateUtil.DateToString(bybs.getOtime(), DateStyle.YYYY_MM_DD));
        }
        if(bybs.getOrderType() == 0) {
            return "/bybs/byDetail";
        }
        return "/bybs/bsDetail";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(Bybs bybs, MyPage<Bybs> page) {
        return bybsService.selectPage(bybs, page);
    }

    @GetMapping
    public String index() {
        return "/bybs/bybs";
    }

    @GetMapping("add")
    public String add(Bybs bybs, ModelMap modelMap) {
        modelMap.put("orderType", bybs.getOrderType());
        if(bybs.getOrderType() == 0) {
            return "/bybs/byAdd";
        }
        return "/bybs/bsAdd";
    }

    @PostMapping("add")
    @ResponseBody
    public MyResponseBody add(BybsDto bybsDto, String goods) {
        MyResponseBody responseBody = MyResponseBody.success();
        bybsDto.setList(JSONArray.parseArray(goods, BybsDetailDto.class));
        bybsService.addBybs(bybsDto);
        return responseBody;
    }

    @PostMapping("edit")
    @ResponseBody
    public MyResponseBody edit(BybsDto bybsDto, String goods) {
        MyResponseBody responseBody = MyResponseBody.success();
        bybsDto.setList(JSONArray.parseArray(goods, BybsDetailDto.class));
        bybsService.editBybs(bybsDto);
        return responseBody;
    }

    @PostMapping("audit")
    @ResponseBody
    public MyResponseBody audit(Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        bybsService.audit(id);
        return responseBody;
    }
}
