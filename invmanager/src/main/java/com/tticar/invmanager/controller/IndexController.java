package com.tticar.invmanager.controller;

import com.tticar.invmanager.common.shiro.LoginUser;
import com.tticar.invmanager.common.shiro.MySecurityUtils;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.SysTenant;
import com.tticar.invmanager.entity.vo.DictCodes;
import com.tticar.invmanager.service.ISysDictService;
import com.tticar.invmanager.service.ISysResourceService;
import com.tticar.invmanager.service.ISysTenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * Created by yandou on 2018/6/13.
 */
@Controller
public class IndexController extends BaseController {

    @Value("${oss.url}")
    private String pre;
    @Autowired
    private ISysResourceService sysResourceService;
    @Autowired
    private ISysTenantService sysTenantService;
    @Autowired
    private HttpSession session;

    /**
     * 权限，菜单控制
     * @param modelMap
     * @return
     */
    @GetMapping("/")
    public String index(ModelMap modelMap) {
        LoginUser loginUser = MySecurityUtils.currentUser();
        modelMap.put("resources", sysResourceService.getTree(loginUser.getRoleId(), null));
        modelMap.put("_urlPre", pre);
        return "index";
    }

//    @GetMapping("home")
//    public String home() {
//        return "home";
//    }

    @GetMapping("fileinput")
    public String fileinput() {
        return "/common/fileinput";
    }

    @PostMapping("setting/save")
    @ResponseBody
    public MyResponseBody settingSave(Long tenantId) {
        SysTenant sysTenant = sysTenantService.selectById(tenantId);
        if (sysTenant != null) {
            LoginUser loginUser = MySecurityUtils.currentUser();
            loginUser.setTenantId(tenantId);
            loginUser.setTenantName(sysTenant.getName());
            session.setAttribute("_tenant_id_", loginUser.getTenantId());
            MySecurityUtils.setCurrentUser(loginUser);
        }
        return MyResponseBody.success();
    }

    @GetMapping("setting")
    public String setting(ModelMap modelMap) {
//        List list = sysDictService.selTree(DictCodes.tticar);
//        modelMap.put("settings", ((Map) list.get(0)).get("children"));
        return "/thirdpart/setting";
    }

}
