package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.IMSalesDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tticar.invmanager.entity.MSalesDetail;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-15
 */
@Controller
@RequestMapping("/mSalesDetail")
public class MSalesDetailController extends BaseController {

    @Autowired
    private IMSalesDetailService mSalesDetailService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(MSalesDetail mSalesDetail) {
        mSalesDetailService.insertOrUpdate(mSalesDetail);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        mSalesDetailService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(mSalesDetailService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(MSalesDetail mSalesDetail, ModelMap modelMap) {
        modelMap.put("id", mSalesDetail.getId());
        return "/mSalesDetail/mSalesDetail_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(MSalesDetail mSalesDetail, MyPage<MSalesDetail> page) {
        return mSalesDetailService.selectPage(mSalesDetail, page);
    }

    @GetMapping
    public String index() {
        return "/mSalesDetail/mSalesDetail";
    }

}
