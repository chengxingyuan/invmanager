package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.MMaterielSku;
import com.tticar.invmanager.entity.MSales;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.IMMaterielSkuService;
import com.tticar.invmanager.service.IMSalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


@Controller
@RequestMapping("/stats")
public class StatsController extends BaseController {

    @Autowired
    private IMSalesService salesService;

    @Autowired
    private IMMaterielSkuService skuService;


    @GetMapping("buszList")
    @ResponseBody
    public Page buszList(MSales sales, MyPage<MSales> page) {
        return salesService.buszStats(sales, page);
    }

    @PostMapping("getSumInfo")
    @ResponseBody
    public Map getSumInfo(MSales sales) {
        return salesService.getSumInfo(sales);
    }

    @GetMapping("incomeList")
    @ResponseBody
    public Page incomeList(MyPage<MMaterielSku> page, String startTime, String endTime, String searchText) {
        return skuService.incomeList(page, startTime, endTime, searchText);
    }

    @PostMapping("getSumInfo2")
    @ResponseBody
    public Map getSumInfo2(String searchText, String startTime, String endTime) {
        return skuService.getSumInfo(startTime, endTime, searchText);
    }

    @GetMapping("stockList")
    @ResponseBody
    public Page stockList(MyPage<MMaterielSku> page, String startTime, String endTime, String searchText, String userName, Long wearhouseId) {
        return skuService.stockList(page, startTime, endTime, searchText, userName, wearhouseId);
    }

    @PostMapping("getSumInfo3")
    @ResponseBody
    public Map getSumInfo3(String startTime, String endTime, String searchText, String userName, Long wearhouseId) {
        return skuService.getSumInfoForStock(startTime, endTime, searchText, userName, wearhouseId);
    }

    @GetMapping("busz")
    public String busz() {
        return "/stats/statsBusz";
    }

    @GetMapping("income")
    public String income() {
        return "/stats/statsIncome";
    }

    @GetMapping("stock")
    public String stock() {
        return "/stats/statsStock";
    }


}
