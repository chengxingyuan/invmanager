package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.MSales;
import com.tticar.invmanager.entity.PPurchase;
import com.tticar.invmanager.entity.WRelativeUnit;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 财务管理 前端控制器
 * </p>
 *
 * @author jiwenbin
 * @since 2018-08-16
 */
@Controller
@RequestMapping("/finance")
public class FinanceController extends BaseController {

    @Autowired
    IPPurchaseService purchaseService;
    @Autowired
    IMSalesService salesService;

    @Autowired
    private IWRelativeUnitService relativeUnitService;
    @Autowired
    private IPurchaseReturnService purchaseReturnService;
    @Autowired
    private ISaleReturnService saleReturnService;

    /**
     * 应收款单列表
     * @return
     */
    @GetMapping("payableList")
    @ResponseBody
    public Page payableList(PPurchase pPurchase, MyPage<PPurchase> page) {
//        pPurchase.setPayStatus(0);
        return purchaseService.selectPage(pPurchase, page);
    }

    /**
     * 应付款单列表
     * @return
     */
    @GetMapping("expendList")
    @ResponseBody
    public Page expendList(PPurchase pPurchase, MyPage<PPurchase> page) {
        return purchaseService.selectExpendPage(pPurchase, page);
    }

    @GetMapping("detailGoodsList")
    @ResponseBody
    public List<Map> detailList(String code) {
        if (StringUtils.isBlank(code)) {
            return null;
        }
        List detailList = new ArrayList<>();
        if (code.contains("XS")) {
            Wrapper wrapper = new EntityWrapper<>();
            wrapper.eq("code", code);
            MSales mSales = salesService.selectOne(wrapper);
            detailList = this.salesService.getSaleDetailList(mSales.getId());
        } else if (code.contains("CT")) {
            detailList = this.purchaseReturnService.getPurchaseDetailList(code);
        }

        return detailList;
    }

    @GetMapping("detailGoodsListForExpend")
    @ResponseBody
    public List<Map> detailGoodsListForExpend(String code) {
        if (StringUtils.isBlank(code)) {
            return null;
        }
        List detailList = new ArrayList<>();
        if (code.contains("CG")) {
            Wrapper wrapper = new EntityWrapper<>();
            wrapper.eq("code", code);
            PPurchase pPurchase = purchaseService.selectOne(wrapper);
            detailList = this.purchaseService.getPurchaseDetailList(pPurchase.getId());
        } else if (code.contains("XT")) {
            detailList = this.saleReturnService.getSaleDetailList(code);
        }

        return detailList;
    }

    /**
     * 应收款单列表
     *
     * @return
     */
    @GetMapping("receivableList")
    @ResponseBody
    public Page receivableList(MSales mSales, MyPage<MSales> page) {
        mSales.setPayStatus(0);
        return salesService.selectPage(mSales, page);
    }


    /**
     * 应付款单
     * @return
     */
    @GetMapping("payable")
    public String payable(ModelMap modelMap, Long relativeId) {
        if(relativeId != null && relativeId != 0) {
            modelMap.put("relativeId", relativeId);
            WRelativeUnit unit = relativeUnitService.selectById(relativeId);
            modelMap.put("relativeName", unit.getName());
        }

        Wrapper w = new EntityWrapper<>();
        w.eq("status", 0);
        w.eq("pay_status", 0);
        w.setSqlSelect("count(1) totalCount,sum(amount_ing) payableAmount");
        Map result = purchaseService.selectMap(w);

        modelMap.put("totalCount", (result == null || result.get("totalCount") == null) ? 0 : result.get("totalCount")); // 未结清单数
        modelMap.put("payableAmount", (result == null || result.get("payableAmount") == null) ? 0 : result.get("payableAmount")); // 待付款总金额
        return "/finance/payable";
    }

    /**
     * 应收款单
     * @return
     */
    @GetMapping("receivable")
    public String receivable(ModelMap modelMap, String cusName) {
        modelMap.put("cusName", cusName);
        Wrapper w = new EntityWrapper<>();
        w.eq("status", 0);
        w.eq("pay_status", 0);
        w.setSqlSelect("count(1) totalCount,sum(paying_fee) receivableAmount");
        Map result = salesService.selectMap(w);

        modelMap.put("totalCount", (result == null || result.get("totalCount") == null) ? 0 : result.get("totalCount")); // 未结清单数
        modelMap.put("receivableAmount", (result == null || result.get("receivableAmount") == null) ? 0 : result.get("receivableAmount")); // 应收款总金额
        return "/finance/receivable";
    }

    /**
     * 应收款单结算
     * */
    @PostMapping("endCountBatch")
    @ResponseBody
    public MyResponseBody endCountBatch(@RequestParam("codes[]") List<String> codes) {
        MyResponseBody responseBody = MyResponseBody.success();
        salesService.endCountForInCome(codes);
        return responseBody;
    }

    /**
     * 应付款单结算
     * */
    @PostMapping("endCountBatchForExpend")
    @ResponseBody
    public MyResponseBody endCountBatchForExpend(@RequestParam("codes[]") List<String> codes) {
        MyResponseBody responseBody = MyResponseBody.success();
        purchaseService.endCountForExpend(codes);
        return responseBody;
    }

}
