package com.tticar.invmanager.controller;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.Enum.PayBusinessType;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.PPay;
import com.tticar.invmanager.entity.PPurchaseDetail;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.IPPayService;
import com.tticar.invmanager.service.IPPurchaseDetailService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 采购单详情 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-03
 */
@Controller
@RequestMapping("/pPurchaseDetail")
public class PPurchaseDetailController extends BaseController {

    @Autowired
    private IPPurchaseDetailService pPurchaseDetailService;

    @Autowired
    private IPPayService payService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(PPurchaseDetail pPurchaseDetail) {
        pPurchaseDetailService.insertOrUpdate(pPurchaseDetail);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        pPurchaseDetailService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(pPurchaseDetailService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(PPurchaseDetail pPurchaseDetail, ModelMap modelMap) {
        modelMap.put("id", pPurchaseDetail.getId());
        return "/pPurchaseDetail/pPurchaseDetail_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(PPurchaseDetail pPurchaseDetail, MyPage<PPurchaseDetail> page) {
        return pPurchaseDetailService.selectPage(pPurchaseDetail, page);
    }

    @PostMapping("payList")
    @ResponseBody
    public List payList(@RequestParam() Long purchaseId) {
        return payService.selectMapList(purchaseId, PayBusinessType.PURCHASE);
    }

    @GetMapping("allList")
    @ResponseBody
    public List allList(@RequestParam() Long purchaseId) {
        return pPurchaseDetailService.selectMapList(purchaseId);
    }

    @GetMapping
    public String index() {
        return "/pPurchaseDetail/pPurchaseDetail";
    }

    /**
     * 统计费用
     */
    @GetMapping("countPayAmount")
    @ResponseBody
    public MyResponseBody countPayAmount(@RequestParam() Long purchaseId) {
        MyResponseBody responseBody = MyResponseBody.success();
        // 根据purchaseId获取采购单详情列表
        Wrapper wrapper1 = new EntityWrapper();
        wrapper1.eq(PPurchaseDetail.PURCHASE_ID, purchaseId);
        List<PPurchaseDetail> pPurchaseDetailList = pPurchaseDetailService.selectList(wrapper1);
        // 根据purchaseId获取结算记录列表
        Wrapper wrapper2 = new EntityWrapper();
        wrapper2.eq(PPay.BUSINESS_ID, purchaseId);
        wrapper2.eq(PPay.BUSINESS_TYPE, PayBusinessType.PURCHASE.getCode());
        List<PPay> pPayList = payService.selectList(wrapper2);
        BigDecimal totalAmount = new BigDecimal(0);
        BigDecimal payAmount = new BigDecimal(0);

        // 计算金额
        if (CollectionUtils.isNotEmpty(pPurchaseDetailList)) {
            for (PPurchaseDetail pPurchaseDetail : pPurchaseDetailList) {
                totalAmount = totalAmount.add(pPurchaseDetail.getAmountActual());
            }
        }
        if (CollectionUtils.isNotEmpty(pPayList)) {
            for (PPay pPay : pPayList) {
                payAmount = payAmount.add(pPay.getAmount());
            }
        }

        Map<String, Object> map = new HashMap<>();
        map.put("totalAmount", totalAmount); // 总金额
        map.put("payAmount", payAmount); // 已付金额
        map.put("waitAmount", totalAmount.subtract(payAmount)); // 待付金额
        responseBody.setData(map);
        return responseBody;
    }

    /**
     * 采购详情入库
     * @param details
     * @return
     */
    @GetMapping(value = "inRepository")
    @ResponseBody
    public MyResponseBody inRepository(@RequestParam("details") String details, @RequestParam("purchaseId") Long purchaseId) {
        List<PPurchaseDetail> paramList = JSONArray.parseArray(details, PPurchaseDetail.class);
        pPurchaseDetailService.detailInRepository(paramList, purchaseId);
        return MyResponseBody.success();
    }
}
