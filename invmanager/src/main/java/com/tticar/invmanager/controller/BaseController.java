package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.tticar.invmanager.common.utils.DateEditor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yandou on 2018/6/13.
 */
public class BaseController {

    public Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected HttpServletResponse response;

    @InitBinder
    protected void webDataBinder(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        dataBinder.registerCustomEditor(Date.class, new DateEditor(true));
    }

    /**
     * 从request组长wrapper.
     *
     * @return
     */
    protected Map getParam() {
        Map<String, Object> param = new HashMap<>();
        Map<String, String[]> map = request.getParameterMap();
        for (String key : map.keySet()) {
            if ("page".equals(key) || "rows".equals(key)) {
                continue;
            }
            param.put(key, org.apache.commons.lang3.StringUtils.join((String[]) map.get(key)));
        }
        return param;
    }

    protected Wrapper getWrapper() {
        Map<String, String[]> map = request.getParameterMap();
        Wrapper wrapper = new EntityWrapper();
        for (String key : map.keySet()) {
            if ("page".equals(key) || "rows".equals(key)) {
                continue;
            }
            String value = org.apache.commons.lang3.StringUtils.join((String[]) map.get(key));
            if (key.contains("#") && org.springframework.util.StringUtils.hasText(value)) {
                String[] keys = key.split("#");
                wrapper.andNew().like(keys[0], value).or().like(keys[1], value);
            } else {
                if (isDate(value)) {
                    wrapper.between(key, value + " 00:00:00", value + " 23:59:59");
                    continue;
                }
                wrapper.like(org.springframework.util.StringUtils.hasText(value), key, value);
            }
        }
        return wrapper;
    }

    private boolean isDate(String datevalue) {
        return isDate(datevalue, DateEditor.DATE_PATTERNS[0]);
    }

    /**
     * 验证是否是日期.
     *
     * @param datevalue
     * @param dateFormat
     * @return
     */
    private boolean isDate(String datevalue, String dateFormat) {
        if (!datevalue.contains("-")) {
            return false;
        }
        try {
            new SimpleDateFormat(dateFormat).parse(datevalue);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 带参重定向
     *
     * @param path
     * @return
     */
    protected String redirect(String path) {
        return "redirect:" + path;
    }

    /**
     * 不带参重定向
     *
     * @param response
     * @param path
     * @return
     */
    protected String redirect(HttpServletResponse response, String path) {
        try {
            response.sendRedirect(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
