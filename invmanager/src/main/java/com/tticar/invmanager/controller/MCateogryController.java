package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.czy.tticar.dubbo.service.InvmanagerGoodsOrderService;
import com.tticar.invmanager.common.exception.MyServiceException;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.common.utils.tree.TreeNode;
import com.tticar.invmanager.entity.MCateogry;
import com.tticar.invmanager.entity.MCateogrySku;
import com.tticar.invmanager.entity.MSku;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.Tree;
import com.tticar.invmanager.service.IMCateogryService;
import com.tticar.invmanager.service.IMCateogrySkuService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 存货分类表 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-03
 */
@Controller
@RequestMapping("/mCateogry")
public class MCateogryController extends BaseController {

    @Autowired
    private IMCateogryService mCateogryService;
    @Autowired
    private IMCateogrySkuService mCateogrySkuService;

    @PostMapping("msku")
    @ResponseBody
    public MyResponseBody pmsku(@RequestBody List<MCateogrySku> cateogrySkus) {
        mCateogryService.updateSku(cateogrySkus);
        return MyResponseBody.success();
    }

    @PostMapping("updateCSku")
    @ResponseBody
    public MyResponseBody updateCSku(@RequestBody List<MSku> mskus, @RequestParam("cateogryId") Long cateogryId) {
        MyResponseBody requestBody = MyResponseBody.success();
//        Long cateogryId = Long.getLong(httpRequest.getParameter("cateogryId"));
        if (cateogryId == null || cateogryId == 0) {
            return requestBody;
        }
        List<MCateogrySku> mCateogrySkus = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(mskus)) {
            for (MSku sku : mskus) {
                MCateogrySku msku = new MCateogrySku();
                msku.setCategoryId(cateogryId);
                msku.setSkuId(sku.getId());
                mCateogrySkus.add(msku);
            }
        }
        mCateogrySkuService.updateList(mCateogrySkus, cateogryId);
        return requestBody;
    }

    @GetMapping("msku")
    @ResponseBody
    public MyResponseBody msku(MCateogrySku cateogrySku) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(mCateogryService.msku(cateogrySku));
        return responseBody;
    }

    @PostMapping
    @ResponseBody
    public MyResponseBody post(MCateogry mCateogry,HttpServletRequest request) {
        Wrapper wrapper = new EntityWrapper<>();
        wrapper.eq(MCateogry.NAME, mCateogry.getName());
        wrapper.ne("status", -1);
        wrapper.eq(MCateogry.PID, mCateogry.getPid() == null ? 0 : mCateogry.getPid());
        MCateogry cateogry = mCateogryService.selectOne(wrapper);
        if (cateogry != null && !cateogry.getId().equals(mCateogry.getId())) {
            throw new MyServiceException("该分类已存在，请勿重复添加");
        }
        if (mCateogry.getToken() != null && !mCateogry.getToken().equals(request.getSession().getAttribute("tokenOnly"))) {
            return MyResponseBody.success();
        }
        request.getSession().removeAttribute("tokenOnly");

        if(mCateogry.getSort() == null) {
            mCateogry.setSort(0);
        }
        mCateogryService.edit(mCateogry);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        mCateogryService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(mCateogryService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(MCateogry mCateogry, ModelMap modelMap, HttpServletRequest request) {
        MCateogry parent = new MCateogry();
        if (mCateogry.getPid() == null) {
            parent.setName("顶级目录");
        } else {
//            MCateogry cateogry = mCateogryService.selectById(mCateogry.getPid());
            parent.setId(mCateogry.getPid());
            parent.setName(mCateogry.getName());
        }
        long token = System.currentTimeMillis();
        request.getSession().setAttribute("tokenOnly",token);
        modelMap.put("parent", parent);
        modelMap.put("id", mCateogry.getId());
        return "/mCateogry/mCateogry_edit";
    }

    /**
     * 获取树形数据.
     *
     * @return
     */
    @GetMapping("tree")
    @ResponseBody
    public List tree(Long id) {
        return mCateogryService.getTree(id);
    }

    @GetMapping("treeAll")
    @ResponseBody
    public List<Tree> treeAll() {
        return mCateogryService.tree();
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(MCateogry mCateogry, MyPage<MCateogry> page) {
        return mCateogryService.selectPage(mCateogry, page);
    }

    @RequestMapping("allList")
    @ResponseBody
    public MyResponseBody allList() {
        MyResponseBody responseBody = MyResponseBody.success();
        Wrapper wrapper = new EntityWrapper<>();
        wrapper.eq(MCateogry.PID, 0);
        wrapper.eq(MCateogry.STATUS, 0);
        wrapper.orderBy("sort",true);
        List list = mCateogryService.selectList(wrapper);
        responseBody.setData(list);
        return responseBody;
    }
    @RequestMapping("allListNew")
    @ResponseBody
    public MyResponseBody allListNew() {
        MyResponseBody responseBody = MyResponseBody.success();
        Wrapper wrapper = new EntityWrapper<>();
        wrapper.eq(MCateogry.STATUS, 0);
        List list = mCateogryService.selectList(wrapper);
        responseBody.setData(list);
        return responseBody;
    }

    @GetMapping
    public String index() {
        return "/mCateogry/mCateogry";
    }

    @PostMapping("tree")
    @ResponseBody
    public List<Tree> tree() {
        Tree root = new Tree(0L,"全部分类");
        root.setChildren(this.mCateogryService.tree());
        List<Tree> list = new ArrayList<>();
        list.add(root);
        return list;
    }

    /**
     * 同步天天爱车分类
     * */
    @GetMapping("synchronization")
    @ResponseBody
    public MyResponseBody synchronization() {
        MyResponseBody responseBody = MyResponseBody.success();
        mCateogryService.synchronizationFromTticar();
        return responseBody;
    }


}
