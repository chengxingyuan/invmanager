package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.MSku;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.IMSkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 物料规格 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-05
 */
@Controller
@RequestMapping("/mSku")
public class MSkuController extends BaseController {

    @Autowired
    private IMSkuService mSkuService;

    @GetMapping("select")
    @ResponseBody
    public List select() {
        Wrapper w = Condition.create();
        w.eq(MSku.STATUS, MSku.ZERO);
        return mSkuService.selectList(w);
    }

    @PostMapping
    @ResponseBody
    public MyResponseBody post(MSku mSku) {
        Wrapper wrapper = new EntityWrapper();
        wrapper.eq("name", mSku.getName());
        wrapper.ne("status", -1);
        if (mSku.getId() != null) {
            wrapper.ne("id", mSku.getId());
        }
        MSku sku = this.mSkuService.selectOne(wrapper);
        if (sku != null) {
            return MyResponseBody.failure("名称已存在");
        }
        if (mSku.getSort() == null) {
            mSku.setSort(0);
        }
        mSkuService.insertOrUpdateOne(mSku);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) throws Exception {
        MyResponseBody responseBody = MyResponseBody.success();
        mSkuService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(mSkuService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(MSku mSku, ModelMap modelMap) {
        modelMap.put("id", mSku.getId());
        return "/mSku/mSku_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(MSku mSku, MyPage<MSku> page) {
        return mSkuService.selectPage(mSku, page);
    }

    @GetMapping
    public String index() {
        return "/mSku/mSku";
    }

    @PostMapping("valid")
    @ResponseBody
    public boolean valid(Long id, String name) {
        if (id == null) {
            Wrapper wrapper = new EntityWrapper();
            wrapper.eq("name", name);
            MSku sku = this.mSkuService.selectOne(wrapper);
            if (sku != null) {
                return false;
            }
        } else {
            Wrapper wrapper = new EntityWrapper();
            wrapper.eq("name", name);
            wrapper.ne("id", id);
            MSku sku = this.mSkuService.selectOne(wrapper);
            if (sku != null) {
                return false;
            }
        }
        return true;
    }

}
