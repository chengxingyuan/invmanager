package com.tticar.invmanager.controller;


import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.shiro.LoginUser;
import com.tticar.invmanager.common.shiro.MySecurityUtils;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.SysResource;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.ISysResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 资源管理 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-17
 */
@Controller
@RequestMapping("sysResource")
public class SysResourceController extends BaseController {

    @Autowired
    private ISysResourceService sysResourceService;

    /**
     * @param roleId 目标角色
     * @return
     */
    @GetMapping("tree")
    @ResponseBody
    public List tree(Long roleId) {
        LoginUser loginUser = MySecurityUtils.currentUser();
        return sysResourceService.getTree(loginUser.getRoleId(), roleId);
    }

    @PostMapping
    @ResponseBody
    public MyResponseBody post(SysResource sysResource) {
        sysResourceService.insertOrUpdate(sysResource);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        sysResourceService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(sysResourceService.selectById(id));
        return responseBody;
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(SysResource sysResource, MyPage<SysResource> page) {
        return sysResourceService.selectPage(sysResource, page);
    }

    @GetMapping("edit")
    public String edit(SysResource parent, ModelMap modelMap) {
        if (Objects.isNull(parent.getPid())) {
            parent.setPid(0l);
            parent.setName("顶级目录");
        }
        modelMap.put("parent", parent);
        modelMap.put("id", parent.getId());
        return "/sys/sysResource/sysResource_edit";
    }

    @GetMapping
    public String index() {
        return "/sys/sysResource/sysResource";
    }

}
