package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.SysUser;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.ISysTenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tticar.invmanager.entity.SysTenant;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 租户 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
@Controller
@RequestMapping("/sysTenant")
public class SysTenantController extends BaseController {

    @Autowired
    private ISysTenantService sysTenantService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(SysTenant sysTenant, SysUser sysUser, String newpassword) {
        sysTenantService.save(sysTenant, sysUser, newpassword);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        sysTenantService.delete(ids);
        return responseBody;
    }

    @GetMapping("select")
    @ResponseBody
    public List select() {
        return sysTenantService.select();
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(sysTenantService.getById(id));
        return responseBody;
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(SysTenant sysTenant, MyPage<SysTenant> page) {
        return sysTenantService.selectPage(sysTenant, page);
    }

    @GetMapping("edit")
    public String edit(SysTenant sysTenant, ModelMap modelMap) {
        modelMap.put("id", sysTenant.getId());
        return "/sys/sysTenant/sysTenant_edit";
    }

    @GetMapping
    public String index() {
        return "/sys/sysTenant/sysTenant";
    }

    @RequestMapping("isNeedRelative")
    @ResponseBody
    public MyResponseBody isNeedRelative(Long id,Integer status) {
        MyResponseBody responseBody = MyResponseBody.success();
        SysTenant sysTenant = new SysTenant();
        sysTenant.setId(id);
        sysTenant.setNeedRelativeInv(status);
        sysTenantService.updateById(sysTenant);
        return responseBody;
    }
}
