package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.exception.MyServiceException;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.SysRole;
import com.tticar.invmanager.entity.SysRoleResource;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
@Controller
@RequestMapping("/sysRole")
public class SysRoleController extends BaseController {

    @Autowired
    private ISysRoleService sysRoleService;

    @GetMapping("select")
    @ResponseBody
    public List select(){
        return sysRoleService.select();
    }

    @PostMapping("resource")
    @ResponseBody
    public MyResponseBody presource(@RequestBody List<SysRoleResource> roleResources) {
        sysRoleService.insertOrUpdate(roleResources);
        return MyResponseBody.success();
    }

    @GetMapping("/resource/{roleId}")
    @ResponseBody
    public MyResponseBody resource(@PathVariable("roleId") Long roleId) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(sysRoleService.getResourceByRoleId(roleId));
        return responseBody;
    }

    @PostMapping
    @ResponseBody
    public MyResponseBody post(SysRole sysRole) {
        Wrapper wrapper = new EntityWrapper<>();
        wrapper.ne("status", -1);
        List<SysRole> sysRoles = sysRoleService.selectList(wrapper);
        if (sysRole.getId() == null) {
            if (sysRoles != null && sysRoles.size() > 0) {
                for (SysRole sysRole1 : sysRoles) {
                    if (sysRole1.getName().equals(sysRole.getName())) {
                        throw new MyServiceException("角色名已存在，请重新命名");
                    }
                }
            }
        }else {
            if (sysRoles != null && sysRoles.size() > 0) {
                for (SysRole sysRole1 : sysRoles) {
                    if (sysRole1.getName().equals(sysRole.getName()) && !sysRole.getId().equals(sysRole1.getId())) {
                        throw new MyServiceException("角色名已存在，请重新命名");
                    }
                }
            }
        }
        sysRoleService.insertOrUpdate(sysRole);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        sysRoleService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(sysRoleService.selectById(id));
        return responseBody;
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(SysRole sysRole, MyPage<SysRole> page) {
        return sysRoleService.selectPage(sysRole, page);
    }

    @GetMapping("edit")
    public String edit(SysRole sysRole, ModelMap modelMap) {
        modelMap.put("id", sysRole.getId());
        return "/sys/sysRole/sysRole_edit";
    }

    @GetMapping
    public String index() {
        return "/sys/sysRole/sysRole";
    }

}
