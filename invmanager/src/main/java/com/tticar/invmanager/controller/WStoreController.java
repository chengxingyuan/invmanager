package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.shiro.LoginUser;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.common.utils.ServiceUtil;
import com.tticar.invmanager.entity.WStore;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.IWStoreService;
import com.tticar.invmanager.tticar.service.IStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 门店 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-24
 */
@Controller
@RequestMapping("/wStore")
public class WStoreController extends BaseController {

    @Autowired
    private IWStoreService wStoreService;

    @Autowired
    private IStoreService tticarStoreService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(WStore wStore) {
        wStoreService.insertOrUpdate(wStore);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        wStoreService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(wStoreService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(WStore wStore, ModelMap modelMap) {
        modelMap.put("id", wStore.getId());
        return "/wStore/wStore_edit";
    }



    @RequestMapping("select")
    @ResponseBody
    public List select(WStore wStore,HttpServletRequest request) {
        return tticarStoreService.selectList();
        //return wStoreService.select(wStore);
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(WStore wStore, MyPage<WStore> page) {
        return wStoreService.selectPage(wStore, page);
    }

    @GetMapping
    public String index() {
        return "/wStore/wStore";
    }

}
