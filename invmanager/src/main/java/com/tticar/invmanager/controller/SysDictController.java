package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.SysDict;
import com.tticar.invmanager.entity.vo.DictCodes;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.ISysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 字典表 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-08
 */
@Controller
@RequestMapping("/sysDict")
public class SysDictController extends BaseController {

    @Autowired
    private ISysDictService sysDictService;

    /**
     * 动态字段.
     *
     * @param code
     * @return
     */
    @GetMapping("form/{code}")
    @ResponseBody
    public MyResponseBody loadForm(@PathVariable("code") String code) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(sysDictService.loadForm(DictCodes.get(code)));
        return responseBody;
    }

    @PostMapping("form")
    @ResponseBody
    public MyResponseBody formSubmit() {
        Map map = request.getParameterMap(); // key:value=code:value
        sysDictService.formSubmit(map);
        return MyResponseBody.success();
    }

    /**
     * 获取字典树.
     *
     * @param code
     * @return
     */
    @GetMapping("{code}/tree")
    @ResponseBody
    public List selTree(@PathVariable("code") String code) {
        return sysDictService.selTree(DictCodes.get(code));
    }

    /**
     * @param code
     * @return
     */
    @GetMapping("{code}/map")
    @ResponseBody
    public MyResponseBody selMap(@PathVariable("code") String code) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(sysDictService.selMap(DictCodes.get(code)));
        return responseBody;
    }

    /**
     * @param code
     * @return
     */
    @RequestMapping("{code}")
    @ResponseBody
    public List selList(@PathVariable("code") String code) {
        return sysDictService.selList(DictCodes.get(code));
    }

    @PostMapping
    @ResponseBody
    public MyResponseBody post(SysDict sysDict) {
        sysDictService.edit(sysDict);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        sysDictService.delete(ids);
        return responseBody;
    }

    @GetMapping("/id/{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(sysDictService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(SysDict sysDict, ModelMap modelMap) {
        if (java.util.Objects.isNull(sysDict.getPid())) {
            sysDict.setPid(0l);
            sysDict.setName("顶级目录");
        }
        modelMap.put("parent", sysDict);
        modelMap.put("id", sysDict.getId());
        return "/sys/sysDict/sysDict_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(SysDict sysDict, MyPage<SysDict> page) {
        return sysDictService.selectPage(sysDict, page);
    }

    @GetMapping
    public String index() {
        return "/sys/sysDict/sysDict";
    }

}
