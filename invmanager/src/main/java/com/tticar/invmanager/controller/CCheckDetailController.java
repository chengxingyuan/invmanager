package com.tticar.invmanager.controller;

import com.alibaba.fastjson.JSON;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.common.utils.date.DateStyle;
import com.tticar.invmanager.common.utils.date.DateUtil;
import com.tticar.invmanager.entity.CCheck;
import com.tticar.invmanager.entity.CCheckDetail;
import com.tticar.invmanager.entity.WWearhouse;
import com.tticar.invmanager.service.ICCheckDetailService;
import com.tticar.invmanager.service.ICCheckService;
import com.tticar.invmanager.service.IWWearhouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 盘点单详情 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-13
 */
@Controller
@RequestMapping("/cCheckDetail")
public class CCheckDetailController extends BaseController {

    @Autowired
    private ICCheckDetailService cCheckDetailService;
    @Autowired
    private ICCheckService checkService;
    @Autowired
    private IWWearhouseService wearhouseService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(CCheckDetail cCheckDetail) {
        cCheckDetailService.insertOrUpdate(cCheckDetail);
        return MyResponseBody.success();
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(cCheckDetailService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(CCheckDetail cCheckDetail, ModelMap modelMap) {
        modelMap.put("id", cCheckDetail.getId());
        return "/cCheckDetail/cCheckDetail_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public List<Map> list(Long checkId) {
        return this.cCheckDetailService.getDetailListById(checkId);
    }

    @GetMapping("printPage")
    public String printPage(Long checkId,ModelMap modelMap) {
        List<Map> detailListById = this.cCheckDetailService.getDetailListById(checkId);
        modelMap.put("thisTime",DateUtil.DateToString(new Date(), DateStyle.YYYY_MM_DD_HH_MM_SS));
        modelMap.put("checkId",checkId);
        return "/cCheckDetail/cCheckDetail_print";
    }

    @RequestMapping("printData")
    @ResponseBody
    public List printData(@RequestParam("checkId") Long checkId){
        return  this.cCheckDetailService.getDetailListById(checkId);
    }

    @GetMapping
    public String index(@RequestParam("checkId") long checkId, ModelMap modelMap) {
        modelMap.put("checkId", checkId);
        CCheck check = this.checkService.selectById(checkId);
        modelMap.put("check", check);
        modelMap.put("ctime", DateUtil.DateToString(check.getCtime(), DateStyle.YYYY_MM_DD));
        WWearhouse house = this.wearhouseService.selectById(check.getHouseId());
        modelMap.put("houseName", house.getName());
        return "/cCheckDetail/cCheckDetail";
    }

}
