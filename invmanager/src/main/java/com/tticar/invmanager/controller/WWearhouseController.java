package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.Enum.CodeType;
import com.tticar.invmanager.common.exception.MyServiceException;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.common.utils.PinYinUtil;
import com.tticar.invmanager.entity.MBrand;
import com.tticar.invmanager.entity.WWearhouse;
import com.tticar.invmanager.entity.vo.KwTree;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.Tree;
import com.tticar.invmanager.service.ICodeCounterService;
import com.tticar.invmanager.service.IWPositionService;
import com.tticar.invmanager.service.IWWearhouseService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-02
 */
@Controller
@RequestMapping("/wWearhouse")
public class WWearhouseController extends BaseController {

    @Autowired
    private IWWearhouseService wWearhouseService;

    @Autowired
    private IWPositionService positionService;

    @Autowired
    private ICodeCounterService codeCounterService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(WWearhouse wWearhouse) {
        //名称校验
        Wrapper wrapper = new EntityWrapper();
        wrapper.eq("name", wWearhouse.getName());
        wrapper.ne("status", -1);
        WWearhouse valid;
        if(wWearhouse.getId() != null) {
             wrapper.ne("id", wWearhouse.getId());
        }
        valid = this.wWearhouseService.selectOne(wrapper);
        if(valid != null) {
            return MyResponseBody.failure("名称已存在");
        }

        if (wWearhouse.getStatus() != 0 && wWearhouse.getId() != null) {
            List<Long> longs = positionService.selectPositionIdsByWearhouseId(wWearhouse.getId());
            if (longs.size() > 0) {
                return MyResponseBody.failure("仓库下有库位，不能禁用");
            }
        }

        wWearhouse.setNamespell(PinYinUtil.getPinyinString(wWearhouse.getName()));
        if(wWearhouse.getId() == null) {
            wWearhouse.setCode(codeCounterService.getCode(CodeType.CK20, "", 2));
        }

        if(StringUtils.isEmpty(wWearhouse.getPrincipal())) {
            wWearhouse.setPrincipal("");
        }
        if(StringUtils.isEmpty(wWearhouse.getPrincipalTel())) {
            wWearhouse.setPrincipalTel("");
        }
        if(StringUtils.isEmpty(wWearhouse.getLocation())) {
            wWearhouse.setLocation("");
        }
        wWearhouseService.insertOrUpdate(wWearhouse);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        wWearhouseService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(wWearhouseService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(WWearhouse wWearhouse, ModelMap modelMap) {
        if (java.util.Objects.isNull(wWearhouse.getPid())) {
            wWearhouse.setPid(0l);
            wWearhouse.setName("顶级目录");
        }
        modelMap.put("parent", wWearhouse);
        modelMap.put("id", wWearhouse.getId());
        return "/wWearhouse/wWearhouse_edit";
    }

    @GetMapping(path = "list")
    @ResponseBody
    public Page list(WWearhouse wWearhouse, MyPage<WWearhouse> page) {
        return wWearhouseService.selectPage(wWearhouse, page);
    }

    @GetMapping(path = "listForAvailable")
    @ResponseBody
    public Page listForAvailable(WWearhouse wWearhouse, MyPage<WWearhouse> page) {
        return wWearhouseService.selectPageForAvailable(wWearhouse, page);
    }

    @PostMapping("list")
    @ResponseBody
    public List<WWearhouse> list() {
        Wrapper wrapper = new EntityWrapper();
        wrapper.eq("status", 0);
        return wWearhouseService.selectList(wrapper);
    }

    @GetMapping
    public String index() {
        return "/wWearhouse/wWearhouse";
    }

    @PostMapping("getRepositoryList")
    @ResponseBody
    public List<Map<String, Object>> supplierList(WWearhouse wWearhouse, MyPage<WWearhouse> page) {
        Page tmp = wWearhouseService.selectPageForSearch(wWearhouse, page);
        return (List<Map<String, Object>>) tmp.getRecords();
    }

    @GetMapping("tree")
    public String tree(ModelMap modelMap) {
        return "/wWearhouse/wWearhouseTree";
    }

    @PostMapping("/tree")
    @ResponseBody
    public List<Tree> tree() {
        return wWearhouseService.tree();
    }

    @GetMapping("treeGrid")
    public String treeGrid(Long skuId, String house, Integer type,Long houseId, ModelMap modelMap) {
        modelMap.put("skuId", skuId);
        modelMap.put("house", house);
        modelMap.put("type", type);
        modelMap.put("houseId", houseId);
        if(type == 0) { //出库
            return "/wWearhouse/wWearhouseTreeGridOut";
        } else { //入库
            return "/wWearhouse/wWearhouseTreeGridIn";
        }
    }

    @GetMapping("outNewVersion")
    public String outNewVersion(Long skuId, ModelMap modelMap) {
        modelMap.put("skuId", skuId);
        return "/wWearhouse/wWearhouseOutNewVersion";

    }

    @PostMapping("/treeGrid")
    @ResponseBody
    public List<KwTree> treeGrid(Long skuId, Integer type, Long houseId) {
        return wWearhouseService.treeGrid(skuId, type, houseId);
    }


    @GetMapping("treeGridBind")
    public String treeGrid(Long materielId, String house, ModelMap modelMap) {
        modelMap.put("materielId", materielId);
        modelMap.put("house", house);
        return "/wWearhouse/wWearhouseTreeGridBind";
    }

    @PostMapping("/treeGridBind")
    @ResponseBody
    public List<KwTree> treeGridBind(Long materielId) {
        return wWearhouseService.treeGridForBind(materielId);
    }

}
