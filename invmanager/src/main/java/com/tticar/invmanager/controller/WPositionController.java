package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.IWPositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tticar.invmanager.entity.WPosition;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 库位表 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-31
 */
@Controller
@RequestMapping("/wPosition")
public class WPositionController extends BaseController {

    @Autowired
    private IWPositionService wPositionService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(WPosition wPosition) {
        wPositionService.edit(wPosition);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        wPositionService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(wPositionService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(WPosition wPosition, ModelMap modelMap) {
        modelMap.put("id", wPosition.getId());
        return "/wPosition/wPosition_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(WPosition wPosition, MyPage<WPosition> page) {
        return wPositionService.selectPage(wPosition, page);
    }

    @GetMapping
    public String index() {
        return "/wPosition/wPosition";
    }

    @PostMapping("getPositionList")
    @ResponseBody
    public List<Map<String, Object>> supplierList(WPosition wPosition, MyPage<WPosition> page) {
        Page tmp = wPositionService.selectPage(wPosition, page);
        return (List<Map<String, Object>>) tmp.getRecords();
    }

    @GetMapping("getPositionListByWearhouseId")
    @ResponseBody
    public List<Map<String, Object>> getPositionListByWearhouseId(/*WPosition wPosition, MyPage<WPosition> page,*/
                                                                  @RequestParam("wearhouseId")Long wearhouseId) {
        MyPage<WPosition> page = new MyPage<>();
        page.setPage(1);
        page.setRows(20);
        WPosition wPosition = new WPosition();
        Page tmp = wPositionService.selectPageByWearhouseId(wPosition, page,wearhouseId);
        return (List<Map<String, Object>>) tmp.getRecords();
    }

}
