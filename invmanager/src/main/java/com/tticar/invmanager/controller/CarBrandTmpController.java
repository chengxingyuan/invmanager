package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.ICarBrandTmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tticar.invmanager.entity.CarBrandTmp;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 车品牌 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-09-19
 */
@Controller
@RequestMapping("/carBrandTmp")
public class CarBrandTmpController extends BaseController {

    @Autowired
    private ICarBrandTmpService carBrandTmpService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(CarBrandTmp carBrandTmp) {
        carBrandTmpService.insertOrUpdate(carBrandTmp);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        carBrandTmpService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(carBrandTmpService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(CarBrandTmp carBrandTmp, ModelMap modelMap) {
        modelMap.put("id", carBrandTmp.getId());
        return "/carBrandTmp/carBrandTmp_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(CarBrandTmp carBrandTmp, MyPage<CarBrandTmp> page) {
        return carBrandTmpService.selectPage(carBrandTmp, page);
    }

    @GetMapping
    public String index() {
        return "/carBrandTmp/carBrandTmp";
    }

}
