package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.common.utils.ServiceUtil;
import com.tticar.invmanager.entity.PPurchase;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.IPPayService;
import com.tticar.invmanager.service.IPPurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tticar.invmanager.entity.PPay;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 采购单支付流水 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-06
 */
@Controller
@RequestMapping("/pPay")
public class PPayController extends BaseController {

    @Autowired
    private IPPayService pPayService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(PPay pPay) {
        pPay.setUsername(ServiceUtil.getLoginUserName());
        pPayService.insertOrUpdate(pPay);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        pPayService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(pPayService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(PPay pPay, ModelMap modelMap) {
        modelMap.put("id", pPay.getId());
        return "/pPay/pPay_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(PPay pPay, MyPage<PPay> page) {
        return pPayService.selectPage(pPay, page);
    }

    @GetMapping
    public String index() {
        return "/pPay/pPay";
    }

}
