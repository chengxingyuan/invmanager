package com.tticar.invmanager.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.service.IBybsDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tticar.invmanager.entity.BybsDetail;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 报溢报损详情 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-18
 */
@Controller
@RequestMapping("/bybsDetail")
public class BybsDetailController extends BaseController {

    @Autowired
    private IBybsDetailService bybsDetailService;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(BybsDetail bybsDetail) {
        bybsDetailService.insertOrUpdate(bybsDetail);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(@RequestParam("ids[]") List<Long> ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        bybsDetailService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(bybsDetailService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(BybsDetail bybsDetail, ModelMap modelMap) {
        modelMap.put("id", bybsDetail.getId());
        return "/bybsDetail/bybsDetail_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(BybsDetail bybsDetail, MyPage<BybsDetail> page) {
        return bybsDetailService.selectPage(bybsDetail, page);
    }

    @GetMapping
    public String index() {
        return "/bybsDetail/bybsDetail";
    }

    @PostMapping("getDetailList")
    @ResponseBody
    public List<Map> getDetailList(Long id) {
        if(id == null || id == 0) {
            return new ArrayList<>();
        }
        return this.bybsDetailService.getDetailList(id);
    }

}
