package com.tticar.invmanager.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.common.exception.MyServiceException;
import com.tticar.invmanager.common.utils.MyResponseBody;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.RelativeGoodsDTO;
import com.tticar.invmanager.mapper.RepositoryCountMapper;
import com.tticar.invmanager.service.IChangeRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tticar.invmanager.entity.ChangeRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 调拨单 前端控制器
 * </p>
 *
 * @author joeyYan
 * @since 2018-12-10
 */
@Controller
@RequestMapping("/changeRepository")
public class ChangeRepositoryController extends BaseController {

    @Autowired
    private IChangeRepositoryService changeRepositoryService;
    @Autowired
    private RepositoryCountMapper repositoryCountMapper;

    @PostMapping
    @ResponseBody
    public MyResponseBody post(ChangeRepository changeRepository) {
        changeRepositoryService.insertOrUpdate(changeRepository);
        return MyResponseBody.success();
    }

    @PostMapping("delete")
    @ResponseBody
    public MyResponseBody delete(Long ids) {
        MyResponseBody responseBody = MyResponseBody.success();
        changeRepositoryService.delete(ids);
        return responseBody;
    }

    @GetMapping("{id}")
    @ResponseBody
    public MyResponseBody get(@PathVariable("id") Long id) {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(changeRepositoryService.selectById(id));
        return responseBody;
    }

    @GetMapping("edit")
    public String edit(ChangeRepository changeRepository, ModelMap modelMap) {
        modelMap.put("id", changeRepository.getId());
        return "/changeRepository/changeRepository_edit";
    }

    @GetMapping("list")
    @ResponseBody
    public Page list(ChangeRepository changeRepository, MyPage<ChangeRepository> page) {
        return changeRepositoryService.selectPage(changeRepository, page);
    }

    @GetMapping
    public String index() {
        return "/changeRepository/changeRepository";
    }

    @GetMapping("getAddGoodsList")
    @ResponseBody
    public List getAddGoodsList(@RequestParam("wearhouseId")Long wearhouseId, HttpServletRequest request){
        if (wearhouseId == null) {
            throw new MyServiceException("请先选择调出仓库");
        }
        Map map = new HashMap<>(4);
        map.put("wearhouseId", wearhouseId);
        Map<String, String[]> parameterMap = request.getParameterMap();
        if (parameterMap.get("q") != null) {
            String goodsName = parameterMap.get("q")[0];
            if (goodsName.contains(":")) {
                goodsName = goodsName.substring(0, goodsName.indexOf(":")) + goodsName.substring(goodsName.indexOf(":")+1,goodsName.length());
            }
            map.put("goodsName", goodsName);
        }
        List<Map> goodsList = repositoryCountMapper.changeRepositoryAddGoodsList(map);
        if (goodsList != null && goodsList.size() > 0) {
            for (int i =0;i < goodsList.size();i++) {
                goodsList.get(i).put("name", goodsList.get(i).get("materielName") + " " + goodsList.get(i).get("skuName"));
            }
        }

        return goodsList;
    }

    @RequestMapping("changeRepositoryAddDetail")
    @ResponseBody
    public List<Map> queryRelativeGoodsForPlaceOrder(@RequestParam("rows")String rows,@RequestParam("wearhouseId")Long wearhouseId) {
        List<Map> goodsMap = JSONArray.parseArray(rows, Map.class);
        if (goodsMap != null && goodsMap.size() > 0) {
            for (int i =0;i<goodsMap.size(); i++) {
                Map map = new HashMap<>(4);
                map.put("id", goodsMap.get(i).get("id"));
                map.put("wearhouseId", wearhouseId);
                List<Map> maps = repositoryCountMapper.changeRepositoryAddGoodsList(map);
                goodsMap.get(i).put("code", maps.get(0).get("code"));
                goodsMap.get(i).put("repositoryCount", maps.get(0).get("count"));
            }
        }
        return goodsMap;
    }

    @RequestMapping("changeRepositoryAddSubmit")
    @ResponseBody
    public MyResponseBody changeRepositoryAdd(String rows, Long fromWearhouseId, Long toWearhouseId, Date changeTime,String remark){
        MyResponseBody response = MyResponseBody.success();
        changeRepositoryService.insertNew(rows, fromWearhouseId, toWearhouseId, changeTime,remark);
        return response;
    }

    @PostMapping("changeCorrect")
    @ResponseBody
    public MyResponseBody changeCorrect(String dbCode) {
        MyResponseBody responseBody = MyResponseBody.success();
        changeRepositoryService.correctChangeOrder(dbCode);
        return responseBody;
    }

    @GetMapping("changeGoodsList")
    @ResponseBody
    public List<Map> changeGoodsList(String code) {
        return this.repositoryCountMapper.getChangeGoodsList(code);
    }
}
