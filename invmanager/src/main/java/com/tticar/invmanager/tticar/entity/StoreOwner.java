package com.tticar.invmanager.tticar.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 店铺所有者
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-12
 */
@TableName("store_owner")
public class StoreOwner implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 店主名称
     */
    private String username;

    /**
     * 用户名（登录使用）
     */
    private String uname;

    /**
     * 手机号
     */
    private Long mobile;

    /**
     * 密码
     */
    private String password;

    /**
     * 密码盐
     */
    private Long salt;

    private Integer gender;

    private Long age;

    private String qq;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 身份证号
     */
    private String idcard;

    /**
     * 状态 1 已审核 2未审核 3已冻结 4已删除
     */
    private Integer status;

    /**
     * 状态 1 已审核 2未审核 3已冻结 4已删除
     */
    @TableField(exist = false)
    private String statusName;

    /**
     * 头像
     */
    private String headerPic;

    /**
     * 店铺类型
0 门店 1供应商
     */
    private Integer type;

    /**
     * 是否已经注册环信
默认0未注册
1注册
     */
    private Integer isHXReady;

    /**
     * 微信唯一标识
     */
    private String openId;

    /**
     * 默认首页  0好店长  1天天爱车
     */
    private Integer shopStatus;

    /**
     * 默认首页  0好店长  1天天爱车
     */
    private Integer defIndex;

    /**
     * 修改状态备注原因
     */
    private String reason;

    /**
     * 回头率
     */
    private String turnBack;

    /**
     * 买家数
     */
    private Integer buysNum;

    /**
     * 操作人(每次修改记录操作人id)
     */
    private Long opter;

    /**
     * 与store表customMgr字段一致，为客户经理编号，但不能修改此字段
     */
    private String customNo;

    /**
     * 0未审核  1审核通过  2审核失败
     */
    private Integer companyNameCheck;

    /**
     * 拒绝原因
     */
    private String companyNameReason;

    private Date createtime;

    private Date modifytime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }
    public Long getMobile() {
        return mobile;
    }

    public void setMobile(Long mobile) {
        this.mobile = mobile;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public Long getSalt() {
        return salt;
    }

    public void setSalt(Long salt) {
        this.salt = salt;
    }
    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }
    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }
    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getHeaderPic() {
        return headerPic;
    }

    public void setHeaderPic(String headerPic) {
        this.headerPic = headerPic;
    }
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    public Integer getIsHXReady() {
        return isHXReady;
    }

    public void setIsHXReady(Integer isHXReady) {
        this.isHXReady = isHXReady;
    }
    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
    public Date getModifytime() {
        return modifytime;
    }

    public void setModifytime(Date modifytime) {
        this.modifytime = modifytime;
    }
    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
    public Integer getShopStatus() {
        return shopStatus;
    }

    public void setShopStatus(Integer shopStatus) {
        this.shopStatus = shopStatus;
    }
    public Integer getDefIndex() {
        return defIndex;
    }

    public void setDefIndex(Integer defIndex) {
        this.defIndex = defIndex;
    }
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    public String getTurnBack() {
        return turnBack;
    }

    public void setTurnBack(String turnBack) {
        this.turnBack = turnBack;
    }
    public Integer getBuysNum() {
        return buysNum;
    }

    public void setBuysNum(Integer buysNum) {
        this.buysNum = buysNum;
    }
    public Long getOpter() {
        return opter;
    }

    public void setOpter(Long opter) {
        this.opter = opter;
    }
    public String getCustomNo() {
        return customNo;
    }

    public void setCustomNo(String customNo) {
        this.customNo = customNo;
    }
    public Integer getCompanyNameCheck() {
        return companyNameCheck;
    }

    public void setCompanyNameCheck(Integer companyNameCheck) {
        this.companyNameCheck = companyNameCheck;
    }
    public String getCompanyNameReason() {
        return companyNameReason;
    }

    public void setCompanyNameReason(String companyNameReason) {
        this.companyNameReason = companyNameReason;
    }

    public String getStatusName() {
        switch (status) {
            case 1 : return "已审核";
            case 2 : return "未审核";
            case 3 : return "已冻结";
            case 4 : return "已删除";
        }
        return "";
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public static final String USERNAME = "username";

    public static final String UNAME = "uname";

    public static final String MOBILE = "mobile";

    public static final String PASSWORD = "password";

    public static final String SALT = "salt";

    public static final String GENDER = "gender";

    public static final String AGE = "age";

    public static final String QQ = "qq";

    public static final String EMAIL = "email";

    public static final String IDCARD = "idcard";

    public static final String STATUS = "status";

    public static final String HEADERPIC = "headerPic";

    public static final String TYPE = "type";

    public static final String ISHXREADY = "isHXReady";

    public static final String CREATETIME = "createtime";

    public static final String MODIFYTIME = "modifytime";

    public static final String OPENID = "openId";

    public static final String SHOPSTATUS = "shopStatus";

    public static final String DEFINDEX = "defIndex";

    public static final String REASON = "reason";

    public static final String TURNBACK = "turnBack";

    public static final String BUYSNUM = "buysNum";

    public static final String OPTER = "opter";

    public static final String CUSTOMNO = "customNo";

    public static final String COMPANYNAMECHECK = "companyNameCheck";

    public static final String COMPANYNAMEREASON = "companyNameReason";

    @Override
    public String toString() {
        return "StoreOwner{" +
        "username=" + username +
        ", uname=" + uname +
        ", mobile=" + mobile +
        ", password=" + password +
        ", salt=" + salt +
        ", gender=" + gender +
        ", age=" + age +
        ", qq=" + qq +
        ", email=" + email +
        ", idcard=" + idcard +
        ", status=" + status +
        ", headerPic=" + headerPic +
        ", type=" + type +
        ", isHXReady=" + isHXReady +
        ", createtime=" + createtime +
        ", modifytime=" + modifytime +
        ", openId=" + openId +
        ", shopStatus=" + shopStatus +
        ", defIndex=" + defIndex +
        ", reason=" + reason +
        ", turnBack=" + turnBack +
        ", buysNum=" + buysNum +
        ", opter=" + opter +
        ", customNo=" + customNo +
        ", companyNameCheck=" + companyNameCheck +
        ", companyNameReason=" + companyNameReason +
        "}";
    }
}
