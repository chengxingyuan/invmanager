package com.tticar.invmanager.tticar.entity;

import java.math.BigDecimal;

/**
 * @author chengxy
 * @date 2018/10/15 17:40
 */
public class GoodsDTO {
    private String goodsName;
    private String standardName;
    private String picUrl;
    private BigDecimal price;
    private Integer count;
    private BigDecimal fee;
    private String goodsId;
    private String skuId;
    private Integer relativeStatus;
    private String tticarSkuId;
    private String house;
    private String materielName;
    //仓库需要发的数量
    private Integer invNeedCount;

    public Integer getInvNeedCount() {
        return invNeedCount;
    }

    public void setInvNeedCount(Integer invNeedCount) {
        this.invNeedCount = invNeedCount;
    }

    public String getMaterielName() {
        return materielName;
    }

    public void setMaterielName(String materielName) {
        this.materielName = materielName;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getTticarSkuId() {
        return tticarSkuId;
    }

    public void setTticarSkuId(String tticarSkuId) {
        this.tticarSkuId = tticarSkuId;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public Integer getRelativeStatus() {
        return relativeStatus;
    }

    public void setRelativeStatus(Integer relativeStatus) {
        this.relativeStatus = relativeStatus;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getStandardName() {
        return standardName;
    }

    public void setStandardName(String standardName) {
        this.standardName = standardName;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }
}
