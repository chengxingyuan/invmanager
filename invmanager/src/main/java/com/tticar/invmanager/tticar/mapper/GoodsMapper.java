package com.tticar.invmanager.tticar.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.czy.tticar.model.GoodsWithBLOBs;
import com.tticar.invmanager.entity.vo.PlaceOrderDTO;
import com.tticar.invmanager.entity.vo.SkuReturnToPage;
import com.tticar.invmanager.tticar.entity.Goods;
import com.tticar.invmanager.tticar.entity.GoodsTmp;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 产品表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-18
 */
@Component("baseMapper1")
public interface GoodsMapper extends BaseMapper<Goods> {

    List<Map> brand(Page page, @Param("ew") Wrapper w);

    List<Map> category(@Param("ew") Wrapper w);

    List<Map> synchronizationCategory();

    List<GoodsTmp> queryTticarGoodsList(Page page,Map map);

    List<SkuReturnToPage> querySkusByGoodsId(Long goodsId);

    List tticarGoodsSkus(Long id);

    List<SkuReturnToPage> getSkuValuesByGoodsId(Long goodsId);

    /**
     * 查询代下单的商品列表
     * */
    List<PlaceOrderDTO> getPlaceOrderDTOList(Map map);

    String queryCategoryGroupNameById(Long categoryId);

    /**
     * 根据三级分类ID查询一级分类的categoryNo
     */
    Long queryCategoryNo(Long categoryId);

    int insertGoodno(GoodsWithBLOBs good);

    /**
     * 查询产品轮播图
     */
    List<Map> queryPicturesDetails(List<Long> ids);

    /**
     * 插入产品价格
     */
    void insertGoodsPrice(Map map);

    Long queryGoodsPriceId(Long goodsId);

    void updateGoodsPrice(Map map);

    Long queryGoodsExtend(Long goodsId);

    void insertGoodsExtend(Long goodsId);
}
