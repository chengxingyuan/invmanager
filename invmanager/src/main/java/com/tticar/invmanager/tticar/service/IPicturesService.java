package com.tticar.invmanager.tticar.service;

import com.tticar.invmanager.tticar.entity.Pictures;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 图片表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-30
 */
public interface IPicturesService extends IService<Pictures> {


}
