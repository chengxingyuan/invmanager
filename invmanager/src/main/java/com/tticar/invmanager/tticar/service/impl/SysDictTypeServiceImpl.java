package com.tticar.invmanager.tticar.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.tticar.entity.SysDictType;
import com.tticar.invmanager.tticar.mapper.SysDictTypeMapper;
import com.tticar.invmanager.tticar.service.ISysDictTypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典类型表 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-24
 */
@Service
public class SysDictTypeServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictType> implements ISysDictTypeService {


}
