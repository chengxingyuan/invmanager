package com.tticar.invmanager.tticar.service.impl;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.czy.tticar.service.dubbodto.InvmanagerGoodsOrderDTO;
import com.tticar.invmanager.entity.vo.GoodsOrderSearchDTO;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.RelativeGoodsDTO;
import com.tticar.invmanager.entity.vo.SkuReturnToPage;
import com.tticar.invmanager.mapper.MMaterielTticarMapper;
import com.tticar.invmanager.service.IInRepositoryDetailService;
import com.tticar.invmanager.tticar.entity.GoodsDTO;
import com.tticar.invmanager.tticar.entity.GoodsOrder;
import com.tticar.invmanager.tticar.mapper.GoodsOrderMapper;
import com.tticar.invmanager.tticar.service.IGoodsOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 产品订单表 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-13
 */
@Service
public class GoodsOrderServiceImpl extends ServiceImpl<GoodsOrderMapper, GoodsOrder> implements IGoodsOrderService {

    @Autowired
    private MMaterielTticarMapper materielTticarMapper;

    @Autowired
    private GoodsOrderMapper goodsOrderMapper;

    @Autowired
    private IInRepositoryDetailService inRepositoryDetailService;

    @Override
    public Page selectPage(Wrapper wrapper, MyPage<GoodsOrder> page,GoodsOrderSearchDTO goodsOrderSearchDTO) {
//        String sqlSegment = wrapper.getSqlSegment();
//        if (sqlSegment == null || !sqlSegment.contains(GoodsOrder.SELLSTOREID)) {
//            Wrapper sw = Condition.create().isNotNull(WStore.TTSTORE_ID);
//            sw.setSqlSelect(Column.create().column(WStore.TTSTORE_ID));
//            List storeIds = iwStoreService.selectObjs(sw);
//            wrapper.in("t." + GoodsOrder.SELLSTOREID, storeIds);
//        }
//        wrapper.orderBy("t." + GoodsOrder.CREATETIME, false);
        Map map = new HashMap<>(8);
        map.put("sellStoreId", goodsOrderSearchDTO.getSellStoreId());
        map.put("storeName", goodsOrderSearchDTO.getName());
        map.put("startTime", goodsOrderSearchDTO.getStartTime());
        map.put("endTime", goodsOrderSearchDTO.getEndTime());
        map.put("orderStatus",goodsOrderSearchDTO.getOrderStatus());
        map.put("sellStoreIdList", goodsOrderSearchDTO.getSellStoreIdList());
        List<InvmanagerGoodsOrderDTO> sellStoreOrderList = goodsOrderMapper.queryGoodsOrderListForInvmanager(map);
//        List<InvmanagerGoodsOrderDTO> sellStoreOrderList = invmanagerGoodsOrderService.getSellStoreOrderList
//                (page.getPage(),page.getRows(), goodsOrderSearchDTO.getSellStoreId(),goodsOrderSearchDTO.getOrderStatus(),
//                        goodsOrderSearchDTO.getName(),goodsOrderSearchDTO.getStartTime(),goodsOrderSearchDTO.getEndTime());


        page.setTotal(sellStoreOrderList.size());
        if ((page.getPage() - 1) * page.getRows() > sellStoreOrderList.size()) {
            page.setPage(1);
        }
        int toSize = page.getPage()* page.getRows();

        sellStoreOrderList = sellStoreOrderList.subList((page.getPage()-1) * page.getRows(),toSize > sellStoreOrderList.size()?sellStoreOrderList.size():toSize);
        page.setRecords(sellStoreOrderList);

        return page;
    }

    @Override
    public void delete(List<Long> ids) {
        List list = new ArrayList();
        for (Long id : ids) {
            GoodsOrder goodsOrder = new GoodsOrder();
            goodsOrder.setId(id);
            goodsOrder.setStatus(1);
            list.add(goodsOrder);
        }
        this.updateBatchById(list);
    }

    @Override
    public List goodsList(String orderId) {
        List<GoodsDTO> goodsDTOs = goodsOrderMapper.goodsList(orderId);
        List<GoodsDTO> goodsDTOsTmp = new ArrayList<>();
        if (goodsDTOs != null && goodsDTOs.size() > 0) {

            for (GoodsDTO goods : goodsDTOs) {
                //
                List<RelativeGoodsDTO> relativeGoodsDTOs = materielTticarMapper.queryRelativeGoodsByTticarGoods(goods.getGoodsId(), goods.getTticarSkuId());

                if (relativeGoodsDTOs != null && relativeGoodsDTOs.size() > 0) {
                    for (int i = 0; i < relativeGoodsDTOs.size(); i++) {
                        //每份套餐包含的数量
                        Long perCount = materielTticarMapper.queryCountFromRelativeTable(relativeGoodsDTOs.get(i).getId(), goods.getGoodsId(), goods.getTticarSkuId());
                        long l = goods.getCount() * perCount;
                        int count = new Long(l).intValue();
                        if (i == 0) {
                            goods.setSkuId(relativeGoodsDTOs.get(0).getId().toString());
                            goods.setRelativeStatus(1);
                            goods.setMaterielName(relativeGoodsDTOs.get(0).getName() + " " + relativeGoodsDTOs.get(0).getSkuName());
                            goods.setInvNeedCount(count);
                            //查询默认出库


                            goods.setHouse(this.inRepositoryDetailService.getSkuHouseByNum(Long.valueOf(goods.getSkuId()), count, null));
                        } else {
                            GoodsDTO goodsDTO = new GoodsDTO();
                            goodsDTO.setGoodsId(goods.getGoodsId());
                            goodsDTO.setGoodsName(goods.getGoodsName());
                            goodsDTO.setStandardName(goods.getStandardName());
                            goodsDTO.setCount(goods.getCount());
                            goodsDTO.setTticarSkuId(goods.getTticarSkuId());
                            goodsDTO.setSkuId(relativeGoodsDTOs.get(i).getId().toString());
                            goodsDTO.setRelativeStatus(1);
                            goodsDTO.setInvNeedCount(count);
                            goodsDTO.setMaterielName(relativeGoodsDTOs.get(i).getName() + " " + relativeGoodsDTOs.get(i).getSkuName());
                            goodsDTO.setHouse(this.inRepositoryDetailService.getSkuHouseByNum(Long.valueOf(goodsDTO.getSkuId()),count,null));
                            goodsDTOsTmp.add(goodsDTO);
                        }
                    }

                } else {
                    goods.setRelativeStatus(0);
                }

                //查询默认出库
               //goods.setHouse(this.inRepositoryDetailService.getSkuHouseByNum(Long.valueOf(goods.getSkuId()), goods.getCount(), null));
            }
        }
        goodsDTOs.addAll(goodsDTOsTmp);
        return goodsDTOs;
//        return invmanagerGoodsOrderService.goodsList(orderId);
    }


    @Override
    public List goodsListNew(String orderId) {
        List<GoodsDTO> goodsDTOs = goodsOrderMapper.goodsList(orderId);
        List<GoodsDTO> goodsDTOsTmp = new ArrayList<>();
        if (goodsDTOs != null && goodsDTOs.size() > 0) {

            for (GoodsDTO goods : goodsDTOs) {
                //
                List<RelativeGoodsDTO> relativeGoodsDTOs = materielTticarMapper.queryRelativeGoodsByTticarGoods(goods.getGoodsId(), goods.getTticarSkuId());

                if (relativeGoodsDTOs != null && relativeGoodsDTOs.size() > 0) {
                    for (int i = 0; i < relativeGoodsDTOs.size(); i++) {
                        //每份套餐包含的数量
                        Long perCount = materielTticarMapper.queryCountFromRelativeTable(relativeGoodsDTOs.get(i).getId(), goods.getGoodsId(), goods.getTticarSkuId());
                        long l = goods.getCount() * perCount;
                        int count = new Long(l).intValue();
                        if (i == 0) {
                            goods.setSkuId(relativeGoodsDTOs.get(0).getId().toString());
                            goods.setRelativeStatus(1);
                            goods.setMaterielName(relativeGoodsDTOs.get(0).getName() + " " + relativeGoodsDTOs.get(0).getSkuName());
                            goods.setInvNeedCount(count);
                            //查询默认出库


                            goods.setHouse(this.inRepositoryDetailService.getSkuHouseByNum(Long.valueOf(goods.getSkuId()), count, null));
                        } else {
                            GoodsDTO goodsDTO = new GoodsDTO();
                            goodsDTO.setGoodsId(goods.getGoodsId());
                            goodsDTO.setGoodsName(goods.getGoodsName());
                            goodsDTO.setStandardName(goods.getStandardName());
                            goodsDTO.setCount(goods.getCount());
                            goodsDTO.setTticarSkuId(goods.getTticarSkuId());
                            goodsDTO.setSkuId(relativeGoodsDTOs.get(i).getId().toString());
                            goodsDTO.setRelativeStatus(1);
                            goodsDTO.setInvNeedCount(count);
                            goodsDTO.setMaterielName(relativeGoodsDTOs.get(i).getName() + " " + relativeGoodsDTOs.get(i).getSkuName());
                            goodsDTO.setHouse(this.inRepositoryDetailService.getSkuHouseByNum(Long.valueOf(goodsDTO.getSkuId()),count,null));
                            goodsDTOsTmp.add(goodsDTO);
                        }
                    }

                } else {
                    goods.setRelativeStatus(0);
                }

                //查询默认出库
                //goods.setHouse(this.inRepositoryDetailService.getSkuHouseByNum(Long.valueOf(goods.getSkuId()), goods.getCount(), null));
            }
        }
//        goodsDTOs.addAll(goodsDTOsTmp);
        return goodsDTOs;
//        return invmanagerGoodsOrderService.goodsList(orderId);
    }

    @Override
    public List<SkuReturnToPage> queryGoodsDetailByOrderId(Long orderId) {
        return goodsOrderMapper.queryGoodsIdByOrderId(orderId);
    }

}
