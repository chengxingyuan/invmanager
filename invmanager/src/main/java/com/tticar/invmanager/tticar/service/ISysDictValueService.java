package com.tticar.invmanager.tticar.service;

import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.tticar.entity.SysDictType;
import com.tticar.invmanager.tticar.entity.SysDictValue;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * <p>
 * 字典类型表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-24
 */
public interface ISysDictValueService extends IService<SysDictValue> {

    List selectSysDictValueList(String dictType);

}
