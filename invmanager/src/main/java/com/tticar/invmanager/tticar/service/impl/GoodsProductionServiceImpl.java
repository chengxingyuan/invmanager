package com.tticar.invmanager.tticar.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.tticar.entity.GoodsProduction;
import com.tticar.invmanager.tticar.mapper.GoodsProductionMapper;
import com.tticar.invmanager.tticar.service.IGoodsProductionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品-产品表 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-18
 */
@Service
public class GoodsProductionServiceImpl extends ServiceImpl<GoodsProductionMapper, GoodsProduction> implements IGoodsProductionService {


}
