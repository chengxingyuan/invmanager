package com.tticar.invmanager.tticar.service;

import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.tticar.entity.StoreOwner;

/**
 * <p>
 * 店铺表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-24
 */
public interface IStoreOwnerService extends IService<StoreOwner> {
    void verifyUser(Long phoneNum, String password);

}
