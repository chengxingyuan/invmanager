package com.tticar.invmanager.tticar.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 图片表
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-30
 */
@TableName("pictures")
public class Pictures implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 商品图片 1／ 门店照片 2/ 广告照片 3／专题照片 4／用户头像 5 / 分类 6 / 商品轮播图 7 / 用户头像 8 / 营业执照 9 / 翼起见证 10 / 配件商 11 / 其他 20 
     */
    private Long refId;

    private String path;

    /**
     * 商品图片 1／ 门店照片 2/ 广告照片 3／专题照片 4／用户头像 5 / 分类 6 / 商品轮播图 7 / 用户头像 8 / 营业执照 9 / 翼起见证 10 / 配件商 11 / 用户身份17 / 授权书 18  /  其他 20 
     */
    private Integer type;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 是否主图
0 否 1是
     */
    private Integer isportal;

    /**
     * 状态
0 启用 1 禁用
     */
    private Integer status;

    /**
     * 备注
     */
    private String memo;

    private Date ctime;

    private Date utime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getRefId() {
        return refId;
    }

    public void setRefId(Long refId) {
        this.refId = refId;
    }
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
    public Integer getIsportal() {
        return isportal;
    }

    public void setIsportal(Integer isportal) {
        this.isportal = isportal;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }
    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }

    public static final String ID = "id";

    public static final String REFID = "refId";

    public static final String PATH = "path";

    public static final String TYPE = "type";

    public static final String SORT = "sort";

    public static final String ISPORTAL = "isportal";

    public static final String STATUS = "status";

    public static final String MEMO = "memo";

    public static final String CTIME = "ctime";

    public static final String UTIME = "utime";

    @Override
    public String toString() {
        return "Pictures{" +
        "id=" + id +
        ", refId=" + refId +
        ", path=" + path +
        ", type=" + type +
        ", sort=" + sort +
        ", isportal=" + isportal +
        ", status=" + status +
        ", memo=" + memo +
        ", ctime=" + ctime +
        ", utime=" + utime +
        "}";
    }
}
