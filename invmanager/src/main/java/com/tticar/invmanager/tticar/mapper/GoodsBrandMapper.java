package com.tticar.invmanager.tticar.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.tticar.entity.GoodsBrand;
import com.tticar.invmanager.tticar.entity.SysDictValue;

/**
 * <p>
 * 表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-30
 */
public interface GoodsBrandMapper extends BaseMapper<GoodsBrand> {

}
