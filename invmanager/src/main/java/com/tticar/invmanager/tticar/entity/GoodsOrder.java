package com.tticar.invmanager.tticar.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 产品订单表
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-13
 */
@TableName("goods_order")
public class GoodsOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 订单编号
     */
    private String orderCode;

    /**
     * 门店id
     */
    private Long storeId;

    /**
     * 供应商id
     */
    private Long sellStoreId;

    /**
     * 收货人
     */
    private String recName;

    /**
     * 收货人电话
     */
    private String recTel;

    private String recProvinceName;

    private String recCityName;

    private String recAreaName;

    /**
     * 详细地址
     */
    private String recAddr;

    /**
     * 邮编
     */
    private String recPostCode;

    /**
     * 总金额
     */
    private BigDecimal totalMoney;

    /**
     * 总运费
     */
    private BigDecimal totalFee;

    /**
     * 总数量
     */
    private Long totalCount;

    /**
     * 配送方式 字典表id
     */
    private Long sendType;

    /**
     * 买家留言
     */
    private String buyMsg;

    /**
     * 卖家留言
     */
    private String sellMsg;

    /**
     * 平台留言
     */
    private String platformMsg;

    /**
     * 支付方式 字典表id
     */
    private Long payType;

    /**
     * 订单状态
     */
    private Integer status;

    /**
     * 是否删除：0 未删除 1 已删除
     */
    private Integer isDel;

    /**
     * 订单生成时间
     */
    private Date createtime;

    private Date updatetime;

    /**
     * 支付来源：0：支付宝 1：微信  10账户余额 11账户余额+支付宝 12账户余额+微信
     */
    private Integer payOrigin;

    /**
     * 物流处理状态,0:新增订单，1：供应商已处理，2：物流已处理
     */
    private Integer deliveryStatus;

    /**
     * 是否结束，并把收益转到可提现余额
默认0 已转1
     */
    private Integer isFinish;

    /**
     * 供应商优惠
     */
    private BigDecimal discount;

    /**
     * 是否是加急订单  0 默认不是  1是
     */
    private Integer isUrgent;

    /**
     * 优惠券优惠
     */
    private BigDecimal reduction;

    /**
     * 支付方式名称
     */
    private String payTypeName;

    /**
     * 发货方式名称
     */
    private String sendTypeName;

    private BigDecimal realRefundMoney;

    /**
     * 实付金额
     */
    private BigDecimal realPayMoney;

    /**
     * 积分优惠
     */
    private BigDecimal intergalDiscount;

    /**
     * 平台金额
     */
    private BigDecimal platformMoney;

    /**
     * 加急产生费用
     */
    private BigDecimal urgentMoney;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }
    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }
    public Long getSellStoreId() {
        return sellStoreId;
    }

    public void setSellStoreId(Long sellStoreId) {
        this.sellStoreId = sellStoreId;
    }
    public String getRecName() {
        return recName;
    }

    public void setRecName(String recName) {
        this.recName = recName;
    }
    public String getRecTel() {
        return recTel;
    }

    public void setRecTel(String recTel) {
        this.recTel = recTel;
    }
    public String getRecProvinceName() {
        return recProvinceName;
    }

    public void setRecProvinceName(String recProvinceName) {
        this.recProvinceName = recProvinceName;
    }
    public String getRecCityName() {
        return recCityName;
    }

    public void setRecCityName(String recCityName) {
        this.recCityName = recCityName;
    }
    public String getRecAreaName() {
        return recAreaName;
    }

    public void setRecAreaName(String recAreaName) {
        this.recAreaName = recAreaName;
    }
    public String getRecAddr() {
        return recAddr;
    }

    public void setRecAddr(String recAddr) {
        this.recAddr = recAddr;
    }
    public String getRecPostCode() {
        return recPostCode;
    }

    public void setRecPostCode(String recPostCode) {
        this.recPostCode = recPostCode;
    }
    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }
    public BigDecimal getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }
    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
    public Long getSendType() {
        return sendType;
    }

    public void setSendType(Long sendType) {
        this.sendType = sendType;
    }
    public String getBuyMsg() {
        return buyMsg;
    }

    public void setBuyMsg(String buyMsg) {
        this.buyMsg = buyMsg;
    }
    public String getSellMsg() {
        return sellMsg;
    }

    public void setSellMsg(String sellMsg) {
        this.sellMsg = sellMsg;
    }
    public String getPlatformMsg() {
        return platformMsg;
    }

    public void setPlatformMsg(String platformMsg) {
        this.platformMsg = platformMsg;
    }
    public Long getPayType() {
        return payType;
    }

    public void setPayType(Long payType) {
        this.payType = payType;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }
    public Integer getPayOrigin() {
        return payOrigin;
    }

    public void setPayOrigin(Integer payOrigin) {
        this.payOrigin = payOrigin;
    }
    public Integer getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(Integer deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }
    public Integer getIsFinish() {
        return isFinish;
    }

    public void setIsFinish(Integer isFinish) {
        this.isFinish = isFinish;
    }
    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }
    public Integer getIsUrgent() {
        return isUrgent;
    }

    public void setIsUrgent(Integer isUrgent) {
        this.isUrgent = isUrgent;
    }
    public BigDecimal getReduction() {
        return reduction;
    }

    public void setReduction(BigDecimal reduction) {
        this.reduction = reduction;
    }
    public String getPayTypeName() {
        return payTypeName;
    }

    public void setPayTypeName(String payTypeName) {
        this.payTypeName = payTypeName;
    }
    public String getSendTypeName() {
        return sendTypeName;
    }

    public void setSendTypeName(String sendTypeName) {
        this.sendTypeName = sendTypeName;
    }
    public BigDecimal getRealRefundMoney() {
        return realRefundMoney;
    }

    public void setRealRefundMoney(BigDecimal realRefundMoney) {
        this.realRefundMoney = realRefundMoney;
    }
    public BigDecimal getRealPayMoney() {
        return realPayMoney;
    }

    public void setRealPayMoney(BigDecimal realPayMoney) {
        this.realPayMoney = realPayMoney;
    }
    public BigDecimal getIntergalDiscount() {
        return intergalDiscount;
    }

    public void setIntergalDiscount(BigDecimal intergalDiscount) {
        this.intergalDiscount = intergalDiscount;
    }
    public BigDecimal getPlatformMoney() {
        return platformMoney;
    }

    public void setPlatformMoney(BigDecimal platformMoney) {
        this.platformMoney = platformMoney;
    }
    public BigDecimal getUrgentMoney() {
        return urgentMoney;
    }

    public void setUrgentMoney(BigDecimal urgentMoney) {
        this.urgentMoney = urgentMoney;
    }

    public static final String ID = "id";

    public static final String ORDERCODE = "orderCode";

    public static final String STOREID = "storeId";

    public static final String SELLSTOREID = "sellStoreId";

    public static final String RECNAME = "recName";

    public static final String RECTEL = "recTel";

    public static final String RECPROVINCENAME = "recProvinceName";

    public static final String RECCITYNAME = "recCityName";

    public static final String RECAREANAME = "recAreaName";

    public static final String RECADDR = "recAddr";

    public static final String RECPOSTCODE = "recPostCode";

    public static final String TOTALMONEY = "totalMoney";

    public static final String TOTALFEE = "totalFee";

    public static final String TOTALCOUNT = "totalCount";

    public static final String SENDTYPE = "sendType";

    public static final String BUYMSG = "buyMsg";

    public static final String SELLMSG = "sellMsg";

    public static final String PLATFORMMSG = "platformMsg";

    public static final String PAYTYPE = "payType";

    public static final String STATUS = "status";

    public static final String ISDEL = "isDel";

    public static final String CREATETIME = "createtime";

    public static final String UPDATETIME = "updatetime";

    public static final String PAYORIGIN = "payOrigin";

    public static final String DELIVERYSTATUS = "deliveryStatus";

    public static final String ISFINISH = "isFinish";

    public static final String DISCOUNT = "discount";

    public static final String ISURGENT = "isUrgent";

    public static final String REDUCTION = "reduction";

    public static final String PAYTYPENAME = "payTypeName";

    public static final String SENDTYPENAME = "sendTypeName";

    public static final String REALREFUNDMONEY = "realRefundMoney";

    public static final String REALPAYMONEY = "realPayMoney";

    public static final String INTERGALDISCOUNT = "intergalDiscount";

    public static final String PLATFORMMONEY = "platformMoney";

    public static final String URGENTMONEY = "urgentMoney";

    @Override
    public String toString() {
        return "GoodsOrder{" +
        "id=" + id +
        ", orderCode=" + orderCode +
        ", storeId=" + storeId +
        ", sellStoreId=" + sellStoreId +
        ", recName=" + recName +
        ", recTel=" + recTel +
        ", recProvinceName=" + recProvinceName +
        ", recCityName=" + recCityName +
        ", recAreaName=" + recAreaName +
        ", recAddr=" + recAddr +
        ", recPostCode=" + recPostCode +
        ", totalMoney=" + totalMoney +
        ", totalFee=" + totalFee +
        ", totalCount=" + totalCount +
        ", sendType=" + sendType +
        ", buyMsg=" + buyMsg +
        ", sellMsg=" + sellMsg +
        ", platformMsg=" + platformMsg +
        ", payType=" + payType +
        ", status=" + status +
        ", isDel=" + isDel +
        ", createtime=" + createtime +
        ", updatetime=" + updatetime +
        ", payOrigin=" + payOrigin +
        ", deliveryStatus=" + deliveryStatus +
        ", isFinish=" + isFinish +
        ", discount=" + discount +
        ", isUrgent=" + isUrgent +
        ", reduction=" + reduction +
        ", payTypeName=" + payTypeName +
        ", sendTypeName=" + sendTypeName +
        ", realRefundMoney=" + realRefundMoney +
        ", realPayMoney=" + realPayMoney +
        ", intergalDiscount=" + intergalDiscount +
        ", platformMoney=" + platformMoney +
        ", urgentMoney=" + urgentMoney +
        "}";
    }
}
