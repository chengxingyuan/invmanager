package com.tticar.invmanager.tticar.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.tticar.entity.GoodsBrand;
import com.tticar.invmanager.tticar.entity.SysDictType;
import com.tticar.invmanager.tticar.mapper.GoodsBrandMapper;
import com.tticar.invmanager.tticar.mapper.SysDictTypeMapper;
import com.tticar.invmanager.tticar.service.IGoodsBrandService;
import com.tticar.invmanager.tticar.service.ISysDictTypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品品牌表 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-24
 */
@Service
public class GoodsBrandServiceImpl extends ServiceImpl<GoodsBrandMapper, GoodsBrand> implements IGoodsBrandService {

    @Override
    public Page selectPage(GoodsBrand goodsBrand, MyPage<GoodsBrand> page) {
        Wrapper wrapper = new EntityWrapper();
        wrapper.like("name", goodsBrand.getName());
        return this.selectMapsPage(page, wrapper);
    }
}
