package com.tticar.invmanager.tticar.mapper;

import com.tticar.invmanager.tticar.entity.GoodsProduction;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 商品-产品表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-18
 */
@Component("GoodsProductionMapper1")
public interface GoodsProductionMapper extends BaseMapper<GoodsProduction> {

}
