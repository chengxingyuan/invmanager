package com.tticar.invmanager.tticar.service;

import com.tticar.invmanager.tticar.entity.GoodsProduction;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品-产品表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-18
 */
public interface IGoodsProductionService extends IService<GoodsProduction> {


}
