package com.tticar.invmanager.tticar.mapper;

import com.tticar.invmanager.tticar.entity.Pictures;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 图片表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-30
 */
@Component("picturesMapper1")
public interface PicturesMapper extends BaseMapper<Pictures> {

}
