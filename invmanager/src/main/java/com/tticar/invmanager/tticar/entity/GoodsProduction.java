package com.tticar.invmanager.tticar.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 商品-产品表
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-18
 */
@TableName("goods_production")
public class GoodsProduction implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long goodId;

    private String skuId;

    private String name;

    private BigDecimal price;

    private BigDecimal pricemember;

    private BigDecimal pricevip;

    /**
     * 零售价
     */
    private BigDecimal pricesell;

    private String inventory;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 修改时间
     */
    private Date modifytime;

    /**
     * 规格图片，图片地址
     */
    private String skuPic;

    /**
     * v2价格
     */
    private BigDecimal pricevip2;

    /**
     * v3价格
     */
    private BigDecimal pricevip3;
    @TableField(exist = false)
    private String isRelative;

    @TableField(exist = false)
    private String goodsIdString;

    public String getGoodsIdString() {
        return goodsIdString;
    }

    public void setGoodsIdString(String goodsIdString) {
        this.goodsIdString = goodsIdString;
    }

    public String getIsRelative() {
        return isRelative;
    }

    public void setIsRelative(String isRelative) {
        this.isRelative = isRelative;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getGoodId() {
        return goodId;
    }

    public void setGoodId(Long goodId) {
        this.goodId = goodId;
    }
    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public BigDecimal getPricemember() {
        return pricemember;
    }

    public void setPricemember(BigDecimal pricemember) {
        this.pricemember = pricemember;
    }
    public BigDecimal getPricevip() {
        return pricevip;
    }

    public void setPricevip(BigDecimal pricevip) {
        this.pricevip = pricevip;
    }
    public BigDecimal getPricesell() {
        return pricesell;
    }

    public void setPricesell(BigDecimal pricesell) {
        this.pricesell = pricesell;
    }
    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }
    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
    public Date getModifytime() {
        return modifytime;
    }

    public void setModifytime(Date modifytime) {
        this.modifytime = modifytime;
    }
    public String getSkuPic() {
        return skuPic;
    }

    public void setSkuPic(String skuPic) {
        this.skuPic = skuPic;
    }
    public BigDecimal getPricevip2() {
        return pricevip2;
    }

    public void setPricevip2(BigDecimal pricevip2) {
        this.pricevip2 = pricevip2;
    }
    public BigDecimal getPricevip3() {
        return pricevip3;
    }

    public void setPricevip3(BigDecimal pricevip3) {
        this.pricevip3 = pricevip3;
    }

    public static final String ID = "id";

    public static final String GOODID = "goodId";

    public static final String SKUID = "skuId";

    public static final String NAME = "name";

    public static final String PRICE = "price";

    public static final String PRICEMEMBER = "pricemember";

    public static final String PRICEVIP = "pricevip";

    public static final String PRICESELL = "pricesell";

    public static final String INVENTORY = "inventory";

    public static final String CREATETIME = "createtime";

    public static final String MODIFYTIME = "modifytime";

    public static final String SKUPIC = "skuPic";

    public static final String PRICEVIP2 = "pricevip2";

    public static final String PRICEVIP3 = "pricevip3";

    @Override
    public String toString() {
        return "GoodsProduction{" +
        "id=" + id +
        ", goodId=" + goodId +
        ", skuId=" + skuId +
        ", name=" + name +
        ", price=" + price +
        ", pricemember=" + pricemember +
        ", pricevip=" + pricevip +
        ", pricesell=" + pricesell +
        ", inventory=" + inventory +
        ", createtime=" + createtime +
        ", modifytime=" + modifytime +
        ", skuPic=" + skuPic +
        ", pricevip2=" + pricevip2 +
        ", pricevip3=" + pricevip3 +
        "}";
    }
}
