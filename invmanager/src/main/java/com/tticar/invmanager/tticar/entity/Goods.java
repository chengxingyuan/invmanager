package com.tticar.invmanager.tticar.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 产品表
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-18
 */
@TableName("goods")
public class Goods implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 商铺id
     */
    private Long storeId;

    /**
     * 品牌id
     */
    private Long brandId;

    /**
     * 分类id
     */
    private Long categoryId;

    /**
     * 店铺分类id
     */
    private Long storeCategoryId;

    /**
     * 产品名称
     */
    private String name;

    /**
     * 产品名称拼音简写
     */
    private String nameSpell;

    /**
     * 游客价格，未登录
80-100
     */
    private BigDecimal price;

    /**
     * 游客价格，未登录
80-100
     */
    private BigDecimal maxPrice;

    /**
     * 店铺已登录价格
100-220
     */
    private BigDecimal pricemember;

    /**
     * 店铺已登录价格
100-220
     */
    private BigDecimal maxPricemember;

    /**
     * 门店申请供应商会员价格
200-20
     */
    private BigDecimal pricevip;

    /**
     * 门店申请供应商会员价格
200-20
     */
    private BigDecimal maxPricevip;

    /**
     * 零售价低
     */
    private BigDecimal pricesell;

    /**
     * 零售价高
     */
    private BigDecimal maxPricesell;

    /**
     * 库存
货源充足／货源紧张／2000件
     */
    private String inventory;

    /**
     * 产地
     */
    private String placeOrigin;

    /**
     * 最低购买数量
     */
    private Long minNum;

    /**
     * 默认是0，表示不限购
     */
    private Long maxNum;

    /**
     * 产品备注简单描述
     */
    private String memo;

    /**
     * 0 上架 1 下架  2待上架 3 删除
     */
    private Integer status;

    /**
     * 上架时间
     */
    private Date uptime;

    /**
     * 下架时间
     */
    private Date downtime;

    /**
     * 规格配置表
颜色 ：
     */
    private String standardCfg;

    /**
     * 图文详情
     */
    private String description;

    /**
     * 代运营产品id
     */
    private Long refGoodsId;

    /**
     * 运费
     */
    private BigDecimal fee;

    /**
     * 置顶排序
     */
    private Integer sort;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 修改时间
     */
    private Date modifytime;

    /**
     * 搜索关键字
     */
    private String keyWords;

    /**
     * 是否启用老客户价（0是1否）
     */
    private Integer isOlder;

    /**
     * 视频地址
     */
    private String video;

    /**
     * 时长
     */
    private String videoDuration;

    /**
     * 商品编号
     */
    private String goodNo;

    /**
     * 是否自动接单 0是 1否（手动接单）
     */
    private Integer isAutoReceipt;

    /**
     * 商品来源 默认0app供应商 1总后台 2供应商后台
     */
    private Integer upSource;

    /**
     * 橱窗（0 上 1 下）
     */
    private Integer showcase;

    /**
     * 上下橱窗操作时间
     */
    private Date showcaseTime;

    /**
     * 推荐状态，0：不推荐，1：优先推荐，NULL和其他为正常推荐
     */
    @TableField("rec_status")
    private Integer recStatus;

    /**
     * v2价格
     */
    private BigDecimal pricevip2;

    /**
     * v3价格
     */
    private BigDecimal pricevip3;

    /**
     * 0 免运费 1有运费  2运费到付
     */
    private Integer feeType;

    @TableField("goodNo")
    private String goodsNo;

    public String getGoodsNo() {
        return goodsNo;
    }

    public void setGoodsNo(String goodsNo) {
        this.goodsNo = goodsNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }
    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }
    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
    public Long getStoreCategoryId() {
        return storeCategoryId;
    }

    public void setStoreCategoryId(Long storeCategoryId) {
        this.storeCategoryId = storeCategoryId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getNameSpell() {
        return nameSpell;
    }

    public void setNameSpell(String nameSpell) {
        this.nameSpell = nameSpell;
    }
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }
    public BigDecimal getPricemember() {
        return pricemember;
    }

    public void setPricemember(BigDecimal pricemember) {
        this.pricemember = pricemember;
    }
    public BigDecimal getMaxPricemember() {
        return maxPricemember;
    }

    public void setMaxPricemember(BigDecimal maxPricemember) {
        this.maxPricemember = maxPricemember;
    }
    public BigDecimal getPricevip() {
        return pricevip;
    }

    public void setPricevip(BigDecimal pricevip) {
        this.pricevip = pricevip;
    }
    public BigDecimal getMaxPricevip() {
        return maxPricevip;
    }

    public void setMaxPricevip(BigDecimal maxPricevip) {
        this.maxPricevip = maxPricevip;
    }
    public BigDecimal getPricesell() {
        return pricesell;
    }

    public void setPricesell(BigDecimal pricesell) {
        this.pricesell = pricesell;
    }
    public BigDecimal getMaxPricesell() {
        return maxPricesell;
    }

    public void setMaxPricesell(BigDecimal maxPricesell) {
        this.maxPricesell = maxPricesell;
    }
    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }
    public String getPlaceOrigin() {
        return placeOrigin;
    }

    public void setPlaceOrigin(String placeOrigin) {
        this.placeOrigin = placeOrigin;
    }
    public Long getMinNum() {
        return minNum;
    }

    public void setMinNum(Long minNum) {
        this.minNum = minNum;
    }
    public Long getMaxNum() {
        return maxNum;
    }

    public void setMaxNum(Long maxNum) {
        this.maxNum = maxNum;
    }
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public Date getUptime() {
        return uptime;
    }

    public void setUptime(Date uptime) {
        this.uptime = uptime;
    }
    public Date getDowntime() {
        return downtime;
    }

    public void setDowntime(Date downtime) {
        this.downtime = downtime;
    }
    public String getStandardCfg() {
        return standardCfg;
    }

    public void setStandardCfg(String standardCfg) {
        this.standardCfg = standardCfg;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public Long getRefGoodsId() {
        return refGoodsId;
    }

    public void setRefGoodsId(Long refGoodsId) {
        this.refGoodsId = refGoodsId;
    }
    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
    public Date getModifytime() {
        return modifytime;
    }

    public void setModifytime(Date modifytime) {
        this.modifytime = modifytime;
    }
    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }
    public Integer getIsOlder() {
        return isOlder;
    }

    public void setIsOlder(Integer isOlder) {
        this.isOlder = isOlder;
    }
    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }
    public String getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(String videoDuration) {
        this.videoDuration = videoDuration;
    }
    public String getGoodNo() {
        return goodNo;
    }

    public void setGoodNo(String goodNo) {
        this.goodNo = goodNo;
    }
    public Integer getIsAutoReceipt() {
        return isAutoReceipt;
    }

    public void setIsAutoReceipt(Integer isAutoReceipt) {
        this.isAutoReceipt = isAutoReceipt;
    }
    public Integer getUpSource() {
        return upSource;
    }

    public void setUpSource(Integer upSource) {
        this.upSource = upSource;
    }
    public Integer getShowcase() {
        return showcase;
    }

    public void setShowcase(Integer showcase) {
        this.showcase = showcase;
    }
    public Date getShowcaseTime() {
        return showcaseTime;
    }

    public void setShowcaseTime(Date showcaseTime) {
        this.showcaseTime = showcaseTime;
    }
    public Integer getRecStatus() {
        return recStatus;
    }

    public void setRecStatus(Integer recStatus) {
        this.recStatus = recStatus;
    }
    public BigDecimal getPricevip2() {
        return pricevip2;
    }

    public void setPricevip2(BigDecimal pricevip2) {
        this.pricevip2 = pricevip2;
    }
    public BigDecimal getPricevip3() {
        return pricevip3;
    }

    public void setPricevip3(BigDecimal pricevip3) {
        this.pricevip3 = pricevip3;
    }
    public Integer getFeeType() {
        return feeType;
    }

    public void setFeeType(Integer feeType) {
        this.feeType = feeType;
    }

    public static final String ID = "id";

    public static final String STOREID = "storeId";

    public static final String BRANDID = "brandId";

    public static final String CATEGORYID = "categoryId";

    public static final String STORECATEGORYID = "storeCategoryId";

    public static final String NAME = "name";

    public static final String NAMESPELL = "nameSpell";

    public static final String PRICE = "price";

    public static final String MAXPRICE = "maxPrice";

    public static final String PRICEMEMBER = "pricemember";

    public static final String MAXPRICEMEMBER = "maxPricemember";

    public static final String PRICEVIP = "pricevip";

    public static final String MAXPRICEVIP = "maxPricevip";

    public static final String PRICESELL = "pricesell";

    public static final String MAXPRICESELL = "maxPricesell";

    public static final String INVENTORY = "inventory";

    public static final String PLACEORIGIN = "placeOrigin";

    public static final String MINNUM = "minNum";

    public static final String MAXNUM = "maxNum";

    public static final String MEMO = "memo";

    public static final String STATUS = "status";

    public static final String UPTIME = "uptime";

    public static final String DOWNTIME = "downtime";

    public static final String STANDARDCFG = "standardCfg";

    public static final String DESCRIPTION = "description";

    public static final String REFGOODSID = "refGoodsId";

    public static final String FEE = "fee";

    public static final String SORT = "sort";

    public static final String CREATETIME = "createtime";

    public static final String MODIFYTIME = "modifytime";

    public static final String KEYWORDS = "keyWords";

    public static final String ISOLDER = "isOlder";

    public static final String VIDEO = "video";

    public static final String VIDEODURATION = "videoDuration";

    public static final String GOODNO = "goodNo";

    public static final String ISAUTORECEIPT = "isAutoReceipt";

    public static final String UPSOURCE = "upSource";

    public static final String SHOWCASE = "showcase";

    public static final String SHOWCASETIME = "showcaseTime";

    public static final String REC_STATUS = "rec_status";

    public static final String PRICEVIP2 = "pricevip2";

    public static final String PRICEVIP3 = "pricevip3";

    public static final String FEETYPE = "feeType";

    @Override
    public String toString() {
        return "Goods{" +
        "id=" + id +
        ", storeId=" + storeId +
        ", brandId=" + brandId +
        ", categoryId=" + categoryId +
        ", storeCategoryId=" + storeCategoryId +
        ", name=" + name +
        ", nameSpell=" + nameSpell +
        ", price=" + price +
        ", maxPrice=" + maxPrice +
        ", pricemember=" + pricemember +
        ", maxPricemember=" + maxPricemember +
        ", pricevip=" + pricevip +
        ", maxPricevip=" + maxPricevip +
        ", pricesell=" + pricesell +
        ", maxPricesell=" + maxPricesell +
        ", inventory=" + inventory +
        ", placeOrigin=" + placeOrigin +
        ", minNum=" + minNum +
        ", maxNum=" + maxNum +
        ", memo=" + memo +
        ", status=" + status +
        ", uptime=" + uptime +
        ", downtime=" + downtime +
        ", standardCfg=" + standardCfg +
        ", description=" + description +
        ", refGoodsId=" + refGoodsId +
        ", fee=" + fee +
        ", sort=" + sort +
        ", createtime=" + createtime +
        ", modifytime=" + modifytime +
        ", keyWords=" + keyWords +
        ", isOlder=" + isOlder +
        ", video=" + video +
        ", videoDuration=" + videoDuration +
        ", goodNo=" + goodNo +
        ", isAutoReceipt=" + isAutoReceipt +
        ", upSource=" + upSource +
        ", showcase=" + showcase +
        ", showcaseTime=" + showcaseTime +
        ", recStatus=" + recStatus +
        ", pricevip2=" + pricevip2 +
        ", pricevip3=" + pricevip3 +
        ", feeType=" + feeType +
        "}";
    }
}
