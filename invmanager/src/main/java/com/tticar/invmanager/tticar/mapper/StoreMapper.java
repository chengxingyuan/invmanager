package com.tticar.invmanager.tticar.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.tticar.entity.Store;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 店铺表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-24
 */
@Component("StoreMapper1")
public interface StoreMapper extends BaseMapper<Store> {

    List<Map> visitGoods(Page page, @Param("storeId") Long storeId);

    List<Map> visitStore(Page page, @Param("storeId") Long storeId);

    List<Map> visitCall(Page page, @Param("storeId") Long storeId, @Param("type") Integer ctype);

    List<Map> visitOrder(Page page, @Param("storeId") Long storeId);
}
