package com.tticar.invmanager.tticar.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.common.exception.MyServiceException;
import com.tticar.invmanager.common.utils.MD5Utils;
import com.tticar.invmanager.common.utils.ServiceUtil;
import com.tticar.invmanager.mapper.WStoreMapper;
import com.tticar.invmanager.tticar.entity.Store;
import com.tticar.invmanager.tticar.entity.StoreOwner;
import com.tticar.invmanager.tticar.mapper.StoreOwnerMapper;
import com.tticar.invmanager.tticar.service.IStoreOwnerService;
import com.tticar.invmanager.tticar.service.IStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 店铺所有者 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-12
 */
@Service
public class StoreOwnerServiceImpl extends ServiceImpl<StoreOwnerMapper, StoreOwner> implements IStoreOwnerService {

    @Autowired
    private WStoreMapper wStoreMapper;

    @Autowired
    private IStoreService storeService;

    @Override
    public void verifyUser(Long phoneNum, String password) {
        Wrapper wrapper = new EntityWrapper<>();
        wrapper.eq("mobile", phoneNum);
        //状态1 已审核通过的
        wrapper.eq("status", 1);
        //type 1 供应商的
        wrapper.eq("type", 1);
        StoreOwner storeOwner = selectOne(wrapper);
        if (storeOwner == null || !MD5Utils.md5ToLower(password).equals(storeOwner.getPassword())) {
            throw new MyServiceException("用户名不存在或者密码错误");
        }
        //验证通过则把店铺放到该用户名下管理
        Wrapper w = new EntityWrapper<>();
        w.eq("storeOwnerId", storeOwner.getId());
        Store store = storeService.selectOne(w);
        List<Long> longs = wStoreMapper.queryStoreBelongUser(/*ServiceUtil.getLoginUserName()*/);
        if (longs != null && longs.size()>0 && longs.contains(store.getId())) {
            throw new MyServiceException("该店铺已授权，无需重新授权");
        }
        wStoreMapper.insertStoreBelongUser(store.getId(), ServiceUtil.getLoginUser().getUsername());
    }
}
