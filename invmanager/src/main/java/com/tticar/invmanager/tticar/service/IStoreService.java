package com.tticar.invmanager.tticar.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.tticar.entity.Store;

import java.util.List;

/**
 * <p>
 * 店铺表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-24
 */
public interface IStoreService extends IService<Store> {

    /**
     * 根据仓储系统用户名匹配天天爱车店铺
     * @return
     */
    List<Store> selectList();

    void applyStore(String mobile);

    List category(String name, Long id);

    Page brand(String name, MyPage page);

    Page pushlet(Page page, String ptype, Long storeId);
}
