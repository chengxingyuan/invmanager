package com.tticar.invmanager.tticar.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

/**
 * <p>
 * 店铺表
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-24
 */
@TableName("store")
public class Store implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 店铺名称
     */
    private String name;

    /**
     * 名字子母缩写
     */
    private String nameSpell;

    /**
     * 电话
     */
    private Long tel;

    /**
     * 店铺拥有者id
     */
    private Long storeOwnerId;

    /**
     * 店铺描述
     */
    private String memo;

    private Long provinceId;

    private Long cityId;

    private Long areaId;

    /**
     * 详细地址
     */
    private String addr;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;

    /**
     * 客户经理 名字
     */
    private String customMgr;

    /**
     * 经营范围
多个，隔开
     */
    private String mgrScope;

    /**
     * 经营品牌
多个逗号隔开
     */
    private String mgrBrands;

    /**
     * 店铺人数
10-20人
500-1000人
     */
    private String personNum;

    /**
     * 门店规模
     */
    private String size;

    /**
     * 店铺说明，存字典表id
多个逗号分隔
     */
    private String comment;

    /**
     * 店铺／门店级别
     */
    private Integer level;

    /**
     * 优惠政策
     */
    private String preferential;

    /**
     * 服务保证
     */
    private String services;

    /**
     * 办公室电话
     */
    private String workTel;

    /**
     * 店铺类型
0 门店 1供应商
     */
    private Integer type;

    /**
     * 供应商类别
storesupply_yp 用品
storesupply_wb 维保
storesupply_pjs 配件商
     */
    private String serviceType;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 修改时间
     */
    private Date modifytime;

    /**
     * 是否可指定物流 0 否  1是
     */
    private Integer isPlatformDelivery;

    /**
     * 企业名称
     */
    private String companyName;

    /**
     * 企业税号
     */
    private String taxId;

    @TableField(exist = false)
    private StoreOwner storeOwner;

    /**
     *  上传的图片
     */
    @TableField(exist = false)
    MultipartFile[] files;

    public MultipartFile[] getFiles() {
        return files;
    }

    public void setFiles(MultipartFile[] files) {
        this.files = files;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getNameSpell() {
        return nameSpell;
    }

    public void setNameSpell(String nameSpell) {
        this.nameSpell = nameSpell;
    }
    public Long getTel() {
        return tel;
    }

    public void setTel(Long tel) {
        this.tel = tel;
    }
    public Long getStoreOwnerId() {
        return storeOwnerId;
    }

    public void setStoreOwnerId(Long storeOwnerId) {
        this.storeOwnerId = storeOwnerId;
    }
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }
    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }
    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }
    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
    public String getCustomMgr() {
        return customMgr;
    }

    public void setCustomMgr(String customMgr) {
        this.customMgr = customMgr;
    }
    public String getMgrScope() {
        return mgrScope;
    }

    public void setMgrScope(String mgrScope) {
        this.mgrScope = mgrScope;
    }
    public String getMgrBrands() {
        return mgrBrands;
    }

    public void setMgrBrands(String mgrBrands) {
        this.mgrBrands = mgrBrands;
    }
    public String getPersonNum() {
        return personNum;
    }

    public void setPersonNum(String personNum) {
        this.personNum = personNum;
    }
    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
    public String getPreferential() {
        return preferential;
    }

    public void setPreferential(String preferential) {
        this.preferential = preferential;
    }
    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }
    public String getWorkTel() {
        return workTel;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
    public Date getModifytime() {
        return modifytime;
    }

    public void setModifytime(Date modifytime) {
        this.modifytime = modifytime;
    }
    public Integer getIsPlatformDelivery() {
        return isPlatformDelivery;
    }

    public void setIsPlatformDelivery(Integer isPlatformDelivery) {
        this.isPlatformDelivery = isPlatformDelivery;
    }
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public Integer getType() {
        return type;
    }

    public void setWorkTel(String workTel) {
        this.workTel = workTel;
    }

    public StoreOwner getStoreOwner() {
        return storeOwner;
    }

    public void setStoreOwner(StoreOwner storeOwner) {
        this.storeOwner = storeOwner;
    }

    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String NAMESPELL = "nameSpell";

    public static final String TEL = "tel";

    public static final String STOREOWNERID = "storeOwnerId";

    public static final String MEMO = "memo";

    public static final String PROVINCEID = "provinceId";

    public static final String CITYID = "cityId";

    public static final String AREAID = "areaId";

    public static final String ADDR = "addr";

    public static final String LONGITUDE = "longitude";

    public static final String LATITUDE = "latitude";

    public static final String CUSTOMMGR = "customMgr";

    public static final String MGRSCOPE = "mgrScope";

    public static final String MGRBRANDS = "mgrBrands";

    public static final String PERSONNUM = "personNum";

    public static final String SIZE = "size";

    public static final String COMMENT = "comment";

    public static final String LEVEL = "level";

    public static final String PREFERENTIAL = "preferential";

    public static final String SERVICES = "services";

    public static final String WORKTEL = "workTel";

    public static final String TYPE = "type";

    public static final String SERVICETYPE = "serviceType";

    public static final String CREATETIME = "createtime";

    public static final String MODIFYTIME = "modifytime";

    public static final String ISPLATFORMDELIVERY = "isPlatformDelivery";

    public static final String COMPANYNAME = "companyName";

    public static final String TAXID = "taxId";

    @Override
    public String toString() {
        return "Store{" +
        "id=" + id +
        ", name=" + name +
        ", nameSpell=" + nameSpell +
        ", tel=" + tel +
        ", storeOwnerId=" + storeOwnerId +
        ", memo=" + memo +
        ", provinceId=" + provinceId +
        ", cityId=" + cityId +
        ", areaId=" + areaId +
        ", addr=" + addr +
        ", longitude=" + longitude +
        ", latitude=" + latitude +
        ", customMgr=" + customMgr +
        ", mgrScope=" + mgrScope +
        ", mgrBrands=" + mgrBrands +
        ", personNum=" + personNum +
        ", size=" + size +
        ", comment=" + comment +
        ", level=" + level +
        ", preferential=" + preferential +
        ", services=" + services +
        ", workTel=" + workTel +
        ", type=" + type +
        ", serviceType=" + serviceType +
        ", createtime=" + createtime +
        ", modifytime=" + modifytime +
        ", isPlatformDelivery=" + isPlatformDelivery +
        ", companyName=" + companyName +
        ", taxId=" + taxId +
        "}";
    }
}
