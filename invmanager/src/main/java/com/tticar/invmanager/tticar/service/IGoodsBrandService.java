package com.tticar.invmanager.tticar.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.tticar.entity.GoodsBrand;

/**
 * <p>
 * 商品品牌表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-24
 */
public interface IGoodsBrandService extends IService<GoodsBrand> {

    Page selectPage(GoodsBrand goodsBrand, MyPage<GoodsBrand> page);

}
