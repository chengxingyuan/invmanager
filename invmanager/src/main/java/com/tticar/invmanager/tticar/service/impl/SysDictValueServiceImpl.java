package com.tticar.invmanager.tticar.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.tticar.entity.SysDictType;
import com.tticar.invmanager.tticar.entity.SysDictValue;
import com.tticar.invmanager.tticar.mapper.SysDictTypeMapper;
import com.tticar.invmanager.tticar.mapper.SysDictValueMapper;
import com.tticar.invmanager.tticar.service.ISysDictValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-24
 */
@Service
public class SysDictValueServiceImpl extends ServiceImpl<SysDictValueMapper, SysDictValue> implements ISysDictValueService {

    @Autowired
    SysDictTypeMapper sysDictTypeMapper;

    @Override
    public List selectSysDictValueList(String dictType) {
        SysDictType param = new SysDictType();
        param.setStatus(0);
        param.setTypeCode(dictType);
        SysDictType sysDictType = sysDictTypeMapper.selectOne(param);

        if (sysDictType == null) {
            return new ArrayList<>();
        }
        Wrapper<SysDictValue> w = new EntityWrapper<>();
        w.eq("dictTypeId", sysDictType.getId());
        w.eq("status", 0);
        return this.selectList(w);
    }
}
