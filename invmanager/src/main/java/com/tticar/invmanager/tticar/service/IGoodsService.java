package com.tticar.invmanager.tticar.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.MMaterielTticar;
import com.tticar.invmanager.entity.vo.GoodsOrderSearchDTO;
import com.tticar.invmanager.entity.vo.GoodsSearchDTO;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.SkuReturnToPage;
import com.tticar.invmanager.tticar.entity.Goods;
import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.tticar.entity.GoodsOrder;
import com.tticar.invmanager.tticar.entity.GoodsProduction;
import com.tticar.invmanager.tticar.entity.GoodsTmp;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 产品表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-18
 */
public interface IGoodsService extends IService<Goods> {

    List<Map> category(Wrapper w);

    List<Map> synchronizationCategory();

    List<Map> brand(MyPage page, Wrapper w);

    Map getById(Long id);

    void insert(MMaterielTticar mMaterielTticar, Goods goods, List<GoodsProduction> goodsProductions,Long materielId);

    Page selectPage(MyPage<GoodsTmp> page, GoodsSearchDTO goodsSearchDTO);

    List<SkuReturnToPage> querySkusByGoodsId(Long goodsId);

    Object queryById(Long id);
}
