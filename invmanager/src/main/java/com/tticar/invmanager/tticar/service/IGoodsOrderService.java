package com.tticar.invmanager.tticar.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.tticar.invmanager.entity.vo.GoodsOrderSearchDTO;
import com.tticar.invmanager.entity.vo.SkuReturnToPage;
import com.tticar.invmanager.tticar.entity.GoodsOrder;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 产品订单表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-13
 */
public interface IGoodsOrderService extends IService<GoodsOrder> {

    Page selectPage(Wrapper wrapper, MyPage<GoodsOrder> page,GoodsOrderSearchDTO goodsOrderSearchDTO);

    void delete(List<Long> ids);

    List goodsList(String orderId);

    List goodsListNew(String orderId);

    List<SkuReturnToPage> queryGoodsDetailByOrderId(Long orderId);


}
