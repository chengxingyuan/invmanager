package com.tticar.invmanager.tticar.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.tticar.entity.StoreOwner;

/**
 * <p>
 * 店铺所有者 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-12
 */
public interface StoreOwnerMapper extends BaseMapper<StoreOwner> {

}
