package com.tticar.invmanager.tticar.service;

import com.baomidou.mybatisplus.service.IService;
import com.tticar.invmanager.tticar.entity.StoreOwner;
import com.tticar.invmanager.tticar.entity.SysDictType;

/**
 * <p>
 * 字典类型表 服务类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-24
 */
public interface ISysDictTypeService extends IService<SysDictType> {


}
