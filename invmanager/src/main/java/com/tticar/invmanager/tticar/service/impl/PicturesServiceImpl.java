package com.tticar.invmanager.tticar.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tticar.invmanager.tticar.entity.Pictures;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.tticar.mapper.PicturesMapper;
import com.tticar.invmanager.tticar.service.IPicturesService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 图片表 服务实现类
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-30
 */
@Service
public class PicturesServiceImpl extends ServiceImpl<PicturesMapper, Pictures> implements IPicturesService {


}
