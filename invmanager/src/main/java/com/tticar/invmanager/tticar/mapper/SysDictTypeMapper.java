package com.tticar.invmanager.tticar.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.tticar.entity.Pictures;
import com.tticar.invmanager.tticar.entity.SysDictType;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 天天爱车字典类型 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-30
 */
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {

}
