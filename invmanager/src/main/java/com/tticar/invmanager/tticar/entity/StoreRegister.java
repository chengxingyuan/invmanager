package com.tticar.invmanager.tticar.entity;

import com.czy.tticar.dubbo.dto.RegisterRequest;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

/**
 * <p>
 * 店铺注册
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-12
 */
public class StoreRegister extends RegisterRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *  上传的图片
     */
    MultipartFile[] files;

    public MultipartFile[] getFiles() {
        return files;
    }

    public void setFiles(MultipartFile[] files) {
        this.files = files;
    }
}
