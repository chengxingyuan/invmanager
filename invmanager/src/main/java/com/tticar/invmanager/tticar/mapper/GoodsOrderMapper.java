package com.tticar.invmanager.tticar.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.czy.tticar.service.dubbodto.InvmanagerGoodsOrderDTO;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.SkuReturnToPage;
import com.tticar.invmanager.tticar.entity.GoodsDTO;
import com.tticar.invmanager.tticar.entity.GoodsOrder;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 产品订单表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-13
 */
@Component("goodsOrderMapper1")
public interface GoodsOrderMapper extends BaseMapper<GoodsOrder> {

    List<Map> get(MyPage page, @Param("ew") Wrapper wrapper);

    List<GoodsDTO> goodsList(@Param("oId") String orderId);

    //临时数据
    List<InvmanagerGoodsOrderDTO> queryGoodsOrderListForInvmanager(Map map);

    List<SkuReturnToPage> queryGoodsIdByOrderId(Long orderId);

    /**
     * 根据天天爱车订单号订单客户名
     */
    String queryCustomName(String goodsId);
}
