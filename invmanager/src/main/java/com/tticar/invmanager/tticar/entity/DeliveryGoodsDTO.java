package com.tticar.invmanager.tticar.entity;

/**
 * @author chengxy
 * @date 2018/10/23 16:12
 * 发货商品
 */
public class DeliveryGoodsDTO {
    private Long goodsOrderId;
    private Long skuId;
    private String house;
    private Integer invNeedCount;

    public Integer getInvNeedCount() {
        return invNeedCount;
    }

    public void setInvNeedCount(Integer invNeedCount) {
        this.invNeedCount = invNeedCount;
    }

    public Long getGoodsOrderId() {
        return goodsOrderId;
    }

    public void setGoodsOrderId(Long goodsOrderId) {
        this.goodsOrderId = goodsOrderId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }
}
