package com.tticar.invmanager.entity.vo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author chengxy
 * @date 2018/8/17 10:16
 */
public class ChangeRepositoryVO {
    private String code;

    private String goodsName;

    private String batchNumber;

    private String fromWearhouseName;

    private String fromPositionName;

    private String toWearhouseName;

    private String toPositionName;

    private Integer count;

    private BigDecimal money;

    private String username;

    private Date ctime;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getFromWearhouseName() {
        return fromWearhouseName;
    }

    public void setFromWearhouseName(String fromWearhouseName) {
        this.fromWearhouseName = fromWearhouseName;
    }

    public String getFromPositionName() {
        return fromPositionName;
    }

    public void setFromPositionName(String fromPositionName) {
        this.fromPositionName = fromPositionName;
    }

    public String getToWearhouseName() {
        return toWearhouseName;
    }

    public void setToWearhouseName(String toWearhouseName) {
        this.toWearhouseName = toWearhouseName;
    }

    public String getToPositionName() {
        return toPositionName;
    }

    public void setToPositionName(String toPositionName) {
        this.toPositionName = toPositionName;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }
}
