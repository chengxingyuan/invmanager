package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 盘点单
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-13
 */
@TableName("c_check")
public class CCheck extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 单号
     */
    private String code;

    /**
     * 商品总数
     */
    @TableField("total_num")
    private Integer totalNum;

    /**
     * 仓库id
     */
    @TableField("house_id")
    private Long houseId;

    /**
     * 状态（0 待校正库存 1 已校正库存，2作废）
     */
    private Integer status;

    /**
     * 操作人姓名
     */
    private String username;

    /**
     * 备注
     */
    private String remark;

    /**
     * 仓库名称
     */
    @TableField(exist = false)
    private String houseName;

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public Integer getStatus() {
        return status;
    }

    public Long getHouseId() {
        return houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public static final String CODE = "code";

    public static final String HOUSE_ID = "house_id";

    public static final String STATUS = "status";

    public static final String USERNAME = "username";

    public static final String REMARK = "remark";

    @Override
    public String toString() {
        return "CCheck{" +
        "code=" + code +
        ", wHouse=" + houseId +
        ", status=" + status +
        ", username=" + username +
        ", remark=" + remark +
        "}";
    }
}
