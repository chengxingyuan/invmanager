package com.tticar.invmanager.entity.vo;

import java.util.Date;
import java.util.List;

/**
 * @author chengxy
 * @date 2018/8/7 15:53
 */
public class InRepositoryAddDTO {

    private String code;

    private Integer inRepositoryType;

    private List<InRepositoryGoodsDTO> inRepositoryGoodsDTOs;

    private Date orderTime;

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getInRepositoryType() {
        return inRepositoryType;
    }

    public void setInRepositoryType(Integer inRepositoryType) {
        this.inRepositoryType = inRepositoryType;
    }

    public List<InRepositoryGoodsDTO> getInRepositoryGoodsDTOs() {
        return inRepositoryGoodsDTOs;
    }

    public void setInRepositoryGoodsDTOs(List<InRepositoryGoodsDTO> inRepositoryGoodsDTOs) {
        this.inRepositoryGoodsDTOs = inRepositoryGoodsDTOs;
    }
}
