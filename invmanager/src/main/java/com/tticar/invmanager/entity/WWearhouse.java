package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-02
 */
@TableName("w_wearhouse")
public class WWearhouse extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long pid;
    /**
     * 仓库名称
     */
    private String name;

    /**
     * 编码
     */
    private String code;

    /**
     * 地址
     */
    private String location;

    /**
     * 负责人
     * */
    @TableField("principal")
    private String principal;

    /**
     * 负责人电话
     */
    @TableField("principal_tel")
    private String principalTel;

    private String namespell;

    private Integer status;

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getPrincipalTel() {
        return principalTel;
    }

    public void setPrincipalTel(String principalTel) {
        this.principalTel = principalTel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    public String getNamespell() {
        return namespell;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public void setNamespell(String namespell) {
        this.namespell = namespell;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static final String PID = "pid";

    public static final String NAME = "name";

    public static final String CODE = "code";

    public static final String LOCATION = "location";

    public static final String NAMESPELL = "namespell";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "WWearhouse{" +
        ", pid=" + pid +
        ", name=" + name +
        ", code=" + code +
        ", location=" + location +
        ", namespell=" + namespell +
        ", status=" + status +
        "}";
    }
}
