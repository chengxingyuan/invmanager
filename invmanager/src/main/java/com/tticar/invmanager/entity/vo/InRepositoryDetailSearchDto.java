package com.tticar.invmanager.entity.vo;

public class InRepositoryDetailSearchDto extends SearchDto {
    private Long wearhouseId;

    private Integer inRepositoryType;

    public Integer getInRepositoryType() {
        return inRepositoryType;
    }

    public void setInRepositoryType(Integer inRepositoryType) {
        this.inRepositoryType = inRepositoryType;
    }

    public Long getWearhouseId() {
        return wearhouseId;
    }

    public void setWearhouseId(Long wearhouseId) {
        this.wearhouseId = wearhouseId;
    }

}
