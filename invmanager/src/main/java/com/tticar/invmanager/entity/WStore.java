package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 门店
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-24
 */
@TableName("w_store")
public class WStore extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 天天爱车id
     */
    @TableField("ttstore_id")
    private Long ttstoreId;

    /**
     * 名称
     */
    private String name;

    /**
     * 店铺管理人
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 店铺联系电话
     */
    private String mobile;

    /**
     * 详细地址
     */
    private String addr;

    /**
     * 简单描述
     */
    private String memo;

    /**
     * 企业名称
     */
    @TableField("company_name")
    private String companyName;

    /**
     * 企业税号
     */
    @TableField("company_num")
    private String companyNum;

    /**
     * 状态
     */
    private Integer status;

    public Long getTtstoreId() {
        return ttstoreId;
    }

    public void setTtstoreId(Long ttstoreId) {
        this.ttstoreId = ttstoreId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    public String getCompanyNum() {
        return companyNum;
    }

    public void setCompanyNum(String companyNum) {
        this.companyNum = companyNum;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static final String TTSTORE_ID = "ttstore_id";

    public static final String NAME = "name";

    public static final String USER_ID = "user_id";

    public static final String MOBILE = "mobile";

    public static final String ADDR = "addr";

    public static final String MEMO = "memo";

    public static final String COMPANY_NAME = "company_name";

    public static final String COMPANY_NUM = "company_num";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "WStore{" +
        "ttstoreId=" + ttstoreId +
        ", name=" + name +
        ", userId=" + userId +
        ", mobile=" + mobile +
        ", addr=" + addr +
        ", memo=" + memo +
        ", companyName=" + companyName +
        ", companyNum=" + companyNum +
        ", status=" + status +
        "}";
    }
}
