package com.tticar.invmanager.entity.vo;

import java.math.BigDecimal;

/**
 * @author chengxy
 * @date 2018/10/26 15:28
 */
public class TticarGoodsSkusDTO {

    private String prices;
    private BigDecimal sellPrice;
    private String skus;
    private String vip0;
    private String vip1;
    private String vip2;
    private String vip3;

    public String getPrices() {
        return prices;
    }

    public void setPrices(String prices) {
        this.prices = prices;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    public String getSkus() {
        return skus;
    }

    public void setSkus(String skus) {
        this.skus = skus;
    }

    public String getVip0() {
        return vip0;
    }

    public void setVip0(String vip0) {
        this.vip0 = vip0;
    }

    public String getVip1() {
        return vip1;
    }

    public void setVip1(String vip1) {
        this.vip1 = vip1;
    }

    public String getVip2() {
        return vip2;
    }

    public void setVip2(String vip2) {
        this.vip2 = vip2;
    }

    public String getVip3() {
        return vip3;
    }

    public void setVip3(String vip3) {
        this.vip3 = vip3;
    }
}
