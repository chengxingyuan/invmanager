package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-19
 */
@TableName("m_materiel_sku")
public class MMaterielSku extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long mid;

    private String code;

    private String skus;

    private String prices;

    @TableField("parts_code")
    private String partsCode;

    @TableField("buy_price")
    private BigDecimal buyPrice;

    @TableField("sell_price")
    private BigDecimal sellPrice;

    @TableField("avg_buy_price")
    private BigDecimal avgBuyPrice;

    @TableField("buy_total_num")
    private Long buyTotalNum;

    public Long getBuyTotalNum() {
        return buyTotalNum;
    }

    public void setBuyTotalNum(Long buyTotalNum) {
        this.buyTotalNum = buyTotalNum;
    }

    public BigDecimal getAvgBuyPrice() {
        return avgBuyPrice;
    }

    public void setAvgBuyPrice(BigDecimal avgBuyPrice) {
        this.avgBuyPrice = avgBuyPrice;
    }

    /**
     * sku图片
     */
    private String img;

    private Integer status;

    public Long getMid() {
        return mid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMid(Long mid) {
        this.mid = mid;
    }
    public String getSkus() {
        return skus;
    }

    public void setSkus(String skus) {
        this.skus = skus;
    }
    public String getPrices() {
        return prices;
    }

    public void setPrices(String prices) {
        this.prices = prices;
    }
    public String getImg() {
        return img;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public String getPartsCode() {
        return partsCode;
    }

    public void setPartsCode(String partsCode) {
        this.partsCode = partsCode;
    }

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public static final String MID = "mid";

    public static final String SKUS = "skus";

    public static final String CODE = "code";

    public static final String PARTSCODE = "parts_code";

    public static final String PRICES = "prices";

    public static final String IMG = "img";

    @Override
    public String toString() {
        return "MMaterielSku{" +
        "mid=" + mid +
        "code=" + code +
        ", skus=" + skus +
        ", partsCode=" + partsCode +
        ", prices=" + prices +
        ", buyPrice=" + buyPrice +
        ", sellPrice=" + sellPrice +
        ", status=" + status +
        ", img=" + img +
        "}";
    }
}
