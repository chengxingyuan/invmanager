package com.tticar.invmanager.entity.vo;

import java.util.Map;

/**
 * Created by yandou on 2018/7/1.
 */
public enum DictCodes {

    materiel, // 存货
    resourceType, // 菜单类型
    materiel_PT, // 支付方式
    materiel_PL, // 价格等级
    relativeNuit_MT, // 会员类型
    relativeNuit, // 往来客户
    relativeNuit_PR, // 性质
    materiel_PM, // 计价方式
    materiel_ATTR, // 存货属性
    materiel_MU, // 计量单位
    tticar, // 天天爱车
    tticar_price, // 价格体系
    tticar_price_price, // 批发价
    tticar_price_vip, // 一级会员价
    tticar_price_vip2, // 二级会员价
    tticar_price_vip3, // 三级会员价
    tticar_is_connect_inv, // 是否关联库存
    tticar_is_open, // 是否开通天天爱车
    purchase_type, // 采购类型
    repository, // 入库类型
    pay_type, // 采购支付类型
    fee_type, // 采购费用类型
    outRepository, // 出库类型
    sales_pay_status, //销售单支付状态
    sales_order_status, //销售单订单状态
    purchase_order_status, //采购单订单状态
    tticar_order_status,//天天爱车订单状态
    online,//天天爱车产品上架/下架
    is_relative,//仓储存货与天天爱车商品是否关联了
    bybs_type,//报溢报损类型
    audit_status,//审核状态
    sale_type,//审核状态
    purchase_next_status//采购单结算状态
    ;

    private static Map<String, Integer> map = null;

    public boolean getEditable() {
        return map.get(this.toString()) == 1;
    }

    public static Map<String, Integer> getMap() {
        return map;
    }

    public static void setMap(Map<String, Integer> map) {
        DictCodes.map = map;
    }

    public static DictCodes get(String code) {
        return DictCodes.valueOf(code);
    }
}
