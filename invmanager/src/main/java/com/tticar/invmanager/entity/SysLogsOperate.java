package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 请求日志内容描述
 * </p>
 *
 * @author jiwenbin
 * @since 2018-09-14
 */
@TableName("sys_logs_operate")
public class SysLogsOperate {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String operate;

    private String ruri;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOperate() {
        return operate;
    }

    public void setOperate(String operate) {
        this.operate = operate;
    }

    public String getRuri() {
        return ruri;
    }

    public void setRuri(String ruri) {
        this.ruri = ruri;
    }
}
