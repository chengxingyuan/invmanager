package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.tticar.invmanager.entity.BaseEntity;

import java.util.Date;

/**
 * <p>
 * 车品牌
 * </p>
 *
 * @author joeyYan
 * @since 2018-09-19
 */
@TableName("car_brand_tmp")
public class CarBrandTmp {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 品牌名称
     */
    @TableField("brand_name")
    private String brandName;

    /**
     * 子品牌名称
     */
    @TableField("sub_name")
    private String subName;

    /**
     * 系列名称
     */
    @TableField("series_name")
    private String seriesName;

    /**
     * 车型名称
     */
    @TableField("model_name")
    private String modelName;

    /**
     * 车款名称
     */
    @TableField("sku_name")
    private String skuName;

    /**
     * 图片地址
     */
    private String src;

    /**
     * 显示状态
     */
    private Integer status;

    private Date ctime;

    private Date utime;

    @TableField(exist = false)
    private Date startTime;

    @TableField(exist = false)
    private Date endTime;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
    public String getSubName() {
        return subName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }
    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }
    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }
    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static final String BRAND_NAME = "brand_name";

    public static final String SUB_NAME = "sub_name";

    public static final String SERIES_NAME = "series_name";

    public static final String MODEL_NAME = "model_name";

    public static final String SKU_NAME = "sku_name";

    public static final String SRC = "src";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "CarBrandTmp{" +
        "brandName=" + brandName +
        ", subName=" + subName +
        ", seriesName=" + seriesName +
        ", modelName=" + modelName +
        ", skuName=" + skuName +
        ", src=" + src +
        ", status=" + status +
        "}";
    }
}
