package com.tticar.invmanager.entity.vo;


import com.baomidou.mybatisplus.plugins.Page;

/**
 * Created by yandou on 2018/6/18.
 */
public class MyPage<T> extends Page {

    private Integer page;
    private Integer rows;

    // easyui 排序
    private String sort;
    private String order;

    public Boolean sort() {
        return "asc".equals(sort);
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        super.setCurrent(page);
        this.page = page;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        super.setSize(rows);
        this.rows = rows;
    }
}
