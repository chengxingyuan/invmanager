package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

import java.math.BigDecimal;

/**
 * <p>
 * 支付流水
 * </p>
 *
 * @author jiwenbin
 * @since 2018-08-06
 */
@TableName("p_pay")
public class PPay extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 业务id
     */
    @TableField("business_id")
    private Long businessId;

    /**
     * 业务类型
     */
    @TableField("business_type")
    private Integer businessType;

    /**
     * 支付类型（0 现金，1 转账，2 支付宝，3 微信）
     */
    private Integer type;

    /**
     * 支付金额
     */
    private BigDecimal amount;

    /**
     * 剩余金额
     */
    @TableField("remain_amount")
    private BigDecimal remainAmount;

    /**
     * 费用说明
     */
    @TableField("fee_note")
    private String feeNote;

    /**
     * 备注
     */
    private String remark;

    /**
     * 操作人姓名
     */
    private String username;

    /**
     * 费用类型（0 采购费用，1 其他费用）
     */
    @TableField("fee_type")
    private Integer feeType;

    /**
     * 用途（0 支付，1 收款）
     */
    @TableField("use_type")
    private Integer useType;

    /**
     * 状态（0 有效，1 无效）
     */
    private Integer status;

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    public Integer getType() {
        return type;
    }

    public BigDecimal getRemainAmount() {
        return remainAmount;
    }

    public void setRemainAmount(BigDecimal remainAmount) {
        this.remainAmount = remainAmount;
    }

    public String getFeeNote() {
        return feeNote;
    }

    public void setFeeNote(String feeNote) {
        this.feeNote = feeNote;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    public String getRemark() {
        return remark;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getUsername() {
        return username;
    }

    public Integer getFeeType() {
        return feeType;
    }

    public void setFeeType(Integer feeType) {
        this.feeType = feeType;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public Integer getUseType() {
        return useType;
    }

    public void setUseType(Integer useType) {
        this.useType = useType;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static final String BUSINESS_ID = "business_id";
    public static final String BUSINESS_TYPE = "business_type";

    public static final String TYPE = "type";

    public static final String REMAIN_AMOUNT = "remain_amount";

    public static final String AMOUNT = "amount";

    public static final String FEE_NOTE = "fee_note";

    public static final String REMARK = "remark";

    public static final String USERNAME = "username";

    public static final String FEE_TYPE = "fee_type";

    public static final String USE_TYPE = "use_type";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "PPay{" +
        "businessId=" + businessId +
        ", PayBusinessType=" + businessType +
        ", type=" + type +
        ", amount=" + amount +
        ", remainAmount=" + remainAmount +
        ", feeNote=" + feeNote +
        ", remark=" + remark +
        ", username=" + username +
        ", feeType=" + feeType +
        ", useType=" + useType +
        ", status=" + status +
        "}";
    }
}
