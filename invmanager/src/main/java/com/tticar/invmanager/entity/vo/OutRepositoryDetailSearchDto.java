package com.tticar.invmanager.entity.vo;

public class OutRepositoryDetailSearchDto extends SearchDto {
    private Long wearhouseId;
    private Integer outRepositoryType;

    public Integer getOutRepositoryType() {
        return outRepositoryType;
    }

    public void setOutRepositoryType(Integer outRepositoryType) {
        this.outRepositoryType = outRepositoryType;
    }

    public Long getWearhouseId() {
        return wearhouseId;
    }

    public void setWearhouseId(Long wearhouseId) {
        this.wearhouseId = wearhouseId;
    }

}
