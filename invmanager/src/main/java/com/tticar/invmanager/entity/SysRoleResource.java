package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 角色资源表
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-28
 */
@TableName("sys_role_resource")
public class SysRoleResource extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("role_id")
    private Long roleId;

    @TableField("resource_id")
    private Long resourceId;

    @TableField("resource_per")
    private String resourcePer;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }
    public String getResourcePer() {
        return resourcePer;
    }

    public void setResourcePer(String resourcePer) {
        this.resourcePer = resourcePer;
    }

    public static final String ROLE_ID = "role_id";

    public static final String RESOURCE_ID = "resource_id";

    public static final String RESOURCE_PER = "resource_per";

    @Override
    public String toString() {
        return "SysRoleResource{" +
        "roleId=" + roleId +
        ", resourceId=" + resourceId +
        ", resourcePer=" + resourcePer +
        "}";
    }
}
