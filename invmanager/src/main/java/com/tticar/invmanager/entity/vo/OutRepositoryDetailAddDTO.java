package com.tticar.invmanager.entity.vo;

import java.util.Date;
import java.util.List;

/**
 * @author chengxy
 * @date 2018/8/8 14:12
 */
public class OutRepositoryDetailAddDTO {
    private Integer type;

    private String code;

    private Integer outRepositoryType;

    private List<InRepositoryGoodsDTO> inRepositoryGoodsDTOs;

    private Date orderTime;

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getOutRepositoryType() {
        return outRepositoryType;
    }

    public void setOutRepositoryType(Integer outRepositoryType) {
        this.outRepositoryType = outRepositoryType;
    }

    public List<InRepositoryGoodsDTO> getInRepositoryGoodsDTOs() {
        return inRepositoryGoodsDTOs;
    }

    public void setInRepositoryGoodsDTOs(List<InRepositoryGoodsDTO> inRepositoryGoodsDTOs) {
        this.inRepositoryGoodsDTOs = inRepositoryGoodsDTOs;
    }
}
