package com.tticar.invmanager.entity.vo;

/**
 * @author chengxy
 * @date 2018/10/19 9:36
 */
public class SkuReturnToPage {
    private Integer relativeStatus;
    private String skuName;
    private String skuValue;
    private Long order;
    private Long id;
    private String goodsId;
    private String inventory;
    private Integer count;
    private Integer combo;
    private Long storageSkuId;

    public Long getStorageSkuId() {
        return storageSkuId;
    }

    public void setStorageSkuId(Long storageSkuId) {
        this.storageSkuId = storageSkuId;
    }

    public Integer getCombo() {
        return combo;
    }

    public void setCombo(Integer combo) {
        this.combo = combo;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getRelativeStatus() {
        return relativeStatus;
    }

    public void setRelativeStatus(Integer relativeStatus) {
        this.relativeStatus = relativeStatus;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getSkuValue() {
        return skuValue;
    }

    public void setSkuValue(String skuValue) {
        this.skuValue = skuValue;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
