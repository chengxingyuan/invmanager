package com.tticar.invmanager.entity.vo;

import java.math.BigDecimal;

/**
 * @author chengxy
 * @date 2018/10/22 10:05
 */
public class RelativeGoodsDTO {
    private Long id;
    private String name;
    private String category;
    private String brand;
    private String skuName;
    private String goodsCode;
    private String partsCode;
    private String suitCar;
    private String supply;
    private String inPrice;
    private String salePrice;
    private Integer repositoryCount;
    private BigDecimal DiscoutFee;
    private BigDecimal allMoney;
    private String tticarGoodsId;
    private String skuValue;
    private Integer comboCount;
    private Integer needInv;

    public Integer getNeedInv() {
        return needInv;
    }

    public void setNeedInv(Integer needInv) {
        this.needInv = needInv;
    }

    public Integer getComboCount() {
        return comboCount;
    }

    public void setComboCount(Integer comboCount) {
        this.comboCount = comboCount;
    }

    public String getSkuValue() {
        return skuValue;
    }

    public void setSkuValue(String skuValue) {
        this.skuValue = skuValue;
    }

    public String getTticarGoodsId() {
        return tticarGoodsId;
    }

    public void setTticarGoodsId(String tticarGoodsId) {
        this.tticarGoodsId = tticarGoodsId;
    }

    public BigDecimal getAllMoney() {
        return allMoney;
    }

    public void setAllMoney(BigDecimal allMoney) {
        this.allMoney = allMoney;
    }

    public BigDecimal getDiscoutFee() {
        return DiscoutFee;
    }

    public void setDiscoutFee(BigDecimal discoutFee) {
        DiscoutFee = discoutFee;
    }

    private BigDecimal unitPrice;
    private BigDecimal transportFee;

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getTransportFee() {
        return transportFee;
    }

    public void setTransportFee(BigDecimal transportFee) {
        this.transportFee = transportFee;
    }

    @Override
    public String toString() {
        return "RelativeGoodsDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", brand='" + brand + '\'' +
                ", skuName='" + skuName + '\'' +
                ", goodsCode='" + goodsCode + '\'' +
                ", partsCode='" + partsCode + '\'' +
                ", suitCar='" + suitCar + '\'' +
                ", supply='" + supply + '\'' +
                ", inPrice=" + inPrice +
                ", salePrice=" + salePrice +
                ", repositoryCount=" + repositoryCount +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getPartsCode() {
        return partsCode;
    }

    public void setPartsCode(String partsCode) {
        this.partsCode = partsCode;
    }

    public String getSuitCar() {
        return suitCar;
    }

    public void setSuitCar(String suitCar) {
        this.suitCar = suitCar;
    }

    public String getSupply() {
        return supply;
    }

    public void setSupply(String supply) {
        this.supply = supply;
    }

    public String getInPrice() {
        return inPrice;
    }

    public void setInPrice(String inPrice) {
        this.inPrice = inPrice;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public Integer getRepositoryCount() {
        return repositoryCount;
    }

    public void setRepositoryCount(Integer repositoryCount) {
        this.repositoryCount = repositoryCount;
    }
}
