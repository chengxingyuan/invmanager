package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 往来客户
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-08
 */
@TableName("w_relative_unit")
public class WRelativeUnit extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;

    /**
     * 助记码
     */
    @TableField("help_code")
    private String helpCode;

    /**
     * 编码
     */
    private String code;

    /**
     * 简称
     */
    @TableField("simple_name")
    private String simpleName;

    /**
     * 性质
     */
    private Integer rproperty;

    /**
     * 联系人
     */
    private String contactor;

    /**
     * 联系电话
     */
    private String tel;

    /**
     * 地址
     */
    private String addr;

    /**
     * 会员类型
     */
    @TableField("member_type")
    private Integer memberType;

    /**
     * 状态
     */
    private Integer status;

    public String getHelpCode() {
        return helpCode;
    }

    public void setHelpCode(String helpCode) {
        this.helpCode = helpCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getSimpleName() {
        return simpleName;
    }

    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }
    public Integer getRproperty() {
        return rproperty;
    }

    public void setRproperty(Integer rproperty) {
        this.rproperty = rproperty;
    }
    public String getContactor() {
        return contactor;
    }

    public void setContactor(String contactor) {
        this.contactor = contactor;
    }
    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }
    public Integer getMemberType() {
        return memberType;
    }

    public void setMemberType(Integer memberType) {
        this.memberType = memberType;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static final String NAME = "name";

    public static final String CODE = "code";

    public static final String SIMPLE_NAME = "simple_name";

    public static final String RPROPERTY = "rproperty";

    public static final String CONTACTOR = "contactor";

    public static final String TEL = "tel";

    public static final String ADDR = "addr";

    public static final String MEMBER_TYPE = "member_type";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "WRelativeUnit{" +
        "name=" + name +
        ", code=" + code +
        ", simpleName=" + simpleName +
        ", rproperty=" + rproperty +
        ", contactor=" + contactor +
        ", tel=" + tel +
        ", addr=" + addr +
        ", memberType=" + memberType +
        ", status=" + status +
        "}";
    }
}
