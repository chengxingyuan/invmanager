package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by yandou on 2018/6/22.
 */
public class BaseEntity implements Serializable {

    public static final Integer ZERO = 0;
    public static final Integer ONE = 1;

    @TableId(value = "id", type = IdType.AUTO)
    protected Long id;

    @TableField("tenant_id")
    protected Long tenantId;

    protected Date ctime;

    protected Date utime;

    @TableField(exist = false)
    protected Date startTime;

    @TableField(exist = false)
    protected Date endTime;

    @TableField(exist = false)
    protected String startTime2;

    @TableField(exist = false)
    protected String endTime2;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }

    public String getStartTime2() {
        return startTime2;
    }

    public void setStartTime2(String startTime2) {
        this.startTime2 = startTime2;
    }

    public String getEndTime2() {
        return endTime2;
    }

    public void setEndTime2(String endTime2) {
        this.endTime2 = endTime2;
    }

    public static final String ID = "id";

    public static final String TENANT_ID = "tenant_id";

    public static final String CTIME = "ctime";

    public static final String UTIME = "utime";

}
