package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 请求日志
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-29
 */
@TableName("sys_logs")
public class SysLogs extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名
     */
    private String name;

    /**
     * 请求地址
     */
    private String ruri;

    /**
     * 请求方法
     */
    private String rmethod;

    /**
     * 请求参数
     */
    private String rargs;

    /**
     * 请求描述
     */
    @TableField(exist = false)
    private String desc;

    /**
     * 请求ip
     */
    @TableField("ip_addr")
    private String ipAddr;

    /**
     * 请求码
     */
    @TableField("statu_code")
    private String statuCode;

    /**
     * 请求标示
     */
    @TableField("session_id")
    private String sessionId;

    /**
     * 请求耗时
     */
    private Long rtime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getRuri() {
        return ruri;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setRuri(String ruri) {
        this.ruri = ruri;
    }
    public String getRmethod() {
        return rmethod;
    }

    public void setRmethod(String rmethod) {
        this.rmethod = rmethod;
    }
    public String getRargs() {
        return rargs;
    }

    public void setRargs(String rargs) {
        this.rargs = rargs;
    }
    public String getIpAddr() {
        return ipAddr;
    }

    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr;
    }
    public String getStatuCode() {
        return statuCode;
    }

    public void setStatuCode(String statuCode) {
        this.statuCode = statuCode;
    }
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    public Long getRtime() {
        return rtime;
    }

    public void setRtime(Long rtime) {
        this.rtime = rtime;
    }

    public static final String NAME = "name";

    public static final String RURI = "ruri";

    public static final String RMETHOD = "rmethod";

    public static final String RARGS = "rargs";

    public static final String IP_ADDR = "ip_addr";

    public static final String STATU_CODE = "statu_code";

    public static final String SESSION_ID = "session_id";

    public static final String RTIME = "rtime";

    @Override
    public String toString() {
        return "SysLogs{" +
        "name=" + name +
        ", ruri=" + ruri +
        ", rmethod=" + rmethod +
        ", rargs=" + rargs +
        ", desc=" + desc +
        ", ipAddr=" + ipAddr +
        ", statuCode=" + statuCode +
        ", sessionId=" + sessionId +
        ", rtime=" + rtime +
        "}";
    }
}
