package com.tticar.invmanager.entity.vo;

public class RepositoryCountSearchDto extends SearchDto {
    private Long wearhouseId;

    public Long getWearhouseId() {
        return wearhouseId;
    }

    public void setWearhouseId(Long wearhouseId) {
        this.wearhouseId = wearhouseId;
    }

    private Long categoryId;

    private String categoryList;

    private String supplierName;

    private Integer shieldZero;

    public Integer getShieldZero() {
        return shieldZero;
    }

    public void setShieldZero(Integer shieldZero) {
        this.shieldZero = shieldZero;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(String categoryList) {
        this.categoryList = categoryList;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
}
