package com.tticar.invmanager.entity.vo;

import java.util.List;

public class GoodsPartDto {
    private Integer type;//类型 0：编辑 1：审核
    private Long id;
    private String orderTime;
    private Long houseId;
    private String remark;

    private GoodsPartDetailDto from;

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public Long getHouseId() {
        return houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

    public GoodsPartDetailDto getFrom() {
        return from;
    }

    public void setFrom(GoodsPartDetailDto from) {
        this.from = from;
    }

    public List<GoodsPartDetailDto> getList() {
        return list;
    }

    public void setList(List<GoodsPartDetailDto> list) {
        this.list = list;
    }

    private List<GoodsPartDetailDto> list;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
