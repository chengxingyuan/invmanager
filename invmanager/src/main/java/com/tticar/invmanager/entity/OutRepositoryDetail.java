package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.util.Date;

/**
 * <p>
 * 出库单详情
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-06
 */
@TableName("out_repository_detail")
public class OutRepositoryDetail extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 所取商品
     */
    @TableField("sku_id")
    private Long skuId;

    /**
     * 出库单号
     */
    private String code;

    /**
     * 业务id
     */
    @TableField("busz_id")
    private Long buszId;

    /**
     * 供应商id
     */
    @TableField("provider_id")
    private Long providerId;

    /**
     * 仓库id
     */
    @TableField("wearhouse_id")
    private Long wearhouseId;

    /**
     * 库位id
     */
    @TableField("position_id")
    private Long positionId;

    /**
     * 单据时间
     */
    @TableField("order_time")
    private Date orderTime;

    /**
     * 出库类型（0.销售出库，1.其他）
     */
    private Integer type;

    /**
     * 数量
     */
    private Integer count;

    /**
     * 操作人
     */
    private String username;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否有效（0，有效，1，无效）
     */
    private Integer status;

    /**
     * 搜索条件 所在仓库
     * */
    @TableField("wearhouse_id")
    private Integer wearhouseIdForSearch;

    /**
     * 搜索条件 开始时间
     * */
    @TableField("ctime")
    private Date startTime;

    /**
     * 搜索条件 结束时间
     * */
    @TableField("ctime")
    private Date endTime;

    @TableField("batch_number")
    private String batchNumber;

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public Integer getWearhouseIdForSearch() {
        return wearhouseIdForSearch;
    }

    public void setWearhouseIdForSearch(Integer wearhouseIdForSearch) {
        this.wearhouseIdForSearch = wearhouseIdForSearch;
    }

    public Long getBuszId() {
        return buszId;
    }

    public void setBuszId(Long buszId) {
        this.buszId = buszId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getProviderId() {
        return providerId;
    }

    public void setProviderId(Long providerId) {
        this.providerId = providerId;
    }
    public Long getWearhouseId() {
        return wearhouseId;
    }

    public void setWearhouseId(Long wearhouseId) {
        this.wearhouseId = wearhouseId;
    }
    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }
    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static final String SKU_ID = "sku_id";

    public static final String CODE = "code";

    public static final String IN_REPOSITORY_DETAIL_ID = "in_repository_detail_id";

    public static final String PROVIDER_ID = "provider_id";

    public static final String WEARHOUSE_ID = "wearhouse_id";

    public static final String POSITION_ID = "position_id";

    public static final String ORDER_TIME = "order_time";

    public static final String TYPE = "type";

    public static final String COUNT = "count";

    public static final String USERNAME = "username";

    public static final String REMARK = "remark";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "OutRepositoryDetail{" +
                "skuId=" + skuId +
                ", code=" + code +
                ", providerId=" + providerId +
                ", wearhouseId=" + wearhouseId +
                ", positionId=" + positionId +
                ", orderTime=" + orderTime +
                ", type=" + type +
                ", count=" + count +
                ", username=" + username +
                ", remark=" + remark +
                ", status=" + status +
                "}";
    }
}
