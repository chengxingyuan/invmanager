package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 调拨单
 * </p>
 *
 * @author joeyYan
 * @since 2018-12-10
 */
@TableName("change_repository")
public class ChangeRepository extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 调拨单号
     */
    private String code;

    /**
     * 商品sku_id
     */
    @TableField("sku_id")
    private Integer skuId;

    /**
     * 调出仓库id
     */
    @TableField("from_wearhouse_id")
    private Long fromWearhouseId;

    /**
     * 调入仓库id
     */
    @TableField("to_wearhouse_id")
    private Long toWearhouseId;

    /**
     * 操作人
     */
    private String username;

    /**
     * 调拨数量
     */
    private Integer count;

    /**
     * 调拨成本
     */
    private BigDecimal money;

    /**
     * 备注
     */
    private String remark;

    /**
     * 0.启用1.禁用
     */
    private Integer status;

    @TableField("order_time")
    private Date orderTime;

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }
    public Long getFromWearhouseId() {
        return fromWearhouseId;
    }

    public void setFromWearhouseId(Long fromWearhouseId) {
        this.fromWearhouseId = fromWearhouseId;
    }
    public Long getToWearhouseId() {
        return toWearhouseId;
    }

    public void setToWearhouseId(Long toWearhouseId) {
        this.toWearhouseId = toWearhouseId;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static final String CODE = "code";

    public static final String SKU_ID = "sku_id";

    public static final String FROM_WEARHOUSE_ID = "from_wearhouse_id";

    public static final String TO_WEARHOUSE_ID = "to_wearhouse_id";

    public static final String USERNAME = "username";

    public static final String COUNT = "count";

    public static final String MONEY = "money";

    public static final String REMARK = "remark";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "ChangeRepository{" +
        "code=" + code +
        ", skuId=" + skuId +
        ", fromWearhouseId=" + fromWearhouseId +
        ", toWearhouseId=" + toWearhouseId +
        ", username=" + username +
        ", count=" + count +
        ", money=" + money +
        ", remark=" + remark +
        ", status=" + status +
        "}";
    }
}
