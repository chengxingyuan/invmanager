package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 库位表
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-31
 */
@TableName("w_position")
public class WPosition extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("wearhouse_id")
    private Long wearhouseId;

    /**
     * 仓库名称
     */
    private String name;

    /**
     * 编码
     */
    private String code;

    private Integer status;

    /**
     * 搜索条件 所属仓库
     * */
    @TableField(exist = false)
    private Integer wearhouseIdForSearch;

    public Integer getWearhouseIdForSearch() {
        return wearhouseIdForSearch;
    }

    public void setWearhouseIdForSearch(Integer wearhouseIdForSearch) {
        this.wearhouseIdForSearch = wearhouseIdForSearch;
    }

    public Long getWearhouseId() {
        return wearhouseId;
    }

    public void setWearhouseId(Long wearhouseId) {
        this.wearhouseId = wearhouseId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static final String WEARHOUSE_ID = "wearhouse_id";

    public static final String NAME = "name";

    public static final String CODE = "code";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "WPosition{" +
        "wearhouseId=" + wearhouseId +
        ", name=" + name +
        ", code=" + code +
        ", status=" + status +
        "}";
    }
}
