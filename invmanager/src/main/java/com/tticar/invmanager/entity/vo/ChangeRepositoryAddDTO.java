package com.tticar.invmanager.entity.vo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author chengxy
 * @date 2018/8/7 15:53
 */
public class ChangeRepositoryAddDTO {



    private Long fromWearhouseId;

    private Long toWearhouseId;

    private BigDecimal money;

    private List<ChangeRepositoryGoodsDTO> changeRepositoryGoodsDTOs;

    public List<ChangeRepositoryGoodsDTO> getChangeRepositoryGoodsDTOs() {
        return changeRepositoryGoodsDTOs;
    }

    public void setChangeRepositoryGoodsDTOs(List<ChangeRepositoryGoodsDTO> changeRepositoryGoodsDTOs) {
        this.changeRepositoryGoodsDTOs = changeRepositoryGoodsDTOs;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Long getToWearhouseId() {
        return toWearhouseId;
    }

    public void setToWearhouseId(Long toWearhouseId) {
        this.toWearhouseId = toWearhouseId;
    }

    public Long getFromWearhouseId() {
        return fromWearhouseId;
    }

    public void setFromWearhouseId(Long fromWearhouseId) {
        this.fromWearhouseId = fromWearhouseId;
    }





}
