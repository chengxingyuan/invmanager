package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 报溢报损详情
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-18
 */
@TableName("bybs_detail")
public class BybsDetail extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 总单id
     */
    @TableField("bybs_id")
    private Long bybsId;

    /**
     * 仓库
     */
    @TableField("wearhouse_id")
    private Long wearhouseId;

    /**
     * 库位id
     */
    @TableField("position_id")
    private Long positionId;

    /**
     * 商品skuid
     */
    @TableField("sku_id")
    private Long skuId;

    /**
     * 批号
     */
    @TableField("batch_number")
    private String batchNumber;

    /**
     * 数量
     */
    private Integer num;

    /**
     * 成本单价
     */
    private BigDecimal price;

    /**
     * 总额
     */
    private BigDecimal fee;

    /**
     * 是否删除
     */
    private Integer status;

    /**
     * 没用到
     */
    private String username;

    /**
     * 备注
     */
    private String remark;

    public Long getBybsId() {
        return bybsId;
    }

    public void setBybsId(Long bybsId) {
        this.bybsId = bybsId;
    }
    public Long getWearhouseId() {
        return wearhouseId;
    }

    public void setWearhouseId(Long wearhouseId) {
        this.wearhouseId = wearhouseId;
    }
    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }
    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }
    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }
    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public static final String BYBS_ID = "bybs_id";

    public static final String WEARHOUSE_ID = "wearhouse_id";

    public static final String POSITION_ID = "position_id";

    public static final String SKU_ID = "sku_id";

    public static final String BATCH_NUMBER = "batch_number";

    public static final String NUM = "num";

    public static final String PRICE = "price";

    public static final String FEE = "fee";

    public static final String STATUS = "status";

    public static final String USERNAME = "username";

    public static final String REMARK = "remark";

    @Override
    public String toString() {
        return "BybsDetail{" +
        "bybsId=" + bybsId +
        ", wearhouseId=" + wearhouseId +
        ", positionId=" + positionId +
        ", skuId=" + skuId +
        ", batchNumber=" + batchNumber +
        ", num=" + num +
        ", price=" + price +
        ", fee=" + fee +
        ", status=" + status +
        ", username=" + username +
        ", remark=" + remark +
        "}";
    }
}
