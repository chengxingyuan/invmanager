package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 租户
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-22
 */
@TableName("sys_tenant")
public class SysTenant implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 租户名称
     */
    private String name;

    /**
     * 描述
     */
    private String memo;

    /**
     * 详细地址
     */
    private String addr;

    /**
     * 联系电话
     */
    private String mobile;

    private Integer status;

    @TableField("need_relative_inv")
    private Integer needRelativeInv;

    @TableField("sys_user_id")
    private Long sysUserId;

    private Date ctime;

    private Date utime;

    public Integer getNeedRelativeInv() {
        return needRelativeInv;
    }

    public void setNeedRelativeInv(Integer needRelativeInv) {
        this.needRelativeInv = needRelativeInv;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public Long getSysUserId() {
        return sysUserId;
    }

    public void setSysUserId(Long sysUserId) {
        this.sysUserId = sysUserId;
    }
    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }
    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }

    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String MEMO = "memo";

    public static final String ADDR = "addr";

    public static final String MOBILE = "mobile";

    public static final String STATUS = "status";

    public static final String SYS_USER_ID = "sys_user_id";

    public static final String CTIME = "ctime";

    public static final String UTIME = "utime";

    @Override
    public String toString() {
        return "SysTenant{" +
        "id=" + id +
        ", name=" + name +
        ", memo=" + memo +
        ", addr=" + addr +
        ", mobile=" + mobile +
        ", status=" + status +
        ", sysUserId=" + sysUserId +
        ", ctime=" + ctime +
        ", utime=" + utime +
        "}";
    }
}
