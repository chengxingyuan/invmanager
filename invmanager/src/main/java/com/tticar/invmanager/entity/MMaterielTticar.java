package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 天天爱车产品
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-25
 */
@TableName("m_materiel_tticar")
public class MMaterielTticar extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("materiel_id")
    private Long materielId;

    @TableField("ttstore_id")
    private Long ttstoreId;

    @TableField("ttgoods_id")
    private Long ttgoodsId;

    @TableField("materiel_sku_ids")
    private String materielSkuIds;

    private String pictrues;

    private String cuser;

    public Long getMaterielId() {
        return materielId;
    }

    public void setMaterielId(Long materielId) {
        this.materielId = materielId;
    }
    public Long getTtstoreId() {
        return ttstoreId;
    }

    public void setTtstoreId(Long ttstoreId) {
        this.ttstoreId = ttstoreId;
    }
    public Long getTtgoodsId() {
        return ttgoodsId;
    }

    public void setTtgoodsId(Long ttgoodsId) {
        this.ttgoodsId = ttgoodsId;
    }
    public String getMaterielSkuIds() {
        return materielSkuIds;
    }

    public void setMaterielSkuIds(String materielSkuIds) {
        this.materielSkuIds = materielSkuIds;
    }
    public String getPictrues() {
        return pictrues;
    }

    public void setPictrues(String pictrues) {
        this.pictrues = pictrues;
    }
    public String getCuser() {
        return cuser;
    }

    public void setCuser(String cuser) {
        this.cuser = cuser;
    }

    public static final String MATERIEL_ID = "materiel_id";

    public static final String TTSTORE_ID = "ttstore_id";

    public static final String TTGOODS_ID = "ttgoods_id";

    public static final String MATERIEL_SKU_IDS = "materiel_sku_ids";

    public static final String PICTRUES = "pictrues";

    public static final String CUSER = "cuser";

    @Override
    public String toString() {
        return "MMaterielTticar{" +
        "materielId=" + materielId +
        ", ttstoreId=" + ttstoreId +
        ", ttgoodsId=" + ttgoodsId +
        ", materielSkuIds=" + materielSkuIds +
        ", pictrues=" + pictrues +
        ", cuser=" + cuser +
        "}";
    }
}
