package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 采购单
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-03
 */
@TableName("p_purchase")
public class PPurchase extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 单号
     */
    private String code;

    /**
     * 供应商id
     */
    @TableField("relative_id")
    private Long relativeId;

    /**
     * 预购商品金额
     */
    private BigDecimal amount;

    /**
     * 实际商品金额
     */
    @TableField("amount_actual")
    private BigDecimal amountActual;

    /**
     * 已付金额
     */
    @TableField("amount_already")
    private BigDecimal amountAlready;

    /**
     * 待付金额
     */
    @TableField("amount_ing")
    private BigDecimal amountIng;

    /**
     * 供应商联系电话
     */
    @TableField("contact_tel")
    private String contactTel;

    /**
     * 供应商联系人
     */
    @TableField("contact_name")
    private String contactName;

    /**
     * 商品总数
     */
    @TableField("total_num")
    private Integer totalNum;

    /**
     * 采购类型
     */
    private Integer ptype;

    /**
     * 操作人姓名
     */
    private String username;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态（0 待入库，1 部分入库，2 全部入库， 3 作废）
     */
    private Integer status;

    /**
     * 支付状态（0 未结清，1已结清）
     */
    @TableField("pay_status")
    private Integer payStatus;

    /**
     * 订单状态（0 未入库 1 已入库 2已完成）
     */
    @TableField("order_status")
    private Integer orderStatus;

    /**
     * 采购时间
     */
    private Date ptime;

    /**
     * 查询条件模糊搜索
     */
    @TableField(exist = false)
    private String searchText;

    @TableField("settlement_status")
    private Integer settlementStatus;

    @TableField("return_order")
    private String returnOrder;

    public String getReturnOrder() {
        return returnOrder;
    }

    public void setReturnOrder(String returnOrder) {
        this.returnOrder = returnOrder;
    }

    public Integer getSettlementStatus() {
        return settlementStatus;
    }

    public void setSettlementStatus(Integer settlementStatus) {
        this.settlementStatus = settlementStatus;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public BigDecimal getAmountIng() {
        return amountIng;
    }

    public void setAmountIng(BigDecimal amountIng) {
        this.amountIng = amountIng;
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public Long getRelativeId() {
        return relativeId;
    }

    public BigDecimal getAmountActual() {
        return amountActual;
    }

    public void setAmountActual(BigDecimal amountActual) {
        this.amountActual = amountActual;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmountAlready() {
        return amountAlready;
    }

    public void setAmountAlready(BigDecimal amountAlready) {
        this.amountAlready = amountAlready;
    }

    public String getContactTel() {
        return contactTel;
    }

    public void setRelativeId(Long relativeId) {
        this.relativeId = relativeId;
    }

    public void setContactTel(String contactTel) {
        this.contactTel = contactTel;
    }
    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }
    public Integer getPtype() {
        return ptype;
    }

    public void setPtype(Integer ptype) {
        this.ptype = ptype;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }
    public Date getPtime() {
        return ptime;
    }

    public void setPtime(Date ptime) {
        this.ptime = ptime;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public static final String CODE = "code";

    public static final String RELATIVE_ID = "relative_id";

    public static final String AMOUNT = "amount";

    public static final String AMOUNT_ACTUAL = "amount_actual";

    public static final String AMOUNT_ALREADY = "amount_already";

    public static final String CONTACT_TEL = "contact_tel";

    public static final String CONTACT_NAME = "contact_name";

    public static final String PTYPE = "ptype";

    public static final String USERNAME = "username";

    public static final String REMARK = "remark";

    public static final String STATUS = "status";

    public static final String PAY_STATUS = "pay_status";

    public static final String PTIME = "ptime";

    @Override
    public String toString() {
        return "PPurchase{" +
        "code=" + code +
        ", relativeId=" + relativeId +
        ", amount=" + amount +
        ", amountActual=" + amountActual +
        ", amountAlready=" + amountAlready +
        ", contactTel=" + contactTel +
        ", contactName=" + contactName +
        ", ptype=" + ptype +
        ", username=" + username +
        ", remark=" + remark +
        ", status=" + status +
        ", payStatus=" + payStatus +
        ", ptime=" + ptime +
        "}";
    }
}
