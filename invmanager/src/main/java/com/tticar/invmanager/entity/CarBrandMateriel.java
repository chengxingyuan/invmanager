package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author joeyYan
 * @since 2018-09-20
 */
@TableName("car_brand_materiel")
public class CarBrandMateriel extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 车型id
     */
    @TableField("car_brand_id")
    private Long carBrandId;

    /**
     * 商品id
     */
    @TableField("materiel_id")
    private Long materielId;

    private String year;

    /**
     * 车型信息
     */
    private String car;

    private Integer status;

    public Long getCarBrandId() {
        return carBrandId;
    }

    public void setCarBrandId(Long carBrandId) {
        this.carBrandId = carBrandId;
    }
    public Long getMaterielId() {
        return materielId;
    }

    public void setMaterielId(Long materielId) {
        this.materielId = materielId;
    }
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static final String CAR_BRAND_ID = "car_brand_id";

    public static final String MATERIEL_ID = "materiel_id";

    public static final String YEAR = "year";

    public static final String CAR = "car";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "CarBrandMateriel{" +
        "carBrandId=" + carBrandId +
        ", materielId=" + materielId +
        ", year=" + year +
        ", car=" + car +
        ", status=" + status +
        "}";
    }
}
