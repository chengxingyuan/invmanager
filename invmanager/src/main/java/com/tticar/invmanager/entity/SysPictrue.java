package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 图片资源
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-16
 */
@TableName("sys_pictrue")
public class SysPictrue extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long pid;

    /**
     * 图片名称
     */
    private String name;

    /**
     * 图片路径
     */
    private String path;

    /**
     * 0文件夹1图片
     */
    private Integer type;

    /**
     * 大小
     */
    private String size;

    /**
     * 尺寸/时长
     */
    private String bsize;

    private String cuser;

    /**
     * 描述
     */
    private String memo;

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
    public String getBsize() {
        return bsize;
    }

    public void setBsize(String bsize) {
        this.bsize = bsize;
    }
    public String getCuser() {
        return cuser;
    }

    public void setCuser(String cuser) {
        this.cuser = cuser;
    }
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public static final String PID = "pid";

    public static final String NAME = "name";

    public static final String PATH = "path";

    public static final String TYPE = "type";

    public static final String SIZE = "size";

    public static final String BSIZE = "bsize";

    public static final String CUSER = "cuser";

    public static final String MEMO = "memo";

    @Override
    public String toString() {
        return "SysPictrue{" +
        "pid=" + pid +
        ", name=" + name +
        ", path=" + path +
        ", type=" + type +
        ", size=" + size +
        ", bsize=" + bsize +
        ", cuser=" + cuser +
        ", memo=" + memo +
        "}";
    }
}
