package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 存货分类表
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-10
 */
@TableName("m_cate_tmp")
public class MCateTmp extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long pid;

    /**
     * 分类名称
     */
    private String name;

    @TableField("group_name")
    private String groupName;

    /**
     * 分类编码
     */
    private String code;

    /**
     * 分类类型：1级分类，2级分类，3级分类
     */
    @TableField("category_type")
    private Integer categoryType;

    /**
     * 是否叶子节点
     */
    @TableField("is_leaf")
    private Integer isLeaf;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态
     */
    private Integer status;

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public Integer getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(Integer categoryType) {
        this.categoryType = categoryType;
    }
    public Integer getIsLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(Integer isLeaf) {
        this.isLeaf = isLeaf;
    }
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static final String PID = "pid";

    public static final String NAME = "name";

    public static final String GROUP_NAME = "group_name";

    public static final String CODE = "code";

    public static final String CATEGORY_TYPE = "category_type";

    public static final String IS_LEAF = "is_leaf";

    public static final String SORT = "sort";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "MCateTmp{" +
        "pid=" + pid +
        ", name=" + name +
        ", groupName=" + groupName +
        ", code=" + code +
        ", categoryType=" + categoryType +
        ", isLeaf=" + isLeaf +
        ", sort=" + sort +
        ", status=" + status +
        "}";
    }
}
