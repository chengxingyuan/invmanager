package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 商品库存数量表
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-10
 */
@TableName("repository_count")
public class RepositoryCount extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 商品skuId
     */
    @TableField("sku_id")
    private Long skuId;

    /**
     * 商品数量
     */
    private Integer count;

    /**
     * 最小库存量
     */
    @TableField("min_count")
    private Integer minCount;

    /**
     * 最大库存量
     */
    @TableField("max_count")
    private Integer maxCount;

    @TableField("wearhouse_id")
    private Long wearhouseId;

    @TableField("position_id")
    private Long positionId;

    public Long getWearhouseId() {
        return wearhouseId;
    }

    public void setWearhouseId(Long wearhouseId) {
        this.wearhouseId = wearhouseId;
    }

    public Integer getMinCount() {
        return minCount;
    }

    public void setMinCount(Integer minCount) {
        this.minCount = minCount;
    }

    public Integer getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(Integer maxCount) {
        this.maxCount = maxCount;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public static final String SKU_ID = "sku_id";

    public static final String COUNT = "count";

    public static final String WEARHOUSE_ID = "wearhouse_id";

    @Override
    public String toString() {
        return "RepositoryCount{" +
                "skuId=" + skuId +
                ", count=" + count +
                "}";
    }
}
