package com.tticar.invmanager.entity.vo;

/**
 * @author chengxy
 * @date 2018/10/18 15:50
 */
public class GoodsSkuDetailsDTO {
    private Long id;
    private String name;

    public Integer getBindStatus() {
        return bindStatus;
    }

    public void setBindStatus(Integer bindStatus) {
        this.bindStatus = bindStatus;
    }

    private Integer bindStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
