package com.tticar.invmanager.entity.vo;

import java.math.BigDecimal;

/**
 * @author chengxy
 * @date 2018/10/30 17:47
 */
public class PlaceOrderDTO {
    private Long goodsId;
    private String skuId;
    private String goodsName;
    private String skuName;
    private String name;
    private String id;
    private BigDecimal unitPrice;
    private BigDecimal transportFee;
    private String prices;
    private Long materielSkuId;
    private BigDecimal totalMoney;
    private Integer totalCount;
    private Integer needInv;

    public Integer getNeedInv() {
        return needInv;
    }

    public void setNeedInv(Integer needInv) {
        this.needInv = needInv;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Long getMaterielSkuId() {
        return materielSkuId;
    }

    public void setMaterielSkuId(Long materielSkuId) {
        this.materielSkuId = materielSkuId;
    }

    public String getPrices() {
        return prices;
    }

    public void setPrices(String prices) {
        this.prices = prices;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getTransportFee() {
        return transportFee;
    }

    public void setTransportFee(BigDecimal transportFee) {
        this.transportFee = transportFee;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 关联的存货名称
     */
    private String InvName;
    /**
     * 关联的存货sku名称
     */
    private String InvSkuName;

    /**
     * 存货库存数量
     */
    private Integer count;

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getInvName() {
        return InvName;
    }

    public void setInvName(String invName) {
        InvName = invName;
    }

    public String getInvSkuName() {
        return InvSkuName;
    }

    public void setInvSkuName(String invSkuName) {
        InvSkuName = invSkuName;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "PlaceOrderDTO{" +
                "goodsId=" + goodsId +
                ", skuId='" + skuId + '\'' +
                ", goodsName='" + goodsName + '\'' +
                ", skuName='" + skuName + '\'' +
                ", name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", unitPrice=" + unitPrice +
                ", transportFee=" + transportFee +
                ", InvName='" + InvName + '\'' +
                ", InvSkuName='" + InvSkuName + '\'' +
                ", count=" + count +
                '}';
    }
}
