package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 单号计数器
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-01
 */
@TableName("code_counter")
public class CodeCounter extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 计数
     */
    private Integer number;

    /**
     * 编码类型
     */
    @TableField("code_type")
    private String codeType;

    /**
     * 0 单号，1 批号，2 编码
     */
    private Integer type;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public static final String NUMBER = "number";

    public static final String CODE_TYPE = "code_type";

    public static final String TYPE = "type";

    @Override
    public String toString() {
        return "CodeCounter{" +
        "number=" + number +
        ", codeType=" + codeType +
        "}";
    }
}
