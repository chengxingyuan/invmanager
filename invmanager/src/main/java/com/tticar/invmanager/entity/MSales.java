package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-13
 */
@TableName("m_sales")
public class MSales extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 单号
     */
    private String code;

    /**
     * 操作人
     */
    private String username;

    /**
     * 订单类型 0：线下订单 1：天天爱车订单
     */
    @TableField("order_type")
    private Integer orderType;

    /**
     * 订单状态 0：代发货 1：已发货 2：已完成
     */
    @TableField("order_status")
    private Integer orderStatus;

    /**
     * 支付状态 0：挂账 1：已结清
     */
    @TableField("pay_status")
    private Integer payStatus;

    /**
     * 客户id
     */
    @TableField("cus_id")
    private Long cusId;

    /**
     * 销售总数量
     */
    @TableField("total_num")
    private Integer totalNum;

    /**
     * 商品总额
     */
    @TableField("goods_fee")
    private BigDecimal goodsFee;

    /**
     * 应付费用
     */
    @TableField("total_fee")
    private BigDecimal totalFee;

    /**
     * 优惠金额
     */
    @TableField("discount_fee")
    private BigDecimal discountFee;

    /**
     * 已付费用
     */
    @TableField("payed_fee")
    private BigDecimal payedFee;

    /**
     * 待付费用
     */
    @TableField("paying_fee")
    private BigDecimal payingFee;

    /**
     * 物流公司名称
     */
    @TableField("logistics_name")
    private String logisticsName;

    /**
     * 物流单号
     */
    @TableField("logistics_no")
    private String logisticsNo;

    /**
     * 配送员
     */
    @TableField("distribute_name")
    private String distributeName;

    /**
     * 配送员电话
     */
    @TableField("distribute_tel")
    private String distributeTel;

    /**
     * 销售日期
     */
    @TableField("order_time")
    private Date orderTime;

    /**
     * 是否删除
     */
    private Integer status;

    @TableField("cus_name")
    private String cusName;

    @TableField("cus_tel")
    private String cusTel;

    @TableField("cus_addr")
    private String cusAddr;

    private String remark;

    @TableField("settlement_status")
    private Integer settlementStatus;

    @TableField("return_order")
    private String returnOrder;

    public String getReturnOrder() {
        return returnOrder;
    }

    public void setReturnOrder(String returnOrder) {
        this.returnOrder = returnOrder;
    }

    public Integer getSettlementStatus() {
        return settlementStatus;
    }

    public void setSettlementStatus(Integer settlementStatus) {
        this.settlementStatus = settlementStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }
    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }
    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }
    public Long getCusId() {
        return cusId;
    }

    public void setCusId(Long cusId) {
        this.cusId = cusId;
    }
    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }
    public BigDecimal getGoodsFee() {
        return goodsFee;
    }

    public void setGoodsFee(BigDecimal goodsFee) {
        this.goodsFee = goodsFee;
    }
    public BigDecimal getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }
    public BigDecimal getDiscountFee() {
        return discountFee;
    }

    public void setDiscountFee(BigDecimal discountFee) {
        this.discountFee = discountFee;
    }
    public BigDecimal getPayedFee() {
        return payedFee;
    }

    public void setPayedFee(BigDecimal payedFee) {
        this.payedFee = payedFee;
    }
    public BigDecimal getPayingFee() {
        return payingFee;
    }

    public void setPayingFee(BigDecimal payingFee) {
        this.payingFee = payingFee;
    }
    public String getLogisticsName() {
        return logisticsName;
    }

    public void setLogisticsName(String logisticsName) {
        this.logisticsName = logisticsName;
    }
    public String getLogisticsNo() {
        return logisticsNo;
    }

    public void setLogisticsNo(String logisticsNo) {
        this.logisticsNo = logisticsNo;
    }
    public String getDistributeName() {
        return distributeName;
    }

    public void setDistributeName(String distributeName) {
        this.distributeName = distributeName;
    }
    public String getDistributeTel() {
        return distributeTel;
    }

    public void setDistributeTel(String distributeTel) {
        this.distributeTel = distributeTel;
    }
    public Integer getStatus() {
        return status;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusTel() {
        return cusTel;
    }

    public void setCusTel(String cusTel) {
        this.cusTel = cusTel;
    }

    public String getCusAddr() {
        return cusAddr;
    }

    public void setCusAddr(String cusAddr) {
        this.cusAddr = cusAddr;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static final String CODE = "code";

    public static final String USERNAME = "username";

    public static final String ORDER_TYPE = "order_type";

    public static final String ORDER_STATUS = "order_status";

    public static final String PAY_STATUS = "pay_status";

    public static final String CUS_ID = "cus_id";

    public static final String TOTAL_NUM = "total_num";

    public static final String GOODS_FEE = "goods_fee";

    public static final String TOTAL_FEE = "total_fee";

    public static final String DISCOUNT_FEE = "discount_fee";

    public static final String PAYED_FEE = "payed_fee";

    public static final String PAYING_FEE = "paying_fee";

    public static final String REMARK = "remark";

    public static final String LOGISTICS_NAME = "logistics_name";

    public static final String LOGISTICS_NO = "logistics_no";

    public static final String DISTRIBUTE_NAME = "distribute_name";

    public static final String DISTRIBUTE_TEL = "distribute_tel";

    public static final String STATUS = "status";

    public static final String ORDER_TIME = "order_time";

    @Override
    public String toString() {
        return "MSales{" +
        "code=" + code +
        ", username=" + username +
        ", orderType=" + orderType +
        ", orderStatus=" + orderStatus +
        ", payStatus=" + payStatus +
        ", cusId=" + cusId +
        ", totalNum=" + totalNum +
        ", goodsFee=" + goodsFee +
        ", totalFee=" + totalFee +
        ", discountFee=" + discountFee +
        ", payedFee=" + payedFee +
        ", payingFee=" + payingFee +
        ", logisticsName=" + logisticsName +
        ", logisticsNo=" + logisticsNo +
        ", distributeName=" + distributeName +
        ", distributeTel=" + distributeTel +
        ", status=" + status +
        ", orderTime=" + orderTime +
        "}";
    }
}
