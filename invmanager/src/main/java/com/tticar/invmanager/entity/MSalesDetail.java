package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-15
 */
@TableName("m_sales_detail")
public class MSalesDetail extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 销售订单
     */
    @TableField("sales_id")
    private Long salesId;

    /**
     * 商品规格id
     */
    @TableField("sku_id")
    private Long skuId;

    /**
     * 批次
     */
    @TableField("batch_code")
    private String batchCode;

    /**
     * 售价
     */
    private BigDecimal price;

    /**
     * 销售数量
     */
    private Integer num;

    /**
     * 总金额
     */
    private BigDecimal fee;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否删除
     */
    private Integer status;

    public Long getSalesId() {
        return salesId;
    }

    public void setSalesId(Long salesId) {
        this.salesId = salesId;
    }
    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }
    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static final String SALES_ID = "sales_id";

    public static final String SKU_ID = "sku_id";

    public static final String BATCH_CODE = "batch_code";

    public static final String PRICE = "price";

    public static final String NUM = "num";

    public static final String FEE = "fee";

    public static final String REMARK = "remark";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "MSalesDetail{" +
        "salesId=" + salesId +
        ", skuId=" + skuId +
        ", batchCode=" + batchCode +
        ", price=" + price +
        ", num=" + num +
        ", fee=" + fee +
        ", remark=" + remark +
        ", status=" + status +
        "}";
    }
}
