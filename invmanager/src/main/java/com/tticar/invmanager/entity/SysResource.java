package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 资源管理
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
@TableName("sys_resource")
public class SysResource implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long pid;

    /**
     * 资源名称
     */
    private String name;

    /**
     * 路径
     */
    private String src;

    /**
     * 权限
     */
    private String permission;

    /**
     * 图标
     */
    private String icon;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 0 横目录1竖目录2菜单3按钮
     */
    private Integer type;

    /**
     * 状态
     */
    private Integer status;

    private Date ctime;

    private Date utime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }
    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }
    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }

    public static final String ID = "id";

    public static final String PID = "pid";

    public static final String NAME = "name";

    public static final String SRC = "src";

    public static final String PERMISSION = "permission";

    public static final String ICON = "icon";

    public static final String SORT = "sort";

    public static final String TYPE = "type";

    public static final String STATUS = "status";

    public static final String CTIME = "ctime";

    public static final String UTIME = "utime";

    @Override
    public String toString() {
        return "SysResource{" +
        "id=" + id +
        ", pid=" + pid +
        ", name=" + name +
        ", src=" + src +
        ", permission=" + permission +
        ", icon=" + icon +
        ", sort=" + sort +
        ", type=" + type +
        ", status=" + status +
        ", ctime=" + ctime +
        ", utime=" + utime +
        "}";
    }
}
