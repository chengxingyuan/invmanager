package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 存货分类规格对应表
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-05
 */
@TableName("m_cateogry_sku")
public class MCateogrySku extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("category_id")
    private Long categoryId;

    @TableField("sku_id")
    private Long skuId;

    /**
     * 分类排序
     */
    private Integer sort;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public static final String CATEGORY_ID = "category_id";

    public static final String SKU_ID = "sku_id";

    public static final String SORT = "sort";

    @Override
    public String toString() {
        return "MCateogrySku{" +
        "categoryId=" + categoryId +
        ", skuId=" + skuId +
        ", sort=" + sort +
        "}";
    }
}
