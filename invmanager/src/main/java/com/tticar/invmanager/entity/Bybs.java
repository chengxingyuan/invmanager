package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-18
 */
@TableName("bybs")
public class Bybs extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 日期
     */
    @TableField("order_time")
    private Date orderTime;

    @TableField("order_type")
    private Integer orderType;

    /**
     * 编码
     */
    private String code;

    /**
     * 仓库
     */
    @TableField("wearhouse_id")
    private Long wearhouseId;

    /**
     * 总量
     */
    private Integer num;

    /**
     * 总额
     */
    private BigDecimal fee;

    /**
     * 状态 -1 删除 0 待审核 1 已审核
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 审核时间
     */
    private Date otime;

    /**
     * 创建人
     */
    private String cname;

    /**
     * 审核人
     */
    private String uname;

    /**
     * 审核人
     */
    private String oname;

    /**
     * 没用到
     */
    private String username;

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public Long getWearhouseId() {
        return wearhouseId;
    }

    public void setWearhouseId(Long wearhouseId) {
        this.wearhouseId = wearhouseId;
    }
    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Date getOtime() {
        return otime;
    }

    public void setOtime(Date otime) {
        this.otime = otime;
    }
    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }
    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }
    public String getOname() {
        return oname;
    }

    public void setOname(String oname) {
        this.oname = oname;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static final String ORDER_TIME = "order_time";
    public static final String ORDER_TYPE = "order_type";
    public static final String CODE = "code";

    public static final String WEARHOUSE_ID = "wearhouse_id";

    public static final String NUM = "num";

    public static final String FEE = "fee";

    public static final String STATUS = "status";

    public static final String REMARK = "remark";

    public static final String OTIME = "otime";

    public static final String CNAME = "cname";

    public static final String UNAME = "uname";

    public static final String ONAME = "oname";

    public static final String USERNAME = "username";

    @Override
    public String toString() {
        return "Bybs{" +
        "orderTime=" + orderTime +
        ", code=" + code +
        ", wearhouseId=" + wearhouseId +
        ", num=" + num +
        ", fee=" + fee +
        ", status=" + status +
        ", remark=" + remark +
        ", otime=" + otime +
        ", cname=" + cname +
        ", uname=" + uname +
        ", oname=" + oname +
        ", username=" + username +
        "}";
    }
}
