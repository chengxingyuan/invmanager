package com.tticar.invmanager.entity.vo;

/**
 * @author chengxy
 * @date 2018/8/7 15:54
 */
public class InRepositoryGoodsDTO {

    private Long skusId;

    private Long goodsId;

    private Long ralativeId;

    private String batchCode;

    private Integer count;

    private String remark;

    private Long positionId;

    private Long wearhouseId;

    private String house;

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public Long getWearhouseId() {
        return wearhouseId;
    }

    public void setWearhouseId(Long wearhouseId) {
        this.wearhouseId = wearhouseId;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public Long getSkusId() {
        return skusId;
    }

    public void setSkusId(Long skusId) {
        this.skusId = skusId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getRalativeId() {
        return ralativeId;
    }

    public void setRalativeId(Long ralativeId) {
        this.ralativeId = ralativeId;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "InRepositoryGoodsDTO{" +
                "skusId=" + skusId +
                ", goodsId=" + goodsId +
                ", ralativeId=" + ralativeId +
                ", batchCode='" + batchCode + '\'' +
                ", count=" + count +
                ", remark='" + remark + '\'' +
                ", positionId=" + positionId +
                ", wearhouseId=" + wearhouseId +
                ", house='" + house + '\'' +
                '}';
    }
}
