package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 存货分类表
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-17
 */
@TableName("m_cateogry")
public class MCateogry extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long pid;

    /**
     * 分类名称
     */
    private String name;

    @TableField("group_name")
    private String groupName;

    /**
     * 分类编码
     */
    private String code;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 排序
     * */
    private Integer sort;

    /**
     * 分类类型
     */
    @TableField("category_type")
    private Integer categoryType;

    @TableField("is_leaf")
    private Integer isLeaf;

    @TableField(exist = false)
    private Long token;

    public Long getToken() {
        return token;
    }

    public void setToken(Long token) {
        this.token = token;
    }

    public Integer getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(Integer categoryType) {
        this.categoryType = categoryType;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * 编辑状态
     */
    @TableField(exist = false)
    private Integer type;

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    public String getCode() {
        return code;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(Integer isLeaf) {
        this.isLeaf = isLeaf;
    }

    public static final String PID = "pid";

    public static final String NAME = "name";

    public static final String GROUP_NAME = "group_name";

    public static final String CODE = "code";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "MCateogry{" +
        "pid=" + pid +
        ", name=" + name +
        ", groupName=" + groupName +
        ", code=" + code +
        ", status=" + status +
        "}";
    }
}
