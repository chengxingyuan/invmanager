package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 存货表
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-15
 */
@TableName("m_materiel")
public class MMateriel extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;

    /**
     * 拼音
     */
    private String pinyin;
    /**
     * 首字母
     */
    private String initials;

    /**
     * 存货编码(MaterielSku的code)
     */
    @TableField(exist = false)
    private String code;

    /**
     * 拼音
     */
    @TableField("name_spell")
    private String nameSpell;

    /**
     * 分类
     */
    @TableField("category_id")
    private Long categoryId;

    /**
     * 品牌
     */
    @TableField("brand_id")
    private Long brandId;

    /**
     * 计价方式
     */
    @TableField("pricing_method")
    private Integer pricingMethod;

    /**
     * 助记码
     */
    @TableField("rem_code")
    private String remCode;

    /**
     * 存货属性
     */
    private String attribute;

    /**
     * 默认供应商
     */
    @TableField("default_uint_id")
    private Long defaultUintId;

    @TableField("default_whouse_id")
    private Long defaultWhouseId;

    @TableField("default_position_id")
    private Long defaultPositionId;

    /**
     * 描述
     */
    private String description;

    private String pictrues;

    /**
     * 总库存
     */
    private Integer ainventory;

    /**
     * 最低库存
     */
    @TableField("min_inventory")
    private Integer minInventory;

    /**
     * 最高库存
     */
    @TableField("max_inventory")
    private Integer maxInventory;

    /**
     * 计量单位
     */
    @TableField("m_unit")
    private String mUnit;

    /**
     * 适配车型
     */
    @TableField("car")
    private String car;

    public Long getTticarGoodsId() {
        return tticarGoodsId;
    }

    public void setTticarGoodsId(Long tticarGoodsId) {
        this.tticarGoodsId = tticarGoodsId;
    }

    @TableField("tticar_goods_id")
    private Long tticarGoodsId;

    private Integer status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getCode() {
        return code;
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getNameSpell() {
        return nameSpell;
    }

    public void setNameSpell(String nameSpell) {
        this.nameSpell = nameSpell;
    }
    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }
    public Integer getPricingMethod() {
        return pricingMethod;
    }

    public void setPricingMethod(Integer pricingMethod) {
        this.pricingMethod = pricingMethod;
    }
    public String getRemCode() {
        return remCode;
    }

    public void setRemCode(String remCode) {
        this.remCode = remCode;
    }
    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }
    public Long getDefaultUintId() {
        return defaultUintId;
    }

    public void setDefaultUintId(Long defaultUintId) {
        this.defaultUintId = defaultUintId;
    }
    public Long getDefaultWhouseId() {
        return defaultWhouseId;
    }

    public void setDefaultWhouseId(Long defaultWhouseId) {
        this.defaultWhouseId = defaultWhouseId;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getPictrues() {
        return pictrues;
    }

    public void setPictrues(String pictrues) {
        this.pictrues = pictrues;
    }
    public String getmUnit() {
        return mUnit;
    }

    public Integer getAinventory() {
        return ainventory;
    }

    public void setAinventory(Integer ainventory) {
        this.ainventory = ainventory;
    }

    public Integer getMinInventory() {
        return minInventory;
    }

    public void setMinInventory(Integer minInventory) {
        this.minInventory = minInventory;
    }

    public Integer getMaxInventory() {
        return maxInventory;
    }

    public void setMaxInventory(Integer maxInventory) {
        this.maxInventory = maxInventory;
    }

    public Long getDefaultPositionId() {
        return defaultPositionId;
    }

    public void setDefaultPositionId(Long defaultPositionId) {
        this.defaultPositionId = defaultPositionId;
    }

    public void setmUnit(String mUnit) {
        this.mUnit = mUnit;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public static final String NAME = "name";

    public static final String CODE = "code";

    public static final String NAME_SPELL = "name_spell";

    public static final String CATEGORY_ID = "category_id";

    public static final String BRAND_ID = "brand_id";

    public static final String PRICING_METHOD = "pricing_method";

    public static final String REM_CODE = "rem_code";

    public static final String ATTRIBUTE = "attribute";

    public static final String DEFAULT_UINT_ID = "default_uint_id";

    public static final String DEFAULT_WHOUSE_ID = "default_whouse_id";

    public static final String DESCRIPTION = "description";

    public static final String PICTRUES = "pictrues";

    public static final String AINVENTORY = "ainventory";

    public static final String MIN_INVENTORY = "min_inventory";

    public static final String MAX_INVENTORY = "max_inventory";

    public static final String M_UNIT = "m_unit";

    public static final String STATUS = "status";

    public static final String CAR = "car";

    @Override
    public String toString() {
        return "MMateriel{" +
        "name=" + name +
        ", code=" + code +
        ", nameSpell=" + nameSpell +
        ", categoryId=" + categoryId +
        ", brandId=" + brandId +
        ", pricingMethod=" + pricingMethod +
        ", remCode=" + remCode +
        ", attribute=" + attribute +
        ", defaultUintId=" + defaultUintId +
        ", defaultWhouseId=" + defaultWhouseId +
        ", description=" + description +
        ", pictrues=" + pictrues +
        ", ainventory=" + ainventory +
        ", minInventory=" + minInventory +
        ", maxInventory=" + maxInventory +
        ", mUnit=" + mUnit +
        ", status=" + status +
        "}";
    }
}
