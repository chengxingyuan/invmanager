package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 库位对应的商品
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-23
 */
@TableName("w_position_materiel")
public class WPositionMateriel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("tenant_id")
    private Long tenantId;

    /**
     * 仓库id
     */
    @TableField("wearhouse_id")
    private Long wearhouseId;

    /**
     * 库位id
     */
    @TableField("position_id")
    private Long positionId;

    /**
     * 商品id
     */
    @TableField("materiel_id")
    private Long materielId;

    /**
     * 是否是默认（0不是默认，1 是默认）
     */
    @TableField("is_default")
    private Integer isDefault;

    private Integer status;

    private Date ctime;

    private Date utime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }
    public Long getWearhouseId() {
        return wearhouseId;
    }

    public void setWearhouseId(Long wearhouseId) {
        this.wearhouseId = wearhouseId;
    }
    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }
    public Long getMaterielId() {
        return materielId;
    }

    public void setMaterielId(Long materielId) {
        this.materielId = materielId;
    }
    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }
    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }

    public static final String ID = "id";

    public static final String TENANT_ID = "tenant_id";

    public static final String WEARHOUSE_ID = "wearhouse_id";

    public static final String POSITION_ID = "position_id";

    public static final String MATERIEL_ID = "materiel_id";

    public static final String IS_DEFAULT = "is_default";

    public static final String STATUS = "status";

    public static final String CTIME = "ctime";

    public static final String UTIME = "utime";

    @Override
    public String toString() {
        return "WPositionMateriel{" +
        "id=" + id +
        ", tenantId=" + tenantId +
        ", wearhouseId=" + wearhouseId +
        ", positionId=" + positionId +
        ", materielId=" + materielId +
        ", isDefault=" + isDefault +
        ", status=" + status +
        ", ctime=" + ctime +
        ", utime=" + utime +
        "}";
    }
}
