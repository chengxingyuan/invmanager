package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;

/**
 * <p>
 * 采购单详情
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-03
 */
@TableName("p_purchase_detail")
public class PPurchaseDetail extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 采购总单id
     */
    @TableField("purchase_id")
    private Long purchaseId;

    /**
     * 商品id
     */
    @TableField("sku_id")
    private Long skuId;

    /**
     * 预购数量
     */
    private Integer count;

    /**
     * 实际采购数量
     */
    @TableField("count_actual")
    private Integer countActual;

    /**
     * 预计采购金额
     */
    private BigDecimal amount;

    /**
     * 实际采购金额
     */
    @TableField("amount_actual")
    private BigDecimal amountActual;

    /**
     * 折扣金额
     */
    @TableField("amount_discount")
    private BigDecimal amountDiscount;

    /**
     * 入库状态（0 未入库，1 已入库）
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 批号
     */
    @TableField("batch_code")
    private String batchCode;

    /**
     * 仓库id
     */
    @TableField("wearhouse_id")
    private Long wearhouseId;

    /**
     * 库位id
     */
    @TableField("position_id")
    private Long positionId;

    public Long getWearhouseId() {
        return wearhouseId;
    }

    public void setWearhouseId(Long wearhouseId) {
        this.wearhouseId = wearhouseId;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public Long getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(Long purchaseId) {
        this.purchaseId = purchaseId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getCountActual() {
        return countActual;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmountActual() {
        return amountActual;
    }

    public void setAmountActual(BigDecimal amountActual) {
        this.amountActual = amountActual;
    }

    public BigDecimal getAmountDiscount() {
        return amountDiscount;
    }

    public void setAmountDiscount(BigDecimal amountDiscount) {
        this.amountDiscount = amountDiscount;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public void setCountActual(Integer countActual) {
        this.countActual = countActual;
    }

    public String getRemark() {
        return remark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public void setRemark(String remark) {
        this.remark = remark;
    }

    public static final String PURCHASE_ID = "purchase_id";

    public static final String SKU_ID = "sku_id";

    public static final String COUNT = "count";

    public static final String COUNT_ACTUAL = "count_actual";

    public static final String AMOUNT = "amount";

    public static final String AMOUNT_ACTUAL = "amount_actual";

    public static final String AMOUNT_DISCOUNT = "amount_discount";

    public static final String REMARK = "remark";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "PPurchaseDetail{" +
                "purchaseId=" + purchaseId +
                ", skuId=" + skuId +
                ", count=" + count +
                ", countActual=" + countActual +
                ", amount=" + amount +
                ", amountActual=" + amountActual +
                ", amountDiscount=" + amountDiscount +
                ", remark=" + remark +
                "}";
    }
}
