package com.tticar.invmanager.entity.vo;

import java.util.List;

public class BybsDto {
    private Integer orderType;
    private Integer type;//类型 0：编辑 1：审核
    private Long id;
    private String orderTime;
    private Long houseId;
    private String remark;
    private List<BybsDetailDto> list;

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public Long getHouseId() {
        return houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<BybsDetailDto> getList() {
        return list;
    }

    public void setList(List<BybsDetailDto> list) {
        this.list = list;
    }
}
