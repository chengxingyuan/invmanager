package com.tticar.invmanager.entity.vo;

/**
 * @author chengxy
 * @date 2018/8/7 15:54
 */
public class ChangeRepositoryGoodsDTO {

    private Long skusId;

    private String batchCode;

    private Integer count;

    private String remark;

    private Long fromPositionId;

    private Long toPositionId;

    public Long getToPositionId() {
        return toPositionId;
    }

    public void setToPositionId(Long toPositionId) {
        this.toPositionId = toPositionId;
    }

    public Long getFromPositionId() {
        return fromPositionId;
    }

    public void setFromPositionId(Long fromPositionId) {
        this.fromPositionId = fromPositionId;
    }

    public Long getSkusId() {
        return skusId;
    }

    public void setSkusId(Long skusId) {
        this.skusId = skusId;
    }



    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
