package com.tticar.invmanager.entity.vo;

import com.tticar.invmanager.entity.MMateriel;
import com.tticar.invmanager.entity.MMaterielSku;

import java.util.List;

/**
 * <p>
 * 存货表
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-11
 */
public class MMaterielWithSkus extends MMateriel {

    private List<MMaterielSku> mMaterielSkus;

    public List<MMaterielSku> getmMaterielSkus() {
        return mMaterielSkus;
    }

    public void setmMaterielSkus(List<MMaterielSku> mMaterielSkus) {
        this.mMaterielSkus = mMaterielSkus;
    }
}
