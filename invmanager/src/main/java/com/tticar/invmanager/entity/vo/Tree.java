package com.tticar.invmanager.entity.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by yandou on 2018/6/22.
 */
public class Tree implements Serializable {

    private Long id;
    private Long pid;
    private String text;
    // closed open
    private String state;
    private List<Tree> children;
    private boolean checked;
    private Map attributes;
    private String iconCls;

    // 递归判断
    private boolean hasChildren;

    public Tree() {
        super();
    }

    public Tree(Long id, String text) {
        this.id = id;
        this.text = text;
    }

    public boolean isHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(boolean hasChildren) {
        this.hasChildren = hasChildren;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<Tree> getChildren() {
        return children;
    }

    public void setChildren(List<Tree> children) {
        this.children = children;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public Map getAttributes() {
        return attributes;
    }

    public void setAttributes(Map attributes) {
        this.attributes = attributes;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getIconCls() {
        return iconCls;
    }

    public void setIconCls(String iconCls) {
        this.iconCls = iconCls;
    }
}
