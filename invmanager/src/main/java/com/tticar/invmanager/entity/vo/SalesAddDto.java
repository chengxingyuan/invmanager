package com.tticar.invmanager.entity.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * 新增采购单商品
 */
public class SalesAddDto {
    private Long salesId;
    private List<SalesGoodsDto> goodsList;
    private String orderTime;
    private Integer type;

    private Long cusId;
    private String cusName;
    private String cusTel;
    private String cusAddr;

    private BigDecimal totalFee;
    private BigDecimal goodsFee;
    private BigDecimal payedFee;
    private BigDecimal payingFee;
    private BigDecimal discountFee;
    private Integer totalNum;

    private BigDecimal payFee;
    private Integer payType;
    private String payRemark;

    private String logisticsName;
    private String logisticsNo;
    private String distributeName;
    private String distributeTel;

    private Integer payStatus;
    private String remark;
    private BigDecimal realPay;

    private String token;

    private Integer orderType;

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public BigDecimal getRealPay() {
        return realPay;
    }

    public void setRealPay(BigDecimal realPay) {
        this.realPay = realPay;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<SalesGoodsDto> getGoodsList() {
        return goodsList;
    }

    public Long getSalesId() {
        return salesId;
    }

    public void setSalesId(Long salesId) {
        this.salesId = salesId;
    }

    public void setGoodsList(List<SalesGoodsDto> goodsList) {
        this.goodsList = goodsList;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getCusId() {
        return cusId;
    }

    public void setCusId(Long cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusTel() {
        return cusTel;
    }

    public void setCusTel(String cusTel) {
        this.cusTel = cusTel;
    }

    public String getCusAddr() {
        return cusAddr;
    }

    public void setCusAddr(String cusAddr) {
        this.cusAddr = cusAddr;
    }

    public BigDecimal getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }

    public BigDecimal getPayFee() {
        return payFee;
    }

    public void setPayFee(BigDecimal payFee) {
        this.payFee = payFee;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public String getPayRemark() {
        return payRemark;
    }

    public void setPayRemark(String payRemark) {
        this.payRemark = payRemark;
    }

    public BigDecimal getGoodsFee() {
        return goodsFee;
    }

    public void setGoodsFee(BigDecimal goodsFee) {
        this.goodsFee = goodsFee;
    }

    public BigDecimal getPayedFee() {
        return payedFee;
    }

    public void setPayedFee(BigDecimal payedFee) {
        this.payedFee = payedFee;
    }

    public BigDecimal getPayingFee() {
        return payingFee;
    }

    public void setPayingFee(BigDecimal payingFee) {
        this.payingFee = payingFee;
    }

    public BigDecimal getDiscountFee() {
        return discountFee;
    }

    public void setDiscountFee(BigDecimal discountFee) {
        this.discountFee = discountFee;
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public String getLogisticsName() {
        return logisticsName;
    }

    public void setLogisticsName(String logisticsName) {
        this.logisticsName = logisticsName;
    }

    public String getLogisticsNo() {
        return logisticsNo;
    }

    public void setLogisticsNo(String logisticsNo) {
        this.logisticsNo = logisticsNo;
    }

    public String getDistributeName() {
        return distributeName;
    }

    public void setDistributeName(String distributeName) {
        this.distributeName = distributeName;
    }

    public String getDistributeTel() {
        return distributeTel;
    }

    public void setDistributeTel(String distributeTel) {
        this.distributeTel = distributeTel;
    }
}
