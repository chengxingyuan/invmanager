package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
@TableName("sys_department")
public class SysDepartment extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long pid;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 部门编码
     */
    private String code;

    /**
     * 状态
     */
    private Integer status;

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static final String PID = "pid";

    public static final String NAME = "name";

    public static final String CODE = "code";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "SysDepartment{" +
        "pid=" + pid +
        ", name=" + name +
        ", code=" + code +
        ", status=" + status +
        "}";
    }
}
