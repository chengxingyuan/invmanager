package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-15
 */
@TableName("goods_part")
public class GoodsPart extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 编码
     */
    private String code;

    /**
     * 仓库
     */
    @TableField("wearhouse_id")
    private Long wearhouseId;

    @TableField("batch_code")
    private String batchCode;

    /**
     * 规格
     */
    @TableField("sku_id")
    private Long skuId;

    private BigDecimal price;

    private Integer num;

    private BigDecimal fee;

    /**
     * 状态 -1 删除 0 待审核 1 已审核 2 已完成
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 完成时间
     */
    private Date otime;

    /**
     * 创建人
     */
    private String cname;

    /**
     * 审核人
     */
    private String uname;

    /**
     * 完成人
     */
    private String oname;

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    /**
     * 订单时间
     */
    @TableField("order_time")
    private Date orderTime;

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public Long getWearhouseId() {
        return wearhouseId;
    }

    public void setWearhouseId(Long wearhouseId) {
        this.wearhouseId = wearhouseId;
    }
    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Date getOtime() {
        return otime;
    }

    public void setOtime(Date otime) {
        this.otime = otime;
    }
    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }
    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }
    public String getOname() {
        return oname;
    }

    public void setOname(String oname) {
        this.oname = oname;
    }

    public static final String CODE = "code";

    public static final String WEARHOUSE_ID = "wearhouse_id";

    public static final String POSITION_ID = "position_id";

    public static final String SKU_ID = "sku_id";

    public static final String BATCH_NUMBER = "batch_number";

    public static final String AMOUNT = "amount";

    public static final String TOTAL_NUM = "total_num";

    public static final String TOTAL_FEE = "total_fee";

    public static final String STATUS = "status";

    public static final String REMARK = "remark";

    public static final String OTIME = "otime";

    public static final String CNAME = "cname";

    public static final String UNAME = "uname";

    public static final String ONAME = "oname";

    @Override
    public String toString() {
        return "GoodsPart{" +
        "code=" + code +
        ", wearhouseId=" + wearhouseId +
        ", skuId=" + skuId +
        ", status=" + status +
        ", remark=" + remark +
        ", otime=" + otime +
        ", cname=" + cname +
        ", uname=" + uname +
        ", oname=" + oname +
        "}";
    }
}
