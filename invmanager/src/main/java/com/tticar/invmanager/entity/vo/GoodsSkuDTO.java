package com.tticar.invmanager.entity.vo;

import java.util.List;

/**
 * @author chengxy
 * @date 2018/10/19 8:55
 */
public class GoodsSkuDTO {
    private String name;
    private Long order;
    private List<GoodsSkuDetailsDTO> list;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public List<GoodsSkuDetailsDTO> getList() {
        return list;
    }

    public void setList(List<GoodsSkuDetailsDTO> list) {
        this.list = list;
    }
}
