package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 盘点单详情
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-13
 */
@TableName("c_check_detail")
public class CCheckDetail extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 盘点总单id
     */
    @TableField("check_id")
    private Long checkId;

    /**
     * 库位d
     */
    @TableField("position_id")
    private Long positionId;

    /**
     * 商品skuid
     */
    @TableField("sku_id")
    private Long skuId;

    /**
     * 批号
     */
    @TableField("batch_number")
    private String batchNumber;

    /**
     * 仓库
     */
    @TableField("wearhouse_id")
    private Long wearhouseId;

    /**
     * 库存数量
     */
    private Integer count;

    /**
     * 盘点数量
     */
    @TableField("check_count")
    private Integer checkCount;

    /**
     * 成本单价
     */
    private BigDecimal amount;

    /**
     * 操作人姓名
     */
    private String username;

    /**
     * 备注
     */
    private String remark;

    public Long getWearhouseId() {
        return wearhouseId;
    }

    public void setWearhouseId(Long wearhouseId) {
        this.wearhouseId = wearhouseId;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public Long getCheckId() {
        return checkId;
    }

    public void setCheckId(Long checkId) {
        this.checkId = checkId;
    }
    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }
    public Integer getCheckCount() {
        return checkCount;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void setCheckCount(Integer checkCount) {
        this.checkCount = checkCount;
    }
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public static final String CHECK_ID = "check_id";

    public static final String SKU_ID = "sku_id";

    public static final String BATCH_CODE = "batch_code";

    public static final String REP_COUNT = "rep_count";

    public static final String CHECK_COUNT = "check_count";

    public static final String AMOUNT = "amount";

    public static final String STATUS = "status";

    public static final String USERNAME = "username";

    public static final String REMARK = "remark";

    @Override
    public String toString() {
        return "CCheckDetail{" +
        "checkId=" + checkId +
        ", skuId=" + skuId +
        ", repCount=" + count +
        ", checkCount=" + checkCount +
        ", amount=" + amount +
        ", username=" + username +
        ", remark=" + remark +
        "}";
    }
}
