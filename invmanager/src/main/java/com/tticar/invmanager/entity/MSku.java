package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 物料规格
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-05
 */
@TableName("m_sku")
public class MSku extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 规格名称
     */
    private String name;

    /**
     * 编码
     */
    private String code;

    /**
     * 排序
     */
    private Integer sort;

    private Integer status;

    @TableField(exist = false)
    private Long[] categoryIds;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long[] getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(Long[] categoryIds) {
        this.categoryIds = categoryIds;
    }

    public static final String NAME = "name";

    public static final String CODE = "code";

    public static final String SORT = "sort";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "MSku{" +
        "name=" + name +
        ", code=" + code +
        ", sort=" + sort +
        ", status=" + status +
        ", categoryIds=" + categoryIds +
        "}";
    }
}
