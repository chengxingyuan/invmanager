package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 盘点单详情
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-15
 */
@TableName("goods_part_detail")
public class GoodsPartDetail extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 拆装总单id
     */
    @TableField("part_id")
    private Long partId;

    /**
     * 仓库
     */
    @TableField("wearhouse_id")
    private Long wearhouseId;

    /**
     * 库位id
     */
    @TableField("position_id")
    private Long positionId;

    /**
     * 商品skuid
     */
    @TableField("sku_id")
    private Long skuId;

    /**
     * 批号
     */
    @TableField("batch_number")
    private String batchNumber;

    /**
     * 拆装数量
     */
    private Integer num;

    /**
     * 拆装单价
     */
    private BigDecimal price;

    /**
     * 拆装总额
     */
    @TableField("fee")
    private BigDecimal fee;

    /**
     * 操作人姓名
     */
    private String username;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否删除
     */
    private Integer status;

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public Long getPartId() {
        return partId;
    }

    public void setPartId(Long partId) {
        this.partId = partId;
    }
    public Long getWearhouseId() {
        return wearhouseId;
    }

    public void setWearhouseId(Long wearhouseId) {
        this.wearhouseId = wearhouseId;
    }
    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }
    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }
    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static final String PART_ID = "part_id";

    public static final String WEARHOUSE_ID = "wearhouse_id";

    public static final String POSITION_ID = "position_id";

    public static final String SKU_ID = "sku_id";

    public static final String BATCH_NUMBER = "batch_number";

    public static final String COUNT = "count";

    public static final String AMOUNT = "amount";

    public static final String TOTAL_AMOUNT = "total_amount";

    public static final String USERNAME = "username";

    public static final String REMARK = "remark";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "GoodsPartDetail{" +
        "partId=" + partId +
        ", wearhouseId=" + wearhouseId +
        ", positionId=" + positionId +
        ", skuId=" + skuId +
        ", batchNumber=" + batchNumber +
        ", username=" + username +
        ", remark=" + remark +
        ", status=" + status +
        "}";
    }
}
