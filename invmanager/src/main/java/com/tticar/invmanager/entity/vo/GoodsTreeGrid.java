package com.tticar.invmanager.entity.vo;

import java.util.List;
import java.util.Map;

public class GoodsTreeGrid {
    private String id;
    private String pid;
    private String text;
    private String state;
    private List<GoodsTreeGrid> children;
    private boolean checked;
    private Map attributes;
    private String iconCls;

    private String categoryName;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public GoodsTreeGrid(String id, String text, String categoryName) {
        this(id, text);
        this.categoryName = categoryName;
    }

    public GoodsTreeGrid(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<GoodsTreeGrid> getChildren() {
        return children;
    }

    public void setChildren(List<GoodsTreeGrid> children) {
        this.children = children;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public Map getAttributes() {
        return attributes;
    }

    public void setAttributes(Map attributes) {
        this.attributes = attributes;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getIconCls() {
        return iconCls;
    }

    public void setIconCls(String iconCls) {
        this.iconCls = iconCls;
    }

}
