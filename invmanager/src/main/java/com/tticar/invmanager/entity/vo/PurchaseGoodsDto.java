package com.tticar.invmanager.entity.vo;

/**
 * 新增采购单商品
 */
public class PurchaseGoodsDto {
    private Long skusId;

    private Long goodsId;

    private Long ralativeId;

    private String batchCode;

    private Double amount;

    private Double amountActual;

    private Integer count;

    private Integer payStatus;

    private String remark;

    private Long wearhouseId;

    private Long positionId;

    //弃用库位版本 仓库id
    private Long repositoryId;

    private Long skuId;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(Long repositoryId) {
        this.repositoryId = repositoryId;
    }

    public Long getSkusId() {
        return skusId;
    }

    public void setSkusId(Long skusId) {
        this.skusId = skusId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getRalativeId() {
        return ralativeId;
    }

    public void setRalativeId(Long ralativeId) {
        this.ralativeId = ralativeId;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getAmountActual() {
        return amountActual;
    }

    public void setAmountActual(Double amountActual) {
        this.amountActual = amountActual;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getWearhouseId() {
        return wearhouseId;
    }

    public void setWearhouseId(Long wearhouseId) {
        this.wearhouseId = wearhouseId;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }
}
