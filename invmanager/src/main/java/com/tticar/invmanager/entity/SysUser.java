package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 管理员账号
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-28
 */
@TableName("sys_user")
public class SysUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 角色id
     */
    @TableField("role_id")
    private Long roleId;

    /**
     * 角色名称
     */
    @TableField("role_name")
    private String roleName;

    /**
     * 部门id
     */
    @TableField("dept_id")
    private Long deptId;

    @TableField("dept_name")
    private String deptName;

    /**
     * 用户名
     */
    private String username;

    /**
     * 别名
     */
    private String nickname;

    private String password;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 状态 0启用1禁用
     */
    private Integer status;

    /**
     * 上次登录时间
     */
    @TableField("last_login_time")
    private Date lastLoginTime;

    /**
     * 登录时间
     */
    @TableField("login_time")
    private Date loginTime;

    /**
     * 登陆次数
     */
    @TableField("login_times")
    private Integer loginTimes;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }
    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }
    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }
    public Integer getLoginTimes() {
        return loginTimes;
    }

    public void setLoginTimes(Integer loginTimes) {
        this.loginTimes = loginTimes;
    }

    public static final String ROLE_ID = "role_id";

    public static final String ROLE_NAME = "role_name";

    public static final String DEPT_ID = "dept_id";

    public static final String DEPT_NAME = "dept_name";

    public static final String USERNAME = "username";

    public static final String NICKNAME = "nickname";

    public static final String PASSWORD = "password";

    public static final String MOBILE = "mobile";

    public static final String STATUS = "status";

    public static final String LAST_LOGIN_TIME = "last_login_time";

    public static final String LOGIN_TIME = "login_time";

    public static final String LOGIN_TIMES = "login_times";

    @Override
    public String toString() {
        return "SysUser{" +
        "roleId=" + roleId +
        ", roleName=" + roleName +
        ", deptId=" + deptId +
        ", deptName=" + deptName +
        ", username=" + username +
        ", nickname=" + nickname +
        ", password=" + password +
        ", mobile=" + mobile +
        ", status=" + status +
        ", lastLoginTime=" + lastLoginTime +
        ", loginTime=" + loginTime +
        ", loginTimes=" + loginTimes +
        "}";
    }
}
