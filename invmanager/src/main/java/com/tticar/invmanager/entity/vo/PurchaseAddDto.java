package com.tticar.invmanager.entity.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * 新增采购单商品
 */
public class PurchaseAddDto {
    private Integer type;
    private List<PurchaseGoodsDto> goodsList;
    private Integer purchaseType;
    private String ptime;
    private Long relativeId;
    private BigDecimal totalMoney;
    private BigDecimal realPay;
    private Integer orderStatus;
    private String remark;
    private Integer payStatus;
    //幂等token
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }

    public BigDecimal getRealPay() {
        return realPay;
    }

    public void setRealPay(BigDecimal realPay) {
        this.realPay = realPay;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getRelativeId() {
        return relativeId;
    }

    public void setRelativeId(Long relativeId) {
        this.relativeId = relativeId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<PurchaseGoodsDto> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<PurchaseGoodsDto> goodsList) {
        this.goodsList = goodsList;
    }

    public Integer getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(Integer purchaseType) {
        this.purchaseType = purchaseType;
    }

    public String getPtime() {
        return ptime;
    }

    public void setPtime(String ptime) {
        this.ptime = ptime;
    }
}
