package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 物料规格值
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-05
 */
@TableName("m_sku_value")
public class MSkuValue extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("sku_id")
    private Long skuId;

    /**
     * 规格值名称
     */
    private String name;

    /**
     * 规格值编号
     */
    private String code;

    /**
     * 排序
     */
    private Integer sort;

    private Integer status;

    /**
     * 是否默认
     */
    @TableField("is_default")
    private Integer isDefault;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    public static final String SKU_ID = "sku_id";

    public static final String NAME = "name";

    public static final String CODE = "code";

    public static final String SORT = "sort";

    public static final String STATUS = "status";

    public static final String IS_DEFAULT = "is_default";

    @Override
    public String toString() {
        return "MSkuValue{" +
        "skuId=" + skuId +
        ", name=" + name +
        ", code=" + code +
        ", sort=" + sort +
        ", status=" + status +
        ", isDefault=" + isDefault +
        "}";
    }
}
