package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.tticar.invmanager.entity.BaseEntity;

import java.util.Date;

/**
 * <p>
 * 车品牌
 * </p>
 *
 * @author joeyYan
 * @since 2018-09-19
 */
@TableName("car_brand")
public class CarBrand {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 父品牌主键
     */
    private Long pid;

    /**
     * 车品牌名中文名
     */
    private String name;

    /**
     * 图片地址
     */
    private String logo;

    /**
     * 首字母
     */
    private String initial;

    /**
     * 首字母
     */
    private String initials;

    /**
     * 等级
     */
    private Integer level;

    /**
     * 显示状态
     */
    private Integer status;

    private Date ctime;

    private Date utime;

    @TableField(exist = false)
    private Date startTime;

    @TableField(exist = false)
    private Date endTime;

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
    public String getInitial() {
        return initial;
    }

    public void setInitial(String initial) {
        this.initial = initial;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public static final String PID = "pid";

    public static final String NAME = "name";

    public static final String LOGO = "logo";

    public static final String INITIAL = "initial";

    public static final String LEVEL = "level";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "CarBrand{" +
        "pid=" + pid +
        ", name=" + name +
        ", logo=" + logo +
        ", initial=" + initial +
        ", level=" + level +
        ", status=" + status +
        "}";
    }
}
