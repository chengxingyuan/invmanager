package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 销售退货表
 * </p>
 *
 * @author joeyYan
 * @since 2019-01-04
 */
@TableName("sale_return")
public class SaleReturn extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 退货单号
     */
    private String code;

    /**
     * 供应商id
     */
    @TableField("custom_id")
    private Long customId;

    /**
     * 退货商品数量
     */
    private Integer count;

    /**
     * 退货单价
     */
    @TableField("unit_price")
    private BigDecimal unitPrice;

    /**
     * 所退商品
     */
    @TableField("sku_id")
    private Long skuId;

    /**
     * 仓库id
     */
    @TableField("repository_id")
    private Long repositoryId;

    /**
     * 退货日期
     */
    @TableField("order_time")
    private Date orderTime;

    /**
     * 实退金额
     */
    @TableField("real_pay")
    private BigDecimal realPay;

    /**
     * 待退金额
     */
    @TableField("need_pay")
    private BigDecimal needPay;

    public BigDecimal getNeedPay() {
        return needPay;
    }

    public void setNeedPay(BigDecimal needPay) {
        this.needPay = needPay;
    }

    /**
     * 结清状态：0.未结清，1.已结清
     */
    @TableField("pay_status")
    private Integer payStatus;

    /**
     * 入库状态：0，未入库，1.已入库
     */
    @TableField("repository_status")
    private Integer repositoryStatus;

    /**
     * 单据状态：-1.删除，0，有效，
     */
    private Integer status;

    /**
     * 开单人
     */
    private String username;

    /**
     * 备注
     */
    private String remark;

    /**
     * 搜索条件
     * */
    @TableField(exist = false)
    private String searchText;
    @TableField(exist = false)
    private Integer orderStatus;
    @TableField(exist = false)
    private Date startTime;
    @TableField(exist = false)
    private Date endTime;

    @TableField("settlement_status")
    private Integer settlementStatus;

    public Integer getSettlementStatus() {
        return settlementStatus;
    }

    public void setSettlementStatus(Integer settlementStatus) {
        this.settlementStatus = settlementStatus;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Override
    public Date getStartTime() {
        return startTime;
    }

    @Override
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    @Override
    public Date getEndTime() {
        return endTime;
    }

    @Override
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public Long getCustomId() {
        return customId;
    }

    public void setCustomId(Long customId) {
        this.customId = customId;
    }
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }
    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }
    public Long getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(Long repositoryId) {
        this.repositoryId = repositoryId;
    }
    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }
    public BigDecimal getRealPay() {
        return realPay;
    }

    public void setRealPay(BigDecimal realPay) {
        this.realPay = realPay;
    }
    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }
    public Integer getRepositoryStatus() {
        return repositoryStatus;
    }

    public void setRepositoryStatus(Integer repositoryStatus) {
        this.repositoryStatus = repositoryStatus;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public static final String CODE = "code";

    public static final String RELATIVE_ID = "relative_id";

    public static final String COUNT = "count";

    public static final String UNIT_PRICE = "unit_price";

    public static final String SKU_ID = "sku_id";

    public static final String REPOSITORY_ID = "repository_id";

    public static final String ORDER_TIME = "order_time";

    public static final String REAL_PAY = "real_pay";

    public static final String PAY_STATUS = "pay_status";

    public static final String REPOSITORY_STATUS = "repository_status";

    public static final String STATUS = "status";

    public static final String USERNAME = "username";

    public static final String REMARK = "remark";

    @Override
    public String toString() {
        return "SaleReturn{" +
        "code=" + code +
        ", relativeId=" + customId +
        ", count=" + count +
        ", unitPrice=" + unitPrice +
        ", skuId=" + skuId +
        ", repositoryId=" + repositoryId +
        ", orderTime=" + orderTime +
        ", realPay=" + realPay +
        ", payStatus=" + payStatus +
        ", repositoryStatus=" + repositoryStatus +
        ", status=" + status +
        ", username=" + username +
        ", remark=" + remark +
        "}";
    }
}
