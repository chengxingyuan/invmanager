package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-08
 */
@TableName("sys_dict")
public class SysDict extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long pid;

    /**
     * 编码
     */
    private String code;

    /**
     * 显示值
     */
    private String name;

    /**
     * 值
     */
    private String value;

    private String img;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 能否修改
     */
    private Integer editable;

    private Integer status;

    /**
     * 备注
     */
    private String memo;

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
    public Integer getEditable() {
        return editable;
    }

    public void setEditable(Integer editable) {
        this.editable = editable;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public static final String PID = "pid";

    public static final String CODE = "code";

    public static final String NAME = "name";

    public static final String VALUE = "value";

    public static final String IMG = "img";

    public static final String SORT = "sort";

    public static final String EDITABLE = "editable";

    public static final String STATUS = "status";

    public static final String MEMO = "memo";

    @Override
    public String toString() {
        return "SysDict{" +
        "pid=" + pid +
        ", code=" + code +
        ", name=" + name +
        ", value=" + value +
        ", img=" + img +
        ", sort=" + sort +
        ", editable=" + editable +
        ", status=" + status +
        ", memo=" + memo +
        "}";
    }
}
