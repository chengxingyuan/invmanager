package com.tticar.invmanager.entity.vo;

import java.util.Date;

/**
 * @author chengxy
 * @date 2018/10/9 9:45
 */
public class GoodsOrderSearchDTO {
    private String name;

    private Integer orderStatus;

    private Date startTime;

    private Date endTime;

    private Long sellStoreId;

    private String sellStoreIdList;

    public String getSellStoreIdList() {
        return sellStoreIdList;
    }

    public void setSellStoreIdList(String sellStoreIdList) {
        this.sellStoreIdList = sellStoreIdList;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSellStoreId() {
        return sellStoreId;
    }

    public void setSellStoreId(Long sellStoreId) {
        this.sellStoreId = sellStoreId;
    }

    @Override
    public String toString() {
        return "GoodsOrderSearchDTO{" +
                "name='" + name + '\'' +
                ", orderStatus=" + orderStatus +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", sellStoreId=" + sellStoreId +
                '}';
    }
}
