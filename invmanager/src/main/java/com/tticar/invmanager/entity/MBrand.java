package com.tticar.invmanager.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.tticar.invmanager.entity.BaseEntity;

/**
 * <p>
 * 存货品牌
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
@TableName("m_brand")
public class MBrand extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 品牌名称
     */
    private String name;

    /**
     * 状态
     */
    private Integer status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static final String NAME = "name";

    public static final String STATUS = "status";

    @Override
    public String toString() {
        return "MBrand{" +
        "name=" + name +
        ", status=" + status +
        "}";
    }
}
