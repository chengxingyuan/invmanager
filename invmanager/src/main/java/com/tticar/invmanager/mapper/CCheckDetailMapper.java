package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.CCheckDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 盘点单详情 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-13
 */
public interface CCheckDetailMapper extends BaseMapper<CCheckDetail> {
    List<Map> getDetailListById(@Param("checkId") Long checkId);
}
