package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.OutRepositoryDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.OutRepositoryDetailSearchDto;
import com.tticar.invmanager.entity.vo.OutRepositoryVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 出库单详情 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-06
 */
@Repository
public interface OutRepositoryDetailMapper extends BaseMapper<OutRepositoryDetail> {

    List<OutRepositoryVO> selectAllByMapper(MyPage<OutRepositoryDetail> page,OutRepositoryDetail outRepositoryDetail);

    List<Map> search(OutRepositoryDetailSearchDto searchDto, MyPage<OutRepositoryDetail> page);

    List<Map> getDetailByCode(@Param("code") String code);

    List<Map> selectPrintList(Map map);
}
