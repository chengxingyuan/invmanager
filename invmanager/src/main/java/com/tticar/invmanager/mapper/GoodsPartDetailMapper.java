package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.GoodsPartDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 盘点单详情 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-15
 */
public interface GoodsPartDetailMapper extends BaseMapper<GoodsPartDetail> {

    List<Map> getDetailList(@Param("partId") Long partId);
}
