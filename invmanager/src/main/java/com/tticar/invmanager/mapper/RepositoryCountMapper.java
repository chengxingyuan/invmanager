package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.RepositoryCount;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.RepositoryCountSearchDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品库存数量表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-10
 */
public interface RepositoryCountMapper extends BaseMapper<RepositoryCount> {
    List<Map> getSkuNum(@Param("skuIds") String skuIds);

    List<Map> getSkuNumByHouseId(@Param("ew") Map map);

    RepositoryCount selectDataBySkuIdAndWearhouseId(@Param("skuId")Long skuId, @Param("wearhouseId")Long wearhouseId);

    RepositoryCount selectDataBySkuIdAndWearhouseIdByTenantId(@Param("skuId")Long skuId, @Param("wearhouseId")Long wearhouseId, @Param("tenantId")Long tenantId);

    RepositoryCount selectDataBySkuIdAndWearhouseIdAndPositionId(@Param("skuId")Long skuId, @Param("wearhouseId")Long wearhouseId,@Param("positionId")Long positionId);

    RepositoryCount selectDataBySkuIdAndWearhouseIdAndPositionIdAndTenantId(@Param("skuId")Long skuId, @Param("wearhouseId")Long wearhouseId,
                                                                            @Param("positionId")Long positionId,@Param("tenantId")Long tenantId);

    List<Map> search(@Param("ew") RepositoryCountSearchDto searchDto, MyPage<RepositoryCount> page);

    List<Map> searchPrintData(Map ids);

    List<Map> getSpecific(Long skuId);

    /**
     * 先出默认仓
     */
    List<RepositoryCount> firstOutDefaultRepository(Long skuId);
    List<RepositoryCount> firstOutDefaultRepositoryByTenantId(@Param("skuId")Long skuId,@Param("tenantId")Long tenantId);

    /**
     * 不够再出其他
     * */
    List<RepositoryCount> nextOutAnother(Long skuId);
    List<RepositoryCount> nextOutAnotherByTenantId(@Param("skuId")Long skuId,@Param("tenantId")Long tenantId);


    List<RepositoryCount> queryOutRepositoryByHouseId(@Param("skuId") Long skuId,@Param("houseId") Long houseId);

    List<Map> changeRepositoryAddGoodsList(Map map);

    List<Map> getChangeGoodsList(String code);

    Long selectTenantIdByStoreId(Long storeId);

    List<Map> selectRepositoryListByTenantId(Long tenantId);

    List<Map> queryGoodsMessageForSupplierApp(Map map);

    List<Map> queryCount(Map map);
}
