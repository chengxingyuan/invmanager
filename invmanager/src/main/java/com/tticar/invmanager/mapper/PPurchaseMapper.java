package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.PPurchase;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.entity.PurchaseReturn;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.PurchaseSearchDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 采购单 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-03
 */
public interface PPurchaseMapper extends BaseMapper<PPurchase> {

    List<Map> getPurchaseDetailList(@Param("purchaseId") Long purchaseId);

    List<Map> search(PurchaseSearchDto searchDto, MyPage<PPurchase> page);

    List<Map> queryIncomeList(MyPage<PPurchase> page,PPurchase purchase);

    List<Map> queryExpendList(MyPage<PPurchase> page,PPurchase purchase);
}
