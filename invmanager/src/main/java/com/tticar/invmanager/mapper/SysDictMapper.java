package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.SysDict;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-08
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

}
