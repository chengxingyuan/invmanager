package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.MCateogrySku;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 存货分类规格对应表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-05
 */
public interface MCateogrySkuMapper extends BaseMapper<MCateogrySku> {

}
