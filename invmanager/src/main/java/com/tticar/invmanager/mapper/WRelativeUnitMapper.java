package com.tticar.invmanager.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.entity.WRelativeUnit;
import com.tticar.invmanager.entity.vo.MyPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 往来客户 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-08
 */
public interface WRelativeUnitMapper extends BaseMapper<WRelativeUnit> {
    List<Long> queryOrderIdByRelativeId(Long relativeId);

    List<Map> search(MyPage<WRelativeUnit> page, @Param("ew") WRelativeUnit wRelativeUnit);
}
