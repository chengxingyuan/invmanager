package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.MSales;
import com.tticar.invmanager.entity.MSalesDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.SalesSearchDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-15
 */
public interface MSalesDetailMapper extends BaseMapper<MSalesDetail> {
    List<Map> getSalesGoods(@Param("salesIds") String salesIds);

    List<Map> getSalesDetailList(@Param("salesId") Long salesId);

    List<Map> getSalesHouseList(@Param("salesId") Long salesId);
}
