package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.SysRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
