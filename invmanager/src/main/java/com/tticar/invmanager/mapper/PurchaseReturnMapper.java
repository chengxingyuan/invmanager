package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.PurchaseReturn;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 退货单表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-12-21
 */
public interface PurchaseReturnMapper extends BaseMapper<PurchaseReturn> {

    List<Map> queryPurchaseReturnList(MyPage<PurchaseReturn> page, PurchaseReturn purchaseReturn);

    List<Map> purchaseReturnDetailGoodsList(String code);

    List<Map> getPurchaseDetailList(String code);
}
