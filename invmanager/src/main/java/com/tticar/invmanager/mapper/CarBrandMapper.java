package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.CarBrand;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车品牌 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-09-19
 */
public interface CarBrandMapper extends BaseMapper<CarBrand> {

    List<Map> getForTree(@Param("pid") Long pid);

    List<Map> getForTreeSearchText(@Param("searchText") String searchText);
}
