package com.tticar.invmanager.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.entity.SysLogs;
import com.tticar.invmanager.entity.SysLogsOperate;

/**
 * <p>
 * 请求日志操作描述 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-29
 */
public interface SysLogsOperateMapper extends BaseMapper<SysLogsOperate> {

}
