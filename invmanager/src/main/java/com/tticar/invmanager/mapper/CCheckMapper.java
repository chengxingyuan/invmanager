package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.CCheck;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 盘点单 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-13
 */
public interface CCheckMapper extends BaseMapper<CCheck> {

}
