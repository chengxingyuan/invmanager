package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.BybsDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 报溢报损详情 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-18
 */
public interface BybsDetailMapper extends BaseMapper<BybsDetail> {
    List<Map> getDetailList(@Param("bybsId") Long bybsId);
}
