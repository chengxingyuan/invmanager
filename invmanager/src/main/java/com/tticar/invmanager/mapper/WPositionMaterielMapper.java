package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.WPositionMateriel;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 * 库位对应的商品 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-23
 */
public interface WPositionMaterielMapper extends BaseMapper<WPositionMateriel> {

    Map getBindInfo(@Param("materielId") Long materielId);
}
