package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.WWearhouse;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-02
 */
public interface WWearhouseMapper extends BaseMapper<WWearhouse> {

}
