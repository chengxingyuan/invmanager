package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.PPurchaseDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 采购单详情 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-03
 */
public interface PPurchaseDetailMapper extends BaseMapper<PPurchaseDetail> {

}
