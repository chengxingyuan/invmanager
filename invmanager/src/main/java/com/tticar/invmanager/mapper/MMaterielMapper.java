package com.tticar.invmanager.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.tticar.invmanager.entity.MMateriel;
import com.tticar.invmanager.entity.vo.GoodsTreeGridSearchDto;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.PlaceOrderDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 存货表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-11
 */
public interface MMaterielMapper extends BaseMapper<MMateriel> {

    List<Map> getPage(MyPage<MMateriel> page, @Param("ew") Wrapper w);

    List<Map> getSkuList(Long id);

    List<Map> getForTree(@Param("searchText") String searchText);

    List<Long> selectSkuIdByMaterielName(@Param("materielName") String materielName);

    List<Map> treegridForCheck(@Param("ew") GoodsTreeGridSearchDto searchDto);

    List<Map> search(MyPage<MMateriel> page, @Param("ew") Map params);

    List<Map> searchForRelative(MyPage<MMateriel> page, @Param("ew") Map params);

    List queryInvMaterielList(Map map);

    /**
     * 根据skuId查询商品信息
     */
    MMateriel queryMaterielBySkuId(Long skuId);

    List<PlaceOrderDTO>  materielListForPlaceOrder(Map map);

    MMateriel selectNotTenantId(@Param("id")Long id,@Param("tenantId")Long tenantId);
}
