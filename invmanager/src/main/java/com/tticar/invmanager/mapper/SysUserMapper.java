package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.SysUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 管理员账号 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-28
 */
@Component("ssyUserMapper1")
public interface SysUserMapper extends BaseMapper<SysUser> {

}
