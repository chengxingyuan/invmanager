package com.tticar.invmanager.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.tticar.invmanager.entity.MMateriel;
import com.tticar.invmanager.entity.MMaterielTticar;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.RelativeGoodsDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 天天爱车产品 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-23
 */
public interface MMaterielTticarMapper extends BaseMapper<MMaterielTticar> {

    List<Map> get(MyPage page, @Param("ew") Wrapper wrapper);

    List<RelativeGoodsDTO> queryRelativeGoodsByTticarGoods(@Param("tticarGoodsId")String tticarGoodsId,@Param("skuValue")String skuValue);

    void cancelRelative(@Param("skuId") Long skuId,@Param("tenantId")Long tenantId,@Param("tticarGoodsId")String tticarGoodsId,@Param("skuValue")String skuValue);

    List<Long> queryGoodsHasRelativeList(Long tenantId);

    List<Map> queryRelativeInvMaterielId(@Param("tticarGoodsId")String tticarGoodsId,@Param("skuValue")String skuValue);

    Long queryTenantId(@Param("tticarGoodsId")String tticarGoodsId,@Param("skuValue")String skuValue);

    Long queryTenantIdByMaterielSkuId(Long skuId);

    List<Long> queryTticarStoreList();

    List<Long> queryTticarStoreListByTenantId();
    /**
     * 仓库skuid，天天爱车商品id，天天爱车商品sku
     * */
    Long queryCountFromRelativeTable(@Param("invSkuId")Long invSkuId,@Param("tticarGoodsId")String tticarGoodsId,@Param("tticarSkuId")String tticarSkuId);

    Map queryDefaultHouse(Long skuId);

    Map queryDefaultHouseByTenantId(@Param("skuId")Long skuId,@Param("tenantId") Long tenantId);
}
