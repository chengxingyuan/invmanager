package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.CarBrandMateriel;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-09-20
 */
public interface CarBrandMaterielMapper extends BaseMapper<CarBrandMateriel> {

}
