package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.MCateTmp;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 存货分类表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-10
 */
public interface MCateTmpMapper extends BaseMapper<MCateTmp> {

}
