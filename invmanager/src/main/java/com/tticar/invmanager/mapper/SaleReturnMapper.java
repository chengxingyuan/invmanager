package com.tticar.invmanager.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.entity.SaleReturn;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 销售退货表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2019-01-04
 */
public interface SaleReturnMapper extends BaseMapper<SaleReturn> {
    List<Map> querySaleReturnList(MyPage<SaleReturn> page, SaleReturn saleReturn);

    List<Map> saleReturnDetailGoodsList(String code);

    List<Map> getSaleDetailList(String code);
}
