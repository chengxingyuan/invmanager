package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.SysResource;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 资源管理 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
public interface SysResourceMapper extends BaseMapper<SysResource> {

}
