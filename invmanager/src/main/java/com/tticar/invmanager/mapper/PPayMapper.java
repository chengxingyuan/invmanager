package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.PPay;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 采购单支付流水 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-06
 */
public interface PPayMapper extends BaseMapper<PPay> {
    List<Map> getPayList(@Param("pay") PPay pay);
}
