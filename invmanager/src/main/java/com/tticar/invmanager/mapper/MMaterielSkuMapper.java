package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.MMaterielSku;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.entity.vo.MyPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-11
 */
public interface MMaterielSkuMapper extends BaseMapper<MMaterielSku> {
    List incomeList(MyPage<MMaterielSku> page, @Param("ew") Map param);

    Map getSumInfo(@Param("ew") Map param);

    List stockList(MyPage<MMaterielSku> page, @Param("ew") Map param);

    Map getSumInfoForStock(@Param("ew") Map param);

    /**
     * 查询天天爱车产品关联的skuid
     */
    Map querySkuIdByTticarGoodsSkuId(@Param("goodsId")String goodsId,@Param("skuId") String skuId);

    Long insertRelativeTticarGoodsAndInvSku(@Param("goodsId")String goodsId,@Param("skuValue")String skuValue,@Param("skuId")Long skuId,@Param("tenantId")Long tenantId);

    Long comboRelative(@Param("goodsId")String goodsId,@Param("skuValue")String skuValue,@Param("skuId")Long skuId,@Param("tenantId")Long tenantId,@Param("count") Object count);

    /**
     * 删除一个goodsId下的所有关联关系
     */
    void deleteRelative(String goodsId);

    /**
     * 根据存货skuId删除关联关系
     * */
    void deleteRelativeBySkuIds(List<Long> ids);

    MMaterielSku selectNotTenantId(@Param("id") Long id,@Param("tenantId") Long tenantId);
}
