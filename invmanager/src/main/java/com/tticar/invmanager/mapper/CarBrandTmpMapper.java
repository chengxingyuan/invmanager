package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.CarBrandTmp;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 车品牌 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-09-19
 */
public interface CarBrandTmpMapper extends BaseMapper<CarBrandTmp> {

}
