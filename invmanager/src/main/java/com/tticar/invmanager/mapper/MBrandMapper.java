package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.MBrand;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 存货品牌 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
public interface MBrandMapper extends BaseMapper<MBrand> {

}
