package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.ChangeRepository;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.entity.vo.MyPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 调拨单 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-12-10
 */
public interface ChangeRepositoryMapper extends BaseMapper<ChangeRepository> {

    List<Map> queryChangeRepositoryList(MyPage<ChangeRepository> page, Map map);

}
