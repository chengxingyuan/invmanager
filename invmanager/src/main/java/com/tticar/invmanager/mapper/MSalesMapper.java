package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.MSales;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.SalesSearchDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-13
 */
public interface MSalesMapper extends BaseMapper<MSales> {
    List<Map> search(SalesSearchDto searchDto, MyPage<MSales> page);

    List<Map> getSaleDetailList(Long saleId);
}
