package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.MCateogry;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 存货分类表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-03
 */
public interface MCateogryMapper extends BaseMapper<MCateogry> {
    List<MCateogry> tree(@Param("tenantId") Long tenantId);
}
