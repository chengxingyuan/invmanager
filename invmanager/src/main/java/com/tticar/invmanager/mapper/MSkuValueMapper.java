package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.MSkuValue;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 物料规格值 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-05
 */
public interface MSkuValueMapper extends BaseMapper<MSkuValue> {

}
