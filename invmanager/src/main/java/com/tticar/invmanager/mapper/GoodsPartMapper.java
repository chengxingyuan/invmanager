package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.GoodsPart;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.entity.vo.MyPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-15
 */
public interface GoodsPartMapper extends BaseMapper<GoodsPart> {

    List<Map> search(@Param("ew") GoodsPart goodsPart, MyPage<GoodsPart> page);

    Map getInfo(@Param("id") Long id);
}
