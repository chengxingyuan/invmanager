package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.InRepositoryDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.entity.OutRepositoryDetail;
import com.tticar.invmanager.entity.vo.InRepositoryDetailSearchDto;
import com.tticar.invmanager.entity.vo.MyPage;
import com.tticar.invmanager.entity.vo.OutRepositoryVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 入库单详情 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-02
 */
@Repository
public interface InRepositoryDetailMapper extends BaseMapper<InRepositoryDetail> {

    List<Map> selectAllByMapper(MyPage<InRepositoryDetail> page, InRepositoryDetailSearchDto searchDto);

    List<Map> batchCodeGrid(@Param("skuId") Long skuId);

    List<Map> getSkuBatchCodeForOut(@Param("skuIds") String skuIds);

    List<Map> getDetailByCode(String code);

    List<Map> getDetailBySku(@Param("ew") Map params);

    List<Map> getDetailBySkuNew(@Param("ew") Map params);

    List<Map> getSkuHouseByNum(@Param("ew") Map params);

    List<Map> getSkuHouseByNumAndTenantId(@Param("ew") Map params);

    Long getSkuNumByBatchCode(@Param("ew") Map params);

    List<Map> getBatchNumByCheckId(@Param("checkId") Long checkId);

    List<Map> getBatchListByHouseId2SkuId(@Param("ew") Map params);

    Double getSkuAvgCostByHouseId(@Param("ew") Map params);

    List<Map> getSkuHouseByHouseId(@Param("ew") Map params);

    List<Map> getMaterielHouseByHouseId(@Param("ew") Map params);

    List<Map> selectPrintList(Map map);
}
