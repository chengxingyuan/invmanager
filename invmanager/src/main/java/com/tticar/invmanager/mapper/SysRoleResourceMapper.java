package com.tticar.invmanager.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tticar.invmanager.entity.SysRoleResource;

import java.util.List;

/**
 * <p>
 * 角色资源表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-28
 */
public interface SysRoleResourceMapper extends BaseMapper<SysRoleResource> {

    List<String> selectPersByRoleId(Long roleId);
}
