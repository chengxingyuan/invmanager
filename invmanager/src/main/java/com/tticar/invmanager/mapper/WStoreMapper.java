package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.WStore;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 门店 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-24
 */
public interface WStoreMapper extends BaseMapper<WStore> {
    int insertStoreBelongUser(@Param("tticarStoreId") Long tticarStoreId, @Param("username") String userName);

    List<Long> queryStoreBelongUser();

    void cancelRelative(Long id);
}
