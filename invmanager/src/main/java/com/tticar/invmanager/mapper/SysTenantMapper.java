package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.SysTenant;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 租户 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
public interface SysTenantMapper extends BaseMapper<SysTenant> {

}
