package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.CodeCounter;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 单号计数器 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-08-01
 */
public interface CodeCounterMapper extends BaseMapper<CodeCounter> {

}
