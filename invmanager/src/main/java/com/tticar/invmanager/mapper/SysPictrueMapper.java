package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.SysPictrue;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 图片资源 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-16
 */
public interface SysPictrueMapper extends BaseMapper<SysPictrue> {

}
