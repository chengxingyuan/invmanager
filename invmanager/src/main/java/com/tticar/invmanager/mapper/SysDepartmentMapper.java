package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.SysDepartment;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-27
 */
public interface SysDepartmentMapper extends BaseMapper<SysDepartment> {

}
