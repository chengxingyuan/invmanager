package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.Bybs;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-10-18
 */
public interface BybsMapper extends BaseMapper<Bybs> {

}
