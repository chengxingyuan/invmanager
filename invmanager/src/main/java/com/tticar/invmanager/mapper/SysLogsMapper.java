package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.SysLogs;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 请求日志 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-06-29
 */
public interface SysLogsMapper extends BaseMapper<SysLogs> {

}
