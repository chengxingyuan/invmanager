package com.tticar.invmanager.mapper;

import com.tticar.invmanager.entity.WPosition;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 库位表 Mapper 接口
 * </p>
 *
 * @author joeyYan
 * @since 2018-07-31
 */
public interface WPositionMapper extends BaseMapper<WPosition> {

    List<Map> getForTree(@Param("ew") Map<String, Object> params);

    List<Map> getForTreeBind(@Param("ew") Map<String, Object> params);

    List<Long> selectPositionIdsByWearhouseId(@Param("wearhouseId") Long wearhouseId);

    String isBindGoods(@Param("positionId")Long positionId);
}
