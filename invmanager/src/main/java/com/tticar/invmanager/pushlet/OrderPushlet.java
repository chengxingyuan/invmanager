package com.tticar.invmanager.pushlet;

import com.baomidou.mybatisplus.entity.Column;
import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.tticar.invmanager.common.utils.MyBeanUtils;
import com.tticar.invmanager.entity.WStore;
import com.tticar.invmanager.service.IWStoreService;
import com.tticar.invmanager.tticar.service.IStoreService;
import nl.justobjects.pushlet.core.Event;
import nl.justobjects.pushlet.core.EventPullSource;

import java.util.List;

/**
 * Created by yandou on 2018/8/6.
 */
public class OrderPushlet extends EventPullSource {

    private IStoreService storeService;
    private IWStoreService iwStoreService;
    private List storeIds = null;

    @Override
    protected long getSleepTime() {
        return 1000;
    }

    @Override
    protected Event pullEvent() {
//        if (storeIds == null) {
//            iwStoreService = MyBeanUtils.getBean(IWStoreService.class);
//            Wrapper wrapper = Condition.create().isNotNull(WStore.TTSTORE_ID);
//            wrapper.setSqlSelect(Column.create().column(WStore.TTSTORE_ID));
//            storeIds = iwStoreService.selectObjs(wrapper);
//        }
//        if (CollectionUtils.isNotEmpty(storeIds)) {
//            if (storeService == null) {
//                storeService = MyBeanUtils.getBean(IStoreService.class);
//            }
//
//
//        }

        Event event = Event.createDataEvent("pushlet");
        event.setField("test", 124);
        return event;
    }
}
